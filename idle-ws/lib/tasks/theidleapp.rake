task reset_token_expiration: :environment do
  User.reset_token_expiration
end
