# encoding: utf-8
require 'fcm'

module PushNotification
  def self.notify(token, message, obj, key, title, id)
    Badge.create({ user_id: id, notification: message })
    badges = Badge.where("user_id = #{id} and is_read = false").count

    fcm = FCM.new(APP_CONFIG['fcm_server_key'])

    options = {
      notification: {
        title: title,
        body: message,
        data: {
          custom_data: obj,
          message: message
        },
        collapse_key: key,
        sound: "default",
        badge: badges,
      },
      data: {
        custom_data: obj,
        message: message,
        notif_tag: key
      }
    }

    response = fcm.send(token, options)
  end

  def self.location(token, obj)
    fcm = FCM.new(APP_CONFIG['fcm_server_key'])

    options = {
      data: {
        custom_data: obj,
        notif_tag: "location_updated"
      }
    }

    response = fcm.send(token, options)
  end
end
