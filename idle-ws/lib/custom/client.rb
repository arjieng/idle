#!/usr/bin/env ruby -w
require "socket"
class Client
  def initialize( server )
    @server = server
    @request = nil
    @response = nil
    listen
    send
    @request.join
    @response.join
  end

  def listen
    @response = Thread.new do
      loop {
        msg = @server.gets.chomp
        puts "#{msg}"
      }
    end
  end

  def send
    puts "Enter the username:"
    @request = Thread.new do
      loop {
        msg = $stdin.gets.chomp
        @server.puts( msg )
      }
    end
  end
end

# server = TCPSocket.open( "192.168.1.173", 3005 )
# server = TCPSocket.open( "192.168.1.173", 3001 )
# server = TCPSocket.open( "192.168.1.173", 3002 )
server = TCPSocket.open( "192.168.1.236", 3001 )

Client.new( server )