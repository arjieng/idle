Rails.application.routes.draw do
  devise_for :admins, path: 'admindir', controllers: { sessions: "admindir/v1/admins/sessions" }
  devise_for :users, path: 'v1', controllers: { registrations: "api/v1/registrations", sessions: "api/v1/sesions"}

  namespace :api, path: '' do
    namespace :v1 do
      devise_scope :user do
        post 'register'                                               => 'registrations#create'
        post 'sign-in'                                                => 'sessions#create'
        post 'social'                                                 => 'sessions#social'
      end

      post 'confirm'                                                  => 'users#confirmation'
      post 'stripe'                                                   => 'users#stripe'
      post 'forgot'                                                   => 'users#forgot'
      post 'resend'                                                   => 'users#resend_code'
      post 'stripe/api'                                               => 'users#stripe_api_key'
      get  'stripe/link'                                              => 'users#stripe_url'

      resources :tasks, except: [:new, :create, :edit, :update, :destroy] do
        resources :ignores, only: [:create]
        resources :bids, only: [:create]
        resources :reports, only: [:create]
      end

      resources :users, path: :accounts, except: [:create, :update, :new, :edit, :show, :destroy, :index] do
        collection{
          put 'location'
          put 'status'
          put 'role'
          post 'password'
          post 'stripe/disconnect'                                    => 'users#disconnect'
          put 'cancel'
          put 'read'
          put 'feedback'

          resources :tasks, only: [:create, :update, :destroy] do
            collection{
              get 'progress'
              get 'history'
              get 'directory'
            }
            resources :bids, only: [:index, :destroy]
            put 'completed'
            put 'finished'
            put 'ratings'
            put 'cancel'
          end

          resources :bids, only: [:index] do
            put 'accept'
            put 'cancel'
          end

          resources :conversations, only: [:index] do
            resources :messages, only: [:index]
          end
        }
      end

      resources :users, only: [:show, :update] do
        resources :reports, only: [:create]
        resources :messages, only: [:create]
        get 'completed'
      end
    end
  end

  namespace :admindir do
    namespace :v1, path: '' do
      resources :users, only: [:index, :show, :destroy]
      resources :tasks, only: [:index, :show]
      resources :admins, only: [:index, :edit, :show, :update, :create, :new]
      resources :reports, only: [:index]
      get 'dashboard'                                                 => 'pages#dashboard'
      get 'login'                                                     => 'pages#login'
    end
  end

  get 'mobile/password'                                               => 'pages#password'
  get 'mobile/stripe'                                                 => 'pages#stripe'
  get 'privacy'                                                       => 'pages#privacy'
  get 'contact'                                                       => 'pages#contact'
  post 'feedback'                                                     => 'pages#feedback'
  root to: 'pages#home'
end
