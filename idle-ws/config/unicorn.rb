require 'rails'

working_directory "/home/deploy/production/idle-ws"
pid "/home/deploy/production/idle-ws/tmp/pids/unicorn.pid"
stderr_path "/home/deploy/production/idle-ws/log/unicorn_error.log"
stdout_path "/home/deploy/production/idle-ws/log/unicorn.log"

# What ports/sockets to listen on, and what options for them.
listen "/tmp/idle_unicorn.todo.sock"

worker_processes 2

# What the timeout for killing busy workers is, in seconds
timeout 120

# Whether the app should be pre-loaded
preload_app true

@resque_pid_1 = nil
@resque_pid_2 = nil

env = Rails.env || "development"

before_fork do |server, worker|
  @resque_pid_1 ||= spawn("bundle exec rake resque:work QUEUE=* RAILS_ENV=#{env}")
  @resque_pid_2 ||= spawn("bundle exec rake resque:work QUEUE=* RAILS_ENV=#{env}")
end

after_fork do |server, worker|
  if defined?(ActiveRecord::Base)
    config = YAML::load(ERB.new(File.read('config/database.yml')).result)[env]
    ActiveRecord::Base.establish_connection(config)
  end
end
