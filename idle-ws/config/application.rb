require File.expand_path('../boot', __FILE__)

require 'rails/all'

# Require the gems listed in Gemfile, including any gems
# you've limited to :test, :development, or :production.
Bundler.require(*Rails.groups)

module IdleWs
  class Application < Rails::Application
    # Settings in config/environments/* take precedence over those specified here.
    # Application configuration should go into files in config/initializers
    # -- all .rb files in that directory are automatically loaded.

    # Set Time.zone default to the specified zone and make Active Record auto-convert to this zone.
    # Run "rake -D time" for a list of tasks for finding time zone names. Default is UTC.
    config.time_zone = 'Central Time (US & Canada)'
    config.active_record.default_timezone = :utc

    # The default locale is :en and all translations from config/locales/*.rb,yml are auto loaded.
    # config.i18n.load_path += Dir[Rails.root.join('my', 'locales', '*.{rb,yml}').to_s]
    # config.i18n.default_locale = :de

    # Configure the default encoding used in templates for Ruby 2.0.
    config.encoding = "utf-8"

    # Do not swallow errors in after_commit/after_rollback callbacks.
    config.active_record.raise_in_transactional_callbacks = true

    config.assets.enabled = true

    # Version of your assets, change this if you want to expire all your assets
    config.assets.version = '1.0'

    config.autoload_paths += Dir["#{config.root}/lib/custom"]

    config.assets.paths << Rails.root.join("app", "assets", "stylesheets")
    config.assets.paths << Rails.root.join("app", "assets", "stylesheets", "sections")
    config.assets.paths << Rails.root.join("app", "assets", "stylesheets", "mixins")
    config.assets.paths << Rails.root.join("app", "assets", "stylesheets", "admindir")
    config.assets.paths << Rails.root.join("app", "assets", "stylesheets", "admindir", "v1")
    config.assets.paths << Rails.root.join("app", "assets", "images", "mobile")
    config.assets.paths << Rails.root.join("app", "assets", "images")
    config.assets.paths << Rails.root.join('app', 'assets', 'fonts')

    config.assets.paths << "#{Rails.root}/public/assets/images"
    config.assets.precompile << /(^[^_\/]|\/[^_])[^\/]*$/
    config.assets.precompile += %w(.svg .eot .woff .ttf)

    config.assets.initialize_on_precompile = false

  end
end
