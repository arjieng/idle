module ApplicationHelper
  include ActionView::Helpers::NumberHelper

  def precision(number)
    number_with_precision(number, precision: 2)
  end

  def user_params(user)
    @profile = { id: user.id, name: user.name, about: user.description, image: user.image.url, school: user.school, ratings: user.rate, is_online: user.is_online?, latitude: user.latitude, longitude: user.longitude }

    conversation = @user.conversation(user)
    if conversation != 0
      @profile.merge!(conversation_id: conversation)
    end

    return @profile
  end

  def javascript_include_action_specific_js
    javascript_include_tag "#{controller.controller_path.tr("/","_")}_#{controller.action_name}" if (File.exists?("#{Rails.root}/app/assets/javascripts/#{controller.controller_path.tr("/","_")}_#{controller.action_name}.js") || File.exists?("#{Rails.root}/app/assets/javascripts/#{controller.controller_path.tr("/","_")}_#{controller.action_name}.js.coffee"))
  end
end
