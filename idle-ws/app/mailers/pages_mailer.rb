class PagesMailer < ApplicationMailer
  include Resque::Mailer

  default from: APP_CONFIG["email"]
  layout "mailer"

  def feedback_email(email, name, message)
    @name = name
    @message = message

    mail(to: "seba@theidleapp.com", subject: "Get In Touch Website Email", reply_to: email)
  end
end
