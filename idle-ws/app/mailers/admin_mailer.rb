class AdminMailer < ApplicationMailer
  include Resque::Mailer

  default from: APP_CONFIG["email"]
  layout "mailer"
  
  def temporary_password_email(admin_id, tmp_password)
    @admin = Admin.find_by(id: admin_id)
    @temporary_password = tmp_password
    
    mail(to: @admin.email, subject: "Welcome to Idle")
  end
end