class UserMailer < ApplicationMailer
  include Resque::Mailer

  default from: APP_CONFIG["email"]
  layout "mailer"

  def forgot_email(user_id)
    @user = User.find_by(id: user_id)

    mail(to: @user.email, subject: "Idle Request Reset Password")
  end

  def confirmation_email(user_id)
    @user = User.find_by(id: user_id)

    mail(to: @user.email, subject: "Idle Confirmation Code")
  end

  def report_email(user_id)
    @user = User.find_by(id: user_id)

    mail(to: "admin@theidleapp.com", subject: "Idle Report User")
  end

  def cancel_email(user_id, message)
    @user = User.find_by(id: user_id)
    @message = message
    mail(to: "seba@theidleapp.com", subject: "User Cancel Account in Idle", reply_to: @user.email)
  end

  def feedback_email(user_id, message)
    @user = User.find_by(id: user_id)
    @message = message

    mail(to: "seba@theidleapp.com", subject: "User Feedback in Idle", reply_to: @user.email)
  end
end
