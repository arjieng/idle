class Api::V1::IgnoresController < ApplicationController
  before_filter :user_authorized

  include ApplicationHelper

  def create
    if @user.ignores.create(task_id: params[:task_id])

      @geolocation = { latitude: @user.latitude, longitude: @user.longitude }
      tasks = Task.filtered(@user.id, params[:type].to_i, @user.filtered_tasks, params[:radius].to_i, @geolocation )

      if params[:task].present?
        tasks = tasks.select { |task| task.task_type == params[:task].to_i }
      end

      if tasks.length > params[:off_set].to_i
        task = tasks.drop(params[:off_set].to_i - 10).take(10).last

        @task = { id: task.id, title: task.title.titleize, description: task.description, task_type: task.task_type, latitude: task.latitude, longitude: task.longitude, amount: precision(task.price), payment: task.payment, status: task.status, bid: precision(task.amount), user: user_params(task.user) }

        render json: { task: @task, status: 201 }, status: 200
      else
        render json: { status: 200 }, status: 200
      end
    else
      render json: { status: 300 }, status: 200
    end
  end
end
