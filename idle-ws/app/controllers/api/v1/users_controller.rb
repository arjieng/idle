class Api::V1::UsersController < ApplicationController
  before_filter :user_authorized, except: [:confirmation, :stripe, :forgot, :password, :resend_code, :stripe_api_key]

  include ApplicationHelper

  def show
    #include user
    user = User.includes(progress: [user: :ratings]).find_by(id: params[:id])

    tasks = user.progress.order("finished_at asc").select { |task| task.status == 6 && task.is_disabled == false }
    total_tasks = tasks.length

    tasks = tasks.take(10)

    @tasks = []
    tasks.each do |task|
      @tasks.push({id: task.id, title: task.title.titleize, description: task.description, task_type: task.task_type, amount: precision(task.price), payment: task.payment, status: task.status, bid: precision(task.amount), date: task.finished, is_cancel: 0, has_rate: 1, user: user_params(task.user) })
    end

    @profile = { id: user.id, name: user.name, image: user.image.url, school: user.school, is_online: user.is_online?, about: user.description, ratings: user.rate, completed: total_tasks, mobile: user.mobile, age: user.age, tasks: @tasks }

    conversation = @user.conversation(user)
    if conversation != 0
      @profile.merge!(conversation_id: conversation)
    end

    @pagination = {
      tasks: total_tasks,
      off_set: 10,
      load_more: total_tasks > 10 ? 1 : 0,
      url: api_v1_user_completed_path(params[:id], token: params[:token], off_set: 10)
    }

    render json: { user: @profile, pagination: @pagination, status: 200 }, status: 200
  end

  def location
    tasks = @user.progress.includes(user: :devices).where("status NOT IN (0, 6)").order("created_at desc")

    if @user.update_attributes(params_user)
      tasks.each do |task|
        @location = { user_id: @user.id, task_id: task.id, latitude: params[:latitude], longitude: params[:longitude] }

        devices = task.user.devices.map { |device| device.token }
        if devices.present?
          PushNotification.location(devices, @location)
        end
      end
      render json: { status: 200 }, status: 200
    else
      render json: { status: 300 }, status: 200
    end
  end

  def status
    @user.is_online = @user.is_online? == 1 ? false : true
    if @user.save
      render json: { status: 200 }, status: 200
    else
      render json: { status: 300 }, status: 200
    end
  end

  def role
    @user.is_performer = @user.is_performer? ? false : true
    if @user.save
      render json: { status: 200 }, status: 200
    else
      render json: { status: 300 }, status: 200
    end
  end

  def read
    badges = @user.badges.where("is_read = false")
    if badges.update_all(is_read: true)
      render json: { status: 200 }, status: 200
    else
      render json: { status: 300 }, status: 200
    end
  end

  def confirmation
    user = User.where("email = ? and confirmation_code = ?", params[:email].downcase, params[:confirmation_code]).last
    if user.present?
      user.update_attributes(confirmation_code: nil, confirmed_at: DateTime.now)

      @user = { id: user.id, name: user.name, email: user.email, image: user.image.url, token: user.token, is_online: user.is_online?, has_stripe: user.has_stripe?, is_confirmed: user.is_confirmed?, is_performer: user.is_performing?, perform:[], post: [] }

      render json:  { user: @user, status: 200 }, status: 200
    else
      render json: { error: "Confirmation code does not match. Try again.", status: 300 }, status: 200
    end
  end

  def resend_code
    user = User.find_by(email: params[:email])
    if user.present?

      user.resend_confirmation_code
      UserMailer.confirmation_email(user.id).deliver

      render json: { success: "Successfully sent confirmation code to SMS and email.", status: 200 }, status: 200
    else
      render json: { error: "Failed to send confirmation code. Try again later!", status: 300 }, status: 200
    end
  end

  def stripe
    user = params[:token].nil? ? User.find_by(email: params[:email].downcase) : User.find_by(token: params[:token])

    if user.present?
      connect = user.stripe_connect(params[:stripe_account_id])
      if connect[0] == true
        render json: { success: connect[1], status: 200 }, status: 200
      else
        render json: { error: connect[1], status: 300 }, status: 200
      end
    else
      render json: { error: "Email is not registered.", status: 300 }, status: 200
    end
  end

  def stripe_url
    render json: { url: @user.stripe_url, status: 200 }, status: 200
  end

  def stripe_api_key
    stripe_key = APP_CONFIG["stripe_secret_key"]

    render json: { stripe_api_key: stripe_key, status: 200 }, status: 200
  end

  def password
    user = User.find_by(email: params[:email])
    if user.present?
      if user.update_attributes(password: params[:password], reset_token: nil, reset_token_at: nil)
        @user = { id: user.id, name: user.name, email: user.email, image: user.image.url, token: user.token, is_online: user.is_online?, has_stripe: user.has_stripe?, is_confirmed: user.is_confirmed?, is_performer: user.is_performing? }
        @user.merge!({ perform: user.in_progress(1), post: user.in_progress(0) })

        render json:  { user: @user, status: 200 }, status: 200
      else
        render json: { error: "Failed to change your password! Try again later!", status: 300 }, status: 200
      end
    else
      render json: { error: "Your email address is currently not registered to the idle app.", status: 300 }, status: 200
    end
  end

  def completed #pagination on user profile
    #include user
    user = User.includes(:progress).find_by(id: params[:user_id])

    tasks = user.progress.order("finished_at asc").offset(params[:off_set]).select { |task| task.status == 3 && task.is_disabled == false } #change to
    total_tasks = tasks.length

    tasks = tasks.drop(params[:off_set].to_i).first(10)

    @tasks = []
    tasks.each do |task|
      @tasks.push({ id: task.id, title: task.title.titleize, date: task.finished, status: task.status, task_type: task.task_type, user: user_params(task.user)  })
    end

    off_set = params[:off_set].to_i + 10

    @pagination = {
      tasks: total_tasks,
      off_set: off_set,
      load_more: off_set < total_tasks ? 1 : 0,
      url: api_v1_user_completed_path(params[:user_id], token: params[:token], off_set: off_set)
    }

    render json: { tasks: @tasks, pagination: @pagination, status: 200 }, status: 200
  end

  def forgot
    user = User.find_by(email: params[:email])

    if user.nil?
      render json: { error: "Email address is not registered to idle. Failed to sent request.", status: 300 }, status: 200
    else
      user.update_attributes(reset_token: generate_token, reset_token_at: DateTime.now)
      UserMailer.forgot_email(user.id).deliver

      render json: { success: "Successfully sent request password reset email.", status: 200 }, status: 200
    end
  end

  def update
    if @user.update_attributes(params_user)
      user = { id: @user.id, name: @user.name, age: @user.age, school: @user.school, image: @user.image.url, about: @user.description }
      render json: { user: user, status: 200 }, status: 200
    else
      if params_user[:password].to_s.length < 8 && params_user[:password].present?
        render json: { error: "Password is too short (minimum is 8 characters)", status: 300 }, status: 200
      else
        render json: { error: "Email address already taken", status: 300 }, status: 200
      end
    end
  end

  def disconnect #disconnect stripe
    connect = @user.stripe_disconnect
    if connect[0] == true
      render json: { success: connect[1], status: 200 }, status: 200
    else
      render json: { error: connect[1], status: 300 }, status: 200
    end
  end

  def cancel
    if @user.update_attributes(is_active: false, is_disabled: true)
      if params[:message].present?
        UserMailer.cancel_email(@user.id, params[:message]).deliver
      end
      render json: { status: 200 }, status: 200
    else
      render json: { error: "Failed to cancel account. Try again later!", status: 300 }, status: 200
    end
  end

  def feedback
    #save and send email to admin
    UserMailer.feedback_email(@user.id, params[:message]).deliver
    render json: { status: 200 }, status: 200
  end

  private
    def params_user
      params.require(:user).permit(:name, :image, :school, :email, :mobile, :age, :description, :is_online, :is_performer, :latitude, :longitude, :password)
    end

    def generate_token
      return SecureRandom.hex(8).upcase
    end
end
