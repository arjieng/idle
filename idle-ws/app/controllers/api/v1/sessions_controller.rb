class Api::V1::SessionsController < Devise::SessionsController

  def create
    user = User.find_by(email: params[:user][:email], is_disabled: false)
    if user.present? and user.is_active?
      if user.is_confirmed? == 1
        resource = warden.authenticate!(scope: resource_name, recall: "#{controller_path}#failure")
        sign_in_and_redirect(resource_name, resource)
      else
        render json: { error: "Your account needs to be confirmed. Do you want to confirm your account now?", status: 201 }, status: 200
      end
    else
      render json: { error: "Your account is not yet registered!", status: 300 }, status: 200
    end
  end

  def social
    user = User.find_by(email: params[:user][:email], provider_id: params[:user][:provider_id])
    if user.present? and user.is_active?
      if user.is_confirmed?
        resource = User.find_for_database_authentication(email: params[:user][:email])

        sign_in resource

        user = User.find_by_id(current_user.id)
        user.update_attributes(user_params)
        user.is_online = true
        user.save

        sign_out resource

        Device.check_token(params[:device], user.id)

        @user = { id: user.id, name: user.name, email: user.email, image: user.image.url, token: user.token, is_online: user.is_online?, has_stripe: user.has_stripe?, is_confirmed: user.is_confirmed?, is_performer: user.is_performing? }
        @user.merge!({ perform: user.in_progress(1), post: user.in_progress(0) })

        render json: { user: @user, status: 200 }, status: 200
      else
        render json: { error: "Your account needs to be confirmed. Do you want to confirm your account now?", status: 201 }, status: 200
      end
    else
      render json: { error: "Your account is not yet registered!", status: 300 }, status: 200
    end
  end

  def failure
    return render json: { error: "Error with your login or password!", status: 300 }, status: 200
  end

  private
    def sign_in_and_redirect(resource_or_scope, resource=nil)
      scope = Devise::Mapping.find_scope!(resource_or_scope)
      resource ||= resource_or_scope
      sign_in(scope, resource) unless warden.user(scope) == resource

      user = User.find_by_id(current_user.id)
      user.update_attributes(user_params)
      user.is_online = true
      user.save

      sign_out resource

      Device.check_token(params[:device], user.id)

      @user = { id: user.id, name: user.name, email: user.email, image: user.image.url, token: user.token, is_online: user.is_online?, has_stripe: user.has_stripe?, is_confirmed: user.is_confirmed?, is_performer: user.is_performing? }
      @user.merge!({ perform: user.in_progress(1), post: user.in_progress(0) })

      return render json: { user: @user, status: 200 }, status: 200
    end

    def user_params
      params.require(:user).permit(:email, :password, :latitude, :longitude, :provider_id, :provider)
    end
end
