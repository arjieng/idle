class Api::V1::BidsController < ApplicationController
  before_filter :user_authorized

  include ApplicationHelper

  def create
    bid = @user.bids.includes(user: :ratings, task: [user: :devices]).create(bid_params)
    if bid.save

      #notify owner that someone bid
      devices = bid.task.user.devices.map { |device| device.token }
      if devices.present?
        @bid = { id: bid.id, amount: precision(bid.amount), task_type: bid.task.task_type, task_id: bid.task.id, user: user_params(bid.user) }
        PushNotification.notify(devices, "Someone just bid on your #{bid.task.title} task. Check it out now!", @bid, "bidding_task", "New Bid on Task", bid.task.user.id)
      end

      @geolocation = { latitude: @user.latitude, longitude: @user.longitude }
      tasks = Task.filtered(@user.id, params[:type].to_i, @user.filtered_tasks, params[:radius], @geolocation )

      if params[:task].present?
        tasks = tasks.select { |task| task.task_type == params[:task].to_i }
      end

      # if tasks.length > params[:off_set].to_i
      #   task = tasks.drop(params[:off_set].to_i - 10).take(10).last
      #
      #   @task = { id: task.id, title: task.title.titleize, description: task.description, task_type: task.task_type, latitude: task.latitude, longitude: task.longitude, amount: precision(task.price), payment: task.payment, status: task.status, bid: precision(task.amount), user: user_params(task.user) }
      #
      #   render json: { task: @task, status: 201 }, status: 200
      # else
        render json: { bid_id: bid.id, status: 200 }, status: 200
      # end
    else
      render json: { error: "Failed to bid on task due to network issues! Please try again later!", status: 300 }, status: 200
    end
  end

  def index
    bids = Bid.includes(:task, user: :ratings).where(is_active: true, is_disabled: false, task_id: params[:task_id].to_i ).order("created_at desc")
    total_bids = bids.length
    bids = bids.limit(10).offset(params[:off_set])

    @bids = []
    bids.each do |bid|
      @bids.push({ id: bid.id, amount: precision(bid.amount), task_type: bid.task.task_type, user: user_params(bid.user) })
    end

    off_set = params[:off_set].to_i + 10

    @pagination = {
        "bids": total_bids,
        "off_set": off_set,
        "load_more": off_set < total_bids ? 1 : 0,
        "url": "/v1/accounts/tasks/#{params[:task_id]}/bids?token=#{params[:token]}&off_set=#{off_set}"
    }

    render json: { bids: @bids, pagination: @pagination, status: 200 }, status: 200
  end

  def accept
    bid = Bid.includes(user: [:devices, :tasks, :progress], task: [:user, bids: [user: [:devices, :tasks, :progress]]]).find_by(id: params[:bid_id])
    task = bid.task

    if task.update_attributes(status: 1, amount: bid.amount)
      UserTask.create(user_id: bid.user_id, task_id: bid.task_id)

      #send notification to performer
      devices = bid.user.devices.map { |device| device.token }
      if devices.present?
        @task = bid.user.in_progress(1).first
        PushNotification.notify(devices, "Congratulations! #{task.owner} just accepted the #{task.title.titleize} task that you bid on. Check out the task now!", @task, "bidding_accepted", "Your Bid accepted!", bid.user.id)
      end

      user_id = bid.user.id #accepted user id

      #send notification to all performers who bid that task already taken
      task.bids.update_all(is_active: false)
      task.bids.select { |bid| bid.user_id != user_id && bid.is_active == true && bid.is_disabled == false }.each do |bid|
        devices = bid.user.devices.map { |device| device.token }
        if devices.present?
          @task = { task_id: task.id }
          PushNotification.notify(devices, "Someone just got accepted on #{task.title.titleize} task that you bid on. Check out more task and bid on them now!", @task, "bidding_taken", "Bid declined!", bid.user.id)
        end
      end

      render json: { success: "Bid is now accepted. Task will now be performed!", status: 200 }, status: 200
    else
      render json: { error: "Failed to accept bid. Try again later!", status: 300 }, status: 200
    end
  end

  def destroy
    bid = Bid.includes(:task, user: :devices).find_by(id: params[:id])

    if bid.update_attributes(is_active: false, is_disabled: true)

      #notify performer bid was declined
      devices = bid.user.devices.map { |device| device.token }
      if devices.present?
        task = bid.task
        PushNotification.notify(devices, "Your bid offer for #{task.title.titleize} task was declined by poster #{task.owner}.", { task_id: task.id }, "bidding_declined", "Bid Declined!", bid.user.id)
      end

      bids = Bid.includes(:task, user: :ratings).where(is_active: true, is_disabled: false, task_id: params[:task_id].to_i).order("created_at desc")

      if bids.length > params[:off_set].to_i
        bid = bids.drop(params[:off_set].to_i - 10).take(10).last

        @bid = { id: bid.id, amount: precision(bid.amount), task_type: bid.task.task_type, user: user_params(bid.user) }

        render json: { bid: @bid, status: 201 }, status: 200
      else
        render json: { status: 200 }, status: 200
      end
    else
      render json: { status: 300 }, status: 200
    end
  end

  def cancel
    bid = Bid.includes(:user, task: [user: [:devices, :ratings]]).find_by(id: params[:bid_id])
    if bid.update_attributes(is_active: false)

      #notify owner performer canceled
      devices = bid.task.user.devices.map { |device| device.token }

      if devices.present?
        @task = { task_id: bid.task.id, bid_id: bid.id }
        PushNotification.notify(devices, "#{bid.user.name.titleize} canceled their bid on your #{bid.task.title.titleize} task.", @task, "bidding_canceled", "Bid canceled!", bid.task.user.id)
      end

      bids = @user.bids.includes(task: [user: :ratings]).where("is_active = true").order("created_at desc")
      if bids.length > params[:off_set].to_i
        bid = bids.drop(params[:off_set].to_i - 10).take(10).last
        task = bid.task

        @task = { id: task.id, title: task.title.titleize, task_type: task.task_type, status: task.status, bid: bid.amount , user: user_params(task.user)}
        render json: { task: @task, status: 201 }, status: 200
      else
        render json: { status: 200 }, status: 200
      end
    else
      render json: { status: 300 }, status: 200
    end
  end

  private
    def bid_params
      params.require(:bid).permit(:task_id, :user_id, :amount)
    end
end
