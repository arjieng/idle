class Api::V1::TasksController < ApplicationController
  before_filter :user_authorized

  include ApplicationHelper

  def index
    types = params[:type].to_i
    off_set = params[:off_set].to_i + 10

    @geolocation = { latitude: @user.latitude, longitude: @user.longitude }
    tasks = Task.filtered(@user.id, types, @user.filtered_tasks, params[:radius].to_i, @geolocation)

    if types == 0 #map
      users = User.where("id != ? and is_disabled = false and is_active = true and is_online = true and confirmation_code IS NULL and latitude between ? and ? and longitude between ? and ?", @user.id, params[:se_lat].to_f, params[:ne_lat].to_f, params[:sw_long].to_f, params[:ne_long].to_f)

      total_users = users.length
      users = users.drop(params[:off_set].to_i).first(10)

      @users = []
      users.each do |user|
        @users.push(user_params(user))
      end

      tasks = tasks.select { |task| (params[:se_lat].to_f..params[:ne_lat].to_f).include?(task.latitude) && (params[:sw_long].to_f..params[:ne_long].to_f).include?(task.longitude) }
      url = api_v1_tasks_path(token: params[:token], sw_lat: params[:sw_lat], sw_long: params[:sw_long], ne_lat: params[:ne_lat], ne_long: params[:ne_long], off_set: off_set)
    else
      if params[:task].present?
        tasks = tasks.select { |task| task.task_type == params[:task].to_i }
      end

      url = api_v1_tasks_path(token: params[:token], radius: params[:radius], task: params[:task], off_set: off_set, type: types)
    end

    total_tasks = tasks.length
    tasks = tasks.drop(params[:off_set].to_i).first(10)


    @tasks = []
    tasks.each do |task|
      bid = task.bids.select { |bid| bid.user_id == @user.id && bid.is_active == true && bid.is_disabled == false }.last

      user_task = {id: task.id, title: task.title.titleize, description: task.description, task_type: task.task_type, latitude: task.latitude, longitude: task.longitude, amount: precision(task.price), payment: task.payment, status: task.status }

      if bid.present?
        user_task.merge!(bid: precision(bid.amount), bid_id: bid.id)
      else
        user_task.merge!(bid: precision(task.amount))
      end

      user_task.merge!(user: user_params(task.user))
      @tasks.push(user_task)
    end

    @pagination = {
      tasks: total_tasks,
      users: (total_users if types == 0),
      off_set: off_set,
      load_more: (off_set < total_tasks) || ((off_set < total_users) if types == 0 ) ? 1 : 0,
      url: url
    }.delete_if{ |k, v| v.nil? }

    render json: { tasks: @tasks, users: (@users if types == 0), pagination: @pagination, status: 200 }.delete_if{ |k, v| v.nil? }, status: 200
  end

  def directory
    @tasks = []
    if @user.is_performing? == 0
      tasks = @user.tasks.includes(:user, bids: [user: :ratings]).where("status = 0 and is_active = true and is_disabled = false").order("created_at desc")
      total_tasks = tasks.count

      tasks = tasks.limit(10).offset(params[:off_set])

      tasks.each do |task|
        @bids = []
        task.bids.select { |bid| bid.is_active = true && bid.is_disabled == false }.each do |bid|
          @bids.push({ id: bid.id, amount: precision(bid.amount), task_type: bid.task.task_type, user: user_params(bid.user) })
        end

        @tasks.push({ id: task.id, title: task.title.titleize, description: task.description, task_type: task.task_type, status: task.status, amount: precision(task.price), payment: task.payment, bid: precision(0), user: {}, bids: @bids, latitude: task.latitude, longitude: task.longitude })
      end
    else
      bids = @user.bids.includes(task: [user: :ratings]).order("created_at desc").select { |bid| bid.task.status == 0 && bid.is_active == true && bid.is_disabled == false }
      total_tasks = bids.length

      bids = bids.drop(params[:off_set].to_i).first(10)

      bids.each do |bid|
        task = bid.task
        @tasks.push({ id: task.id, title: task.title.titleize, description: task.description, task_type: task.task_type, status: task.status, amount: precision(task.price), payment: task.payment, bid: precision(bid.amount), bid_id: bid.id, user: user_params(task.user), bids: [], latitude: task.latitude, longitude: task.longitude })
      end
    end

    off_set = params[:off_set].to_i + 10

    @pagination = {
      tasks: total_tasks,
      off_set: off_set,
      load_more: off_set < total_tasks ? 1 : 0,
      url: directory_api_v1_tasks_path(token: params[:token], off_set: off_set)
    }

    render json: { tasks: @tasks, pagination: @pagination, status: 200 }, status: 200
  end

  def history
    if @user.is_performing? == 0
      tasks = @user.tasks.includes(:user, :ratings).where("status = 6 and is_active = true and is_disabled = false").order("created_at desc")
      total_tasks = tasks.count

      tasks = tasks.limit(10).offset(params[:off_set])
    else
      tasks = @user.progress.includes(:user, :ratings).where("status = 6").order("created_at desc")
      total_tasks = tasks.count

      tasks = tasks.limit(10).offset(params[:off_set])
    end

    @tasks = []
    tasks.each do |task|
      user = @user.is_performing? == 0 ? task.performer : task.user
      ratings = task.ratings.select { |rating| rating.user_id == user.id }.last
      @tasks.push({ id: task.id, title: task.title.titleize, description: task.description, task_type: task.task_type, status: task.status, amount: precision(task.price), payment: task.payment, bid: precision(task.amount), user: user_params(user), bids: [], date: task.finished, ratings: ratings.present? ? ratings.rate : 0 })
    end

    off_set = params[:off_set].to_i + 10

    @pagination = {
      tasks: total_tasks,
      off_set: off_set,
      load_more: off_set < total_tasks ? 1 : 0,
      url: history_api_v1_tasks_path(token: params[:token], off_set: off_set)
    }

    render json: { tasks: @tasks, pagination: @pagination, status: 200 }, status: 200
  end

  def create
    task = @user.tasks.create(task_params)
    if task.save
      @task = { id: task.id, title: task.title.titleize,  description: task.description, task_type: task.task_type, amount: precision(task.price), payment: task.payment, status: task.status, bid: precision(task.amount), user: {}, bids:[] }
      render json: { task: @task, status: 200 }, status: 200
    else
      render json: { error: "Failed to create task! Try again later!", status: 300 }, status: 200
    end
  end

  def update
    task = Task.find_by(id: params[:id])

    if task.update_attributes(task_params)
      render json: { success: "Successfully updated task!", status: 200 }, status: 200
    else
      render json: { error: "Failed to update task. Try again later!", status: 300 }, status: 200
    end
  end

  def show
    task = Task.includes(:performer, :user, :ratings, bids: [user: :ratings]).find_by(id: params[:id])
    is_owner = @user.is_owner?(task.user_id)

    @task = { id: task.id, title: task.title.titleize, description: task.description, task_type: task.task_type, amount: precision(task.price), payment: task.payment, status: task.status, bid: precision(task.amount)  }

    if is_owner == 1
      @profile = {}

      @bids = []
      if task.status != 0
        @profile = user_params(task.performer)
      else
        task.bids.select { |bid| bid.is_active == true && bid.is_disabled == false }.each do |bid|
          @bids.push({ id: bid.id, amount: precision(bid.amount), user: user_params(bid.user) })
        end
      end

      @task.merge!(bids: @bids)
    else
      @profile = user_params(task.user)

      if task.status == 0
        bid = task.bids.find { |bid| bid.user_id == @user.id && bid.is_active == true && bid.is_disabled == false}
        @task.merge!(bid: precision(bid.nil? ? 0 : bid.amount), bid_id: bid.nil? ? 0 : bid.id)
      else
        @task.merge!(bid: precision(task.amount), has_rate: task.has_rate?(task.user.id))
      end

      #views api
    end

    render json: { task: @task.merge!(user: @profile), status: 200 }, status: 200
  end

  def completed
    task = Task.includes(:user, :performer, user_task: [user: :devices]).find_by(id: params[:task_id])

    if task.user.id == @user.id
      status = params[:status].to_i
      if task.update_attributes(status: status)
        if status == 1
          message = "#{task.owner} marked this task as not yet complete! Please check the task and contact poster regarding the issue!"
        else
          message = "#{task.owner} has confirmed that this task is completed. We would really appreciate it if you could rate #{task.owner}!"
        end

        @task = { task_id: task.id, status: task.status }

        devices = task.performer.devices.map{ |device| device.token }
        if devices.present?
          PushNotification.notify(devices, message, @task, "task_completed", "Task Completed", task.performer.id)
        end

        if status == 1
          render json: { success: "#{task.performer.name} has been alerted that task is not yet completed!", status: 200 }, status: 200
        else
          render json: { success: "We would really appreciate it if you could rate your performer!", status: 200 }, status: 200
        end
      else
        render json: { error: "Failed to mark task as completed.", status: 300 }, status: 200
      end
    else
      if task.update_attributes(status: 4)
        @task = { task_id: task.id, status: task.status }

        #push notification to owner task is completed
        devices = task.user.devices.map { |device| device.token }
        if devices.present?
          PushNotification.notify(devices, "#{task.performer.name} has completed your task. We would really appreciate it if you could rate your performer!", @task, "task_completed", "Task Completed", task.user.id)
        end

        render json: { success: "#{task.owner} will be notified that you have completed the task! Tune in for confirmation", status: 200 }, status: 200
      else
        render json: { error: "Failed to mark task as completed.", status: 300 }, status: 200
      end
    end
  end

  def ratings
    task = Task.includes(:ratings, :user).find_by(id: params[:task_id])

    if task.ratings.create(ratings_params)
      render json: { status: 200 }, status: 200
    else
      render json: { status: 300 }, status: 200
    end
  end

  def finished
    task = Task.includes(:ratings, performer: :devices).find_by(id: params[:task_id])
    if task.update_attributes(status: 6, finished_at: DateTime.now)
      task.ratings.create(ratings_params)

      @task = { task_id: task.id, status: task.status }

      if !task.is_cash?
        charge = Charge.new(charge_params)
        payment = charge.payment(task.performer.stripe_account_id, params[:stripe_token], "#{task.performer.name} paid #{task.title.titleize} task owned by #{task.owner.titleize}")

        if payment[0] == true
          if charge.save

            #push notification on performer
            devices = task.performer.devices.map { |device| device.token }
            if devices.present?
              PushNotification.notify(devices, "#{task.owner} has rated your performance. Check out more tasks on the map to work on!", @task, "task_finished", "Task Mark Finish", task.performer.id)
            end

            render json: { status: 200 }, status: 200
          else
            render json: { error: "Failed to create charge on stripe. Try again later!", status: 300 }, status: 200
          end
        else
          render json: { error: payment[1], status: 300 }, status: 200
        end
      else
        if task.create_charge(charge_params)

          #push notification on performer
          devices = task.performer.devices.map { |device| device.token }
          if devices.present?
            PushNotification.notify(devices, "#{task.owner} has rated your performance. Check out more tasks on the map to work on!", @task, "task_finished", "Task Mark Finish", task.performer.id)
          end

          render json: { status: 200 }, status: 200
        else
          render json: { status: 300 }, status: 200
        end
      end
    else
      render json: { status: 300 }, status: 200
    end
  end

  def cancel
    task = Task.includes(performer: :devices, user: :devices).find_by(id: params[:task_id])
    type = params[:type].to_i #[0 - performer canceled, 1 - poster canceled]

    if task.user_id == @user.id
      if type == 0
        if task.update_attributes(status: params[:status], amount: params[:status].to_i == 0 ? 0 : task.amount)
          status = params[:status].to_i

          if status == 0
            message = "#{task.owner} granted your request to cancel #{task.title.titleize}."
            success = "Do you want to report #{task.performer.name}?"
          else
            message = "#{task.owner} denied your request to cancel the task that you are performing. Please message #{task.owner}."
            success = "#{task.performer.name} was notified that you denied their cancellation request"
          end

          devices = task.performer.devices.map { |device| device.token }
          if devices.present?
            PushNotification.notify(devices, message, { task_id: task.id, status: status }, "task_canceled", "Task Cancelled", task.performer.id)
          end

          render json: { success: success, status: 200 }, status: 200
        else
          render json: { error: "Failed to confirm cancellation", status: 300 }, status: 200
        end
      else
        if task.update_attributes(status: 3)
          devices = task.performer.devices.map { |device| device.token }
          if devices.present?
            PushNotification.notify(devices, "#{task.owner} wants to cancel their #{task.title.titleize} task that you are currently performing.", { task_id: task.id, status: 3 }, "task_canceled", "Task Cancelled", task.performer.id)
          end

          render json: { success: "#{task.performer.name} will be notified of the cancellation.", status: 200 }, status: 200
        else
          render json: { error: "Failed to mark task as cancelled!", status: 300 }, status: 200
        end
      end
    else
      if type == 0
        if task.update_attributes(status: 2)
          devices = task.user.devices.map { |device| device.token }
          if devices.present?
            PushNotification.notify(devices, "#{task.performer.name} wants to cancel your in progress task.", { task_id: task.id, status: 2 }, "task_canceled", "Task Cancelled", task.user.id)
          end

          render json: { success: "#{task.owner} will be notified that you have cancelled the task!", status: 200 }, status: 200
        else
          render json: { error: "Failed to mark task as cancelled!", status: 300 }, status: 200
        end
      else
        if task.update_attributes(status: params[:status])
          status = params[:status].to_i

          if status == 0
            task.update_attributes(amount: 0)
            message = "#{task.performer.name} confirmed your request to cancel #{task.title.titleize} task."
            success = "Do you want to report task of #{task.owner}?"
          else
            message = "#{task.performer.name} denied your request to cancel #{task.title.titleize} task. Please message #{task.performer.name}."
            success = "#{task.owner} is now notified that you denied the cancellation request"
          end

          devices = task.user.devices.map { |device| device.token }
          if devices.present?
            PushNotification.notify(devices, message, { task_id: task.id, status: status }, "task_canceled", "Task Cancelled", task.user.id)
          end

          render json: { success: success, status: 200 }, status: 200
        else
          render json: { error: "Failed to confirm cancellation", status: 300 }, status: 200
        end
      end
    end
  end

  def destroy
    task = Task.includes(:user, bids: [user: :devices]).find_by(id: params[:id])
    if task.update_attributes(is_disabled: true)

      #sent notification to people who bid
      devices = []
      task.bids.select{ |bid| bid.is_active == true && bid.is_disabled == false }.each do |bid|
        devices = bid.user.devices.map { |device| device.token }
        if devices.present?
          @task = { task_id: task.id }
          PushNotification.notify(devices, "#{task.owner} deleted the task!. Check out more tasks on the map to work on!", @task, "task_deleted", "Task Deleted!", bid.user.id)
        end
      end

      task.bids.update_all(is_active: false, is_disabled: true)

      tasks = @user.tasks.includes(:user, bids: [user: [:ratings, :devices]]).where("status = 0 and is_active = true and is_disabled = false").order("created_at desc")

      if tasks.length > params[:off_set].to_i
        task = tasks.drop(params[:off_set].to_i - 10).take(10).last

        @bids = []
        task.bids.select { |bid| bid.is_active = true && bid.is_disabled == false }.each do |bid|
          @bids.push({ id: bid.id, amount: precision(bid.amount), user: user_params(bid.user) })
        end

        @task = { id: task.id, title: task.title.titleize, description: task.description, task_type: task.task_type, status: task.status, amount: precision(task.price), payment: task.payment, bid: precision(0), user: {}, bids: @bids }

        render json: { task: @task, status: 201 }, status: 200
      else
        render json: { success: "Successfully deleted task!", status: 200 }, status: 200
      end
    else
      render json: { error: "Failed to delete task. There might be an error on your network. Try again later!", status: 300 }, status: 200
    end
  end

  private
    def task_params
      params.require(:task).permit(:title, :description, :task_type, :price, :latitude, :longitude, :is_cash, :amount)
    end

    def charge_params
      params.require(:charge).permit(:user_id, :amount, :task_id)
    end

    def ratings_params
      params.require(:ratings).permit(:user_id, :rate)
    end
end
