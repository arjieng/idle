class Api::V1::ConversationsController < ApplicationController
  before_filter :user_authorized
  include ActionView::Helpers::DateHelper

  def index
    conversations = @user.mailbox.conversations.includes(receipts: :receiver)
    total_conversations = conversations.length

    conversations = conversations.drop(params[:off_set].to_i).first(10)

    @conversations = []
    conversations.each do |conversation|
      @conversations.push(conversations(conversation))
    end

    off_set = params[:off_set].to_i + 10

    @pagination = {
      conversations: total_conversations,
      off_set: off_set,
      load_more: total_conversations > off_set ? 1 : 0,
      url: api_v1_conversations_path(off_set: off_set, token: params[:token])
    }

    render json: { conversations: @conversations, pagination: @pagination, status: 200 }, status: 200
  end

  # def read
  #   conversation = @user.mailbox.conversations.find_by_id(params[:message_id])
  #   if conversation.mark_as_read(@user)
  #     badges = @user.badges.where("is_read = false and conversation_id = ?", conversation.id)
  #     badges.update_all(is_read: true)
  #
  #     render json: { badges: Badge.total(@user.id), message_badge: @user.message_badge, status: 200 }, status: 200
  #   else
  #     render json: { status: 201 }, status: 200
  #   end
  # end

  private
    def time(date)
      if date.strftime("%^B %d, %Y") == DateTime.now.strftime("%^B %d, %Y")
        distance = distance_of_time_in_words(date, DateTime.now)
        return distance == "Just Now" ? distance : distance
      else
        return date.strftime("%m/%d/%y")
      end
    end

    def conversations(conversation)
      sender = conversation.receipts.where("receiver_id != ?", @user.id).last.receiver
      receipt = conversation.receipts_for(@user).last
      notification = conversation.last_message

      is_owner = notification.sender_id == @user.id ? 1 : 0

      message = { user: { id: sender.id, name: sender.name, image: sender.image.url }, message: notification.body }

      return { id: conversation.id, is_read: is_owner == 1 ? 1 : (receipt.is_read? ? 1 : 0) }.merge!(message)
    end
end
