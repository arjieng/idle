class Api::V1::ReportsController < ApplicationController
  before_filter :user_authorized

  def create
    if @user.reports.create(report_params)
      # send email to admin
      render json: { success: "Thank you for your message, we will get back to you shortly", status: 200 }, status: 200
    else
      render json: { error: "Failed to submit report due to network connection. Try again later!", status: 300 }, status: 200
    end
  end

  private
    def report_params
      params.require(:report).permit(:task_id, :user_id, :description, :block_id)
    end
end
