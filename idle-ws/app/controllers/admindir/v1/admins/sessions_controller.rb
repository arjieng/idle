class Admindir::V1::Admins::SessionsController < Devise::SessionsController
	layout "admin"

	def new
		super
	end

  def create
		super
    admin = Admin.find_by(email: admin_params[:email])
    if admin.present? && admin.valid_password?(admin_params[:password])
      if admin.is_disabled == false
				resource = warden.authenticate!(scope: resource_name, recall: "#{controller_path}#failure")
				set_flash_message!(:notice, :signed_in)

				sign_in_and_redirect(resource_name, resource)
      else
				set_flash_message!(:alert, "Your account is disabled. Please contact your administor!")
				redirect_to :back
      end
    else
			set_flash_message!(:alert, "Incorrect credentials. Contact your administrator!")
			redirect_to :back
    end
  end

  def failure
		set_flash_message!(:alert, "Error with your login or password!")
		redirect_to :back
  end

  private
    def sign_in_and_redirect(resource_or_scope, resource=nil)
      scope = Devise::Mapping.find_scope!(resource_or_scope)
      resource ||= resource_or_scope
      sign_in(scope, resource) unless warden.user(scope) == resource

      @admin = Admin.find_by_id(current_admin.id)
    end

    def admin_params
      params.require(:admin).permit(:email, :password)
    end
end
