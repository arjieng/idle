class Admindir::V1::TasksController < ApplicationController
  before_filter :authenticate_admin
  layout "admin"

  def index
    condition_arr = []
    
    if params[:search_key].to_s.present?
      condition_arr = ["LOWER(title) LIKE ?", "%#{params[:search_key].to_s.downcase}%"]
    end
    
    @tasks = Task.includes(:user, :bids).where(condition_arr).order("created_at desc").paginate(page: params[:page], per_page: 10)
  end

  def show
    @task = Task.includes(:performer, :bids, user: :ratings).find_by(id: params[:id])
    # user = task.user
    # task_type = task_type(task.task_type)
  	# @task = { id: task.id,
  	#  title: task.title.titleize,
  	#  description: task.description,
  	#  price: task.price,
  	#  payment: task.is_cash == true ? "Cash" : "Stripe",
  	#  status: status(task.status),
  	#  task_type: task_type[2],
  	#  task_image: task_type[1],
  	#  task_class: task_type[0],
  	#  latitude: task.latitude,
  	#  longitude: task.longitude,
  	#  owner: {
  	#  	name: user.name.titleize,
  	#  	school: user.school.titleize,
  	#  	image: user.image.url,
  	#  	ratings: user.rate
	 # }
	# }
  end

  private
    def task_type(num)
    	case num
  	when 0
  		["quickie", "/assets/0.png", "QUICK TASK"]
  	when 1
  		["grocery", "/assets/1.png", "GROCERY"]
  	when 2
  		["laundry", "/assets/2.png", "LAUNDRY"]
  	when 3
  		["transport", "/assets/3.png", "TRANSPORT"]
  	when 4
  		["other", "/assets/4.png", "OTHER"]
  	end
    end

    def status(num)
    	case num
    	when 0
    		[ num, "Available" ]
  	when 5
  		[ num, "Complete" ]
  	when 6
  		[ num, "Finished" ]
  	else
  		[ num, "In Progress" ]
    	end
    end
end
