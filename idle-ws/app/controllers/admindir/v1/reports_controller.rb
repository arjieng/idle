class Admindir::V1::ReportsController < ApplicationController
  before_filter :authenticate_admin
  layout "admin"
  require 'will_paginate/array'

  def index
  	@reports = Report.order("created_at DESC").paginate(page: params[:page], per_page: 15)
  end
end
