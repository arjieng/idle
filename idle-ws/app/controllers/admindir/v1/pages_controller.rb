class Admindir::V1::PagesController < ApplicationController
  before_filter :authenticate_admin

  layout "admin"

  def dashboard
    @schools = User.select(:school).uniq.count
    users = User.all
    tasks = Task.where("is_active = true and is_disabled = false")
    charges = Charge.all

    @users = users.select{ |user| user.is_active == true and user.is_disabled == false }.size
    @tasks = tasks.size
    @charges = {
      spent: charges.map{ |charge| charge.amount }.sum,
      stripe: charges.select{ |charge| charge.charge_id != nil }.map{ |charge| charge.amount }.sum,
      cash: charges.select{ |charge| charge.charge_id == nil }.map{ |charge| charge.amount }.sum
    }

    @statistics = {
      downloads: Device.count,
      cancelled: users.select{ |user| user.is_active == false and user.is_disabled == true }.size,
      schools: @schools,
      users: @users,
      new: users.select{ |user| user.is_active == true and user.is_disabled == false and user.created_at > 1.month.ago }.size,
      task_created: @tasks,
      task_completed: tasks.select{ |task| task.status == 6 }.size,
      spent: charges.map{ |charge| charge.amount }.sum.nil? ? 0 : charges.map{ |charge| charge.amount }.sum,
      revenue: @charges[:stripe].nil? ? 0 : (@charges[:stripe] * 0.5)
    }

    #if now > ago : increase
    #formula
    # Increase = Now - Ago , %Increase =  ( Increase / Ago ) * 100
    # Decrease = Ago - Now , %Decrease =  ( Decrease / Ago ) * 100

   @thismonth = Time.now.beginning_of_month..Time.now.end_of_month
   @monthago = 1.month.ago.beginning_of_month..1.month.ago.end_of_month

    #Download
    month_now = Device.select{ |download| download.created_at > 1.month.ago }.count
    month_ago = Device.select{ |download| download.created_at < 1.month.ago and download.created_at > 2.month.ago }.count

    #Cancelled
    cancelled_now = users.select{ |user| user.is_active == false and user.is_disabled == true and user.created_at > 1.month.ago }.size
    cancelled_ago = users.select{ |user| user.is_active == false and user.is_disabled == true and user.created_at < 1.month.ago and user.created_at > 2.month.ago }.size

    #Schools
    schools_now = User.select(:school).where(created_at: @thismonth).uniq.count
    schools_ago = User.select(:school).where(created_at: @monthago).uniq.count

    #Active User
    active_users_now = users.select{ |user| user.is_active == true and user.is_disabled == false and user.created_at > 1.month.ago }.size
    active_users_ago = users.select{ |user| user.is_active == true and user.is_disabled == false and user.created_at < 1.month.ago and user.created_at > 2.month.ago }.size

    #New User
    new_user_now = users.select{ |user| user.is_active == true and user.is_disabled == false and user.created_at > 1.month.ago }.size
    new_user_ago = users.select{ |user| user.is_active == true and user.is_disabled == false and user.created_at < 1.month.ago and user.created_at > 2.month.ago }.size

    #Task Created
    tasks_now = tasks.select{ |task| task.created_at > 1.month.ago }.size
    tasks_ago = tasks.select{ |task| task.created_at < 1.month.ago and task.created_at > 2.month.ago }.size

    #Task Completed
    completed_now = tasks.select{ |task| task.created_at > 1.month.ago and task.status == 6 }.size
    completed_ago = tasks.select{ |task| task.created_at < 1.month.ago and task.created_at > 2.month.ago and task.status == 6 }.size

    #Amount Spent
    amount_now = charges.select{ |charge| charge.created_at > 1.month.ago}.map{ |charge| charge.amount }.sum
    amount_ago = charges.select{ |charge| charge.created_at < 1.month.ago and charge.created_at > 2.month.ago }.map{ |charge| charge.amount }.sum
    if amount_ago.nil?
      amount_ago = 0
    end

    #Revenue
    charges_now = {
      stripe: charges.select{ |charge| charge.charge_id != nil and charge.created_at > 1.month.ago }.map{ |charge| charge.amount }.sum
    }

    charges_ago = {
      stripe: charges.select{ |charge| charge.charge_id != nil and charge.created_at < 1.month.ago and charge.created_at > 2.month.ago}.map{ |charge| charge.amount }.sum
    }

    total_charges_now = charges_now[:stripe].nil? ? 0 : (charges_now[:stripe] * 0.5)
    total_charges_ago = charges_ago[:stripe].nil? ? 0 : (charges_ago[:stripe] * 0.5)

    @percentage =
    {
      downloads:  month_ago > 0 ? month_now  > month_ago ? ((month_now - month_ago) / month_ago) * 100 : ((month_ago - month_now) / month_ago) * -100 : 100,
      cancelled: cancelled_ago > 0 ? cancelled_now > cancelled_ago ? ((cancelled_now - cancelled_ago) / cancelled_ago) * 100 : ((cancelled_ago - cancelled_now) / cancelled_ago) * -100 : 100,
      schools: schools_ago > 0 ? schools_now > schools_ago ? ((schools_now - schools_ago) / schools_ago) * 100 : ((schools_ago - schools_now) / schools_ago) * -100 : 100,
      users: active_users_ago > 0 ? active_users_now > active_users_ago ? ((active_users_now - active_users_ago) / active_users_ago) * 100 : ((active_users_ago - active_users_now) / active_users_ago) * -100 : 100,
      new: new_user_ago > 0 ? new_user_now > new_user_ago ? ((new_user_now - new_user_ago) / new_user_ago) * 100 : ((new_user_ago - new_user_now) / new_user_ago) * -100 : 100,
      task_created: tasks_ago > 0 ? tasks_now > tasks_ago ? ((tasks_now - tasks_ago) / tasks_ago) * 100 : ((tasks_ago - tasks_now) / tasks_ago) * -100 : 100,
      task_completed:completed_ago > 0 ? completed_now > completed_ago ? ((completed_now - completed_ago) / completed_ago) * 100 : ((completed_ago - completed_now) / completed_ago) * -100 : 100,
      spent: (amount_ago > 0) ? (amount_now > amount_ago) ? ((amount_now - amount_ago) / amount_ago) * 100 : ((amount_ago - amount_now) / amount_ago) * -100 : 100,
      revenue:total_charges_ago > 0 ? total_charges_now > total_charges_ago ? ((total_charges_now - total_charges_ago) / total_charges_ago) * 100 : ((total_charges_ago - total_charges_now) / total_charges_ago) * -100 : 100
    }

    # @thisyear = Time.now.beginning_of_year..Time.now.end_of_year
    # @startday = Time.now.beginning_of_week

    #Task Created vs Completed
    @week_tasks = {
      labels: ['M', 'T', 'W', 'T', 'F', 'S', 'S'],
      created: [],
      completed: [],
      max: 0
    }

    this_week = Date.current.beginning_of_week..Date.current.end_of_week
    this_week.each do |date|
      @week_tasks[:created].push( tasks.where("DATE(created_at) = ? AND status != 6", date).count )
      @week_tasks[:completed].push( tasks.where("DATE(created_at) = ? AND status = 6", date).count )

      if @week_tasks[:created].last > @week_tasks[:max] || @week_tasks[:completed].last > @week_tasks[:max]
        if @week_tasks[:created].last > @week_tasks[:completed].last
          @week_tasks[:max] = @week_tasks[:created].last
        else
          @week_tasks[:max] = @week_tasks[:completed].last
        end
      end
    end
    
    @monthly_tasks = {
      labels: ['J', 'F', 'M', 'A', 'M', 'J', 'J', 'A', 'S', 'O', 'N', 'D'],
      created: [],
      completed: [],
      max: 0
    }

    i = 0
    start_date = Date.current.beginning_of_year

    while i < 12
      date = start_date + i.months

      @monthly_tasks[:created].push( tasks.where("DATE(created_at) >= ? AND DATE(created_at) <= ? AND status != 6", date.beginning_of_month, date.end_of_month).count )
      @monthly_tasks[:completed].push( tasks.where("DATE(created_at) >= ? AND DATE(created_at) <= ? AND status = 6", date.beginning_of_month, date.end_of_month).count )

      if @monthly_tasks[:created].last > @monthly_tasks[:max] || @monthly_tasks[:completed].last > @monthly_tasks[:max]
        if @monthly_tasks[:created].last > @monthly_tasks[:completed].last
          @monthly_tasks[:max] = @monthly_tasks[:created].last
        else
          @monthly_tasks[:max] = @monthly_tasks[:completed].last
        end
      end

      i += 1
    end
    
    end_date = Date.current.end_of_year
    year_tasks = tasks.where("DATE(created_at) >= ? AND DATE(created_at) <= ?", start_date, end_date)
    
    @year_tasks = {
      labels: [Date.current.year.to_s],
      created: [ year_tasks.where("status != 6").count ],
      completed: [ year_tasks.where(status: 6).count ],
      max: 0
    }

    @year_tasks[:max] = @year_tasks[:created].last > @year_tasks[:completed].last ? @year_tasks[:created].last : @year_tasks[:completed].last
    
    # Task Type Created vs Completed
    @task_types = {
      labels: ['Q', 'G', 'L', 'T', 'O'],
      week: {
        created: [],
        completed: [],
        max: 0
      },
      monthly: {
        created: [],
        completed: [],
        max: 0
      },
      year: {
        created: [],
        completed: [],
        max: 0
      }
    }
    
    year_max = 0
    
    [0,1,2,3,4].each do |type|
      # weekly task types
      @task_types[:week][:created].push( tasks.where("DATE(created_at) IN (?) AND status != 6 AND task_type = ?", this_week, type).count / 7 )
      @task_types[:week][:completed].push( tasks.where("DATE(created_at) IN (?) AND status = 6 AND task_type = ?", this_week, type).count / 7 )
      
      # monthly task types
      @task_types[:monthly][:created].push( tasks.where("DATE(created_at) IN (?) AND status != 6 AND task_type = ?", Date.current.beginning_of_year..Date.current.end_of_year, type).count / 12 )
      @task_types[:monthly][:completed].push( tasks.where("DATE(created_at) IN (?) AND status = 6 AND task_type = ?", Date.current.beginning_of_year..Date.current.end_of_year, type).count / 12 )
      
      # current year task types
      @task_types[:year][:created].push( year_tasks.where("status != 6 AND task_type = ?", type).count )
      @task_types[:year][:completed].push( year_tasks.where("status = 6 AND task_type = ?", type).count )
      
      # retrieve weekly max task type
      if @task_types[:week][:created].last > @task_types[:week][:max] || @task_types[:week][:completed].last > @task_types[:week][:max]
        if @task_types[:week][:created].last > @task_types[:week][:completed].last
          @task_types[:week][:max] = @task_types[:week][:created].last
        else
          @task_types[:week][:max] = @task_types[:week][:completed].last
        end
      end
      
      # retrieve monthly max task type
      if @task_types[:monthly][:created].last > @task_types[:monthly][:max] || @task_types[:monthly][:completed].last > @task_types[:monthly][:max]
        if @task_types[:monthly][:created].last > @task_types[:monthly][:completed].last
          @task_types[:monthly][:max] = @task_types[:monthly][:created].last
        else
          @task_types[:monthly][:max] = @task_types[:monthly][:completed].last
        end
      end
      
      # retrieve current year max task type
      if year_max < @task_types[:year][:created].last || year_max < @task_types[:year][:completed].last
        if @task_types[:year][:created].last > @task_types[:year][:completed].last
          year_max = @task_types[:year][:created].last
        else
          year_max = @task_types[:year][:completed].last
        end
      end
    end
    
    # set current year max task type
    @task_types[:year][:max] = year_max
    # i = 0
    # begin
      # t = tasks.where(created_at:@startday.beginning_of_day + ((60*60*24) * i)..@startday.end_of_day + ((60*60*24) * i),status: 0)
      # @task_monthly[:created].push(t.count)
      # t = tasks.where(created_at:@startday.beginning_of_day + ((60*60*24) * i)..@startday.end_of_day + ((60*60*24) * i),status: -1)
      # @task_monthly[:completed].push(t.count)
      # i += 1
    # end until i > Time.days_in_month(Time.now.month, Time.now.year) - 1
# #     
#     
    # #Task Created vs Completed
    # task_yearly = []
    # i = 0
    # begin
      # t = tasks.where(created_at:@startday.beginning_of_day + ((60*60*24) * i)..@startday.end_of_day + ((60*60*24) * i),status: 0)
      # task_daily.push(t)
      # i += 1
    # end until i > ((Time.now.year % 4 == 0) ? 366 : 365)


    #Task Type
      #0 - Quick #1 - Grocery #2 - Laundry #3 - Transport #4 - Other

      #Task Type Created vs Completed Weekly
      # @task_type_weekly =
      # {
      #   created = {
      #     quick: tasks.where(created_at: @thisweek, task_type: 0,status: 0),
      #     grocery: tasks.where(created_at: @thisweek, task_type: 1,status: 0),
      #     laundry: tasks.where(created_at: @thisweek, task_type: 2,status: 0),
      #     transport: tasks.where(created_at: @thisweek, task_type: 3,status: 0),
      #     other: tasks.where(created_at: @thisweek, task_type: 4,status: 0)
      #   },
      #   completed =
      #   {
      #     quick: tasks.where(created_at: @thisweek, task_type: 0, status: 6),
      #     grocery: tasks.where(created_at: @thisweek, task_type: 1, status: 6),
      #     laundry: tasks.where(created_at: @thisweek, task_type: 2, status: 6),
      #     transport: tasks.where(created_at: @thisweek, task_type: 3, status: 6),
      #     other: tasks.where(created_at: @thisweek, task_type: 4, status: 6)
      #   }
      # }

      # #Task Type Created vs Completed Monthly
      # @task_type_monthly =
      # {
      #   created = {
      #     quick: tasks.where(created_at: @thismonth, task_type: 0,status: 0),
      #     grocery: tasks.where(created_at: @thismonth, task_type: 1,status: 0),
      #     laundry: tasks.where(created_at: @thismonth, task_type: 2,status: 0),
      #     transport: tasks.where(created_at: @thismonth, task_type: 3,status: 0),
      #     other: tasks.where(created_at: @thismonth, task_type: 4,status: 0)
      #   },
      #   completed =
      #   {
      #     quick: tasks.where(created_at: @thismonth, task_type: 0, status: 6),
      #     grocery: tasks.where(created_at: @thismonth, task_type: 1, status: 6),
      #     laundry: tasks.where(created_at: @thismonth, task_type: 2, status: 6),
      #     transport: tasks.where(created_at: @thismonth, task_type: 3, status: 6),
      #     other: tasks.where(created_at: @thismonth, task_type: 4, status: 6)
      #   }

      # }

      # #Task Type Created vs Completed
      # @task_type_yearly =
      # {
      #   created = {
      #     quick: tasks.where(created_at: @thisyear, task_type: 0,status: 0),
      #     grocery: tasks.where(created_at: @thisyear, task_type: 1,status: 0),
      #     laundry: tasks.where(created_at: @thisyear, task_type: 2,status: 0),
      #     transport: tasks.where(created_at: @thisyear, task_type: 3,status: 0),
      #     other: tasks.where(created_at: @thisyear, task_type: 4,status: 0)
      #   },
      #   completed =
      #   {
      #     quick: tasks.where(created_at: @thisyear, task_type: 0, status: 6),
      #     grocery: tasks.where(created_at: @thisyear, task_type: 1, status: 6),
      #     laundry: tasks.where(created_at: @thisyear, task_type: 2, status: 6),
      #     transport: tasks.where(created_at: @thisyear, task_type: 3, status: 6),
      #     other: tasks.where(created_at: @thisyear, task_type: 4, status: 6)
      #   }
      # }

      @pie_chart = {
        quickie: tasks.select{ |task| task.task_type == 0 }.size.to_f / @tasks * 100,
        grocery: tasks.select{ |task| task.task_type == 1 }.size.to_f / @tasks * 100,
        laundry: tasks.select{ |task| task.task_type == 2 }.size.to_f / @tasks * 100,
        transport: tasks.select{ |task| task.task_type == 3 }.size.to_f / @tasks * 100,
        other: tasks.select{ |task| task.task_type == 4 }.size.to_f / @tasks * 100
      }
  end




  def settings

  end

  def contact

  end

end
