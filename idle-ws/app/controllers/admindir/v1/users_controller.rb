class Admindir::V1::UsersController < ApplicationController
  before_filter :authenticate_admin
  layout "admin"
  require 'will_paginate/array'

  def index
    condition_arr = ["is_active = true"]
    
    if params[:search_key].to_s.present?
      condition_arr[0] += " AND LOWER(name) LIKE ?"
      condition_arr << "%#{params[:search_key].to_s.downcase}%"
    end
    
    @users = User.includes(:ratings).where(condition_arr).order("created_at desc").paginate(page: params[:page], per_page: 15)
  end

  def show
  	@user = User.includes(:ratings, bids: [ task: [ :user_task, :user ]], user_tasks: [ task: [ user: :ratings ] ] , tasks: [ :bids, user_task: [ user: :ratings ]], progress: [:user, :ratings]).find_by(id: params[:id])
  	@tasks = @user.tasks.select{ |task| task.is_active == true && task.is_disabled == false }.sort

    @poster_in_progress = @user.in_progress(0).paginate(page: params[:posted_inprogress_page], per_page: 5)
    @poster_bids = @tasks.select{ |task| task.status == 0 }.paginate(page: params[:posted_bids_page], per_page: 5)
    @poster_history = @tasks.select{ |task| task.status == 6 }.paginate(page: params[:posted_history_page], per_page: 5)

    @performer_in_progress = @user.in_progress(1).paginate(page: params[:performed_inprogress_page], per_page: 5)
    @performer_bids = @user.bids.order("created_at desc").select { |bid| bid.task.status == 0 && bid.is_active == true && bid.is_disabled == false }
    @performer_history = @user.progress.select{ |task| task.status == 6 }.paginate(page: params[:performed_history_page], per_page: 5)
  end

  def destroy
    @user = User.find_by(id: params[:id])

    if @user.nil?
    elsif @user.update_attributes({ is_active: false, is_disabled: true })
      redirect_to admindir_v1_users_url
    end
  end

  private
  	def task_type number
  		case number
  		when 0
			["quickie", "/assets/0.png"]
		when 1
			["grocery", "/assets/1.png"]
		when 2
			["laundry", "/assets/2.png"]
		when 3
			["transport", "/assets/3.png"]
		when 4
			["other", "/assets/4.png"]
  		end
  	end
end
