class Admindir::V1::AdminsController < ApplicationController
  before_filter :authenticate_admin
  layout "admin"

  def index
    condition_arr = []
    
    if params[:search_key].to_s.present?
      condition_arr = ["LOWER(name) LIKE ?", "%#{params[:search_key].to_s.downcase}%"]
    end
    
    @admins = Admin.where(condition_arr).order("created_at desc").paginate(page: params[:page], per_page: 15)
  end
  
  def new
    @admin = Admin.new(role_id: 0, password: Devise.friendly_token)
    set_edit_view_variables("Idle | New Admin", "New Admin", "Save", { url: admindir_v1_admins_path(@admin), method: "post" })
    
    render :edit
  end

  def show

  end

  def edit
    @admin = Admin.find_by(id: params[:id])
    set_edit_view_variables("Idle | Edit Profile", "Edit Profile", "Update Profile", { url: edit_admindir_v1_admin_path(@admin) })
  end
  
  def create
    @admin = Admin.new(admin_params)
    
    if @admin.save
      AdminMailer.temporary_password_email(@admin.id, admin_params['password']).deliver
      redirect_to admindir_v1_admins_path
    else
      set_edit_view_variables("Idle | New Admin", "New Admin", "Save", { url: admindir_v1_admins_path(@admin), method: "post" })
      render :edit
    end
  end

  def update
    @admin = Admin.find_by(id: params[:id])
    if @admin.update_attributes(admin_params.merge(name: admin_params[:first_name] + " " + admin_params[:last_name]))
      redirect_to :back
    else
      redirect_to :back
    end
  end

  private
    def admin_params
      params.require(:admin).permit(:first_name, :last_name, :username, :email, :about, :name, :image, :password, :role_id)
    end
    
    def set_edit_view_variables(page_title, form_title, button_title, form_options)
      @page_title   = page_title
      @form_title   = form_title
      @button_title = button_title
      @form_options = form_options
    end
end
