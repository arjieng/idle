class SessionsController < Devise::SessionsController
  respond_to :json

  def create
    @admin = Admin.find_by(email: params[:admin][:email])
    if @admin.present?
      if @admin.is_disabled?
        render json: "Your account is disabled and cannot proceed when signing in. Please contact TheIdleApp Admin Team for access.", status: 300
      else
        resource = warden.authenticate!(scope: resource_name, recall: "#{controller_path}#failure")
        sign_in_and_redirect(resource_name, resource)
      end
    else
      render json: "This account is not authorized to login in to TheIdleApp Admin Portal. Please contact TheIdleApp Admin Team for authorization!" , status: 300
    end
  end

  def destroy
    scope = Devise::Mapping.find_scope!(resource_name)
    warden.user(scope)
    warden.raw_session.inspect
    warden.logout(scope)

    redirect_to admins_pages_path
  end

  def failure
    return render json: "Login credentials invalid!", status: 300
  end

  private
    def sign_in_and_redirect(resource_or_scope, resource=nil)
      scope = Devise::Mapping.find_scope!(resource_or_scope)
      resource ||= resource_or_scope
      sign_in(scope, resource) unless warden.user(scope) == resource
      @admin = Admin.find_by(id: current_admin.id)
      if @admin.present?
        return render json: { success: "success", status: 200}, status: 200
      else
        return render json: "The account that you're signing is incorrect. Please enter correct credentials for log in!", status: 300
      end
    end

end
