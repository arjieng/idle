class PagesController < ApplicationController
  layout "mobile", only: [:password, :stripe]

  def password
    user = User.find_by(email: params[:email])

    if user.present?
      @email = user.email
      if user.reset_token.nil? && user.reset_token_at.nil?
        @message = "This link is already expired! Click the button below to open Idle app."
      else
        @message = "You will now be redirected back to the Idle app for your request reset password."
      end
      @is_registered = true
    else
      @is_registered = false
    end
  end

  def stripe

  end

  def home

  end

  def privacy

  end

  def contact

  end

  def feedback
    PagesMailer.feedback_email(params[:email], params[:name], params[:message]).deliver
    flash[:notice] = "Successfully sent your message to Idle Admin!"

    redirect_to :back
  end
end
