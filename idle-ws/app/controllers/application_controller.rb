class ApplicationController < ActionController::Base
  protect_from_forgery with: :null_session, if: Proc.new { |c| c.request.format == 'application/json' }
  before_filter :force_www!

  def user_authorized
    @user = User.find_by(token: params[:token])
    if @user.present?
      return @user
    else
      return render json: { error: "You are not authorized to perform this action!", status: 300 }, status: 200
    end
  end

  def authenticate_admin
    if !admin_signed_in?
      redirect_to new_admin_session_path
    end
  end

  protected
    def force_www!
      if request.host[0..3] != "www." && Rails.env != "development" && request.host[0..3] != "172"
        redirect_to "#{request.protocol}www.#{request.host_with_port}#{request.fullpath}", :status => 301
      end
    end

    def after_sign_in_path_for(resource)
      admindir_v1_dashboard_path
    end
end
