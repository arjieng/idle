class Admin < ActiveRecord::Base
  devise :database_authenticatable, :registerable, :recoverable, :rememberable, :trackable, :validatable, :lockable, :timeoutable
  mount_uploader :image, AdminUploader
  
  before_create :set_name
  
  private
  
    def set_name
      self.name = self.first_name + " " + self.last_name
    end
end
