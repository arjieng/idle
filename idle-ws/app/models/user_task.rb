class UserTask < ActiveRecord::Base
  belongs_to :user
  belongs_to :performer, foreign_key: "user_id", class_name: "User"
  belongs_to :task
  belongs_to :progress, foreign_key: "task_id", class_name: "Task"
end
