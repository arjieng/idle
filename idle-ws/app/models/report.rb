class Report < ActiveRecord::Base
  belongs_to :user
  belongs_to :block, :class_name => "User"
  belongs_to :task
end
