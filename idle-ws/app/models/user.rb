class User < ActiveRecord::Base
  require 'twilio-ruby'
  require 'net/http'

  include ActionView::Helpers::NumberHelper

  # Include default devise modules. Others available are:
  devise :database_authenticatable, :registerable, :recoverable, :rememberable, :trackable, :validatable

  has_many :devices
  has_many :tasks
  has_many :bids
  has_many :user_tasks
  has_many :progress, through: :user_tasks
  has_many :ratings
  has_many :ignores
  has_many :reports
  has_many :blocks, through: :reports
  has_many :charges
  has_many :badges

  after_create :generate_token
  after_create :generate_confirmation
  after_create :confirmation_sms

  mount_uploader :image, UserUploader
  acts_as_messageable

  def is_confirmed?
    self.confirmed_at.nil? ? 0 : 1
  end

  def is_online?
    self.is_online == true ? 1 : 0
  end

  def has_stripe?
    self.stripe_account_id.nil? ? 0 : 1
  end

  def rate
    rate = self.ratings.map{ |ratings| ratings.rate }.average
    return rate.nan? ? 0 : rate.round
  end

  def is_owner?(id_param)
    self.id == id_param ? 1 : 0
  end

  def is_performing?
    self.is_performer? ? 1 : 0
  end

  def conversation(user)
    conversation = self.mailbox.conversations_with(user).first

    return conversation.present? ? conversation.id : 0
  end

  def in_progress(user_type)
    if user_type == 0
      tasks = self.tasks.includes(performer: :ratings).where("status NOT IN (0, 6)").order("created_at desc")
    else
      tasks = self.progress.includes(:ratings, user: :ratings).where("status NOT IN (0, 6)").order("user_tasks.created_at desc")
    end

    @progress = []
    tasks.each do |task|
      user = user_type == 0 ? task.performer : task.user
      user_task = { id: user.id, name: user.name, about: user.description, image: user.image.url, school: user.school, ratings: user.rate, is_online: user.is_online?, conversation_id: conversation(user), latitude: user.latitude, longitude: user.longitude }

      progress = { id: task.id, title: task.title.titleize, description: task.description, task_type: task.task_type, status: task.status, amount: number_with_precision(task.price, precision: 2), payment: task.payment, bid: number_with_precision(task.amount, precision: 2), latitude: task.latitude, longitude: task.longitude }

      if user_type == 1
        progress.merge!(has_rate: task.has_rate?(user.id))
      end

      progress.merge!(user: user_task)

      @progress.push(progress)
    end

    return @progress
  end

  def filtered_tasks
    tasks = []
    tasks = tasks.inject(self.ignores.pluck(:task_id), :<<)
    # tasks = tasks.inject(self.bids.where("is_active = true and is_disabled = false").pluck(:task_id), :<<)

    return tasks.present? ? tasks : 0
  end

  def stripe_url
    first_name = self.name.split(" ").first if !self.name.nil?
    last_name  = self.name.split(" ").last if !self.name.nil?

    return "https://connect.stripe.com/oauth/authorize?response_type=code&client_id=#{APP_CONFIG['stripe_client_id']}&scope=read_write&stripe_user[first_name]=#{first_name}&stripe_user[last_name]=#{last_name}"
  end

  def resend_confirmation_code
    confirmation_sms
  end

  def self.reset_token_expiration
    User.where("reset_token IS NOT NULL and reset_token < ?", DateTime.now.utc.beginning_of_day).each do |user|
      user.update_attributes(reset_token: nil, reset_token_at: nil)
      puts "User ID: #{user.id} - #{user.email} reset token is now invalid!"
    end
  end

  def stripe_connect(code)
    uri = URI.parse("https://connect.stripe.com/oauth/token")

    response = Net::HTTP.post_form(uri, { client_secret: APP_CONFIG['stripe_secret_key'], code: code, grant_type: "authorization_code" })

    json = JSON.parse response.body.to_s
    if json['stripe_user_id'].present?
      self.stripe_account_id = json['stripe_user_id']
      self.save

      return true, "Successfully connect stripe account"
    else
      return false, "Failed to connect stripe account"
    end
  end

  def stripe_disconnect
    account = Stripe::Account.retrieve(self.stripe_account_id)
    account.deauthorize(APP_CONFIG['stripe_client_id'])

    self.stripe_account_id = nil
    self.save

    return true, "Successfully removed account"

  rescue Stripe::StripeError => e
    logger.error "Stripe Error => " + e.message

    return false, "Failed to delete account"
  end

  private
    def generate_token
      secret = Digest::SHA1.hexdigest(SecureRandom.urlsafe_base64)
      self.token = User.find_by_token(secret.to_s) ? generate_token : secret
      self.save
    end

    def generate_confirmation
      code = SecureRandom.hex(2)
      self.confirmation_code = User.find_by_confirmation_code(code.upcase) ? generate_confirmation : code.upcase
      self.save
    end

    def confirmation_sms
      @client = Twilio::REST::Client.new APP_CONFIG['twilio_acc_sid'], APP_CONFIG['twilio_auth_token']

      if Rails.env != "development"
        self.mobile = self.mobile.include?("+1") == false ? ('+1' + self.mobile) : self.mobile
        self.save
      end

      phone_number = self.mobile

      @client.api.account.messages.create(
        body: 'Idle confirmation code: ' + self.confirmation_code,
        to: phone_number,
        from: APP_CONFIG['twilio_mobile_from']
      )
    rescue Twilio::REST::RestError => e
      logger.error "SMS error: " + e.message
    end
end

class Array
  def sum
    inject( nil ) { |sum,x| sum ? sum+x : x }
  end

  def average
    sum.to_f / size.to_f
  end
end
