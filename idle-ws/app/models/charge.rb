class Charge < ActiveRecord::Base
  belongs_to :user
  belongs_to :task

  def payment(stripe_account_id, token, description)
    total_amount = self.amount + fee
    payment = Stripe::Charge.create({ amount: to_cents(total_amount), currency: "usd", source: token, description: description, destination: { amount: to_cents(self.amount), account: stripe_account_id }} )

    self.charge_id = payment.id
    self.stripe_account_id = stripe_account_id
    self.last_4 = payment.source.last4
    self.amount = fee

    return true, "Success"
  rescue Stripe::StripeError => e
    logger.error "Stripe Error: " + e.message
    return false, e.message
  end

  private
    def to_cents(money)
      (money * 100).round
    end

    def fee
      (((self.amount.to_f + 0.3) / ((100.to_f - 7.9) / 100.to_f)) - self.amount.to_f).round(2)
    end
end
