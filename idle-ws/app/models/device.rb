class Device < ActiveRecord::Base
  belongs_to :user

  def self.check_token(device_params, user_id)
    device = self.find_by(token: device_params[:token], platform: device_params[:platform])

    if device.present?
      device.update_attribute(:user_id, user_id)
    else
      self.create(token: device_params[:token], platform: device_params[:platform], user_id: user_id)
    end
  end

  private
    def deactivate_tokens
      devices = Device.where("is_active = true and updated_at < ?", 10.days.ago)
      devices.update_all(is_active: false)
    end
end
