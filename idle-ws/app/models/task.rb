class Task < ActiveRecord::Base
  belongs_to :user
  has_many :bids
  has_one :user_task
  has_one :performer, through: :user_task
  has_many :ratings
  has_many :ignores
  has_many :reports
  has_one :charge

  geocoded_by latitude: :lat, longitude: :lon

  scope :filtered, -> (user_id, page, filtered_tasks, radius, geolocation) {
      if page == 0
        includes(:bids, user: :ratings).where("status = 0 and is_active = true and user_id != ? and id NOT IN (?) and is_disabled = false", user_id, filtered_tasks)
      elsif page == 1
        includes(:bids, user: :ratings).where("status = 0 and is_active = true and user_id != ? and id NOT IN (?) and is_disabled = false", user_id, filtered_tasks).near([geolocation[:latitude], geolocation[:longitude]], radius, order: 'distance')
      end
  }

  def owner
    self.user.name
  end

  def payment
    self.is_cash? ? "Cash" : "Stripe"
  end

  def finished
    self.finished_at.strftime("%^b %d, %Y")
  end

  def has_rate?(user_id)
    self.ratings.select { |rating| rating.user_id == user_id }.present? ? 1 : 0
  end
end
