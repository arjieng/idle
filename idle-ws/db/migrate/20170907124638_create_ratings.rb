class CreateRatings < ActiveRecord::Migration
  def change
    create_table :ratings do |t|
      t.belongs_to :user
      t.belongs_to :task
      t.integer :rate, default: 1
      t.timestamps null: false
    end
  end
end
