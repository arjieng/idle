class CreateDevices < ActiveRecord::Migration
  def change
    create_table :devices do |t|
      t.string :token
      t.integer :platform
      t.belongs_to :user
      t.boolean :is_active, default: true
      t.boolean :is_disabled, default: false
      t.timestamps null: false
    end
  end
end
