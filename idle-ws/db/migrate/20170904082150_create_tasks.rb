class CreateTasks < ActiveRecord::Migration
  def change
    create_table :tasks do |t|
      t.belongs_to :user
      t.string :title
      t.text :description
      t.float :price, default: 0.0
      t.float :latitude, default: 0.0
      t.float :longitude, default: 0.0
      t.integer :task_type, default: 0
      t.float :amount, default: 0.0
      t.boolean :is_cash, default: true
      t.timestamps null: false
    end
  end
end
