class AddStripAccountIdInUser < ActiveRecord::Migration
  def up
    add_column :users, :stripe_account_id, :string
  end

  def down
    remove_column :users, :stripe_account_id
  end
end
