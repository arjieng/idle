class AddIsPerformerInUser < ActiveRecord::Migration
  def up
    add_column :users, :is_performer, :boolean, default: false
  end

  def down
    add_column :users, :is_performer
  end
end
