class CreateBids < ActiveRecord::Migration
  def change
    create_table :bids do |t|
      t.belongs_to :user
      t.belongs_to :task
      t.float :amount, default: 0.0
      t.boolean :is_active, default: true
      t.timestamps null: false
    end
  end
end
