class AddFinishedAtInTask < ActiveRecord::Migration
  def up
    add_column :tasks, :finished_at, :timestamp
  end

  def down
    remove_column :tasks, :finished_at
  end
end
