class RemoveBidIdInIgnore < ActiveRecord::Migration
  def up
    remove_column :ignores, :bid_id
  end

  def down
    add_column :ignores, :bid_id, :integer
  end
end
