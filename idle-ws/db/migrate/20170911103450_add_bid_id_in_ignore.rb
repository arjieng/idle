class AddBidIdInIgnore < ActiveRecord::Migration
  def up
    add_column :ignores, :bid_id, :integer
  end

  def column
    remove_column :ignores, :bid_id
  end
end
