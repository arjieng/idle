class AddIsActiveInTask < ActiveRecord::Migration
  def up
    add_column :tasks, :is_active, :boolean, default: true
  end

  def down
    remove_column :tasks, :is_active
  end
end
