class AddColumnsInUser < ActiveRecord::Migration
  def up
    add_column :users, :name, :string
    add_column :users, :image, :string
    add_column :users, :age, :integer
    add_column :users, :mobile, :string
    add_column :users, :token, :string
    add_column :users, :provider, :string
    add_column :users, :provider_id, :string
    add_column :users, :is_online, :boolean, default: true
    add_column :users, :latitude, :float, default: 0.0
    add_column :users, :longitude, :float, default: 0.0
    add_column :users, :confirmation_code, :string
    add_column :users, :is_active, :boolean, default: true
    add_column :users, :is_disabled, :boolean, default: false
    add_column :users, :description, :text
  end

  def down
    remove_column :users, :name
    remove_column :users, :image
    remove_column :users, :age
    remove_column :users, :mobile
    remove_column :users, :token
    remove_column :users, :provider
    remove_column :users, :provider_id
    remove_column :users, :is_online
    remove_column :users, :latitude
    remove_column :users, :longitude
    remove_column :users, :confirmation_code
    remove_column :users, :is_active
    remove_column :users, :is_disabled
    remove_column :users, :description
  end
end
