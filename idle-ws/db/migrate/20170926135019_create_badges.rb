class CreateBadges < ActiveRecord::Migration
  def change
    create_table :badges do |t|
      t.belongs_to :user
      t.string :notification
      t.boolean :is_read, default: false
      t.timestamps null: false
    end
  end
end
