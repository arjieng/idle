class AddIsDisabledInBid < ActiveRecord::Migration
  def up
    add_column :bids, :is_disabled, :boolean, default: false
  end

  def down
    remove_column :bids, :is_disabled
  end
end
