class AddFinishedAtInUser < ActiveRecord::Migration
  def up
    add_column :users, :finished_at, :timestamp
  end

  def down
    remove_column :users, :finished_at
  end
end
