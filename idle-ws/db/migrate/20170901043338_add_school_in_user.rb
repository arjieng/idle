class AddSchoolInUser < ActiveRecord::Migration
  def up
    add_column :users, :school, :string
  end

  def down
    remove_column :users, :school
  end
end
