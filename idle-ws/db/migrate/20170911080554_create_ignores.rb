class CreateIgnores < ActiveRecord::Migration
  def change
    create_table :ignores do |t|
      t.belongs_to :user
      t.belongs_to :task
      t.timestamps null: false
    end
  end
end
