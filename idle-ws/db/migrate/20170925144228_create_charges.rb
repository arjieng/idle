class CreateCharges < ActiveRecord::Migration
  def change
    create_table :charges do |t|
      t.belongs_to :user
      t.belongs_to :task
      t.float :amount, defaut: 0.0
      t.string :charge_id
      t.string :last_4
      t.string :stripe_account_id
      t.timestamps null: false
    end
  end
end
