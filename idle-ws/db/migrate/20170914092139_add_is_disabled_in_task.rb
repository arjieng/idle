class AddIsDisabledInTask < ActiveRecord::Migration
  def up
    add_column :tasks, :is_disabled, :boolean, default: false
  end

  def down
    remove_column :tasks, :is_disabled
  end
end
