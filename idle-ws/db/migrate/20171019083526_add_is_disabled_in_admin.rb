class AddIsDisabledInAdmin < ActiveRecord::Migration
  def up
    add_column :admins, :is_disabled, :boolean, default: false
  end

  def down
    remove_column :admins, :is_disabled
  end
end
