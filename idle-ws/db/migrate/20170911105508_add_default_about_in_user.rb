class AddDefaultAboutInUser < ActiveRecord::Migration
  def up
    change_column :users, :description, :text, default: ""
  end

  def down
    change_column :users, :description, :text, default: nil
  end
end
