class AddIsCompletedInUserTask < ActiveRecord::Migration
  def up
    add_column :user_tasks, :is_completed, :boolean, default: false
  end

  def down
    remove_column :user_tasks, :is_completed
  end
end
