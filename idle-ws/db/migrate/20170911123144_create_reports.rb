class CreateReports < ActiveRecord::Migration
  def change
    create_table :reports do |t|
      t.belongs_to :user
      t.belongs_to :task
      t.integer :block_id
      t.text :description
      t.timestamps null: false
    end
  end
end
