class AddStatusInTask < ActiveRecord::Migration
  def up
    add_column :tasks, :status, :integer, default: 0
  end

  def down
    add_column :tasks, :status
  end
end
