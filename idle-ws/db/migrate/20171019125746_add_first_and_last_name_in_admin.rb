class AddFirstAndLastNameInAdmin < ActiveRecord::Migration
  def up
    add_column :admins, :first_name, :string
    add_column :admins, :last_name, :string
    add_column :admins, :role_id, :integer
    add_column :admins, :about, :text
    add_column :admins, :username, :string
  end

  def down
    remove_column :admins, :first_name
    remove_column :admins, :last_name
    remove_column :admins, :role_id
    remove_column :admins, :about
    remove_column :admins, :username
  end
end
