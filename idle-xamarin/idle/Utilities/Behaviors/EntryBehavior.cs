﻿using System;
using System.Diagnostics;
using Xamarin.Forms;

namespace idle
{
    public class EntryBehavior : Behavior<Entry>
    {
        protected override void OnAttachedTo(Entry bindable)
        {
            base.OnAttachedTo(bindable);

            bindable.Focused += Bindable_Focused;
            bindable.TextChanged += Bindable_TextChanged;
        }

        protected override void OnDetachingFrom(Entry bindable)
        {
            base.OnDetachingFrom(bindable);

            bindable.Focused -= Bindable_Focused;
            bindable.TextChanged -= Bindable_TextChanged;
        }

		protected void Bindable_Focused(object sender, FocusEventArgs e)
		{
			Entry entryField = (Entry)sender;
            //entryField.Focus();
			entryField.Placeholder = String.Empty;
            entryField.FontSize = 12.scale();
		}

        void Bindable_TextChanged(object sender, TextChangedEventArgs e)
        {
    //        Entry entryField = (Entry)sender;
    //        double fontSize = entryField.FontSize;
    //        if(e.NewTextValue.Length == 0){
    //            entryField.FontSize = 12.scale();
    //        }

    //        if(e.NewTextValue.Length >= 36 && e.NewTextValue.Length < 39){
    //            if(e.NewTextValue.Length > e.OldTextValue.Length){
				//	entryField.FontSize = (fontSize - 1);
            //            }
    //            else{
    //                entryField.FontSize = fontSize + 1;
				//}
     //       }
     //       else if (e.NewTextValue.Length == 35){
     //           if(e.NewTextValue != null && e.OldTextValue != null){
					//if (e.NewTextValue.Length < e.OldTextValue.Length)
					//{
					//	entryField.FontSize = fontSize + 1;
					//}
            //    }
            //}
        }
    }
}
