﻿using System;
using System.Diagnostics;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace idle
{
    public class EditorBehavior : Behavior<CustomEditor>
    {
        double layoutHeight;
        double newLayoutHeight = -1;
        bool isloaded = false;
        CustomEditor customEditor;
        protected override void OnAttachedTo(CustomEditor bindable)
        {
            base.OnAttachedTo(bindable);
            customEditor = bindable;
            bindable.Focused += Bindable_Focused;
            bindable.Unfocused += Bindable_Unfocused;
            bindable.TextChanged += Bindable_TextChanged;
            bindable.PropertyChanged += Bindable_PropertyChanged;
            Debug.WriteLine("Attach Editor");
        }

        protected override void OnDetachingFrom(CustomEditor bindable)
        {
            base.OnDetachingFrom(bindable);

            bindable.Focused -= Bindable_Focused;
            bindable.TextChanged -= Bindable_TextChanged;
            bindable.Unfocused -= Bindable_Unfocused;
            Debug.WriteLine("Deattach Editor");
        }

        void Bindable_PropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
        {
           
            if (customEditor.IsFocused == true)
            {
                newLayoutHeight = customEditor.Height;
            }
            else
            {
                layoutHeight = customEditor.MinimumHeightRequest;
            }
        }

        protected void Bindable_Focused(object sender, FocusEventArgs e)
		{
           var editor = ((CustomEditor)sender);
			Debug.WriteLine("Editor_F {0}", customEditor.MinimumHeightRequest);

           // await Task.Run(() =>
           //{
           //    Animation anims = new Animation((obj) => editor.HeightRequest = obj, 35.scale(), newLayoutHeight, Easing.CubicInOut);
           //    editor.Animate("editorAnimate2", anims, 16, 500, Easing.CubicInOut, null, null);
           //});
            //editor.AbortAnimation("editorAnimate2");
            editor.HeightRequest = -1;
		}

        void Bindable_Unfocused(object sender, FocusEventArgs e)
        {
            var editor = ((CustomEditor)sender);
            Animation anims = new Animation((obj) => editor.HeightRequest = obj, editor.Height, Constants.MESSAGE_EDITOR_HEIGHT, Easing.CubicInOut);
            editor.Animate("editorAnimate", anims, 16, 500, Easing.CubicInOut, null, null);
        }

        void Bindable_TextChanged(object sender, TextChangedEventArgs e)
        {
            //newLayoutHeight = ((CustomEditor)sender).Height;
            if (isloaded == false)
            {
                layoutHeight = ((CustomEditor)sender).Height;
                isloaded = true;
            }
        }
    }
}
