﻿﻿using System;
using System.Collections.ObjectModel;
using System.Diagnostics;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Xamarin.Forms;

namespace idle
{
    public class DataClass
    {
        public static DataClass dataClass;

        public double latitude = 0.0, longitude = 0.0;
        public bool isLogin = false, hasLocation = false;
        public string stripeLink, email, bid;
        public User BidUser { get; set; }

        public ObservableCollection<Tasks> TaskList { get; set; }
        public ObservableCollection<Tasks> TaskOffersList { get; set; }
		public ObservableCollection<Bid> BidList { get; set; }
        public ObservableCollection<Tasks> InProgressPosterList { get; set; }
        public ObservableCollection<Tasks> InProgressPerformList { get; set; }
		public ObservableCollection<Conversation> MyConversations { get; set; }
        public int Radius { get; set; }
        public Tasks PinToRemove { get; set; }
        public int PinTypeToRemove { get; set; }

        public string stripeToken { get; set; }

        //user login model
        public int UserID { get { if (Application.Current.Properties.ContainsKey("user_token")) { return int.Parse(Application.Current.Properties["user_id"].ToString()); } else { return 0; } } }
        public string UserEmail { get { if (Application.Current.Properties.ContainsKey("user_token")) { return Application.Current.Properties["user_email"].ToString(); } else { return String.Empty; } } }
        public string UserImage {
            get { if (Application.Current.Properties.ContainsKey("user_token")) {
#if DEBUG
                    return "https://storage.googleapis.com/theidleapp/development/images/users/10/8fc25b55a16f4430a597.png";
#else
                    return Application.Current.Properties["user_image"].ToString();
#endif
				}
				else
				{
					return String.Empty;
				}
			}
		}

        public int UserIsOnline { get { if (Application.Current.Properties.ContainsKey("user_is_online")) { return int.Parse(Application.Current.Properties["user_is_online"].ToString()); } else { return 1; } } set { } }
        public int UserHasStripe { get { if (Application.Current.Properties.ContainsKey("user_has_stripe")) { return int.Parse(Application.Current.Properties["user_has_stripe"].ToString()); } else { return 0; } } }
        public int UserIsConfirmed { get { if (Application.Current.Properties.ContainsKey("user_is_confirmed")) { return int.Parse(Application.Current.Properties["user_is_confirmed"].ToString()); } else { return 0; } } }
        public int UserIsPerfomer { get { if (Application.Current.Properties.ContainsKey("user_is_performer")) { return int.Parse(Application.Current.Properties["user_is_performer"].ToString()); } else { return 1; } } set { Application.Current.Properties["user_is_performer"] = value.ToString();  Application.Current.SavePropertiesAsync(); } }
        public string UserToken {  get { if (Application.Current.Properties.ContainsKey("user_token")) { return Application.Current.Properties["user_token"].ToString();  }  else { return String.Empty; } } }

        public UriImageSource UserImageURL { get; set; }
		public static DataClass GetInstance
		{
			get
            {
				if (dataClass == null)
				{
					dataClass = new DataClass();
				}

				return dataClass;
			}
		}

        public DataClass()
        {
            
        }

        public void Update(bool IsAppStart){
            if(Application.Current.Properties.ContainsKey("user_token")){
                if (IsAppStart == false)
                {
                    Application.Current.Properties["user_perform_progress"] = JsonConvert.SerializeObject(DataClass.GetInstance.InProgressPerformList);
                    Application.Current.Properties["user_post_progress"] = JsonConvert.SerializeObject(DataClass.GetInstance.InProgressPosterList);
                    Application.Current.SavePropertiesAsync();
                }
                else
                {
                    InProgressPerformList.Clear();
                    var tasks = JsonConvert.DeserializeObject<ObservableCollection<Tasks>>(Application.Current.Properties["user_perform_progress"].ToString());
                    if(tasks != null){
						foreach (var task in tasks)
						{
							InProgressPerformList.Add(task);
						}
                    }

                    InProgressPosterList.Clear();
					tasks = JsonConvert.DeserializeObject<ObservableCollection<Tasks>>(Application.Current.Properties["user_post_progress"].ToString());
                    if(tasks != null){
						foreach (var task in tasks)
						{
							InProgressPosterList.Add(task);
						}
                    }
                }
            }
        }
    }
}
