﻿﻿﻿using System;
using Xamarin.Forms;

namespace idle
{
    public class Constants
    {
        /* TASK TYPE LEGEND
         * 0 - Quickie
         * 1 - Grocery
         * 2 - Laundry
         * 3 - Transport
         * 4 - Other
        */

        //share idle
        public static readonly string SHARE_IDLE_TITLE = "The Idle App";
        public static readonly string SHARE_IDLE_BODY = "Your Friendly Neighborhood Task Platform.";
        public static readonly string SHARE_IDLE_LINKS = "We can't wait for you to join our community on Idle! To get started, click on the link below:\n\niOS: https://itunes.apple.com/ph/app/the-idle-app/id1167283648?mt=8\nAndroid: https://play.google.com/store/apps/details?id=com.idle.idleapp&hl=en";

        // PINS ICONS
        public static readonly String[] pinTypes = { "QuickiePin.png", "GroceryPin.png", "LaundryPin.png", "TransportPin.png", "OtherPin.png" };
        public static readonly String[] pin1Point5X = { "QuickiePin@1.5x.png", "GroceryPin@1.5x.png", "LaundryPin@1.5x.png", "TransportPin@1.5x.png", "OtherPin@1.5x.png" };
        public static readonly String[] pinTwoX = { "QuickiePin@2x.png", "GroceryPin@2x.png", "LaundryPin@2x.png", "TransportPin@2x.png", "OtherPin@2x.png" };
        public static readonly String[] pinThreeX = { "QuickiePin@3x.png", "GroceryPin@3x.png", "LaundryPin@3x.png", "TransportPin@3x.png", "OtherPin@3x.png" };

        public static string PinIcon(int taskType)
        {
            if (Device.RuntimePlatform == Device.iOS)
            {
                return pinTypes[taskType];
            }
            else
            {
                if (App.DeviceScale <= 1.0f)
                {
                  return pinTypes[taskType];
                }
                else if(App.DeviceScale > 1.0f && App.DeviceScale <= 1.5f )
                {
                  return pin1Point5X[taskType];
                }
                else if (App.DeviceScale > 1.5f && App.DeviceScale <= 2.0f)
                {
                  return pinTwoX[taskType];
                }
                else
                {
                  return pinThreeX[taskType];
                }
            }
        }

        // TASKS COLORS
        public static readonly Color QuickieColor = Color.FromHex("#FCC13F");
        public static readonly Color GroceryColor = Color.FromHex("#CA7271");
        public static readonly Color LaundryColor = Color.FromHex("#A483AE");
        public static readonly Color TransportColor = Color.FromHex("#7CB342");
        public static readonly Color OtherColor = Color.FromHex("#6090BE");

        // BACKGROUND COLOR
        public static readonly Color THEME_COLOR_GREEN = Color.FromHex("#4DB6AC");
        public static readonly Color THEME_COLOR_WHITE = Color.FromHex("#FFFFFF");
        public static readonly Color BACKGROUND_PRICE = Color.FromHex("#F4E3E3");
        public static readonly Color COMPLETED_TASK = Color.FromHex("#4DB6AC");
        public static readonly Color BACKGROUND_RATINGS = Color.FromHex("#F9CE1D");
        public static readonly Color BACKGROUND_ENTRY = Color.FromHex("#71C5BD");
        public static readonly Color BACKGROUND_VIEWTASK = Color.FromHex("#f8f8f9");
        public static readonly Color PINK_COLOR = Color.FromHex("#f4e3e3");
        public static readonly Color THEME_COLOR_LIGHT_GREEN = Color.FromHex("#a2d7d2");
        public static readonly Color MESSAGE_SEPARATOR_GRAY = Color.FromHex("#eaeaea");

        // BUTTONS COLORS
        public static readonly Color BUTTON_GRAY = Color.FromHex("#BBBBBB"); //173 App logo
        public static readonly Color BUTTON_GREEN = Color.FromHex("#4DB6AC");
        public static readonly Color BUTTON_WHITE = Color.FromHex("#FFFFFF");
        public static readonly Color BUTTON_FACEBOOK = Color.FromHex("#71C5BD");
        public static readonly Color FACEBOOK_COLOR = Color.FromHex("#02297c");
        public static readonly Color GRAY_BUTTON = Color.FromHex("#F2F2F2");

        // FONT COLORS
        public static readonly Color DARK_STRONG = Color.FromHex("#111111");
        public static readonly Color DARK_MEDIUM = Color.FromHex("#1D1D26");
        public static readonly Color DARK_WEAK = Color.FromHex("#666666");
        public static readonly Color LIGHT_GRAY = Color.FromHex("#f5f5f5"); // use in entry placholder
        public static readonly Color DARK = Color.FromHex("#A8A8AA");
        public static readonly Color LIGHT = Color.FromHex("#FFFFFF");
        public static readonly Color DARK_GREEN = Color.FromHex("#006954");
        public static readonly Color LIGHT_GREEN = Color.FromHex("#b2e0db");
        public static readonly Color DARK_GRAY = Color.FromHex("#5d5d5d");
		public static readonly Color LINK_COLOR = Color.FromHex("#3366bb");


        // FONTS
        public static readonly string LULO = Device.RuntimePlatform == Device.iOS ? "LuloCleanOne" : "Fonts/LuloCleanOne.otf#LuloCleanOne";
        public static readonly string LULO_BOLD = Device.RuntimePlatform == Device.iOS ? "LuloCleanOne-Bold" : "Fonts/LuloCleanOneBold.otf#LuloCleanOneBold";
        public static readonly string PROXIMA = Device.RuntimePlatform == Device.iOS ? "ProximaNova-Black" : "Fonts/ProximaNova-Black.otf#ProximaNova-Black";
        public static readonly string PROXIMA_BOLD = Device.RuntimePlatform == Device.iOS ? "ProximaNova-Bold" : "Fonts/ProximaNova-Bold.otf#ProximaNova-Bold";
        public static readonly string PROXIMA_REG = Device.RuntimePlatform == Device.iOS ? "ProximaNova-Regular" : "Fonts/ProximaNova-Regular.otf#ProximaNova-Regular";

        // NAVIGATION CONTROL TEMPLATE
        public static readonly double NAVIGATION_HEIGHT = Device.RuntimePlatform == Device.iOS ? 64.scale() : 40.scale();
        public static readonly double SIGNUP_LOGO_HEIGHT = ChangeValueTo(99, 97).scale();

        // MAP CONSTANTS
        public static readonly Thickness MAP_MARGIN = Device.RuntimePlatform == Device.iOS ? new Thickness(0, -64, 0, 0).scale() : new Thickness(0, -40, 0, 0).scale();
        public static readonly Thickness MAP_GRADIENT_MARGIN = Device.RuntimePlatform == Device.iOS ? new Thickness(0, -82, 0, 0).scale() : new Thickness(0, -52, 0, 0).scale();
        public static readonly double MAP_INPROGRESS_HEIGHT = ChangeValueTo(224.0, 215.0).scale();
        public static readonly double LIST_INPROGRESS_HEIGHT = ChangeValueTo(195.0, 186.0).scale();
        public static readonly double INPROGRESS_ONE_HEIGHT = ChangeValueTo(65.0, 62.0).scale();
        public static readonly double INPROGRESS_TWO_HEIGHT = ChangeValueTo(130.0, 124.0).scale();
        public static readonly int INPROGRESS_ROW_HEIGHT = ChangeValueTo(65, 62).scale();
        public static readonly double INPROGRESS_GRID_HEIGHT = ChangeValueTo(57.0, 54.0).scale();

        //FONT SIZES
        public static readonly double ENTRY_FONTSIZE = 12.scale(); // proxima reg
        public static readonly double ENTRY_PLACEHOLDER_FONTSIZE = 11.scale(); // proxima reg
        public static readonly double BUTTON_FONTSIZE = 13.scale(); // proxima reg
		public static readonly double TITLE_FONTSIZE = 16.scale(); // proxima reg

		//SignIn Page
		public static readonly double APPLOGO_SIZE = ChangeValueTo(168, 167).scale();
        public static readonly double SMALL_FONTSIZE = 7.scale(); // proxima reg
        public static readonly double TASK_TITLE_FONTSIZE = 13.scale();

        //SignIn Page
        public static readonly double BUTTON_HEIGHT = ChangeValueTo(51, 50).scale();

        public static readonly Thickness APPLOGO_MARGIN = ChangeValueTo(new Thickness(0, 58.scale(), 0, 22.scale()), new Thickness(0, 54.scale(), 0, 22.scale()));
        public static readonly Thickness FACEBOOK_BUTTON_MARGIN = ChangeValueTo(new Thickness(0, 20.scale(), 0, 8.5.scale()), new Thickness(0, 15.scale(), 0, 11.scale()));
        public static readonly Thickness SIGNUP_BUTTON_MARGIN = ChangeValueTo(new Thickness(0, 0, 0, 0), new Thickness(-20, 0, 0, 0)).scale();
        public static readonly Thickness SIGNIN_LABEL_MARGIN = ChangeValueTo(new Thickness(0, 5, 0, 0), new Thickness(0, 5, 0, 0)).scale();
        public static readonly Thickness CONTINUE_BUTTON_MARGIN = ChangeValueTo(new Thickness(0, -2, 0, 0), new Thickness(0, -4, 0, 0)).scale();
        public static readonly Thickness SIGNIN_ROOT_BUTTON_MARGIN = ChangeValueTo(new Thickness(0, 0, 0, 0), new Thickness(0, -25, 0, 0)).scale();
        public static readonly Thickness SIGNINPAGE_ENTRY_MARGIN = ChangeValueTo(new Thickness(21, -4.5, 10, 4), new Thickness(21, -4.5, 10, 5)).scale();

        //SignUp Page
        public static readonly double SIGNUP_ROWHEIGHT = ChangeValueTo(50, 50).scale();
        public static readonly double SIGNUP_ROWHEIGHT_TWO = ChangeValueTo(51, 51).scale();
        public static readonly double SIGNUP_ENTRY_ROWHEIGHT = ChangeValueTo(49, 47).scale();
        public static readonly double SIGNUP_ENTRY_ROW_HEIGHT = ChangeValueTo(50, 49).scale();

        public static readonly Thickness SIGNUP_ENTRY_MARGIN = ChangeValueTo(new Thickness(21, -3, 0, 0), new Thickness(21, -10, 0, 0)).scale();
        public static readonly Thickness SIGNUP_ROW_ENTRY_MARGIN = ChangeValueTo(new Thickness(21, -8, 15, -4), new Thickness(21, -15, 15, -2)).scale();
        public static readonly Thickness SIGNUP_AGE_ENTRY_MARGIN = ChangeValueTo(new Thickness(0, -5, 15, 0), new Thickness(0, -10, 15, 0)).scale();

        public static readonly Thickness SIGNUP_LABEL_MARGIN = ChangeValueTo(new Thickness(21, 9, 0, 0), new Thickness(21, 9, 0, 0)).scale();
        public static readonly Thickness SIGNUP_CLOSE_BUTTON_MARGIN = ChangeValueTo(new Thickness(0, 18, 0, 0), new Thickness(0, 0, 0, 0)).scale();

        //Homepage
        public static readonly double TASK_IMAGE_SIZE = ChangeValueTo(69, 69).scale();
        public static readonly double EMPTY_ROW_HEIGHT = ChangeValueTo(75, 68).scale();
        public static readonly double USER_IMAGE_SIZE = 106.scale();
        public static readonly double TASK_ROW_HEIGHT = ChangeValueTo(82, 77).scale();
        public static readonly double BUTTON_RADIUS = ChangeValueTo(3, 3).scale();
        public static readonly double USER_IMAGE_RADIUS = ChangeValueTo(4, 4).scale();
        public static readonly double INPROGRESS_LABEL_SPACING = ChangeValueTo(2, 1).scale();

        public static readonly Thickness HOME_INPROGRESS_MARGIN = ChangeValueTo(new Thickness(0, 11, 0, 0), new Thickness(0, 12, 0, 0)).scale();
        public static readonly Thickness HOME_FRAME_MARGIN = ChangeValueTo(new Thickness(13, 2, 2, 0), new Thickness(13, 2, 2, 0)).scale();

        //InProgress
        public static readonly double PROGRESS_FRAME_SIZE = ChangeValueTo(9, 9).scale();
        public static readonly float PROGRESS_FRAME_RADIUS = (float)(PROGRESS_FRAME_SIZE / 2);

        // SWITCH
        public static readonly Color SWITCH_ON_BACKGROUND_COLOR = Color.FromHex("#b8e2de");
        public static readonly Color SWITCH_ON_COLOR = Color.FromHex("#4db6ac");
        public static readonly Color SWITCH_OFF_BACKGROUND_COLOR = Color.FromHex("#B1B1B1");
        public static readonly Color SWITCH_OFF_COLOR = Color.FromHex("#bbbbbb");

        //PROFILE PAGE
        public static readonly double LABEL_SPACING = Device.RuntimePlatform == Device.iOS ? 5.scale() : 1.4.scale();
        public static readonly double PROFILE_IMAGE_SIZE = ChangeValueTo(55, 55).scale();
        public static readonly double TASK_RATING_HEIGHT = Device.RuntimePlatform == Device.iOS ? 68.scale() : 67.scale();
        public static readonly Color PROFILE_LABEL_COLOR = Color.FromHex("#8e8e92");
        public static readonly Thickness LABEL_SPACING_MARGIN = ChangeValueTo(new Thickness(12, 4, 20, 0), new Thickness(12, 0, 20, 0)).scale();
        public static readonly Thickness TASK_RATING_MARGIN = ChangeValueTo(new Thickness(0, 15, 0, 0), new Thickness(0, 13, 0, 0)).scale();
        public static readonly Thickness COMPLETED_TASK_MARGIN = ChangeValueTo(new Thickness(0, 12, 0, 0), new Thickness(0, 10, 0, 0)).scale();
        public static readonly Thickness COMPLETED_DATA_MARGIN = ChangeValueTo(new Thickness(0, 6, 0, 0), new Thickness(0, 1, 0, 0)).scale();
        public static readonly Thickness RATING_DATA_MARGIN = ChangeValueTo(new Thickness(0, 14, 0, 0), new Thickness(0, 12, 0, 0)).scale();
        public static readonly Thickness FILTER_MARGIN = ChangeValueTo(new Thickness(0, 16, 0, 0), new Thickness(0, 14, 0, 0)).scale();
        public static readonly Thickness MESSAGE_MARGIN = ChangeValueTo(new Thickness(0, -8, 0, 0), new Thickness(0, -8, 0, 0)).scale();
        public static readonly int PROFILE_ROW_HEIGHT = ChangeValueTo(77, 74).scale();
        public static readonly Thickness STAR_DATE_MARGIN = ChangeValueTo(new Thickness(0, 9, 0, 0), new Thickness(0, 9, 0, 0)).scale();
        public static readonly Rectangle SWITCH_BACKGROUND = new Rectangle(13, 3, 34, 12).scale();
        public static readonly Rectangle SWITCH_BUTTON = new Rectangle(13, 0, 18, 18).scale();
        public static readonly Rectangle SWITCH_LABEL = new Rectangle(0, 26, 60, 6).scale();
        public static readonly double BACKGROUND_RADIUS = (SWITCH_BACKGROUND.Height / 2.0);
        public static readonly double SWITCHBUTTON_RADIUS = (SWITCH_BUTTON.Width / 2.0);


		//MYTASKPAGE SWITCH
		public static readonly Rectangle ROLE_SWITCH_BACKGROUND = new Rectangle(8, 3, 34, 12).scale();
		public static readonly Rectangle ROLE_SWITCH_BUTTON = new Rectangle(8, 0, 18, 18).scale();
		public static readonly double ROLE_BACKGROUND_RADIUS = (ROLE_SWITCH_BACKGROUND.Height / 2.0);
		public static readonly double ROLE_SWITCHBUTTON_RADIUS = (ROLE_SWITCH_BUTTON.Width / 2.0);
        public static readonly Rectangle ROLE_SWITCH_LABEL = new Rectangle(0, 20, 50, 6).scale();


        // Worker Pin
        public static readonly Rectangle PIN_IMAGE = new Rectangle(0, 0, 53, 64).scale();
        public static readonly Rectangle PIN_PROFILE = new Rectangle(9, 9, 35, 35).scale();

        //VIEW TASK AND TASK SUMMARY PAGE
        public static readonly Thickness BY_MARGIN = ChangeValueTo(new Thickness(0, 5, 0, 0), new Thickness(0, 4, 0, 0)).scale();
        public static readonly Thickness NAME_MARGIN = ChangeValueTo(new Thickness(0, 4, 0, 0), new Thickness(0, 2, 0, 0)).scale();
        public static readonly Thickness SCHOOL_MARGIN = ChangeValueTo(new Thickness(0, 3, 0, 0), new Thickness(0, 2, 0, 0)).scale();
        public static readonly Thickness TITLE_MARGIN = ChangeValueTo(new Thickness(10, 12, 20, 0), new Thickness(10, 11, 20, 0)).scale();
        public static readonly Thickness TITLE_MARGIN_TS = ChangeValueTo(new Thickness(10, 12, 5, 0), new Thickness(10, 11, 5, 0)).scale();
        public static readonly Double DESCRIPTION_SPACING = ChangeValueTo(12, 10).scale();
        public static readonly Double TASKTYPE_SPACING = ChangeValueTo(8, 6).scale();
        public static readonly Thickness PAYMENT_MARGIN = ChangeValueTo(new Thickness(18, 16, 0, 0), new Thickness(18, 14, 0, 0)).scale();
        public static readonly Thickness CENTER_MARGIN = ChangeValueTo(new Thickness(0, 79, 0, 0), new Thickness(0, 74, 0, 0)).scale();
        public static readonly Thickness RATE_MARGIN = ChangeValueTo(new Thickness(0, 79, 0, 0), new Thickness(0, 76, 0, 0)).scale();
        public static readonly Thickness RATE_MARGIN_SHOW = ChangeValueTo(new Thickness(0, 20, 0, 0), new Thickness(0, 17, 0, 0)).scale();
        public static readonly Double RATE_HEIGHT = ChangeValueTo(64, 63).scale();
        public static readonly Double RATE_SPACING = ChangeValueTo(10, 8).scale();
        public static readonly Thickness RATE_POSTER_SHOW = ChangeValueTo(new Thickness(0, 10, 0, 0), new Thickness(0, 7, 0, 0)).scale();
		public static readonly double RATE_POSTER_HEIGHT = ChangeValueTo(64, 84).scale();

        //Task UI
        public static readonly double TASK_USER_IMAGE_SIZE = ChangeValueTo(34, 34).scale();
        public static readonly double TASK_INFO_ROW_HEIGHT = ChangeValueTo(64.5, 62.2).scale();
        public static readonly int TASKPAGE_ROW_HEIGHT = ChangeValueTo(112, 109).scale();

        //New Task
        public static readonly double NEWTASK_IMAGE_SIZE = ChangeValueTo(47, 47).scale();
        public static readonly double NEWTASK_IMAGE_RADIUS = ChangeValueTo(2, 3).scale();
        public static readonly double NEWTASK_DESC_HIEIGHT = ChangeValueTo(127, 123).scale();
        public static readonly double NEWTASK_COST_HIEIGHT = ChangeValueTo(24, 23).scale();
        public static readonly double NEWTASK_BUTTON_HIEIGHT = ChangeValueTo(40, 39).scale();
        public static readonly double NEWTASK_TITLE_ROW_HIEIGHT = ChangeValueTo(43, 52).scale();

        public static readonly Thickness NEWTASK_LABEL_MARGIN = ChangeValueTo(new Thickness(21, 2, 0, 0), new Thickness(21, 0, 20, -2)).scale();
        public static readonly Thickness NEWTASK_MARGIN = ChangeValueTo(new Thickness(0, -9, 0, -30), new Thickness(0, 9, 0, -20)).scale();
        public static readonly Thickness NEWTASK_DESC_MARGIN = ChangeValueTo(new Thickness(0, 6, 0, 0), new Thickness(0, -16, 0, 0)).scale();
        public static readonly Thickness NEWTASK_ENTRY_MARGIN = ChangeValueTo(new Thickness(0, -2, 0, 0), new Thickness(-20, -8, 0, -2)).scale();
        public static readonly Thickness NEWTASK_PAYMENT_MARGIN = ChangeValueTo(new Thickness(21, 9, 0, 1), new Thickness(21, 9, 0, -7)).scale();

        public static readonly double EDITOR_NEWTASK_SPACING = Device.RuntimePlatform == Device.iOS ? 6 : 24;

        // Edit Profile
        public static readonly Thickness AGE_ENTRY_MARGIN = ChangeValueTo(new Thickness(0, -3, 0, 0), new Thickness(0, -10, 0, 0)).scale();
        public static readonly double EDITOR_PROFILE_SPACING = Device.RuntimePlatform == Device.iOS ? 6 : 24;
        public static readonly double EDITOR_PROFILE_IMAGE = Device.RuntimePlatform == Device.iOS ? 99 : 104;

        //Forget password
        public static readonly Thickness FP_ENTRY_MARGIN = ChangeValueTo(new Thickness(0, 0, 0, 1), new Thickness(0, -10, 0, 0)).scale();
        public static readonly Thickness FP_LABEL_MARGIN = ChangeValueTo(new Thickness(0, 16, 0, 0), new Thickness(0, 13, 0, 0)).scale();

        // Report Task
        public static readonly Color REPORT_TASK_BORDER = Color.FromHex("#ebebeb");
        public static readonly double REPORT_LABEL_SPACING = Device.RuntimePlatform == Device.iOS ? 3.scale() : 1.3.scale();
        public static readonly double BOX_HEIGHT = ChangeValueTo(150, 146).scale();
        public static readonly double DESCRIPTION_HEIGHT = ChangeValueTo(36, 36).scale();
        public static readonly Thickness REPORT_MARGIN = ChangeValueTo(new Thickness(12, 12, 12, 12), new Thickness(12, 10, 12, 10)).scale();
        public static readonly Thickness R_NAME_MARGIN = ChangeValueTo(new Thickness(0, 3, 0, 0), new Thickness(0, 1, 0, 0)).scale();
        public static readonly Thickness R_SCHOOL_MARGIN = ChangeValueTo(new Thickness(0, 1, 0, 0), new Thickness(0, -1, 0, 0)).scale();
        public static readonly double R_DESCRIPTION_SPACING = ChangeValueTo(7, 4).scale();
        public static readonly double REPORT_IMAGE_RADIUS = ChangeValueTo(2, 7).scale();
        public static readonly Thickness EDITOR_REPORT_MARGIN = ChangeValueTo(new Thickness(15, 5, 15, 0), new Thickness(20, 3, 20, 0)).scale();
        public static readonly double REPORT_USER_HEIGHT = ChangeValueTo(81, 81).scale();
        public static readonly Thickness REPORT_USER_MARGIN = ChangeValueTo(new Thickness(0, 5, 0, 0), new Thickness(0, 4, 0, 0)).scale();
        public static readonly Thickness REPORT_QUESTION_MARGIN = ChangeValueTo(new Thickness(20, 8, 0, 0), new Thickness(20, 6, 0, 0)).scale();

        // Additional Pages
        public static readonly Thickness EDITOR_COMMON_MARGIN = ChangeValueTo(new Thickness(15, 15, 15, 0), new Thickness(20, 15, 20, 0)).scale();
        public static readonly Thickness EDITOR_LINE_MARGIN = ChangeValueTo(new Thickness(0, 0, 0, 0), new Thickness(0, 5, 0, 0)).scale();
        public static readonly Thickness ENTRY_LINE_MARGIN = ChangeValueTo(new Thickness(12, 4, 12, 0), new Thickness(12, -4, 12, 0)).scale();
        public static readonly Thickness GET_INTOUCH_MARGIN = ChangeValueTo(new Thickness(20, 12, 20, 12), new Thickness(20, 10, 20, 10)).scale();
        public static readonly Thickness PRICING_HEADER_MARGIN = ChangeValueTo(new Thickness(20, 12, 20, 12), new Thickness(20, 10, 20, 10)).scale();
        public static readonly Thickness PRICING_RADIO_MARGIN = ChangeValueTo(new Thickness(20, 0, 20, 0), new Thickness(20, 0, 20, 0)).scale();
        public static readonly Thickness PRICING_MARGIN = ChangeValueTo(new Thickness(0, 0, 20, 0), new Thickness(0, 0, 20, 0)).scale();
        //Bid offer
        public static readonly double BID_INFO_ROW_HEIGHT = ChangeValueTo(76.8, 74.2).scale();
        public static readonly int BIDPAGE_ROW_HEIGHT = ChangeValueTo(124, 121).scale();

        public static readonly double BID_FONTSIZE = 7.scale();
        public static readonly double BID_AMOUNT_FONTSIZE = 12.5.scale();


        //Bid Summary
        public static readonly Thickness BID_INFO_MARGIN = ChangeValueTo(new Thickness(0, 13.6, 0, 3.3), new Thickness(0, 10, 0, 0)).scale();
        public static readonly Thickness BID_MESSAGE_MARGIN = ChangeValueTo(new Thickness(0, 2, 0, 0), new Thickness(0, -8, 0, 0)).scale();
        public static readonly Thickness BID_ACTION_MARGIN = ChangeValueTo(new Thickness(0, 8, 0, 0), new Thickness(0, 0, 0, 0)).scale();
        public static readonly double BID_BUTTON_HEIGHT = ChangeValueTo(50, 49).scale();

        //MyTaskPage
        public static readonly int MYTASK_BUTTON_HIEIGHT = ChangeValueTo(40, 39).scale();
        public static readonly int MYTASK_ROW_HEIGHT = ChangeValueTo(81, 78).scale();
        public static readonly double MYTASK_INFO_ROW_HEIGHT = 64.5.scale();
        public static readonly double MYTASK_ROWSPAN = ChangeValueTo(-1, -1).scale();

        //inbox page
        public static readonly int INBOX_ROW_HEIGHT = ChangeValueTo(68, 66).scale();
        public static readonly double INBOX_INFO_ROW_HEIGHT = ChangeValueTo(64, 62).scale();
        public static readonly Thickness INBOX_ROW_MARGIN = ChangeValueTo(new Thickness(0, -3, 13, 0), new Thickness(0, 0, 13, 0)).scale();

        // Message
        public static readonly double MESSAGE_EDITOR_HEIGHT = ChangeValueTo(28, 28).scale();
        public static readonly double MESSAGE_LINESPACING_HEIGHT = ChangeValueTo(3, 15).scale();
        public static readonly int MESSAGE_EDITOR_RADIUS = (int)(MESSAGE_EDITOR_HEIGHT / 2);
        public static readonly double MESSAGE_BUTTON_HEIGHT = ChangeValueTo(38, 38).scale();
        public static readonly double MESSAGE_SPACING = ChangeValueTo(6, 1.5);
        public static readonly Rectangle MESSAGE_EDITOR_SPACING = new Rectangle(0, 15, App.ScreenWidth, MESSAGE_BUTTON_HEIGHT).scale();

        public static readonly ImageSource testImage = "IdleLogo";

        // Payment page
        public static readonly double PAYMENT_ROWHEIGHT_TWO = ChangeValueTo(52, 52).scale();
        public static readonly Thickness PAYMENT_ROW_ENTRY_MARGIN = ChangeValueTo(new Thickness(21, 0, 15, -4), new Thickness(21, -7, 15, -2)).scale();
        public static readonly Thickness PAYMENT_AGE_ENTRY_MARGIN = ChangeValueTo(new Thickness(0, 0, 15, 0), new Thickness(0, -5, 15, 0)).scale();
        public static readonly Thickness PAYMENT_EXPIRE_MARGIN = ChangeValueTo(new Thickness(21, 8, 15, 0), new Thickness(21, 0, 15, 0)).scale();

        //Connecting Stripe
        public static readonly double CONNECTRSTRIPE_LABEL_SPACING = Device.RuntimePlatform == Device.iOS ? 8.scale() : 1.8.scale();
		public static readonly Thickness CONNECTRSTRIPE_MARGIN = ChangeValueTo(new Thickness(20, 12, 20, 12), new Thickness(20, 10, 20, 10)).scale();
		public static readonly Thickness CONNECTRSTRIPE_LINK_MARGIN = ChangeValueTo(new Thickness(20, 0, 20, 10), new Thickness(20, 0, 20, 10)).scale();


		//Tutorial page
		public static readonly double TUTORIAL_PAGE_INDICATOR_HEIGHT =ChangeValueTo(7.scale(),7.scale());
        public static readonly int INDICATOR_RADIUS = (int)( TUTORIAL_PAGE_INDICATOR_HEIGHT / 2);
        public static readonly double TUTORIAL_LINESPACING = ChangeValueTo(3.scale() , 1.3.scale()) ;
		public static readonly double TUTORIAL_IMAGE_ROW_HEIGHT = ChangeValueTo(370, 344).scale();
		public static readonly double TUTORIAL_TITLE_ROW_HEIGHT = ChangeValueTo(22, 22).scale();
		public static readonly double TUTORIAL_DESC_ROW_HEIGHT = ChangeValueTo(124, 164).scale();

		//overloaded method for changes in iOS and Android
		public static string ChangeValueTo(string iOSValue, string androidValue)
        {
            return Device.RuntimePlatform == Device.iOS ? iOSValue : androidValue;
        }
        public static float ChangeValueTo(float iOSValue, float androidValue)
        {
            return Device.RuntimePlatform == Device.iOS ? iOSValue : androidValue;
        }
        public static double ChangeValueTo(double iOSValue, double androidValue)
        {
            return Device.RuntimePlatform == Device.iOS ? iOSValue : androidValue;
        }
        public static int ChangeValueTo(int iOSValue, int androidValue)
        {
            return Device.RuntimePlatform == Device.iOS ? iOSValue : androidValue;
        }
        public static Thickness ChangeValueTo(Thickness iOSValue, Thickness androidValue)
        {
            return Device.RuntimePlatform == Device.iOS ? iOSValue : androidValue;
        }
        //MasterDetailPage 
        //margins
        public static readonly Thickness MASTERDETAIL_LISTVIEW_MARGIN = ChangeValueTo(new Thickness(0, 45, 0, 0), new Thickness(0, 25, 0, 0)).scale();
        public static readonly Thickness LOGOUT_MARGIN = ChangeValueTo(new Thickness(0, 0, 0, 4), new Thickness(0, 0, 0, -8)).scale();
        //double
        public static readonly double LIST_ROW_HEIGHT = ChangeValueTo(43, 43).scale();

        //stripe message
        public static readonly FormattedString STRIPE_LABEL =  new FormattedString()
        {
            Spans =
            {
                new Span() { Text = "Idle uses Stripe to handle payments for transactions. Stripe is an online and mobile payment platform that makes it really easy to send and accept payments through your phone. Learn more here:", ForegroundColor = Color.Black, FontFamily = Constants.PROXIMA_REG , FontSize = Constants.BUTTON_FONTSIZE },
                new Span() { Text = " https://stripe.com/about", ForegroundColor = Constants.LINK_COLOR, FontFamily = Constants.PROXIMA_REG, FontSize = Constants.BUTTON_FONTSIZE }
            }
        };
            
        //Google API Key
        public static readonly string GOOGLE_API_KEY = Device.RuntimePlatform == Device.iOS ? "AIzaSyCxwTAKwcoAy4dbOTXqbxBlcshKq7k4pGc" : "AIzaSyBWaiwSsgTLPjFWhjAGc6H_R8Uf9vFXa3M";
        public static readonly string GOOGLE_DIRECTION_API = "AIzaSyD1goawpgEMAH7kezjrFLbf7MLEmUCFLww";
 
#if STAGING
        public static readonly string URL = "http://173.255.226.102";
        public static readonly string PORT = "4000";
        public static readonly string ROOT = URL +":"+ PORT;
        public static readonly string ROOT_URL = ROOT + "/v1";
        //public static readonly string STRIPE_API_KEY = "sk_test_7Yeg7AMwP8ADYNi52CWboo6r";

#elif PRODUCTION
        public static readonly string URL = "https://www.theidleapp.com";
        public static readonly string PORT = "443";
        public static readonly string ROOT = URL;
        public static readonly string ROOT_URL = ROOT + "/v1";
        //public static readonly string STRIPE_API_KEY = "sk_live_JnTUTdZjHj8H0ckKiTwCi77E";
#endif

#if DEBUG == false
        public static readonly string SIGN_IN_URL = "/sign-in";
        public static readonly string SIGN_UP_URL = "/register";
        public static readonly string FB_URL = "/social";
        public static readonly string STRIPE_URL = "/stripe";
        public static readonly string CONFIRMATION_URL = "/confirm";
        public static readonly string RESEND_CONFIRMATION_URL = "/resend";
        public static readonly string ACCOUNTS_URL = "/accounts";
        public static readonly string USERS_URL = "/users";
        public static readonly string STATUS_URL = "/status";
        public static readonly string FORGOT_PASSWORD_URL = "/forgot";
		public static readonly string CHANGE_PASSWORD_URL = "/password";
		public static readonly string LOCATION_URL = "/location";
        public static readonly string FETCH_TASK_URL = "/tasks";
        public static readonly string DIRECTORY_URL = "/directory";
		public static readonly string CHANGE_ROLE_URL = "/role";
        public static readonly string TOKEN_URL = "?token=";
		public static readonly string BID_URL = "/bids";
        public static readonly string HISTORY_URL = "/history";
		public static readonly string REPORT_URL = "/reports";
		public static readonly string CONVERSATION_URL = "/conversations";
		public static readonly string MESSAGE_URL = "/messages";
#endif
	}
}
