﻿using System;
using System.Diagnostics;
using Xamarin.Forms;
//using idle.Views.SignUpPages;
namespace idle
{
    public class ViewTemplate : DataTemplateSelector
    {
        
        protected override DataTemplate OnSelectTemplate(object item, BindableObject container)
        {
            if(item !=null)
            {
                return new DataTemplate(() =>
                {
                    var dtemp = (View)item;
                   dtemp.BindingContext = ((View)item).BindingContext;
                    return dtemp;
                });
            }
            return null;
        }
    }
}

