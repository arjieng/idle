﻿using System;
namespace idle
{
    public class Bid
    {
		public int id { get; set; }
        public int task_id { get; set; }
		public string amount { get; set; }
        public int task_type { get; set; }
        public User user { get; set; }
    }
}
