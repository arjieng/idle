﻿using System;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using Xamarin.Forms;

namespace idle
{
    public class Message
    {
		public int id { get; set; }
		public string body { get; set; }
        public string image { get; set; }
		public User user { get; set; }
        public string timestamp { get; set;}
    }
}
