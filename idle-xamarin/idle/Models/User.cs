﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using System.Windows.Input;
using Xamarin.Forms;

namespace idle
{
    public class User : INotifyPropertyChanged
    {
        string _name;
        int _age;
        string _school;
        string _email;
        string _mobile;
        string _image;
        string _about;
        int _ratings;
        int _conversation_id;
        int _is_online;

		public int id { get; set; }
		public string name { get { return _name; } set { _name = value; OnPropertyChanged(); } }
		public string email { get { return _email; } set { _email = value; OnPropertyChanged(); } }
		public string image { get { return _image; } set { _image = value; OnPropertyChanged(); } }
		public int age { get { return _age; } set { _age = value; OnPropertyChanged(); } }
		public string school { get { return _school; } set { _school = value; OnPropertyChanged(); } }
        public int is_online { get { return _is_online; } set { _is_online = value;  OnPropertyChanged();} }
		public string about { get { return _about; } set { _about = value; OnPropertyChanged(); } }
        public int ratings { get { return _ratings; } set { _ratings = value; OnPropertyChanged(); } }
		public string mobile { get { return _mobile; } set { _mobile = value; OnPropertyChanged(); } }
		public double latitude { get; set; }
		public double longitude { get; set; }
		public string provider { get; set; }
		public string provider_id { get; set; }
        public int has_stripe { get; set; }
        public int is_confirmed { get; set; }
        public int is_performer { get; set; }
        public string token { get; set; }
        public int completed { get; set; }
        public int conversation_id { get { return _conversation_id; } set { _conversation_id = value;  OnPropertyChanged(); } }
        public int is_owner { get; set;}

        public List<Tasks> tasks { get; set; }
		public List<Tasks> post { get; set; }
        public List<Tasks> perform { get; set; }


		public event PropertyChangedEventHandler PropertyChanged;
		protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = "")
		{
			PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
		}

		public void Update(User user)
		{
			name = user.name;
			age = user.age;
			school = user.school;
			email = user.email;
			mobile = user.mobile;
			image = user.image;
			about = user.about;
            ratings = user.ratings;
            conversation_id = user.conversation_id;
		}
    }
}
