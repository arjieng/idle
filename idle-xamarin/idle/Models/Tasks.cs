﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using Xamarin.Forms.GoogleMaps;

namespace idle
{
    public class Tasks : INotifyPropertyChanged
    {
        int _task_type;
        string _title;
        string _description;
        string _amount;
        string _payment;
        string _bid;
        int _bid_id;
        int _status;
        int _ratings;
        int _is_paid;
        int _is_canceled;
        int _is_completed;
        int _has_rate;
        User _user;

		public int id { get; set; }
		public string title { get { return _title; } set { _title = value; OnPropertyChanged(); } }
		public string description { get { return _description; } set { _description = value; OnPropertyChanged(); } }
		public int task_type { get { return _task_type; } set { _task_type = value; OnPropertyChanged(); } }
        public string amount { get { return _amount; } set { _amount = value; OnPropertyChanged(); } }
        public string payment { get { return _payment; } set { _payment = value; OnPropertyChanged(); } }
        public int status { get { return _status; } set { _status = value;  OnPropertyChanged(); } }
        public string bid { get { return _bid; } set { _bid = value; OnPropertyChanged();}  }
		public double latitude { get; set; }
        public double longitude { get; set; }
        public string date { get; set; }
        public int is_canceled { get { return _is_canceled; } set { _is_canceled = value; OnPropertyChanged(); } }
        public int ratings { get { return _ratings; } set { _ratings = value; OnPropertyChanged(); } }
        public int is_paid { get { return _is_paid; } set { _is_paid = value; OnPropertyChanged(); } }
        public int bid_id { get { return _bid_id; } set { _bid_id = value; OnPropertyChanged(); } }
        public int is_completed { get { return _is_completed; } set { _is_completed = value; OnPropertyChanged(); } }
        public int has_rate { get { return _has_rate; } set { _has_rate = value; OnPropertyChanged(); } }

        public User user { get { return _user; } set { _user = value; OnPropertyChanged(); } }
        public List<Bid> bids { get; set; }

		public event PropertyChangedEventHandler PropertyChanged;
		protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = "")
		{
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }

		public void Update(Tasks task)
		{
			task_type = task.task_type;
			title = task.title;
			description = task.description;
			amount = task.amount;
			payment = task.payment;
            status = task.status;
            user = task.user;
            bid = task.bid;
            ratings = task.ratings;
            is_paid = task.is_paid;
            is_canceled = task.is_canceled;
            bid_id = task.bid_id;
            has_rate = task.has_rate;
		}

        public static explicit operator Pin(Tasks t)
        {
            return new Pin()
            {
				Type = PinType.Place,
				Label = "",
				Position = new Xamarin.Forms.GoogleMaps.Position(t.latitude, t.longitude),
				Tag = t.id,
				IsVisible = true
            };
        }
    }
}
