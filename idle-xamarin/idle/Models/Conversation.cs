﻿using System;
using System.ComponentModel;
using System.Runtime.CompilerServices;

namespace idle
{
    public class Conversation : INotifyPropertyChanged
    {
        string _message;
        public int id { get; set; }
		public int is_read { get; set; }
        public User user { get;set;}
        public string message { get { return _message; } set { _message = value; OnPropertyChanged("message");}}
        public string time { get; set; }
        public int isReply { get; set; }

        public event PropertyChangedEventHandler PropertyChanged;
		protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = "")
		{
			PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
		}
    } 
}
