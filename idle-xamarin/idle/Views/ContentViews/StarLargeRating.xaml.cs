﻿using System;
using System.Collections.Generic;

using Xamarin.Forms;

namespace idle
{
    public partial class StarLargeRating : ContentView
    {
		public static readonly BindableProperty RatingProperty = BindableProperty.Create("Rating", typeof(int), typeof(StarLargeRating), 0, propertyChanged: OnPropertyChanged);
        public static readonly BindableProperty RateLabelProperty = BindableProperty.Create("RateLabel", typeof(string), typeof(StarLargeRating), string.Empty);
		public StarLargeRating()
		{
			InitializeComponent();
		}

		static void OnPropertyChanged(BindableObject bindable, object oldValue, object newValue)
		{
			var stackLayout = (bindable as ContentView).Content.FindByName<StackLayout>("StarLayout");
			for (int i = 0; i < 5; i++)
			{
                if(i < Int32.Parse(newValue.ToString()))
                {
                    if(((FileImageSource)(stackLayout.Children[i] as Image).Source).File!="StarLargeFull.png")
                    {
                        (stackLayout.Children[i] as Image).Source = "StarLargeFull.png";
					}
                }
                else
                {
					if (((FileImageSource)(stackLayout.Children[i] as Image).Source).File!= "StarLargeEmpty.png")
					{
						(stackLayout.Children[i] as Image).Source = "StarLargeEmpty.png";
					}
                }
			}
		}

		public int Rating
		{
			get { return Int32.Parse(base.GetValue(RatingProperty).ToString()); }
			set { SetValue(RatingProperty, value); }
		}

		void OnStarTapped(object sender, EventArgs e)
		{
            Rating = int.Parse(((Image)sender).StyleId);
		}

		public string RateLabel
		{
			get { return base.GetValue(RateLabelProperty).ToString(); }
			set { SetValue(RateLabelProperty, value); }
		}
    }
}
