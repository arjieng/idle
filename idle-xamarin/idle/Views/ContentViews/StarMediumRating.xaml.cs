﻿using System;
using System.Collections.Generic;

using Xamarin.Forms;

namespace idle
{
    public partial class StarMediumRating : ContentView
    {
		public static readonly BindableProperty RatingProperty = BindableProperty.Create("Rating", typeof(int), typeof(StarMediumRating), 0, propertyChanged: OnPropertyChanged);
		public StarMediumRating()
		{
			InitializeComponent();
		}

		private static void OnPropertyChanged(BindableObject bindable, object oldValue, object newValue)
		{
			var stackLayout = (bindable as ContentView).Content.FindByName<StackLayout>("StarLayout");
			for (int i = 0; i < Int32.Parse(newValue.ToString()) && i < 5; i++)
			{
				(stackLayout.Children[i] as Image).Source = "StarMediumFull.png";
			}
		}

		public int Rating
		{
			get { return Int32.Parse(base.GetValue(RatingProperty).ToString()); }
			set { SetValue(RatingProperty, value); }
		}
    }
}
