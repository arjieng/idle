﻿using System;
using System.Collections.Generic;

using Xamarin.Forms;

namespace idle
{
    public partial class WorkerPinView : StackLayout
    {
        readonly string _WorkerPin;
        readonly string _WorkerImage;
        readonly String[] workerPinTypes = { "QuickieWorkerPin.png", "GroceryWorkerPin.png", "LaundryWorkerPin.png", "TransportWorkerPin.png", "OtherWorkerPin.png", "UserLocationPin.png"};

        public WorkerPinView(int taskType, string workerImage)
        {
            InitializeComponent();
            _WorkerPin = workerPinTypes[taskType];
            _WorkerImage = workerImage;
            BindingContext = this;
        }

		public string WorkerPin
		{
			get { return _WorkerPin; }
		}

		public string WorkerImage
		{
			get { return _WorkerImage; }
		}
    }
}
