﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.Threading.Tasks;
using idle.Views;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Xamarin.Forms;
using System.Linq;
using System.Collections;
using System.Threading;

namespace idle.Views
{
    public partial class MyTaskPage : ContentPage, iFileConnector, iRestConnector
    {
        DataClass dataClass = DataClass.GetInstance;
        bool IsClicked = false, addTaskIsClick, IsSwitchClicked = false;
        int buttonType = 0, offSet = 0, TabNumber = 0, serviceType = 0;
        Button previousButton;
        CancellationTokenSource cts;
#if DEBUG
        FileReader fileReader = new FileReader();
#else
        JToken pagination;
        RestServices webService = new RestServices();
#endif

        public MyTaskPage()
        {
            InitializeComponent();
            DependencyService.Get<DependencyServices.iStatusStyle>().StatusStyle(1);

#if DEBUG
            fileReader.FileReaderDelegate = this;
#else
            webService.WebServiceDelegate = this;
#endif

            titlePage.Text = "TASKS";
            titlePage.FontFamily = Constants.LULO;
            titlePage.TextColor = Constants.DARK_STRONG;


            //set default clicked button
            OnSetButton_Clicked(inProgressButton);

            leftButton.Image = "BurgerDark";
            leftButton.Command = new Command((obj) => { MyMasterDetailPage detail = (MyMasterDetailPage)this.Parent.Parent; detail.IsPresented = true; DependencyService.Get<DependencyServices.iStatusStyle>().StatusStyle(0); });
            rightButton.Image = "PlusDark";
            rightButton.Command = new Command((obj) => 
            { 
                if (addTaskIsClick == false)
                {
					if (dataClass.UserIsPerfomer == 1)
					{
						DisplayAlert("Performer Restriction", "You are currently on performing mode. Switch back to poster mode in order for you to post new tasks.", "Okay");
                        addTaskIsClick = false;
					}
					else
					{
                        addTaskIsClick = true;
						Navigation.PushModalAsync(new NewTaskPage(8));
					}
                }
            });
                

            dataClass.TaskList = new ObservableCollection<Tasks>();
            dataClass.BidList = new ObservableCollection<Bid>();
            FetchData(0);

			var tapGestureRecognizer = new TapGestureRecognizer();
			tapGestureRecognizer.Tapped += SwitchButton_TapGestureRecognizer;
			SwitchButton.GestureRecognizers.Add(tapGestureRecognizer);
        }

        protected override void OnAppearing()
        {
            base.OnAppearing();

			MessagingCenter.Subscribe<App>(this, "BiddingAccepted", (App) =>
			{
				if (TabNumber == 0)
				{
					taskList.ItemsSource = dataClass.InProgressPerformList;
				}
				else if (TabNumber == 1)
				{
					if (dataClass.TaskList.ToList().Exists(bidTask => bidTask.id == (dataClass.InProgressPerformList[0]).id))
					{
						dataClass.TaskList.Remove(dataClass.TaskList.ToList().Find(bidTask => bidTask.id == (dataClass.InProgressPerformList[0]).id));
					}
				}
			});

			MessagingCenter.Subscribe<App, JObject>(this, "BiddingDeclined", (sender, obj) =>
			{
				if (dataClass.TaskList.ToList().Exists(bidTask => bidTask.id == int.Parse(obj["custom_data"]["task_id"].ToString())))
				{
					dataClass.TaskList.Remove(dataClass.TaskList.ToList().Find(bidTask => bidTask.id == int.Parse(obj["custom_data"]["task_id"].ToString())));
				}
			});

			MessagingCenter.Subscribe<App, JObject>(this, "TaskFinished", (sender, obj) =>
			{
				if (TabNumber == 0)
				{
					if (dataClass.InProgressPerformList.ToList().Exists(inProgressTask => inProgressTask.id == int.Parse(obj["custom_data"]["task_id"].ToString())))
					{
						dataClass.InProgressPerformList.Remove(dataClass.InProgressPerformList.ToList().Find(inProgressTask => inProgressTask.id == int.Parse(obj["custom_data"]["task_id"].ToString())));
					}
					taskList.ItemsSource = dataClass.InProgressPerformList;
				}
			});

			MessagingCenter.Subscribe<App, JObject>(this, "TaskDeleted", (sender, obj) =>
			{
				if (TabNumber == 1)
				{
					if (dataClass.TaskList.ToList().Exists(bidTask => bidTask.id == int.Parse(obj["custom_data"]["task_id"].ToString())))
					{
						dataClass.TaskList.Remove(dataClass.TaskList.ToList().Find(bidTask => bidTask.id == int.Parse(obj["custom_data"]["task_id"].ToString())));
					}
				}
			});

			MessagingCenter.Subscribe<App>(this, "TaskCancelled", (App) =>
			{
				if (TabNumber == 0)
				{
					taskList.ItemsSource = dataClass.UserIsPerfomer == 1 ? dataClass.InProgressPerformList : dataClass.InProgressPosterList;
				}
			});

            MyMasterDetailPage.isHomePage = true;

            SwitchButton.IsToggled = dataClass.UserIsPerfomer;

            addTaskIsClick = false;
#if DEBUG
            fileReader.FileReaderDelegate = this;
#else
            webService.WebServiceDelegate = this;
#endif
            if (TabNumber == 0)
            {
                FetchData(0);
            }
        }

        protected override void OnDisappearing()
        {
            base.OnDisappearing();

			MessagingCenter.Unsubscribe<App>(this, "BiddingAccepted");
			MessagingCenter.Unsubscribe<App, JObject>(this, "BiddingDeclined");
			MessagingCenter.Unsubscribe<App, JObject>(this, "TaskFinished");
			MessagingCenter.Unsubscribe<App, JObject>(this, "TaskDeleted");
			MessagingCenter.Unsubscribe<App>(this, "TaskCancelled");

            MyMasterDetailPage.isHomePage = false;

			if (cts != null)
			{
				cts.Cancel();
			}
        }

#if DEBUG
        async void FetchData(int tab)
#else
        void FetchData(int tab)
#endif
        {
            Device.BeginInvokeOnMainThread(async () =>
            {
                switch (tab)
                {
                    case 0: // in progress
                        if (dataClass.UserIsPerfomer == 1)
                        {
                            taskList.ItemsSource = dataClass.InProgressPerformList;
                        }
                        else
                        {
                            taskList.ItemsSource = dataClass.InProgressPosterList;
                        }

                        dataClass.TaskList.Clear();

                        buttonType = 0;
                        IsClicked = false;
                        IsSwitchClicked = false;
                        break;
                    case 1: // bids
                        dataClass.TaskList.Clear();
                        buttonType = 1;
                        serviceType = 0;
                        cts = new CancellationTokenSource();
#if DEBUG
                        if (dataClass.UserIsPerfomer == 1)
                            await fileReader.ReadFile("BidsPerformer.json", true, cts.Token);
                        else
                            await fileReader.ReadFile("BidsOwner.json", true, cts.Token);
#else
                        loadingIndicator.IsRunning = true;
                        var url = Constants.ROOT_URL + Constants.ACCOUNTS_URL + Constants.FETCH_TASK_URL + Constants.DIRECTORY_URL + "?token=" + dataClass.UserToken;
						
						try
						{
                            await webService.GetRequest(url, cts.Token);
						}
						catch (OperationCanceledException)
						{
							DependencyService.Get<iConsole>().DisplayText("Web Service: Cancelled!");
						}
						catch (Exception)
						{
							DependencyService.Get<iConsole>().DisplayText("Web Service: Failed!");
						}

						cts = null;
#endif
                        break;
                    case 2: // history
                        dataClass.TaskList.Clear();
                        buttonType = 2;
                        serviceType = 0;
                        cts = new CancellationTokenSource();
#if DEBUG
                        await fileReader.ReadFile("History.json", true, cts.Token);
#else
                        loadingIndicator.IsRunning = true;
						
						try
						{
							await webService.GetRequest(Constants.ROOT_URL + Constants.ACCOUNTS_URL + Constants.FETCH_TASK_URL + Constants.HISTORY_URL + Constants.TOKEN_URL + dataClass.UserToken + "&off_set=0", cts.Token);
						}
						catch (OperationCanceledException)
						{
							DependencyService.Get<iConsole>().DisplayText("Web Service: Cancelled!");
						}
						catch (Exception)
						{
							DependencyService.Get<iConsole>().DisplayText("Web Service: Failed!");
						}

						cts = null;
#endif
                        break;
                }
            });
        }

		async void SwitchButton_TapGestureRecognizer(Object sender, EventArgs e)
		{
            if (!IsSwitchClicked)
            {
                IsSwitchClicked = true;

                if (SwitchButton.IsToggled == 1)
                {
                    SwitchButton.IsToggled = 0;
                }
                else
                {
                    SwitchButton.IsToggled = 1;
                }

                dataClass.UserIsPerfomer = SwitchButton.IsToggled;

#if DEBUG == false
                serviceType = 1;
                var json = JsonConvert.SerializeObject(new { token = dataClass.UserToken });
                var url = Constants.ROOT_URL + Constants.ACCOUNTS_URL + Constants.CHANGE_ROLE_URL;
				
				cts = new CancellationTokenSource();
				try
				{
                    await webService.PutRequestAsync(url, json, cts.Token);
				}
				catch (OperationCanceledException)
				{
					DependencyService.Get<iConsole>().DisplayText("Web Service: Cancelled!");
				}
				catch (Exception)
				{
					DependencyService.Get<iConsole>().DisplayText("Web Service: Failed!");
				}

				cts = null;

#endif
            }
		}

		async void OnMyTask_Clicked(object sender, EventArgs e)
        {
            if (IsClicked == false)
            {  
                IsClicked = true;

                var button = (Button)sender;
                TabNumber = int.Parse(button.StyleId);

                if (dataClass.InProgressPerformList.Count > 0 || dataClass.TaskList.Count > 0 || dataClass.InProgressPosterList.Count > 0)
                {
                    try
                    {
                        var first = taskList.ItemsSource.Cast<object>().FirstOrDefault();
#if DEBUG
                        taskList.ScrollTo(first, ScrollToPosition.Start, false);
#endif
                    }
                    catch (Exception ex)
                    {
                        Debug.WriteLine(ex.Message);
                    }
                }

                taskList.ItemsSource = null;
                OnSetButton_Clicked(button);

                await  Task.Run(() => { FetchData(TabNumber); });
            }
        }

        void OnSetButton_Clicked(Button button)
        {
            if (previousButton != null)
            {
                previousButton.BackgroundColor = Color.White;
                previousButton.TextColor = Constants.DARK_MEDIUM;
                previousButton.FontFamily = Constants.PROXIMA_REG;
                previousButton.BorderColor = Constants.LIGHT_GRAY;
            }

            button.BackgroundColor = Constants.BUTTON_GREEN;
            button.BorderColor = Constants.BUTTON_GREEN;
            button.TextColor = Color.White;
            button.FontFamily = Constants.PROXIMA_BOLD;
            previousButton = button;
        }

        async void OnTask_ItemTapped(object sender, ItemTappedEventArgs e)
        {
			taskList.SelectedItem = null;

            var task = e.Item as Tasks;

            if (dataClass.UserIsPerfomer == 0)
            {
                if (buttonType == 1)
                {
                    dataClass.BidList.Clear();

                    foreach (var bid in ((Tasks)e.Item).bids) { bid.task_type = ((Tasks)e.Item).task_type; dataClass.BidList.Add(bid); }
                }
//#if DEBUG == false
				int id = ((Tasks)e.Item).id;
                Tasks bindTask = (Tasks)e.Item;
                await Navigation.PushAsync(new TaskSummaryPage(id, bindTask, offSet, 1));
//#endif
            }
            else
            {
                // offSet, task, pageType
                await Navigation.PushAsync(new ViewTaskPage(offSet, (Tasks)e.Item, 3));
            }
        }

        async void OnLast_ItemAppearing(object sender, Xamarin.Forms.ItemVisibilityEventArgs e)
        {
#if DEBUG == false
            if (buttonType != 0)
            {
                var currentItem = e.Item as Tasks;
                var lastItem = dataClass.TaskList[dataClass.TaskList.Count - 1];
                if (currentItem == lastItem && int.Parse(pagination["load_more"].ToString()) == 1)
                {
                    loadingIndicator.IsRunning = true;
					
					cts = new CancellationTokenSource();
					try
					{
                        await webService.GetRequest(Constants.ROOT + pagination["url"], cts.Token);
					}
					catch (OperationCanceledException)
					{
						DependencyService.Get<iConsole>().DisplayText("Web Service: Cancelled!");
					}
					catch (Exception)
					{
						DependencyService.Get<iConsole>().DisplayText("Web Service: Failed!");
					}

					cts = null;
                }
            }
#endif
        }

		public void ReceiveJSONData(JObject jsonData)
		{

		}

        public void ReceiveJSONData(JObject jsonData, CancellationToken ct)
        {
            IsClicked = false;

            Device.BeginInvokeOnMainThread(() =>
            {
                if (int.Parse(jsonData["status"].ToString()) == 200)
                {
                    switch (serviceType)
                    {
                        case 0:

                            var tempTasks = JsonConvert.DeserializeObject<ObservableCollection<Tasks>>(jsonData["tasks"].ToString());
                            taskList.ItemsSource = null;
                            foreach (var task in tempTasks)
                            {
                                dataClass.TaskList.Add(task);
                            }
#if DEBUG == false
                            pagination = jsonData["pagination"];
                            offSet = int.Parse(pagination["off_set"].ToString());
#endif
                            taskList.ItemsSource = dataClass.TaskList;

                            IsClicked = false;
                            IsSwitchClicked = false;
							break;
						case 1:
                            FetchData(TabNumber);
							break;
					}
                }
                else
                {
                    DisplayAlert("test", "movers", "okey");
                }
            });

		    loadingIndicator.IsRunning = false;
        }
       
        public  void ReceiveTimeoutError(string title, string error)
        {
            IsClicked = false;
            IsSwitchClicked = false;
			loadingIndicator.IsRunning = false;
			Device.BeginInvokeOnMainThread(async delegate
			{
				await DisplayAlert(title, error, "Okay");
			});
        }
    }
}
