﻿using System;
using Xamarin.Forms;

namespace idle
{
	public class ListViewNoScroll : ListView
	{
		public static readonly BindableProperty ScrollProperty = BindableProperty.Create("Scroll", typeof(bool), typeof(ListViewNoScroll), false);

		public bool Scroll
		{
			get { return (bool)GetValue(ScrollProperty); }
			set { SetValue(ScrollProperty, value); }
		}
	}
}
