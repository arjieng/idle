﻿using System;
using Xamarin.Forms;

namespace idle
{
	public class CustomEntry : Entry
	{
		public static readonly BindableProperty ReturnKeyProperty = BindableProperty.Create("ReturnKey", typeof(string), typeof(CustomEntry), "Return");

		public string ReturnKey
		{
			get { return (string)GetValue(ReturnKeyProperty); }
			set { SetValue(ReturnKeyProperty, value); }
		}

		public static readonly BindableProperty AutoCapitalizationProperty = BindableProperty.Create("AutoCapitalization", typeof(string), typeof(CustomEntry), "None");

		public string AutoCapitalization
		{
			get { return (string)GetValue(AutoCapitalizationProperty); }
			set { SetValue(AutoCapitalizationProperty, value); }
		}

		public static readonly BindableProperty ButtonModeProperty = BindableProperty.Create("ButtonMode", typeof(string), typeof(CustomEntry), null);

		public string ButtonMode
		{
			get { return (string)GetValue(ButtonModeProperty); }
			set { SetValue(ButtonModeProperty, value); }
		}

		public static readonly BindableProperty MoveUpProperty = BindableProperty.Create("MoveUp", typeof(bool), typeof(CustomEntry), true);

		public bool MoveUp
		{
			get { return (bool)GetValue(MoveUpProperty); }
			set { SetValue(MoveUpProperty, value); }
		}

        public Entry NextEntry { get; set; }
	}
}
