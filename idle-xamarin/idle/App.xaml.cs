using System;
using System.Collections.ObjectModel;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using idle.Views;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Plugin.FirebasePushNotification;
using Plugin.Geolocator.Abstractions;
using Xamarin.Forms;

namespace idle
{
    public partial class App : Application
    {
        public static double ScreenHeight { get; set; }
        public static double ScreenWidth { get; set; }
        public static float DeviceScale { get; set; }
        public static double ScreenScale { get { return (ScreenWidth + ScreenHeight) / (320.0F + 568.0F); } }
        public static int DeviceType { get { return Constants.ChangeValueTo(0, 1); } }
        public static string UDID { get; set; }
        public static bool isAlreadyLoaded { get; set; }

        public static EventHandler<int> UiTestTapPin;
        LocationHelper locationHelper = LocationHelper.GetInstance;
        StripeHelper stripeHelper = new StripeHelper();
        ChangeStatusHelper changeStatusHelper = new ChangeStatusHelper();
        DataClass dataClass = DataClass.GetInstance;

        public App()
        {
            InitializeComponent();

			dataClass.InProgressPosterList = new ObservableCollection<Tasks>();
			dataClass.InProgressPerformList = new ObservableCollection<Tasks>();

            if (Application.Current.Properties.ContainsKey("user_token"))
			{
				ShowMainPage();
			}
			else
			{
				Logout();
			}

            CrossFirebasePushNotification.Current.OnTokenRefresh += (s, p) =>
            {
                App.UDID = p.Token;
            };

            CrossFirebasePushNotification.Current.OnNotificationReceived += (s, p) =>
            {
                
                DependencyService.Get<iConsole>().DisplayText("Push notification: " + JObject.Parse(p.Data["custom_data"]));

                if (Device.RuntimePlatform == Device.iOS)
                {
                    if(p.Data.ContainsKey("gcm.notification.collapse_key") && p.Data.ContainsKey("gcm.notification.data"))
                    {
                        var key = p.Data["gcm.notification.collapse_key"];
                        var data = JObject.Parse(p.Data["gcm.notification.data"]);
                        SendNotificationToMessaging(key, data);
                    }
                    else
                    {
                        var customData = JObject.Parse(p.Data["custom_data"]);
                        MessagingCenter.Send<App, JObject>(this, "PerformerLocationUpdated", customData);
                    }
                }
                else
                {
                    var key = p.Data["notif_tag"];

                    var customData = JObject.Parse(p.Data["custom_data"]);

                    if (p.Data.ContainsKey("message"))
                    {
                        var message = p.Data["message"];
                        var obj = JObject.FromObject(new { custom_data = customData, message = message });
                        SendNotificationToMessaging(key, obj);
                    }
                    else
                    {
                        Device.BeginInvokeOnMainThread(() =>
                        {
                            MessagingCenter.Send<App, JObject>(this, "PerformerLocationUpdated", customData);
                        });                    
                    }
                }
            };

            CrossFirebasePushNotification.Current.OnNotificationOpened += (s, p) =>
            {
                //var key = p.Data["gcm.notification.collapse_key"];
                //var data = JObject.Parse(p.Data["gcm.notification.data"].ToString());

                //SendNotificationToMessaging(key, data);
            };
        }

        protected async override void OnStart()
        {
            // Handle when your app starts
            if (Application.Current.Properties.ContainsKey("user_token"))
            {
                await FetchLocation();
                await locationHelper.StartListening();
				Badge.SetBadge(0);
            }

#if DEBUG == false
			if (!Application.Current.Properties.ContainsKey("stripe_api_key"))
			{
                stripeHelper.GetStripeAPIKey();
			}
#endif
		}

        protected async override void OnSleep()
        {
            // Handle when your app sleeps
            await locationHelper.StopListening();
			
#if DEBUG == false
            if (Application.Current.Properties.ContainsKey("user_token"))
            {
                if (dataClass.UserIsOnline == 1)
                {
                    changeStatusHelper.ChangeUserStatus();
                }
            }
#endif
            dataClass.Update(false);
        }

        protected async override void OnResume()
        {
            // Handle when your app resumes

            if (Views.SignUpPages.SignUpSecondPage.isStripeClick)
            {

            }

            if (Application.Current.Properties.ContainsKey("user_token"))
            {
                await FetchLocation();
                await locationHelper.StartListening();
				Badge.SetBadge(0);
            }
          
        }

        async Task FetchLocation()
        {
            if (DependencyService.Get<iLocationServices>().IsLocationServiceOn())
            {
                Position GeoPosition = await locationHelper.GetCurrentLocation();
                if (GeoPosition != null)
                {
                    dataClass.latitude = GeoPosition.Latitude;
                    dataClass.longitude = GeoPosition.Longitude;
                    dataClass.hasLocation = true;
                }
            }
        }

        void SendNotificationToMessaging(string key, JObject jsonData)
		{
			if (key.Equals("bidding_task"))
			{
				Device.BeginInvokeOnMainThread(async () =>
				{
					var message = jsonData["message"].ToString();
					await MainPage.DisplayAlert("Bid on Task", message, "Okay");
					MessagingCenter.Send<App, JObject>(this, "BiddingOffers", jsonData);
				});
			}
            else if(key.Equals("bidding_accepted"))
            {
                Device.BeginInvokeOnMainThread( async () =>
                {
					var message = jsonData["message"].ToString();
                    var task = JsonConvert.DeserializeObject<Tasks>(jsonData["custom_data"].ToString());
                    DependencyService.Get<iConsole>().DisplayText("Task: " + task.title);
					dataClass.InProgressPerformList.Insert(0, task);
					await MainPage.DisplayAlert("Bid Accepted", message, "Check Now");
					MessagingCenter.Send<App>(this, "BiddingAccepted");
                } );
            }
            else if(key.Equals("bidding_declined") || key.Equals("bidding_taken"))
            {
                Device.BeginInvokeOnMainThread(async () =>
                {
                    var message = jsonData["message"].ToString();
                    await MainPage.DisplayAlert("Bid Declined", message, "Okay");
                    MessagingCenter.Send<App, JObject>(this, "BiddingDeclined", jsonData);
                });
            }
			else if (key.Equals("bidding_canceled"))
			{
				Device.BeginInvokeOnMainThread(async () =>
				{
					var message = jsonData["message"].ToString();
					await MainPage.DisplayAlert("Bid Cancelled", message, "Okay");
					MessagingCenter.Send<App, JObject>(this, "BidCancelled", jsonData);
				});
			}
			else if (key.Equals("message_received"))
			{
                MessagingCenter.Send<App, JObject>(this, "MessageReceived", jsonData);
			}
            else if(key.Equals("task_canceled"))
            {
				//task canceled
				Device.BeginInvokeOnMainThread(async () =>
				{
					var message = jsonData["message"].ToString();
					if (int.Parse(jsonData["custom_data"]["status"].ToString()) == 0)
					{
                        if(dataClass.UserIsPerfomer == 1)
                        {
							if (dataClass.InProgressPerformList.ToList().Exists(inProgressTask => inProgressTask.id == int.Parse(jsonData["custom_data"]["task_id"].ToString())))
							{
								dataClass.InProgressPerformList.Remove(dataClass.InProgressPerformList.ToList().Find(inProgressTask => inProgressTask.id == int.Parse(jsonData["custom_data"]["task_id"].ToString())));
							}
                        }
                        else
                        {
							if (dataClass.InProgressPosterList.ToList().Exists(inProgressTask => inProgressTask.id == int.Parse(jsonData["custom_data"]["task_id"].ToString())))
							{
                                dataClass.InProgressPosterList.Remove(dataClass.InProgressPosterList.ToList().Find(inProgressTask => inProgressTask.id == int.Parse(jsonData["custom_data"]["task_id"].ToString())));
							}
                        }
					}
					await MainPage.DisplayAlert("Task Cancelled", message, "Okay");
					MessagingCenter.Send<App, JObject>(this, "TaskCancelled", jsonData);
				});
            }
            else if(key.Equals("task_completed"))
            {
                //completed push notification for poster
                Device.BeginInvokeOnMainThread(async () =>
                {
                    var message = jsonData["message"].ToString();
                    await MainPage.DisplayAlert("Task Completed", message, "Okay");
                    MessagingCenter.Send<App, JObject>(this, "TaskCompleted", jsonData);
                });
            }
            else if(key.Equals("task_finished"))
            {
				Device.BeginInvokeOnMainThread(async () =>
				{
					var message = jsonData["message"].ToString();
					await MainPage.DisplayAlert("Task Finished", message, "Okay");
					MessagingCenter.Send<App, JObject>(this, "TaskFinished", jsonData);
				});
            }
			else if (key.Equals("task_deleted"))
			{
				Device.BeginInvokeOnMainThread(async () =>
				{
					var message = jsonData["message"].ToString();
					await MainPage.DisplayAlert("Task Deleted", message, "Okay");
					MessagingCenter.Send<App, JObject>(this, "TaskDeleted", jsonData);
				});
			}
		}

		public static void ShowMainPage()
		{
			Application.Current.MainPage = new MyMasterDetailPage();
		}

		public static void Logout()
		{ 
			Application.Current.MainPage = new NavigationPage(new Views.SignInPage());
		}

		public static void ShowResetPasswordPage()
		{
			Application.Current.MainPage = new Views.ChangePasswordPage();
		}
	}
}
