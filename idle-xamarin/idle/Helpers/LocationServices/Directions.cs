﻿using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Threading;
using System.Threading.Tasks;
using Newtonsoft.Json.Linq;
using Xamarin.Forms;
using Xamarin.Forms.GoogleMaps;

namespace idle
{
	public class Directions
	{
		HttpClient client;
        NetworkHelper networkHelper = NetworkHelper.GetInstance;
        CancellationTokenSource cts;
        JObject result;

		public Directions()
		{
			client = new HttpClient();
			client.DefaultRequestHeaders.Accept.Clear();
			client.MaxResponseContentBufferSize = 256000;
		}

		async Task<JObject> GetRequestAsync(string url, CancellationToken ct)
		{
            HttpResponseMessage response = await client.GetAsync(new Uri(url), ct);
            if (response.IsSuccessStatusCode)
            {
                return JObject.Parse(await response.Content.ReadAsStringAsync().ConfigureAwait(false));
            }
            return JObject.Parse("{\"status\":\"ERROR\"}");
        }

		public async Task<List<Position>> RequestDirections(string origin, string destination, string key, string waypoint = "", string mode = "walking")
		{
			string url = "https://maps.googleapis.com/maps/api/directions/json?origin=" + origin + "&destination=" + destination + "&key=" + key + "&mode=" + mode;
			url += String.IsNullOrWhiteSpace(waypoint) ? String.Empty : "&waypoints =" +waypoint;

            string encodedPolylines = String.Empty;

            if (networkHelper.HasInternet())
            {
				cts = new CancellationTokenSource();
				try
				{
					result = await GetRequestAsync(url, cts.Token);
				}
				catch (OperationCanceledException)
				{
					DependencyService.Get<iConsole>().DisplayText("Helper Location: Cancelled!");
				}
				catch (Exception)
				{
					DependencyService.Get<iConsole>().DisplayText("Helper Location: Failed!");
				}

				cts = null;
				
				if (result != null)
				{
					if (result["routes"].HasValues)
					{
						encodedPolylines = result["routes"][0]["overview_polyline"]["points"].ToString();
					}
				}
            }

			return Decoder(encodedPolylines);
		}

		List<Position> Decoder(string encodedPoints)
		{
			// https://www.codeproject.com/Tips/312248/Google-Maps-Direction-API-V-Polyline-Decoder
			if (encodedPoints == null || encodedPoints == "") return null;

			List<Position> points = new List<Position>();

			try
			{
				char[] polylinechars = encodedPoints.ToCharArray();
				int index = 0;

				int currentLat = 0;
				int currentLng = 0;
				int next5bits;
				int sum;
				int shifter;

				while (index < polylinechars.Length)
				{
					// calculate next latitude
					sum = 0;
					shifter = 0;
					do
					{
						next5bits = (int)polylinechars[index++] - 63;
						sum |= (next5bits & 31) << shifter;
						shifter += 5;
					} while (next5bits >= 32 && index < polylinechars.Length);

					if (index >= polylinechars.Length)
						break;

					currentLat += (sum & 1) == 1 ? ~(sum >> 1) : (sum >> 1);

					//calculate next longitude
					sum = 0;
					shifter = 0;
					do
					{
						next5bits = (int)polylinechars[index++] - 63;
						sum |= (next5bits & 31) << shifter;
						shifter += 5;
					} while (next5bits >= 32 && index < polylinechars.Length);

					if (index >= polylinechars.Length && next5bits >= 32)
						break;

					currentLng += (sum & 1) == 1 ? ~(sum >> 1) : (sum >> 1);
					points.Add(new Position(Convert.ToDouble(currentLat) / 100000.0, Convert.ToDouble(currentLng) / 100000.0));
				}
			}
			catch (Exception)
			{
				;
			}
			return points;
		}
	}
}
