﻿using System;
using System.Threading.Tasks;
using Plugin.Geolocator.Abstractions;

namespace idle
{
    public interface LocationInterface
    {
		bool IsLocationAvailable();
		bool IsLocationEnabled();
		Task<Position> GetCurrentLocation();
        Task StartListening();
        Task StopListening();
    }
}
