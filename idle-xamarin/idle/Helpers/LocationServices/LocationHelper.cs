﻿﻿using System;
using Xamarin.Forms;

using Plugin.Geolocator;
using System.Threading.Tasks;
using Plugin.Geolocator.Abstractions;
using Plugin.Permissions.Abstractions;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System.Net.Http;
using System.Text;
using System.Threading;

namespace idle
{
    public class LocationHelper : LocationInterface
    {
        public static LocationHelper location;
        public static LocationHelper GetInstance
        {
            get
            {
                if (location == null)
                {
                    location = new LocationHelper();
                }
                return location;
            }
        }

        public bool IsLocationAvailable()
        {
            if (!CrossGeolocator.IsSupported)
                return false;

            return CrossGeolocator.Current.IsGeolocationAvailable;
        }

        public bool IsLocationEnabled()
        {
            return CrossGeolocator.Current.IsGeolocationEnabled;
        }

        public async Task<Position> GetCurrentLocation()
        {
            Position position = null;
           
            var status = await PermissionHelper.CheckPermissions(Permission.Location);
            if (status == PermissionStatus.Granted)
            {
                try
                {
                    var locator = CrossGeolocator.Current;
                    locator.DesiredAccuracy = 100;

                    position = await locator.GetLastKnownLocationAsync();

                    if (position != null)
                    {
                        return position;
                    }

                    if (!locator.IsGeolocationEnabled || !locator.IsGeolocationAvailable)
                    {
                        return null;
                    }

                    position = await locator.GetPositionAsync(TimeSpan.FromSeconds(2), null, true);

                    if (position != null)
                    {
                        return position;
                    }
                    else
                    {
                        return null;
                    }
                }
                catch (Exception ex)
                {
                    System.Diagnostics.Debug.WriteLine(ex.ToString());
                    return null;
                }
            }
            else
            {
                Application.Current?.MainPage?.DisplayAlert("Cannot fetch location", "", "OK");
                return null;
            }

        }

        public async Task StartListening()
        {
            IsAlreadyNotify = false;
            if (DependencyService.Get<iLocationServices>().IsLocationServiceOn())
            {
                var status = await PermissionHelper.CheckPermissions(Permission.Location);
                if (status == PermissionStatus.Granted)
                {
                    if (CrossGeolocator.Current.IsListening)
                        return;
                    var isListening = await CrossGeolocator.Current.StartListeningAsync(TimeSpan.FromSeconds(1), 1, true);
                    if (isListening)
                    {
						client = new HttpClient();
						client.DefaultRequestHeaders.Accept.Clear();
						client.MaxResponseContentBufferSize = 256000;
                        CrossGeolocator.Current.PositionChanged += OnUser_PositionChange;
                    }
                }
                else
                {
                    Application.Current?.MainPage?.DisplayAlert("Cannot start listening location", "", "OK");
                }
            }
        }

        DataClass dataClass = DataClass.GetInstance;
		NetworkHelper networkHelper = NetworkHelper.GetInstance;
		HttpClient client;
        CancellationTokenSource cts;

        async void OnUser_PositionChange(object sender, PositionEventArgs e)
        {
#if DEBUG == false
            var position = e.Position;

            if (Application.Current.Properties.ContainsKey("user_token"))
            {
                var json = JsonConvert.SerializeObject(new { latitude = position.Latitude, longitude = position.Longitude , token = DataClass.GetInstance.UserToken });
                if(!(dataClass.latitude.Equals(position.Latitude) && dataClass.longitude.Equals(position.Longitude)))
                {
					cts = new CancellationTokenSource();
					try
					{
						await PutRequestAsync(Constants.ROOT_URL + Constants.ACCOUNTS_URL + Constants.LOCATION_URL, json, cts.Token);
					}
					catch (OperationCanceledException)
					{
						DependencyService.Get<iConsole>().DisplayText("Helper Location: Cancelled!");
					}
					catch (Exception)
					{
						DependencyService.Get<iConsole>().DisplayText("Helper Location: Failed!");
					}

					cts = null;

                    DependencyService.Get<iConsole>().DisplayText("Helper Location: " + dataClass.latitude + "    " + dataClass.longitude);
					dataClass.latitude = e.Position.Latitude;
					dataClass.longitude = e.Position.Longitude;
                }
            }
#endif
        }

        public async Task StopListening()
		{
			if (!CrossGeolocator.Current.IsListening)
				return;
            
			CrossGeolocator.Current.PositionChanged -= OnUser_PositionChange;
			await CrossGeolocator.Current.StopListeningAsync();
		}

        public static bool IsAlreadyNotify = false;
		public async Task PutRequestAsync(string url, string dictionary, CancellationToken ct)
		{
            if (networkHelper.HasInternet())
            {
                IsAlreadyNotify = false;
	                if (await networkHelper.IsHostReachable() == true)
	                {
		                    var uri = new Uri(url);
		                    var content = new StringContent(dictionary, Encoding.UTF8, "application/json");

		                    HttpResponseMessage response = await client.PutAsync(uri, content, ct);

		                    if (response.IsSuccessStatusCode)
		                    {
		                        var result = await response.Content.ReadAsStringAsync().ConfigureAwait(false);
		                    }
		                    else
		                    {
		                        //300, 404, 500
		                        await Application.Current.MainPage.DisplayAlert("Error", response.StatusCode.ToString(), "Okay");
		                    }
	                }
                else
	                {
	                    await Application.Current.MainPage.DisplayAlert("Host Unreachable!", "The URL host for Idle cannot be reached and seems to be unavailable. Please try again later!", "Okay");
	                }
            }
            else
            {
                if (IsAlreadyNotify == false)
                {
                    IsAlreadyNotify = true;
                    await Application.Current.MainPage.DisplayAlert("No Internet Connection!", "Please check your internet connection, and try again!", "Okay");
                }
            }
		}
    }
}
