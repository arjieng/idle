﻿using System;
//using System.Diagnostics;
using System.Globalization;
using Xamarin.Forms;

namespace idle
{
    public class TextNotNullConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            //Debug.WriteLine("1 true");
            var text = (string)value;
            if (!string.IsNullOrEmpty(text))
            {
                if (text.ToString().Equals("Add Description"))
                    return false;

                return true;
            }
            else
            {
                return false;
            }
            //if(value is CustomEditor)
            //{
            //    var cEditor = (CustomEditor)value;
            //    if(!string.IsNullOrEmpty(cEditor.Text))
            //    if (!cEditor.Text.Equals(cEditor.Placeholder))
            //        return true;
            //}
            //else if(value is CustomEntry)
            //{
            //    if (!string.IsNullOrEmpty(((CustomEntry)value).Text))
            //        return true;
            //}
            //Debug.WriteLine("2 false");
            //return false;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
