﻿using System;
using System.Diagnostics;
using System.Globalization;
using Xamarin.Forms;

namespace idle
{
    public class IsZeroDouble : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if(value != null){
				if (value is string)
				{

					double bid = double.Parse(value.ToString());
					if (bid == 0.0)
					{
						return true;
					}
				}
            }

            return false;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
