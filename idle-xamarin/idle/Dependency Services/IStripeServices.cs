﻿using System;

namespace idle
{
    public interface IStripeServices
    {
        void CardToToken(CreditCard creditCard, string stripeAPIKey, Action<string, string> onStripeComplete);
    }
}
