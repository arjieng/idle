﻿using System;
namespace idle
{
	public interface iLocationServices
	{
		void OpenSettingsUrl();
		bool IsLocationServiceOn();
	}
}
