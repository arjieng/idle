﻿﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Diagnostics;
using Xamarin.Forms;
using System.Linq;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using idle.Views;
using System.Threading.Tasks;
using System.Threading;

namespace idle
{
    public partial class MasterPage : ContentPage, iRestConnector
    {
        public ListView ListView { get { return listView; } }
        public bool isLogout = false;

        public static EventHandler<Type> PageSelected { get; set; }
        public static EventHandler<User> UpdateProfile { get; set; }
#if DEBUG == false
        RestServices webService = new RestServices();
#endif

        ObservableCollection<MasterPageItem> masterPageItems;
        int lastPageIndex;
        public MasterPage()
        {
            InitializeComponent();
            BindingContext = new { image = DataClass.GetInstance.UserImage };
#if DEBUG == false
			webService.WebServiceDelegate = this;
#endif
            masterPageItems = new ObservableCollection<MasterPageItem>();
            masterPageItems.Add(new MasterPageItem { Title = "Home", TargetType = typeof(Views.HomePage), IsSelected = 1 });
            masterPageItems.Add(new MasterPageItem { Title = "Profile", TargetType = typeof(Views.ProfilePage), IsSelected = 0 });
            masterPageItems.Add(new MasterPageItem { Title = "Tasks", TargetType = typeof(Views.MyTaskPage), IsSelected = 0 });
            masterPageItems.Add(new MasterPageItem { Title = "Messages", TargetType = typeof(Views.InboxPage), IsSelected = 0 });
            masterPageItems.Add(new MasterPageItem { Title = "Payment", TargetType = typeof(Views.ConnectStripePage), IsSelected = 0 });
            masterPageItems.Add(new MasterPageItem { Title = "Settings", TargetType = typeof(Views.SettingsPage), IsSelected = 0 });
			masterPageItems.Add(new MasterPageItem { Title = "Share", TargetType = typeof(Views.ShareIdlePage), IsSelected = 0 });

			listView.ItemsSource = masterPageItems;

            imageCirc.ImageTapped += OnProfile_ImageTapped;

            PageSelected += (sender, e) =>
            {
                OnMaster_PageSelected(masterPageItems, e);
            };

            UpdateProfile += OnProfile_Update;
		}

        private async void OnProfile_Update(object sender, User e)
        {
            Application.Current.Properties["user_image"] = e.image;
            await Application.Current.SavePropertiesAsync();
            BindingContext = new { image = DataClass.GetInstance.UserImage };
        }

        protected override void OnAppearing()
        {
            base.OnAppearing();
            DependencyService.Get<DependencyServices.iStatusStyle>().StatusStyle(0);
            BindingContext = new { image = DataClass.GetInstance.UserImage };
            isLogout = false;
        }

        private void OnMaster_PageSelected(ObservableCollection<MasterPageItem> masterPageItems, Type e)
        {
            masterPageItems[lastPageIndex].IsSelected = 0;
            lastPageIndex = masterPageItems.ToList().FindIndex((obj) => obj.TargetType == e);
            var page = masterPageItems[lastPageIndex];
            page.IsSelected = 1;
        }

        void OnList_ItemTapped(object sender, ItemTappedEventArgs e)
        {
            listView.SelectedItem = null;
        }
        private void OnProfile_ImageTapped(object sender, ImageTappedEventArgs e)
        {
            MyMasterDetailPage detail = (MyMasterDetailPage)this.Parent;
            detail.IsPresented = false;

            ((MyMasterDetailPage)this.Parent).Detail.Navigation.PushAsync(new Views.ProfilePage(0, DataClass.GetInstance.UserID));

            PageSelected.Invoke(this, typeof(Views.ProfilePage));
            DependencyService.Get<DependencyServices.iStatusStyle>().StatusStyle(1);

            MyMasterDetailPage.stackCount += 1;
			if (MyMasterDetailPage.pageType == null)
			{
				MyMasterDetailPage.pageType = new List<Type>();
                MyMasterDetailPage.pageType.Add(typeof(Views.HomePage));
			}
            MyMasterDetailPage.isToastBack = false;
            MyMasterDetailPage.pageType.Add(typeof(Views.ProfilePage));
        }

        void OnSideMenuClose_Clicked(object sender, System.EventArgs e)
        {
            MyMasterDetailPage detail = (MyMasterDetailPage)this.Parent;
            detail.IsPresented = false;
            DependencyService.Get<DependencyServices.iStatusStyle>().StatusStyle(1);
        }

        void OnProfile_Clicked(object sender, System.EventArgs e)
        {
            MyMasterDetailPage detail = (MyMasterDetailPage)this.Parent;
            detail.IsPresented = false;
            DependencyService.Get<DependencyServices.iStatusStyle>().StatusStyle(1);
            //0 - type owner
            //1 - ID
            detail.Detail = new NavigationPage(new Views.ProfilePage(0, 1));
        }

        async void OnLoggedOut_Clicked(Object sender, EventArgs e)
        {
            if (isLogout == false)
            {
                isLogout = true;
                if (DataClass.GetInstance.UserIsOnline == 1)
                {
#if DEBUG
                    DataClass.GetInstance.UserIsOnline = 0;
                    SavedProperties_Remove();
                    App.Logout();
                    isLogout = false; 
#else
					var json = JsonConvert.SerializeObject(new { token = DataClass.GetInstance.UserToken });
                    var url = Constants.ROOT_URL + Constants.ACCOUNTS_URL + Constants.STATUS_URL;

					CancellationTokenSource cts;
					cts = new CancellationTokenSource();
					try
					{
                        await webService.PutRequestAsync(url, json, cts.Token);
					}
					catch (OperationCanceledException)
					{
						DependencyService.Get<iConsole>().DisplayText("Web Service: Cancelled!");
					}
					catch (Exception)
					{
						DependencyService.Get<iConsole>().DisplayText("Web Service: Failed!");
					}

					cts = null;
#endif
                }
                else
                {
                    SavedProperties_Remove();
                    isLogout = false;
					App.Logout();
                }
			}
        }

        async void SavedProperties_Remove()
        {
			Application.Current.Properties.Remove("user_token");
			Application.Current.Properties.Remove("user_id");
			Application.Current.Properties.Remove("user_image");
			Application.Current.Properties.Remove("user_is_online");
			Application.Current.Properties.Remove("user_has_stripe");
			Application.Current.Properties.Remove("user_is_confirmed");
			Application.Current.Properties.Remove("user_is_performer");
			Application.Current.Properties.Remove("user_perform_progress");
			Application.Current.Properties.Remove("user_post_progress");
            await Application.Current.SavePropertiesAsync();
        }

		public void ReceiveJSONData(JObject jsonData)
		{

		}

        public void ReceiveJSONData(JObject jsonData, CancellationToken ct)
        {
            if (int.Parse(jsonData["status"].ToString()) == 200 )
            {
                SavedProperties_Remove();
                isLogout = false;
                App.Logout();
            }
            else
            {
                isLogout = false;
            }
        }

        public void ReceiveTimeoutError(string title, string error)
        {
            DisplayAlert(title, error, "Okay");
            isLogout = false;
        }
    }
}
