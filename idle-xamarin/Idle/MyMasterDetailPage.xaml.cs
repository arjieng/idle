﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Xamarin.Forms;
using System.Linq;
using System.Collections.ObjectModel;
using System.Threading;

namespace idle
{
    public partial class MyMasterDetailPage : MasterDetailPage, iRestConnector
    {

#if DEBUG == false
        RestServices webService = new RestServices();
        CancellationTokenSource cts;
#endif
        DataClass dataClass = DataClass.GetInstance;
        public static bool isHomePage;
        public MyMasterDetailPage()
        {

            InitializeComponent();
#if DEBUG == false
            webService.WebServiceDelegate = this;
#endif
            IsGestureEnabled = Device.RuntimePlatform == Device.iOS ? false : true;
            masterPage.ListView.ItemSelected += OnItem_Selected;

#if DEBUG == false
            if (Device.RuntimePlatform == Device.Android)
            {
                MessagingCenter.Subscribe<Object, string>(this, "StripeConnectCode_Droid", async (sender, code) =>
                {
                    webService.WebServiceDelegate = this;
                    var json = JsonConvert.SerializeObject(new { token = DataClass.GetInstance.UserToken, stripe_account_id = code });

					cts = new CancellationTokenSource();
					try
					{
                        await webService.PostRequestAsync(Constants.ROOT_URL + Constants.STRIPE_URL, json, cts.Token);
					}
					catch (OperationCanceledException)
					{
						DependencyService.Get<iConsole>().DisplayText("Web Service: Cancelled!");
					}
					catch (Exception)
					{
						DependencyService.Get<iConsole>().DisplayText("Web Service: Failed!");
					}

					cts = null;
		        });
			}
#endif
		}

        protected override void OnAppearing()
        {
            base.OnAppearing();
            MessagingCenter.Unsubscribe<Object, string>(this, "ResetPassword");
        }
        
		void OnItem_Selected(object sender, SelectedItemChangedEventArgs e)
		{
			var item = e.SelectedItem as MasterPageItem;
            if(pageType == null)
            {
                pageType = new List<Type>();
                pageType.Add(typeof(Views.HomePage));
            }
            isToastBack = false;
            if (item != null)
            {
                MasterPage.PageSelected.Invoke(this, item.TargetType);
                if (item.TargetType.Equals(typeof(Views.ProfilePage)))
                {
					Detail.Navigation.PushAsync(new Views.ProfilePage(0, DataClass.GetInstance.UserID));
                    stackCount += 1;
                    pageType.Add(typeof(Views.ProfilePage));
                }
                else if (item.TargetType.Equals(typeof(Views.TaskPage)))
                {
                    Detail.Navigation.PushAsync(new Views.TaskPage(6, 2));
                    stackCount += 1;
                    pageType.Add(typeof(Views.TaskPage));
                }
                else if (item.TargetType.Equals(typeof(Views.ConnectStripePage)))
                {
					Page displayPage = (Page)Activator.CreateInstance(typeof(Views.ConnectStripePage), null);
					Detail.Navigation.PushAsync(displayPage);
                    stackCount += 1;
                    pageType.Add(typeof(Views.ConnectStripePage));
                }   
                else
                {
					Page displayPage = (Page)Activator.CreateInstance(item.TargetType);
					Detail.Navigation.PushAsync(displayPage);
                    stackCount += 1;
                    pageType.Add(item.TargetType);
                }
				
				masterPage.ListView.SelectedItem = null;
				IsPresented = false;
			}
        }

        public static int stackCount = 0;
        public static bool isToastBack = false;
        public static List<Type> pageType = null;

        protected override bool OnBackButtonPressed()
        {
            if (!isHomePage)
            {
				base.OnBackButtonPressed();
            }
            else if(IsPresented)
            {
                base.OnBackButtonPressed();
            }
            else
            {
                if(stackCount == 0)
                {
                    if (isToastBack)
                    {
                        isToastBack = false;
						if(DependencyService.Get<ICloseApplication>() != null)
						{
						    DependencyService.Get<ICloseApplication>().ClosingApplication();
						}
					}
                    else
					{
						DependencyService.Get<iToastServices>().DisplayToast("Press back again to close the idle app.");
						isToastBack = true;
                        pageType = null;
					}
                }
                else
                {
                    stackCount -= 1;
                    MasterPage.PageSelected.Invoke(this, pageType[stackCount]);
                    base.OnBackButtonPressed();
                }
            }
            return true;
        }

		public void ReceiveJSONData(JObject jsonData)
		{

		}

        public async void ReceiveJSONData(JObject jsonData, CancellationToken ct)
        {
            if(int.Parse(jsonData["status"].ToString()) == 200)
            {
                Application.Current.Properties["user_has_stripe"] = (dataClass.UserHasStripe == 0 ? 0: 1) ;
				await Application.Current.SavePropertiesAsync();
				Detail = new NavigationPage((Page)Activator.CreateInstance(typeof(Views.ConnectStripePage)));
            }
            else
            {
                
            }
        }

        public void ReceiveTimeoutError(string title, string error)
        {
           DisplayAlert(title,error,"Okay");
        }
    }
}
