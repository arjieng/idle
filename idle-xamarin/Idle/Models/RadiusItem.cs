﻿using System;
using System.ComponentModel;
using System.Runtime.CompilerServices;

namespace idle
{
    public class RadiusItem : INotifyPropertyChanged
    {
        bool _isSelected;
        public bool isSelected { get { return _isSelected; } set { _isSelected = value; OnPropertyChanged(); } }
        public int radius { get; set; }

        public event PropertyChangedEventHandler PropertyChanged;
		protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = "")
		{
			PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
		}
    }
}
