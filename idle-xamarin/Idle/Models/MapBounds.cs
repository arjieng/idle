﻿using System;
using Xamarin.Forms.GoogleMaps;

namespace idle
{
    public class MapBounds
    {
		public double neLatitude { get; set; }
		public double neLongitude { get; set; }
		public double swLatitude { get; set; }
		public double swLongitude { get; set; }

		public void Update(MapBounds bounds)
		{
			neLatitude = bounds.neLatitude;
			neLongitude = bounds.neLongitude;
			swLatitude = bounds.swLatitude;
			swLongitude = bounds.swLongitude;
		}

        public static explicit operator MapBounds(Bounds v)
        {
            return new MapBounds()
            {
				neLatitude = v.NorthEast.Latitude,
				neLongitude = v.NorthEast.Longitude,
				swLatitude = v.SouthWest.Latitude,
				swLongitude = v.SouthWest.Longitude
			};
        }
    }
}
