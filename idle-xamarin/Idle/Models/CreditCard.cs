﻿using System;

namespace idle
{
    public class CreditCard
    {
		public string Numbers { get; set; }
		public string Month { get; set; }
		public string Year { get; set; }
		public string Cvc { get; set; }

		public CreditCard()
		{
			Numbers = "";
			Month = "";
			Year = "";
			Cvc = "";
		}
    }
}
