﻿using System;
using Xamarin.Forms;

namespace idle
{
    public class SettingsItem
    {
		public string title { get; set; }
        public Type TargetType { get; set; }
        public ImageSource icon { get; set; }
	}

    public class ShareIdleItem
    {
		public string title { get; set; }
		public int type { get; set; }
		public ImageSource icon { get; set; }
    }
}
