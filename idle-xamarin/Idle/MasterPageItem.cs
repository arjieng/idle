﻿using System;
using System.ComponentModel;

namespace idle
{
    public class MasterPageItem : INotifyPropertyChanged
    {
        public string Title { get; set; }
        public Type TargetType { get; set; }
        public int _isSelected;

        public int IsSelected { 
            get { return _isSelected; }
            set { _isSelected = value; OnPropertyChanged("IsSelected");} }

        public event PropertyChangedEventHandler PropertyChanged;

        protected virtual void OnPropertyChanged(string propertyName) 
        { 
            if (PropertyChanged != null )
            {
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName)); 
            } 
        }
    }
}
