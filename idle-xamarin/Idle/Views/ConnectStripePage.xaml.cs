﻿using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Xamarin.Forms;

namespace idle.Views
{
    public partial class ConnectStripePage : RootViewPage, iRestConnector
    {
        bool isClicked = true;
        DataClass dataClass = DataClass.GetInstance;
        CancellationTokenSource cts;
#if DEBUG == false
        RestServices webService = new RestServices();
        string stripeLink;
#endif
        int serviceType;

        public ConnectStripePage()
        {
            InitializeComponent();

            this.PageTitle = "STRIPE CONNECT";
            this.TitleFontFamily = Constants.LULO;
            this.TitleFontColor = Constants.DARK_STRONG;
            this.LeftIcon = "BurgerDark";
            this.LeftButtonCommand = new Command((obj) => { MyMasterDetailPage detail = (MyMasterDetailPage)this.Parent.Parent; detail.IsPresented = true; DependencyService.Get<DependencyServices.iStatusStyle>().StatusStyle(0); });

            BindingContext = new { hasStripe = dataClass.UserHasStripe };

            if (Device.RuntimePlatform == Device.iOS)
            {
                MessagingCenter.Subscribe<Object, string>(this, "StripeConnectCode", (sender, code) =>
                {
#if DEBUG == false
					Stripe_Connect(code);
#endif
                });
            }
            RequestStripe();
        }

        public ConnectStripePage(string stripeCode = null)
        {
            InitializeComponent();

            this.PageTitle = "STRIPE CONNECT";
            this.TitleFontFamily = Constants.LULO;
            this.TitleFontColor = Constants.DARK_STRONG;
            this.LeftIcon = "BurgerDark";
            this.LeftButtonCommand = new Command((obj) => { MyMasterDetailPage detail = (MyMasterDetailPage)this.Parent.Parent; detail.IsPresented = true; DependencyService.Get<DependencyServices.iStatusStyle>().StatusStyle(0); });

            BindingContext = new { hasStripe = dataClass.UserHasStripe };

            if (Device.RuntimePlatform == Device.iOS)
            {
                MessagingCenter.Subscribe<Object, string>(this, "StripeConnectCode", (sender, code) =>
                {
#if DEBUG == false
					Stripe_Connect(code);
#endif
                });
            }
            if (string.IsNullOrEmpty(stripeCode))
            {
                RequestStripe();
            }
            else
            {
                Stripe_Connect(stripeCode);
            }
        }

        protected override void OnAppearing()
        {
            base.OnAppearing();

            MyMasterDetailPage.isHomePage = true;

#if DEBUG == false
			webService.WebServiceDelegate = this;
#endif
            DependencyService.Get<DependencyServices.iStatusStyle>().StatusStyle(1);
        }

        void StripeLine_Tapped(object sender, System.EventArgs e)
        {
            Device.OpenUri(new Uri("https://stripe.com/about"));
        }

        async void Stripe_Connect(string code)
        {
            serviceType = 1;
#if DEBUG == false
			webService.WebServiceDelegate = this;
			var json = JsonConvert.SerializeObject(new { token = DataClass.GetInstance.UserToken, stripe_account_id = code });
			
			cts = new CancellationTokenSource();
			try
			{
                await webService.PostRequestAsync(Constants.ROOT_URL + Constants.STRIPE_URL, json, cts.Token);
			}
			catch (OperationCanceledException)
			{
				DependencyService.Get<iConsole>().DisplayText("Web Service: Cancelled!");
			}
			catch (Exception)
			{
				DependencyService.Get<iConsole>().DisplayText("Web Service: Failed!");
			}

			cts = null;
#endif
        }

        protected override void OnDisappearing()
        {
            base.OnDisappearing();
            MyMasterDetailPage.isHomePage = false;
            MessagingCenter.Unsubscribe<Object, string>(this, "StripeConnectCode");
        }

        async void RequestStripe()
        {
#if DEBUG == false
            webService.WebServiceDelegate = this;
            serviceType = 0;
			
			cts = new CancellationTokenSource();
			try
			{
                await webService.GetRequest(Constants.ROOT_URL + Constants.STRIPE_URL + "/link?token=" + dataClass.UserToken, cts.Token);
			}
			catch (OperationCanceledException)
			{
				DependencyService.Get<iConsole>().DisplayText("Web Service: Cancelled!");
			}
			catch (Exception)
			{
				DependencyService.Get<iConsole>().DisplayText("Web Service: Failed!");
			}

			cts = null;
#endif
        }

        async void OnConnectStripeButton_Clicked(object sender, EventArgs e)
        {
            if (isClicked == false)
            {
                isClicked = true;

                if (dataClass.UserHasStripe == 0)
                {
#if DEBUG
					Application.Current.Properties["user_has_stripe"] = 1;
					BindingContext = new { hasStripe = dataClass.UserHasStripe };
					//MyMasterDetailPage detail = (MyMasterDetailPage)this.Parent.Parent;
					//detail.Detail = new NavigationPage(new Views.HomePage());

					await Application.Current.SavePropertiesAsync();
#else
                    //serviceType = 1;
                    Device.BeginInvokeOnMainThread( () =>Device.OpenUri(new Uri(stripeLink)));
#endif
                }
                else
                {
#if DEBUG
					await DisplayAlert("Stripe Disconnected", "Stripe disconnected", "Okay");
					Application.Current.Properties["user_has_stripe"] = 0;
					BindingContext = new { hasStripe = dataClass.UserHasStripe };

					await Application.Current.SavePropertiesAsync();
#else
                    serviceType = 2;

                    var json = JsonConvert.SerializeObject(new { token = dataClass.UserToken });
                    var url = Constants.ROOT_URL + Constants.ACCOUNTS_URL + Constants.STRIPE_URL + "/disconnect";
					
					cts = new CancellationTokenSource();
					try
					{
                        await webService.PostRequestAsync(url, json, cts.Token);
					}
					catch (OperationCanceledException)
					{
						DependencyService.Get<iConsole>().DisplayText("Web Service: Cancelled!");
					}
					catch (Exception)
					{
						DependencyService.Get<iConsole>().DisplayText("Web Service: Failed!");
					}

					cts = null;
#endif
                }
				await Task.Delay(1000);
				isClicked = false;
            }
        }

		public void ReceiveJSONData(JObject jsonData)
		{

		}

        public void ReceiveJSONData(JObject jsonData, CancellationToken ct)
        {
            //DisplayAlert("Alert" , "Connected to stripe","Okay");
            if (int.Parse(jsonData["status"].ToString()) == 200)
            {
                switch (serviceType)
                {
                    case 0: // requesting for stripe
#if DEBUG == false
                        stripeLink = jsonData["url"].ToString();
#endif
                        isClicked = false;
                        break;
                    case 1:// connecting stripe
                        Application.Current.Properties["user_has_stripe"] = 1;
                        BindingContext = new { hasStripe = dataClass.UserHasStripe };
                        //MyMasterDetailPage detail = (MyMasterDetailPage)this.Parent.Parent;
                        //detail.Detail = new NavigationPage(new Views.HomePage());

                        Application.Current.SavePropertiesAsync();
						break;
                    case 2: // disconnecting stripe
                        DisplayAlert("Stripe Disconnected","Stripe disconnected", "Okay");
                        Application.Current.Properties["user_has_stripe"] = 0;
                        BindingContext = new { hasStripe = dataClass.UserHasStripe };

                        Application.Current.SavePropertiesAsync();
                        break;
                }
            }
            else
            {
                DisplayAlert("Alert", jsonData["error"].ToString(), "Okay");
                isClicked = false;
            }
        }

        public async void ReceiveTimeoutError(string title, string error)
        {
            await DisplayAlert(title, error, "Okay");
        }
    }
}
