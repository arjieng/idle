﻿﻿using System;
using System.Collections.Generic;
using Xamarin.Forms;

namespace idle.Views
{
    public partial class SettingsPage : RootViewPage
    {
        public SettingsPage()
        {
            InitializeComponent();

			this.PageTitle = "SETTINGS";
			this.TitleFontFamily = Constants.LULO;
			this.TitleFontColor = Constants.DARK_STRONG;
			this.LeftIcon = "BurgerDark";
			this.LeftButtonCommand = new Command((obj) => { MyMasterDetailPage detail = (MyMasterDetailPage)this.Parent.Parent; detail.IsPresented = true; DependencyService.Get<DependencyServices.iStatusStyle>().StatusStyle(0); });

            var settingsItem = new List<SettingsItem>();
            // add more item here
            settingsItem.Add(new SettingsItem { title = "Get In Touch", TargetType = typeof(GetInTouchPage) , icon="Touch" });
            settingsItem.Add(new SettingsItem { title = "Pricing", TargetType = typeof(PricingPage) , icon = "Price" });
            settingsItem.Add(new SettingsItem { title = "Tutorials", TargetType = typeof(GeneralTutorialPage), icon = "Tutorial" });
            settingsItem.Add(new SettingsItem { title = "Update Account", TargetType = typeof(UpdateAccountPage), icon = "Edit" });
            settingsItem.Add(new SettingsItem { title = "Cancel Account", TargetType = typeof(CancelAccountPage) , icon = "Cancel" });

            settings.ItemsSource = settingsItem;
        }

        protected override void OnAppearing()
        {
            base.OnAppearing();
            MyMasterDetailPage.isHomePage = true;
            DependencyService.Get<DependencyServices.iStatusStyle>().StatusStyle(1);
        }

        protected override void OnDisappearing()
        {
            base.OnDisappearing();
            MyMasterDetailPage.isHomePage = false;
        }

		void OnSettings_ItemTapped(object sender, ItemTappedEventArgs e)
		{
            settings.SelectedItem = null;
            var item = (SettingsItem)e.Item;
            if (item.title.Equals("Tutorials"))
            {
                Navigation.PushModalAsync((Page)Activator.CreateInstance(((SettingsItem)e.Item).TargetType));
            }
            else
            {
                Navigation.PushAsync((Page)Activator.CreateInstance(((SettingsItem)e.Item).TargetType));
            }
		}
    }
}
