﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Xamarin.Forms;

namespace idle.Views
{
    public partial class ViewTaskPage : RootViewPage, iRestConnector
    {
        DataClass dataClass = DataClass.GetInstance;
        int serviceType = 0, pageType = 0, offSet = 0;
        Tasks taskData;
        CancellationTokenSource cts;
#if DEBUG == false
        RestServices webService = new RestServices();
#endif

        public ViewTaskPage(int offSet, Tasks task, int page)
        {
            InitializeComponent();

            this.TitleFontFamily = Constants.LULO;
            this.TitleFontColor = Constants.DARK_STRONG;
            this.LeftIcon = "ArrowDark";
            this.LeftButtonCommand = new Command((obj) => { Navigation.PopAsync(); });
            this.PageTitle = "VIEW TASK";
            //set page where it came from 
            //0: Task Offers
            //1: Task Categories Page 
            //2: Map Home Page
            //3: My Task Page
            //5: Profile Page
            pageType = page;
            taskData = task;
            this.offSet = offSet;
            BindingContext = taskData;

#if DEBUG == false
            webService.WebServiceDelegate = this;
			
            Task.Run(async () => 
            {
    			cts = new CancellationTokenSource();
    			try
    			{
                     await webService.GetRequest(Constants.ROOT_URL + Constants.FETCH_TASK_URL + "/" + taskData.id + "?token=" + dataClass.UserToken, cts.Token); 
    			}
    			catch (OperationCanceledException)
    			{
    				DependencyService.Get<iConsole>().DisplayText("Web Service: Cancelled!");
    			}
    			catch (Exception)
    			{
    				DependencyService.Get<iConsole>().DisplayText("Web Service: Failed!");
    			}

    			cts = null;
            });
#endif
            MessagingCenter.Unsubscribe<Object, string>(this, "StripeConnectCode");
#if DEBUG
            MessagingCenter.Subscribe<Object, string>(this, "StripeConnectCode", (sender, code) =>
#else
            MessagingCenter.Subscribe<Object, string>(this, "StripeConnectCode", async (sender, code) =>
#endif
            {
#if DEBUG == false
                serviceType = 9;
				webService.WebServiceDelegate = this;
				var json = JsonConvert.SerializeObject(new { token = DataClass.GetInstance.UserToken, stripe_account_id = code });
				
				cts = new CancellationTokenSource();
				try
				{
                    await webService.PostRequestAsync(Constants.ROOT_URL + Constants.STRIPE_URL, json, cts.Token);
				}
				catch (OperationCanceledException)
				{
					DependencyService.Get<iConsole>().DisplayText("Web Service: Cancelled!");
				}
				catch (Exception)
				{
					DependencyService.Get<iConsole>().DisplayText("Web Service: Failed!");
				}

				cts = null;
#endif
			});
        }

        protected override void OnAppearing()
        {
            base.OnAppearing();

			MessagingCenter.Subscribe<App>(this, "BiddingAccepted", (App) =>
			{
				if ((dataClass.InProgressPerformList[0]).id == taskData.id)
				{
					if (pageType == 1)
					{
						TaskPage.categoryTaskCount -= 1;
					}

					OnTaskList_Removed(true);
				}
			});

			MessagingCenter.Subscribe<App, JObject>(this, "BiddingDeclined", (sender, obj) =>
			{
				Device.BeginInvokeOnMainThread(async () =>
				{
					if (int.Parse(obj["custom_data"]["task_id"].ToString()) == taskData.id)
					{
						if (pageType == 3)
						{
							if (dataClass.TaskList.ToList().Exists(bidTask => bidTask.id == int.Parse(obj["custom_data"]["task_id"].ToString())))
							{
								dataClass.TaskList.Remove(taskData);
							}
							await Navigation.PopAsync();
						}
						else if (pageType == 2 || pageType == 1 || pageType == 0)
						{
							taskData.bid = "0.00";
							BindingContext = taskData;

							if (pageType == 0)
							{
								if (dataClass.TaskList.ToList().Exists(selectedTask => selectedTask.id == taskData.id))
								{
									(dataClass.TaskList.ToList().Find(selectedTask => selectedTask.id == taskData.id)).Update(taskData);
								}
							}
						}
					}
				});
			});

			MessagingCenter.Subscribe<App, JObject>(this, "TaskCompleted", (sender, obj) =>
			{
				if (int.Parse(obj["custom_data"]["task_id"].ToString()) == taskData.id)
				{
					taskData.status = int.Parse(obj["custom_data"]["status"].ToString());
					BindingContext = taskData;
				}
			});

			MessagingCenter.Subscribe<App, JObject>(this, "TaskFinished", (sender, obj) =>
			{
				if (int.Parse(obj["custom_data"]["task_id"].ToString()) == taskData.id && (pageType == 3 || pageType == 2))
				{
					taskData.status = int.Parse(obj["custom_data"]["status"].ToString());
					BindingContext = taskData;
					if (dataClass.InProgressPerformList.ToList().Exists(inProgressTask => inProgressTask.id == taskData.id))
					{
						dataClass.InProgressPerformList.Remove(dataClass.InProgressPerformList.ToList().Find(inProgressTask => inProgressTask.id == taskData.id));
					}
					Navigation.PopAsync();
				}
			});

			MessagingCenter.Subscribe<App, JObject>(this, "TaskDeleted", (sender, obj) =>
			{
				if (int.Parse(obj["custom_data"]["task_id"].ToString()) == taskData.id && (pageType == 3 || pageType == 2 || pageType == 1 || pageType == 0))
				{
					if (dataClass.TaskList.ToList().Exists(bidTask => bidTask.id == int.Parse(obj["custom_data"]["task_id"].ToString())))
					{
						dataClass.TaskList.Remove(taskData);
					}

					if (pageType == 0)
					{
						if (dataClass.TaskOffersList.ToList().Exists(bidTask => bidTask.id == (dataClass.InProgressPerformList[0]).id))
						{
							dataClass.TaskOffersList.Remove(dataClass.TaskOffersList.ToList().Find(bidTask => bidTask.id == (dataClass.InProgressPerformList[0]).id));
						}
					}

					Navigation.PopAsync();
				}
			});

			MessagingCenter.Subscribe<App, JObject>(this, "TaskCancelled", (sender, obj) =>
			{
				if (int.Parse(obj["custom_data"]["task_id"].ToString()) == taskData.id)
				{
					taskData.status = int.Parse(obj["custom_data"]["status"].ToString());
					if (taskData.status == 0)
					{
						taskData.bid = "0.00";
					}
					BindingContext = taskData;
				}
			});

            DependencyService.Get<DependencyServices.iStatusStyle>().StatusStyle(1);
        }

        protected override void OnDisappearing()
        {
            base.OnDisappearing();
            MessagingCenter.Unsubscribe<Object, string>(this, "StripeConnectCode");
            MessagingCenter.Unsubscribe<App>(this, "BiddingAccepted");
            MessagingCenter.Unsubscribe<App, JObject>(this, "BiddingDeclined");
            MessagingCenter.Unsubscribe<App, JObject>(this, "TaskCompleted");
            MessagingCenter.Unsubscribe<App, JObject>(this, "TaskFinished");
            MessagingCenter.Unsubscribe<App, JObject>(this, "TaskDeleted");
            MessagingCenter.Unsubscribe<App, JObject>(this, "TaskCancelled");

			if (cts != null)
			{
				cts.Cancel();
			}
        }

        async void OnMessage_Clicked(object sender, idle.ImageTappedEventArgs e)
        {
            //taskData.user;
            await Navigation.PushAsync(new MessagePage(UserName.Text, taskData.user,taskData.user.conversation_id));
        }

        async void OnAction_Clicked(object sender, System.EventArgs e)
        {
            var myAction = await DisplayActionSheet(null, "Cancel", null, "Report Task");
            if (myAction != null)
            {
                if (myAction.ToString() == "Report Task")
                {
                    await Navigation.PushAsync(new ReportTaskPage(taskData.id, taskData));
                }
            }
        }

        async void OnAcceptButton_Clicked(object sender, EventArgs e)
        {
            if (((Button)sender).Text == "BID")
            {
                if (dataClass.UserIsPerfomer == 1)
                {
                    if (String.IsNullOrEmpty(bidEntry.Text) == true)
                    {
                        await DisplayAlert("Bid Amount Required", "You must enter an amount before submitting a bid.", "Okay");
                    }
                    else
                    {
                        var myAction = await DisplayActionSheet("Do you want to bid on this task?", "No", null, "Yes");
                        if (myAction != null)
                        {
                            if (myAction.ToString() == "Yes")
                            {
                                if (taskData.payment.Equals("Stripe") && dataClass.UserHasStripe == 0)
                                {
                                    var answer = await DisplayAlert("Stripe Connect Account", "You need to set up your stripe account to allow this payment.", "Later", "Connect Now");
                                    if (answer != true)
                                    {
#if DEBUG == false
										if (cts != null)
										{
											cts.Cancel();
										}
										await Task.Run(async () =>
										{
    										cts = new CancellationTokenSource();
    										try
    										{
												serviceType = 8;
												var url = Constants.ROOT_URL + Constants.STRIPE_URL + "/link?token=" + dataClass.UserToken;
												await webService.GetRequest(url, cts.Token);
    										}
    										catch (OperationCanceledException)
    										{
    											DependencyService.Get<iConsole>().DisplayText("Web Service: Cancelled!");
    										}
    										catch (Exception)
    										{
    											DependencyService.Get<iConsole>().DisplayText("Web Service: Failed!");
    										}
                                            cts = null;
                                        });
#endif
									}
                                }
                                else
                                {
#if DEBUG
									await DisplayAlert("Bid Submitted", "Thank you for bidding. Poster will review your bid.", "Okay");

									taskData.bid_id = 100;
									taskData.bid = bidEntry.Text;
									BindingContext = taskData;

									bidEntry.Text = string.Empty;

									if (pageType == 0)
									{
										if (dataClass.TaskList.ToList().Exists(selectedTask => selectedTask.id == taskData.id))
										{
											(dataClass.TaskList.ToList().Find(selectedTask => selectedTask.id == taskData.id)).Update(taskData);
										}
									}
#else
                                    serviceType = 2;
                                    var json = pageType == 0 ? JsonConvert.SerializeObject(new { bid = new { task_id = taskData.id, user_id = taskData.user.id, amount = double.Parse(bidEntry.Text) }, token = dataClass.UserToken, off_set = offSet }) : JsonConvert.SerializeObject(new { bid = new { task_id = taskData.id, user_id = taskData.user.id, amount = double.Parse(bidEntry.Text) }, token = dataClass.UserToken, off_set = offSet, task = taskData.task_type });
									if (cts != null)
									{
										cts.Cancel();
									}
                                    await Task.Run(async delegate 
                                    {
    									cts = new CancellationTokenSource();
    									try
    									{
                                             await webService.PostRequestAsync(Constants.ROOT_URL + Constants.FETCH_TASK_URL + "/" + taskData.id + "/bids", json, cts.Token); 
    									}
    									catch (OperationCanceledException)
    									{
    										DependencyService.Get<iConsole>().DisplayText("Web Service: Cancelled!");
    									}
    									catch (Exception)
    									{
    										DependencyService.Get<iConsole>().DisplayText("Web Service: Failed!");
    									}

    									cts = null;
                                    });
#endif
                                }
							}
                        }
                    }
                }
                else
                {
                    var action = await DisplayAlert("Poster Restriction", "You are currently on posting mode. Do you want to turn on performing mode for you to bid on task?", "Yes", "No");
                    if (action == true)
                    {
#if DEBUG
                        dataClass.UserIsPerfomer = 1;
#else
                        serviceType = 3;
						if (cts != null)
						{
							cts.Cancel();
						}
						await Task.Run(async () =>
						{
    						cts = new CancellationTokenSource();
    						try
    						{
								var json = JsonConvert.SerializeObject(new { token = dataClass.UserToken });
								await webService.PutRequestAsync(Constants.ROOT_URL + Constants.ACCOUNTS_URL + Constants.CHANGE_ROLE_URL, json, cts.Token);
    						}
    						catch (OperationCanceledException)
    						{
    							DependencyService.Get<iConsole>().DisplayText("Web Service: Cancelled!");
    						}
    						catch (Exception)
    						{
    							DependencyService.Get<iConsole>().DisplayText("Web Service: Failed!");
    						}

    						cts = null;
                        });
#endif
						OnAcceptButton_Clicked(sender, e);
                    }
                }
            }
            else if (((Button)sender).Text == "COMPLETE")
            {
				var myAction = await DisplayActionSheet("Are you sure you want to mark task as completed?", "Not Yet Done", null, "Yes");
                if (myAction != null)
                {
                    if (myAction.ToString() == "Yes")
                    {
#if DEBUG
                        int ListIndex = dataClass.InProgressPerformList.IndexOf(taskData);
                        dataClass.InProgressPerformList[ListIndex] = taskData;

						await DisplayAlert("Mark Task as Complete", taskData.user.name + " will be notified that you have completed the task. Tune in for confirmation", "Okay");
						taskData.status = 4;
						BindingContext = taskData;
#else
						serviceType = 6;
						var json = JsonConvert.SerializeObject(new { token = dataClass.UserToken });
						if (cts != null)
						{
							cts.Cancel();
						}
                        await Task.Run(async delegate 
                        {
    						cts = new CancellationTokenSource();
    						try
    						{
                                 await webService.PutRequestAsync(Constants.ROOT_URL + Constants.ACCOUNTS_URL + Constants.FETCH_TASK_URL + "/" + taskData.id + "/completed", json, cts.Token); 
    						}
    						catch (OperationCanceledException)
    						{
    							DependencyService.Get<iConsole>().DisplayText("Web Service: Cancelled!");
    						}
    						catch (Exception)
    						{
    							DependencyService.Get<iConsole>().DisplayText("Web Service: Failed!");
    						}

    						cts = null;
                        });
#endif
					}
                }
			}
        }

        async void OnIgnoreButton_Clicked(object sender, EventArgs e)
        {
            if (((Button)sender).Text == "IGNORE")
            {
                var myAction = await DisplayActionSheet("Are you sure you want to ignore task?", "No", null, "Yes");
                if (myAction != null)
                {
                    if (myAction.ToString() == "Yes")
                    {
#if DEBUG
                        OnTaskList_Removed(true);
#else
                        serviceType = 1;
						var json = pageType == 0 ? JsonConvert.SerializeObject(new { token = dataClass.UserToken, off_set = offSet, radius = dataClass.Radius, type = 1 }) : JsonConvert.SerializeObject(new { token = dataClass.UserToken, off_set = offSet, radius = dataClass.Radius, type = 1, task = taskData.task_type });
						if (cts != null)
						{
							cts.Cancel();
						}
                        await Task.Run(async delegate 
                        {
    						cts = new CancellationTokenSource();
    						try
    						{
                                 await webService.PostRequestAsync(Constants.ROOT_URL + Constants.FETCH_TASK_URL + "/" + taskData.id + "/ignores", json, cts.Token); 
    						}
    						catch (OperationCanceledException)
    						{
    							DependencyService.Get<iConsole>().DisplayText("Web Service: Cancelled!");
    						}
    						catch (Exception)
    						{
    							DependencyService.Get<iConsole>().DisplayText("Web Service: Failed!");
    						}

    						cts = null;
                        });
#endif
					}
                }
            }
            else if (((Button)sender).Text == "CANCEL")
            {
                var AlertString = "Are you sure you want to cancel this task? " + taskData.user.name + " will be notified to authorize cancellation.";
                var action = await DisplayAlert("Cancel Task?", AlertString, "No", "Yes");
                if (action == false)
                {
#if DEBUG
                    dataClass.InProgressPerformList.Remove((Tasks)BindingContext);
                    await Navigation.PopAsync();
#else
                    serviceType = 5;
					//API for cancel task
					var json = JsonConvert.SerializeObject(new { token = dataClass.UserToken, type = 0, status = 2 });
					if (cts != null)
					{
						cts.Cancel();
					}
					await Task.Run(async delegate
					{
    					cts = new CancellationTokenSource();
    					try
    					{
    						await webService.PutRequestAsync(Constants.ROOT_URL + Constants.ACCOUNTS_URL + Constants.FETCH_TASK_URL + "/" + taskData.id + "/cancel", json, cts.Token);
    					}
    					catch (OperationCanceledException)
    					{
    						DependencyService.Get<iConsole>().DisplayText("Web Service: Cancelled!");
    					}
    					catch (Exception)
    					{
    						DependencyService.Get<iConsole>().DisplayText("Web Service: Failed!");
    					}

    					cts = null;
                    });
#endif
				}
            }
        }

        async void OnCancelTaskButton_Clicked(object sender, EventArgs e)
        {
            Button button = (Button)sender;
            if (button.Text.Equals("SUBMIT"))
            {
#if DEBUG
                await DisplayAlert("Task Finish", "Thank you for performing this task. Check out more posted task on map and bid again!", "Okay");

                dataClass.InProgressPerformList.Remove((Tasks)BindingContext);
                dataClass.PinToRemove = new Tasks();
                dataClass.PinToRemove = (Tasks)BindingContext;
                dataClass.PinTypeToRemove = 0;
                await Navigation.PopAsync();
#else
                serviceType = 7;
                //API for submit rate
                var json = JsonConvert.SerializeObject(new { token = dataClass.UserToken, ratings = new { user_id = taskData.user.id, rate = RateMe.Rating } });
				if (cts != null)
				{
					cts.Cancel();
				}
				await Task.Run(async delegate
				{
    				cts = new CancellationTokenSource();
    				try
    				{
    					await webService.PutRequestAsync(Constants.ROOT_URL + Constants.ACCOUNTS_URL + Constants.FETCH_TASK_URL + "/" + taskData.id + "/ratings", json, cts.Token);
    				}
    				catch (OperationCanceledException)
    				{
    					DependencyService.Get<iConsole>().DisplayText("Web Service: Cancelled!");
    				}
    				catch (Exception)
    				{
    					DependencyService.Get<iConsole>().DisplayText("Web Service: Failed!");
    				}

    				cts = null;
                });
#endif
            }
            else if (button.Text.Equals("CANCEL"))
            {
                var myAction = await DisplayActionSheet("Do you want to cancel your bid?", "No", "Proceed");
                if (myAction != null)
                {
                    if (myAction.ToString() == "Proceed")
                    {
#if DEBUG
                        OnTaskList_Removed(true);
#else
                        serviceType = 4;
                        //API for cancel bid
                        var json = JsonConvert.SerializeObject(new { token = dataClass.UserToken, off_set = offSet });
						if (cts != null)
						{
							cts.Cancel();
						}
						await Task.Run(async delegate
						{
    						cts = new CancellationTokenSource();
    						try
    						{
    							await webService.PutRequestAsync(Constants.ROOT_URL + Constants.ACCOUNTS_URL + Constants.BID_URL + "/" + taskData.bid_id + "/cancel", json, cts.Token);
    						}
    						catch (OperationCanceledException)
    						{
    							DependencyService.Get<iConsole>().DisplayText("Web Service: Cancelled!");
    						}
    						catch (Exception)
    						{
    							DependencyService.Get<iConsole>().DisplayText("Web Service: Failed!");
    						}

    						cts = null;
                        });
#endif
                    }
                }
            }
            else if (button.Text.Equals("CONFIRM CANCELLATION"))
            {
                var action = await DisplayAlert("Confirm Cancellation?", "Are you sure you want to cancel this task offered by " + taskData.user.name + "?", "No", "Yes");
                var json = "";
                if (action == false)
                {
                    serviceType = 10;
                    json = JsonConvert.SerializeObject(new { token = dataClass.UserToken, type = 1, status = 0 });
                }
                else
                {
                    serviceType = 11;
                    json = JsonConvert.SerializeObject(new { token = dataClass.UserToken, type = 1, status = 1 });
                }
#if DEBUG == false
				if (cts != null)
				{
					cts.Cancel();
				}
				await Task.Run(async delegate
				{
    				cts = new CancellationTokenSource();
    				try
    				{
    					await webService.PutRequestAsync(Constants.ROOT_URL + Constants.ACCOUNTS_URL + Constants.FETCH_TASK_URL + "/" + taskData.id + "/cancel", json, cts.Token);
    				}
    				catch (OperationCanceledException)
    				{
    					DependencyService.Get<iConsole>().DisplayText("Web Service: Cancelled!");
    				}
    				catch (Exception)
    				{
    					DependencyService.Get<iConsole>().DisplayText("Web Service: Failed!");
    				}

    				cts = null;
                });
#endif
            }
        }

        void OnTaskList_Removed(bool IsPopAsync){

            if (pageType == 0)
            {
                if (dataClass.TaskList.Count > 0)
                {
                    dataClass.TaskList.RemoveAt(dataClass.TaskList.ToList().FindIndex((obj) => obj.id == taskData.id));
                }
                dataClass.PinToRemove = new Tasks();
                dataClass.PinToRemove = taskData;
                dataClass.PinTypeToRemove = 0;
                if (dataClass.TaskOffersList != null)
                {
                    dataClass.TaskOffersList.Remove((Tasks)BindingContext);
                }
            }
            else if (pageType == 1)
            {
                dataClass.PinToRemove = new Tasks();
                dataClass.PinToRemove = taskData;
                dataClass.PinTypeToRemove = 0;
                dataClass.TaskList.Remove((Tasks)BindingContext);
            }
            else if (pageType == 2)
            {
                dataClass.TaskList.Remove(taskData);
                dataClass.PinToRemove = new Tasks();
                dataClass.PinToRemove = taskData;
                dataClass.PinTypeToRemove = 0;
            }
            else if (pageType == 3)
            {
				if (dataClass.TaskList.Count > 0)
				{
					dataClass.TaskList.RemoveAt(dataClass.TaskList.ToList().FindIndex((obj) => obj.id == taskData.id));
				}
            }

            if(IsPopAsync)
            {
                Navigation.PopAsync();    
            }
        }

		void OnProfile_Tapped(object sender, EventArgs e)
		{
			Navigation.PushAsync(new ProfilePage(1, taskData.user.id));
		}

        void OnEntry_TextChanged(object sender, Xamarin.Forms.TextChangedEventArgs e)
        {
            double amount;
			var isValid = double.TryParse(e.NewTextValue, out amount);

			if (!isValid && bidEntry.Text.Length > 1)
			{
				bidEntry.Text = e.OldTextValue;
			}
            else
            {
                bidEntry.Text = e.NewTextValue;
            }
            bidEntry.WidthRequest = bidEntry.Text.Length > 4 ? (bidEntry.Text.Length - 4) * (bidEntry.FontSize * 0.5) + 40 : 40;
        }

		public void ReceiveJSONData(JObject jsonData)
		{

		}

        public void ReceiveJSONData(JObject jsonData, CancellationToken ct)
		{
            Device.BeginInvokeOnMainThread(async () =>{
				if (int.Parse(jsonData["status"].ToString()) == 300)
                {
                    
                }
                else
				{
                    int HTTPStatus = int.Parse(jsonData["status"].ToString());
                    int ListIndex = 0;
                    if((serviceType == 1 || serviceType == 2 || serviceType == 4) && HTTPStatus == 201)
                    {
                        var task = JsonConvert.DeserializeObject<Tasks>(jsonData["task"].ToString());
						if (pageType == 0)
                        {
                            dataClass.TaskOffersList.Add(task);   
                        }
                        else if(pageType == 1)
                        {
                            dataClass.TaskList.Add(task);    
						}
                    }

					switch (serviceType)
					{
						case 0: //fetch no alert
                            taskData.Update(JsonConvert.DeserializeObject<Tasks>(jsonData["task"].ToString()));
                            this.BindingContext = taskData;

							break;
                        case 1:  //ignore task API no alert
							if (pageType == 1 && HTTPStatus == 200)
							{
								TaskPage.categoryTaskCount -= 1;
							}
							OnTaskList_Removed(true);

                            break;
                        case 2: //accept task API alert
                            await DisplayAlert("Bid Submitted", "Thank you for bidding. Poster will review your bid.", "Okay");

                            taskData.bid_id = int.Parse(jsonData["bid_id"].ToString());
							taskData.bid = bidEntry.Text;
                            BindingContext = taskData;

                            bidEntry.Text = string.Empty;

                            if (pageType == 0)
                            {
								if (dataClass.TaskList.ToList().Exists(selectedTask => selectedTask.id == taskData.id))
								{
									(dataClass.TaskList.ToList().Find(selectedTask => selectedTask.id == taskData.id)).Update(taskData);
								}
                            }

							//if (pageType == 1 && HTTPStatus == 200)
							//{
							//	TaskPage.categoryTaskCount -= 1;
							//}
							//OnTaskList_Removed(true);

							break;
						case 3: //Change Status API no alert
                            dataClass.UserIsPerfomer = 1;

							break;
                        case 4: //Cancel Bid API no alert
                        	//OnTaskList_Removed(true);
							taskData.bid = "0.00";
							BindingContext = taskData;
                            if (pageType == 3)
                            {
                                OnTaskList_Removed(true);
                            }

                            if (pageType == 0)
                            {
								if (dataClass.TaskList.ToList().Exists(selectedTask => selectedTask.id == taskData.id))
								{
									(dataClass.TaskList.ToList().Find(selectedTask => selectedTask.id == taskData.id)).Update(taskData);
								}
                            }

							break;
						case 5: //Cancel Task API with alert
                            await DisplayAlert("Task Cancelled", jsonData["success"].ToString(), "Okay");
							taskData.status = 2;
							BindingContext = taskData;

							break;
                        case 6: //Complete Task API with alert
							ListIndex = dataClass.InProgressPerformList.IndexOf(taskData);
							dataClass.InProgressPerformList[ListIndex] = taskData;

                            await DisplayAlert("Mark Task as Complete", jsonData["success"].ToString(), "Okay");
							taskData.status = 4;
							BindingContext = taskData;

                            break;
                        case 7: //SUBMIT COMPLETE TASK no alert
                            taskData.status = 5;
                            taskData.has_rate = 1;
                            BindingContext = taskData;

							break;
                        case 8: // open uri
                            Device.OpenUri(new Uri(jsonData["url"].ToString()));

                            break;
						case 9: //connecting to stripe
							Application.Current.Properties["user_has_stripe"] = 1;
							break;

						case 10: //allow cancellation with alert
							if (dataClass.InProgressPerformList.ToList().Exists(inProgressTask => inProgressTask.id == taskData.id))
							{
								dataClass.InProgressPerformList.Remove(dataClass.InProgressPerformList.ToList().Find(inProgressTask => inProgressTask.id == taskData.id));
							}
							taskData.status = 0;
                            taskData.bid = "0.00";
							BindingContext = taskData;
							var action = await DisplayAlert("Cancellation Allowed", jsonData["success"].ToString(), "No", "Yes");
							if (action == false)
							{
								await Navigation.PushAsync(new ReportTaskPage(taskData.id, taskData, 1));
							}
                            else
                            {
                                await Navigation.PopAsync();
                            }
							break;

						case 11: //deny cancellation with alert
                            await DisplayAlert("Cancellation Denied", jsonData["success"].ToString(), "Okay");
							taskData.status = 1;
							BindingContext = taskData;
							break;
					}
				}
            });
		}

		public void ReceiveTimeoutError(string title, string error)
		{
			Device.BeginInvokeOnMainThread(async delegate
			{
				await DisplayAlert(title, error, "Okay");
			});
		}
    }
}
