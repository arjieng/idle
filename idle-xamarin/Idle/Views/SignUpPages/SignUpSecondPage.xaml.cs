﻿using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Xamarin.Forms;

namespace idle.Views.SignUpPages
{
    public partial class SignUpSecondPage : ContentView, iRestConnector
    {
        DataClass dataClass = DataClass.GetInstance;
        bool isClicked = false;
        public static bool isStripeClick;
        CancellationTokenSource cts;
#if DEBUG == false
		RestServices webService = new RestServices();
#endif

        public SignUpSecondPage()
        {
            InitializeComponent();

#if DEBUG == false
            webService.WebServiceDelegate = this;
#endif
            if (Device.RuntimePlatform == Device.iOS)
            {
                MessagingCenter.Unsubscribe<Object, string>(this, "StripeConnectCode");
                MessagingCenter.Subscribe<Object, string>(this, "StripeConnectCode", async (sender, code) =>
                {
#if DEBUG == false

                    webService.WebServiceDelegate = this;
                    var json = JsonConvert.SerializeObject(new { email = DataClass.GetInstance.email, stripe_account_id = code });

					cts = new CancellationTokenSource();
					try
					{
                        await webService.PostRequestAsync(Constants.ROOT_URL + Constants.STRIPE_URL, json, cts.Token);
					}
					catch (OperationCanceledException)
					{
						DependencyService.Get<iConsole>().DisplayText("Web Service: Cancelled!");
					}
					catch (Exception)
					{
						DependencyService.Get<iConsole>().DisplayText("Web Service: Failed!");
					}

					cts = null;
#endif
                });
            }
		}

        void OnSkipButton_Clicked(object sender, EventArgs e)
        {
            SignUpPage.PositionChange.Invoke(this, 2);
        }

		void StripeLine_Tapped(object sender, System.EventArgs e)
		{
			Device.OpenUri(new Uri("https://stripe.com/about"));
		}

        void OnConnectStripeButton_Clicked(object sender, EventArgs e)
        {
            if (isClicked == false)
            {
                isClicked = true;
#if DEBUG
				SignUpPage.PositionChange.Invoke(this, 2);
#else
				if (Device.RuntimePlatform == Device.Android)
				{
					if (Application.Current.Properties.ContainsKey("user_stripelink"))
					{
                        dataClass.stripeLink = Application.Current.Properties["user_stripelink"].ToString();
					}
				}

				Device.OpenUri(new Uri(dataClass.stripeLink));
				isStripeClick = true;



                Task.Delay(1000);
                isClicked = false;
               
                //string json = JsonConvert.SerializeObject(new { email = dataClass.UserEmail,  stripe_account_id = "" });
                //string url = Constants.ROOT_URL + Constants.STRIPE_URL;
				//await webService.PutRequestAsync(url, json);
                //SignUpPage.PositionChange.Invoke(this, 2);
#endif
			}
            else
            {

            }
        }

		public void ReceiveJSONData(JObject jsonData)
		{

		}

        public void ReceiveJSONData(JObject jsonData, CancellationToken ct)
        {
            if (int.Parse(jsonData["status"].ToString()) == 200)
            {
                SignUpPage.PositionChange.Invoke(this, 2);
            }
            else if (int.Parse(jsonData["status"].ToString()) == 300)
            {
                Application.Current.MainPage.DisplayAlert("Stripe Connect Failed", jsonData["error"].ToString(), "Okay");
            }
            else
            {
                
            }
            isClicked = true;
        }

        public void ReceiveTimeoutError(string title, string error)
        {
            Application.Current.MainPage.DisplayAlert(title, error, "Okay");
        }
    }
}
