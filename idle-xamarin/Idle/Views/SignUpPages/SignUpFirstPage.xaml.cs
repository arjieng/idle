﻿﻿﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Threading;
using System.Threading.Tasks;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Plugin.Geolocator.Abstractions;
using Plugin.Permissions.Abstractions;
using Xamarin.Forms;

namespace idle.Views.SignUpPages
{
    public partial class SignUpFirstPage : ContentView, iRestConnector, iFileConnector
    {
        DataClass dataClass = DataClass.GetInstance;
        LocationHelper locationHelper = LocationHelper.GetInstance;
        bool isClicked = false, isFBClicked = false, isFB_Clicked = false ;
        string provider = "facebook", providerId = "" , password = "9A8759FF7A8B";
        CancellationTokenSource cts;
#if DEBUG
        FileReader fileReader = new FileReader();
#else
		RestServices webService = new RestServices();
#endif

        public SignUpFirstPage()
        {
            InitializeComponent();
#if DEBUG
			fileReader.FileReaderDelegate = this;
#else
			webService.WebServiceDelegate = this;
#endif
			dataClass.InProgressPerformList = new ObservableCollection<Tasks>();
		}

        void SignUpFacebook_Clicked(object sender, System.EventArgs e)
        {
            if (isFB_Clicked == false)
            {
                isFBClicked = true;
                isFB_Clicked = true;
				DependencyService.Get<IFacebookManager>().Login(OnLoginComplete);
            }
        }

		void OnLoginComplete(FacebookUser facebookUser, string message)
		{
            isFB_Clicked = false;
            if (facebookUser != null)
            {
				Device.BeginInvokeOnMainThread(() =>
				{
					this.nameEntry.Text = facebookUser.Name;
					this.emailEntry.Text = facebookUser.Email;
					this.ageEntry.Text = facebookUser.Age;
					providerId = facebookUser.Id;

					this.passwordEntry.IsEnabled = false;
					this.passwordEntry.Placeholder = String.Empty;
					this.repeatPasswordEntry.IsEnabled = false;
					this.repeatPasswordEntry.Placeholder = String.Empty;
				});
            }
            else
            {
                //error in facebook
                isFBClicked = false;
            }
		}

        void EntryError(Entry entry, string errorMessage)
        {
            entry.Text = "";
            entry.PlaceholderColor = Color.Red;
            entry.Placeholder = errorMessage;
        }

        async void OnNextButton_Clicked(object sender, EventArgs e)
        {
            bool isNotError = true;
            if (string.IsNullOrEmpty(nameEntry.Text))
            {
                EntryError(nameEntry, "Name is required!");
                isNotError = false;
            }

            if (string.IsNullOrEmpty(emailEntry.Text))
            {
                EntryError(emailEntry, "Email is required for sign in!");
                isNotError = false;
            }

			if (string.IsNullOrEmpty(this.mobileNumberEntry.Text))
			{
				EntryError(mobileNumberEntry, "Mobile number is required for confirmation!");
				isNotError = false;
			}

            if (isFBClicked == false)
            {
				if (string.IsNullOrEmpty(passwordEntry.Text))
				{
					EntryError(passwordEntry, "Password is required for sign in!");
					isNotError = false;
				}
				else if (!passwordEntry.Text.Equals(repeatPasswordEntry.Text))
				{
					EntryError(passwordEntry, "Password does not match");
                    EntryError(repeatPasswordEntry, "");
					isNotError = false;
				}
            }

            if (isNotError && isClicked == false)
            {
                isClicked = true;
                cts = new CancellationTokenSource();
#if DEBUG
				string json = JsonConvert.SerializeObject(new { user = new { id = 1, name = this.nameEntry.Text, email = this.emailEntry.Text, password = this.passwordEntry.Text,  age = Age_Validation().ToString(), school = this.schoolEntry.Text, mobile = this.mobileNumberEntry.Text, latitude = dataClass.latitude, longitude = dataClass.longitude, perform = new List<Tasks>(), post = new List<Tasks>(), is_performer = 0, has_stripe = 0, is_online = 1, is_confirmed = 1, token = "123456789" }, status = "200", stripe = "" });
                await fileReader.WriteFile("user.json", json, false, cts.Token);
#else
                await FetchLocation();

				await Task.Run(async () =>
				{
    				string json;
    				if (isFBClicked)
                    {
						json = JsonConvert.SerializeObject(new { user = new { name = this.nameEntry.Text, age = Age_Validation(), school = this.schoolEntry.Text, email = this.emailEntry.Text, password = password, mobile = this.mobileNumberEntry.Text, latitude = dataClass.latitude, longitude = dataClass.longitude, provider = provider, provider_id = providerId }, device = new { platform = App.DeviceType, token = App.UDID } });

					}
					else
                    {
						json = JsonConvert.SerializeObject(new { user = new { name = this.nameEntry.Text, age = Age_Validation(), school = this.schoolEntry.Text, email = this.emailEntry.Text, password = this.passwordEntry.Text, mobile = this.mobileNumberEntry.Text, latitude = dataClass.latitude, longitude = dataClass.longitude }, device = new { platform = App.DeviceType, token = App.UDID } });

					}
					string url = Constants.ROOT_URL + Constants.SIGN_UP_URL;
    				
    				try
    				{
    					await webService.PostRequestAsync(url, json, cts.Token);
    				}
    				catch (OperationCanceledException)
    				{
    					DependencyService.Get<iConsole>().DisplayText("Web Service: Cancelled!");
    				}
    				catch (Exception)
    				{
    					DependencyService.Get<iConsole>().DisplayText("Web Service: Failed!");
    				}

    				cts = null;
                });
#endif
            }
        }

		void OnAge_TextChanged(object sender, Xamarin.Forms.TextChangedEventArgs e)
		{
			if (ageEntry.Text.Length > 3)
			{
				ageEntry.Text = e.OldTextValue;
			}
			else
			{
				ageEntry.Text = e.NewTextValue;
			}
		}

		void OnEntryContainer_Tapped(Object sender, EventArgs e)
		{
            var parent = sender as StackLayout;
            var entry = parent.Children[1] as Entry;
			Focus_Entry(entry);
		}

        void OnNameEntry_Completed(Object sender, EventArgs e)
        {
            Focus_Entry(this.ageEntry);
        }

		void OnAgeEntry_Completed(Object sender, EventArgs e)
		{
			Focus_Entry(this.schoolEntry);
		}

        void OnSchoolEntry_Completed(Object sender, EventArgs e)
        {
            Focus_Entry(this.emailEntry);
        }


        void OnEmailEntry_Completed(Object sender, EventArgs e)
        {
            if (isFBClicked == false)
            {
                Focus_Entry(this.passwordEntry);
            }
            else
            {
                Focus_Entry(this.mobileNumberEntry);
            }
        }

        void OnPasswordEntry_Completed(Object sender, EventArgs e)
        {
            Focus_Entry(this.repeatPasswordEntry);
        }

        void OnRepeatEntry_Completed(Object sender, EventArgs e)
        {
            Focus_Entry(this.mobileNumberEntry);
        }

        void Focus_Entry(Entry entry)
        {
            entry.Focus();
        }

        int Age_Validation(){
            if(String.IsNullOrEmpty(this.ageEntry.Text)){
                return 0;
            }
            else{
                return int.Parse(this.ageEntry.Text);
            }
        }

		async Task FetchLocation()
		{
			if (DependencyService.Get<iLocationServices>().IsLocationServiceOn())
			{
				Position GeoPosition = await locationHelper.GetCurrentLocation();
				if (GeoPosition != null)
				{
					dataClass.latitude = GeoPosition.Latitude;
					dataClass.longitude = GeoPosition.Longitude;
					dataClass.hasLocation = true;
				}
            }
		}

		public void ReceiveJSONData(JObject jsonData)
		{

		}

        public void ReceiveJSONData(JObject jsonData, CancellationToken ct)
        {
            Device.BeginInvokeOnMainThread( async () => {
                
				if (int.Parse(jsonData["status"].ToString()) == 200)
				{
                    if(Device.RuntimePlatform == Device.Android)
                    {
						if (Application.Current.Properties.ContainsKey("user_stripelink"))
						{
							Application.Current.Properties["user_stripelink"] = jsonData["stripe"].ToString();
						}
                    }
					dataClass.stripeLink = jsonData["stripe"].ToString();
					dataClass.email = this.emailEntry.Text;

					if (Application.Current.Properties.ContainsKey("user_email"))
					{
						Application.Current.Properties["user_email"] = this.emailEntry.Text;
					}
					else
					{
						Application.Current.Properties.Add("user_email", this.emailEntry.Text);
					}

					await Application.Current.SavePropertiesAsync();
					await locationHelper.StartListening();

					SignUpPage.PositionChange.Invoke(this, 1);
				}
				else
				{
					var errors = jsonData["errors"];
					if (!string.IsNullOrEmpty(errors["email"].ToString()))
					{
						EntryError(this.emailEntry, errors["email"].ToString());
					}
					if (!string.IsNullOrEmpty(errors["password"].ToString()))
					{
						this.passwordEntry.FontSize = 11.scale();
						EntryError(this.passwordEntry, errors["password"].ToString());
					}
				}

				isClicked = false;
                isFBClicked = false;
			});
        }

        public void ReceiveTimeoutError(string title, string error)
        {
			isClicked = false;
            isFBClicked = false;
#if DEBUG == false
			Device.BeginInvokeOnMainThread(async delegate
			{
				await App.Current.MainPage.DisplayAlert(title, error, "Okay");
			});
#endif
		}
    }
}
