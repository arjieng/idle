﻿﻿﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Threading;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Xamarin.Forms;

namespace idle.Views.SignUpPages
{
    public partial class SignUpThirdPage : ContentView, iRestConnector, iFileConnector
    {
        DataClass dataClass = DataClass.GetInstance;
        bool isClicked = false , isResendClick;
        CancellationTokenSource cts;
#if DEBUG
		FileReader fileReader = new FileReader();
#else
        RestServices webService = new RestServices();
#endif

        public SignUpThirdPage()
        {
            InitializeComponent();
#if DEBUG
			fileReader.FileReaderDelegate = this;
#else
            webService.WebServiceDelegate = this;
#endif
            //if (Device.RuntimePlatform == Device.iOS)
            //{
            //    confirmationCodeEntry.HorizontalTextAlignment = TextAlignment.Center;
            //}
        }

        async void OnDoneButton_ClickedAsync(object sender, EventArgs e)
        {
            if (isClicked == false)
            {

				//if (String.IsNullOrEmpty(this.confirmationCodeEntry.Text) )
                if (String.IsNullOrEmpty(this.confirmationCodeEntry.Text) || confirmationCodeEntry.Text == confirmationCodeEntry.Placeholder  )
                {
                    this.confirmationCodeEntry.FontSize = 11.scale();
                    EntryError(this.confirmationCodeEntry, "Confirmation code required to proceed!");
                }
                else
                {

                    isClicked = true;
                    cts = new CancellationTokenSource();
#if DEBUG
					await fileReader.ReadFile("user.json", false, cts.Token);
#else
                    webService.WebServiceDelegate = this;
                    string json = JsonConvert.SerializeObject(new { email = dataClass.email, confirmation_code = this.confirmationCodeEntry.Text });
                
                    string url = Constants.ROOT_URL + Constants.CONFIRMATION_URL;
					
					try
					{
                        await webService.PostRequestAsync(url, json, cts.Token);
					}
					catch (OperationCanceledException)
					{
						DependencyService.Get<iConsole>().DisplayText("Web Service: Cancelled!");
					}
					catch (Exception)
					{
						DependencyService.Get<iConsole>().DisplayText("Web Service: Failed!");
					}

					cts = null;
#endif
                }

            }
		}

        void Handle_Completed(object sender, System.EventArgs e)
        {
            confirmationCodeEntry.Unfocus();
        }

        async void OnResendButton_Clicked(object sender, EventArgs e)
        {
            if (isResendClick == false)
            {
                isResendClick = true;
                cts = new CancellationTokenSource();
#if DEBUG
				await fileReader.ReadFile("user.json", false, cts.Token);
#else
                webService.WebServiceDelegate = this;
                string json = JsonConvert.SerializeObject(new { email = dataClass.email });

                string url = Constants.ROOT_URL + Constants.RESEND_CONFIRMATION_URL;
				
				try
				{
                    await webService.PostRequestAsync(url, json, cts.Token);
				}
				catch (OperationCanceledException)
				{
					DependencyService.Get<iConsole>().DisplayText("Web Service: Cancelled!");
				}
				catch (Exception)
				{
					DependencyService.Get<iConsole>().DisplayText("Web Service: Failed!");
				}

				cts = null;
#endif
            }
		}


		void EntryError(CustomEditor entry, string errorMessage)
        //void EntryError(Entry entry, string errorMessage)
		{
			entry.Text = "";
            entry.PlaceholderColor = Color.Red;
			entry.Placeholder = errorMessage;
		}

		public void ReceiveJSONData(JObject jsonData)
		{

		}

		public void ReceiveJSONData(JObject jsonData, CancellationToken ct)
		{
            if(jsonData["status"].ToString().Equals("200"))
            {
                if (isResendClick)
                {
                    Application.Current.MainPage.DisplayAlert("Confirmation Code Sent",jsonData["success"].ToString(), "Okay");
                }
                else
                {
                    dataClass.isLogin = true;

					var user = JsonConvert.DeserializeObject<User>(jsonData["user"].ToString());

					Application.Current.Properties["user_id"] = user.id;
					Application.Current.Properties["user_email"] = user.email;
					Application.Current.Properties["user_image"] = user.image;
					Application.Current.Properties["user_is_online"] = user.is_online;
					Application.Current.Properties["user_has_stripe"] = user.has_stripe;
					Application.Current.Properties["user_is_confirmed"] = user.is_confirmed;
					Application.Current.Properties["user_is_performer"] = user.is_performer;
					Application.Current.Properties["user_token"] = user.token;
					Application.Current.Properties["user_perform_progress"] = JsonConvert.SerializeObject(user.perform);
					Application.Current.Properties["user_post_progress"] = JsonConvert.SerializeObject(user.post);

					Application.Current.SavePropertiesAsync();

					dataClass.Update(true);

                    App.ShowMainPage();
                }
            }
            else
            {
				confirmationCodeEntry.FontSize = 11.scale();
                EntryError(confirmationCodeEntry, jsonData["error"].ToString());
            }

            isResendClick = false;
            isClicked = false;
		}

		public void ReceiveTimeoutError(string title, string error)
		{
			isClicked = false;
            Application.Current.MainPage.DisplayAlert(title, error, "Okay");
		}
    }
}
