﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.Threading;
using System.Threading.Tasks;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Plugin.Geolocator.Abstractions;
using Xamarin.Forms;

namespace idle.Views
{
    public partial class SignInPage : ContentPage, iRestConnector, iFileConnector
    {
        DataClass dataClass = DataClass.GetInstance;
        LocationHelper locationHelper = LocationHelper.GetInstance;
        bool isClicked = false, isSignUpClick = false, isForgetPasswordClick, isInThisPage, isFBClicked = false;
        int serviceType;
        CancellationTokenSource cts;
#if DEBUG
        FileReader fileReader = new FileReader();
#else
        RestServices webService = new RestServices();
#endif
        public SignInPage()
        {

            InitializeComponent();

            isInThisPage = true;

            DependencyService.Get<DependencyServices.iStatusStyle>().StatusStyle(0);

            if (!Application.Current.Properties.ContainsKey("firstload"))
            {
                Application.Current.Properties["firstload"] = 1;
                Navigation.PushModalAsync(new Views.GeneralTutorialPage(), false);
            }

            isClicked = false;

			//Stripe return for android only
			if (Device.RuntimePlatform == Device.Android)
            {
#if DEBUG == false
                MessagingCenter.Subscribe<Object, string>(this, "StripeConnectCode_Droid", async (sender, code) =>
                {

                    if (isInThisPage)
                    {
                        if (Application.Current.Properties.ContainsKey("user_email"))
                        {
                            DataClass.GetInstance.email = (Application.Current.Properties["user_email"].ToString());
                        }
                        serviceType = 1;
                        webService.WebServiceDelegate = this;
                        var json = JsonConvert.SerializeObject(new { email = DataClass.GetInstance.email, stripe_account_id = code });

						cts = new CancellationTokenSource();
						try
						{
                            await webService.PostRequestAsync(Constants.ROOT_URL + Constants.STRIPE_URL, json, cts.Token);
						}
						catch (OperationCanceledException)
						{
							DependencyService.Get<iConsole>().DisplayText("Web Service: Cancelled!");
						}
						catch (Exception)
						{
							DependencyService.Get<iConsole>().DisplayText("Web Service: Failed!");
						}

						cts = null;
                    }
			    });
#endif
            }

			//forget password
			MessagingCenter.Subscribe<Object, string>(this, "ResetPassword", (obj, email) =>
			{
				Device.BeginInvokeOnMainThread(async () =>
				{
					Application.Current.Properties["user_email"] = email;
					await Application.Current.SavePropertiesAsync();
					dataClass.email = email;
					App.ShowResetPasswordPage();
				});
			});
        }

        protected override void OnAppearing()
        {
            base.OnAppearing();
#if DEBUG
            fileReader.FileReaderDelegate = this;
#else
            webService = new RestServices();
            webService.WebServiceDelegate = this;
#endif
            isSignUpClick = false;
            isClicked = false;
            isFBClicked = false;
            isForgetPasswordClick = false;

            if (!Application.Current.Properties.ContainsKey("firstload"))
            {
                Application.Current.Properties["firstload"] = 1;
                Navigation.PushAsync(new Views.GeneralTutorialPage());
            }

            EmailOnCached_Show();
            DependencyService.Get<DependencyServices.iStatusStyle>().StatusStyle(0);

            if (Application.Current.Properties.ContainsKey("user_email"))
            {
                emailEntry.Text = (Application.Current.Properties["user_email"].ToString());
                DataClass.GetInstance.email = (Application.Current.Properties["user_email"].ToString());
            }
        }

        protected override void OnDisappearing()
        {
            base.OnDisappearing();
            if (Device.RuntimePlatform == Device.Android)
            {
                MessagingCenter.Unsubscribe<Object, string>(this, "StripeConnectCode_Droid");
            }
            EmailOnCached_Show();
            this.passwordEntry.Text = String.Empty;
        }

        async void OnSignUp_Clicked(object sender, EventArgs e)
        {
            if (isSignUpClick == false)
            {
                isSignUpClick = true;

                await Navigation.PushModalAsync(new SignUpPage());
            }
        }

        void OnEmailEntryContainer_Tapped(Object sender, EventArgs e)
        {
            Focus_Entry(emailEntry);
        }
        void OnPasswordEntryContainer_Tapped(Object sender, EventArgs e)
        {
            Focus_Entry(passwordEntry);
        }

        async void OnContinueButton_Clicked(object sender, EventArgs e)
        {
            bool isNotError = true;

            if (string.IsNullOrEmpty(emailEntry.Text))
            {
                EntryError(emailEntry, "Email is required!");
                isNotError = false;
            }
            if (string.IsNullOrEmpty(passwordEntry.Text))
            {
                EntryError(passwordEntry, "Password is required!");
                isNotError = false;
            }

            if (isNotError && isClicked == false)
            {
                isClicked = true;
                var userEmail = emailEntry.Text;
                var userPass = passwordEntry.Text;

                await FetchLocation();

                emailEntry.Text = userEmail;
                passwordEntry.Text = userPass;
                await Task.Run(async () =>
                {
                    cts = new CancellationTokenSource();
#if DEBUG
                    string json = JsonConvert.SerializeObject(new { user = new { id = 1, email = this.emailEntry.Text, password = this.passwordEntry.Text, latitude = dataClass.latitude, longitude = dataClass.longitude, perform = new List<Tasks>(), post = new List<Tasks>(), is_performer = 0, has_stripe = 0, is_online = 1, is_confirmed = 1, token = "000000", }, status = "200" });
                    await fileReader.ReadFile("User.json", true, cts.Token);
#else
                    serviceType = 0;
                    string json = JsonConvert.SerializeObject(new { user = new { email = this.emailEntry.Text, password = this.passwordEntry.Text, latitude = dataClass.latitude, longitude = dataClass.longitude }, device = new { platform = App.DeviceType, token = App.UDID } });

					try
					{
                        await webService.PostRequestAsync(Constants.ROOT_URL + Constants.SIGN_IN_URL, json, cts.Token);
					}
					catch (OperationCanceledException)
					{
						DependencyService.Get<iConsole>().DisplayText("Web Service: Cancelled!");
					}
					catch (Exception)
					{
						DependencyService.Get<iConsole>().DisplayText("Web Service: Failed!");
					}

					cts = null;

#endif
                });
            }
        }

        void OnEmailEntry_Completed(Object sender, EventArgs e)
        {
            Focus_Entry(this.passwordEntry);
        }

        void EntryError(Entry entry, string errorMessage)
        {
            entry.Text = "";
            entry.PlaceholderColor = Color.Red;
            entry.Placeholder = errorMessage;
        }

        void Focus_Entry(Entry entry)
        {
            entry.Focus();
        }

        void EmailOnCached_Show()
        {
            if (Application.Current.Properties.ContainsKey("user_email"))
            {
                this.emailEntry.Text = DataClass.GetInstance.UserEmail;
            }
            else
            {
                this.emailEntry.Text = string.Empty;
            }
        }

        async Task FetchLocation()
        {
            if (DependencyService.Get<iLocationServices>().IsLocationServiceOn())
            {
                Position GeoPosition = await locationHelper.GetCurrentLocation();
                if (GeoPosition != null)
                {
                    dataClass.latitude = GeoPosition.Latitude;
                    dataClass.longitude = GeoPosition.Longitude;
                    dataClass.hasLocation = true;
                }
            }
        }

        void SignInFacebook_Clicked(object sender, System.EventArgs e)
        {
            if (isFBClicked == false)
            {
                isFBClicked = true;
                DependencyService.Get<IFacebookManager>().Login(OnLoginComplete);
            }
        }

        void OnLoginComplete(FacebookUser facebookUser, string message)
        {
            if (facebookUser != null)
            {
                Device.BeginInvokeOnMainThread(async delegate
                {
                    await FetchLocation();
                    await Task.Run(async () =>
                    {
                        cts = new CancellationTokenSource();
#if DEBUG
                        string json = JsonConvert.SerializeObject(new { user = new { id = 1, email = facebookUser.Email, provider = "facebook", provider_id = facebookUser.Id, latitude = dataClass.latitude, longitude = dataClass.longitude, perform = new List<Tasks>(), post = new List<Tasks>(), is_performer = 0, has_stripe = 0, is_online = 1, is_confirmed = 1, token = "000000" }, status = "200" });
                        await fileReader.WriteFile("user.json", json, false, cts.Token);
#else
						serviceType = 0;
						string json = JsonConvert.SerializeObject(new { user = new { email = facebookUser.Email, provider = "facebook", provider_id = facebookUser.Id, latitude = dataClass.latitude, longitude = dataClass.longitude }, device = new { platform = App.DeviceType, token = App.UDID } });

						try
						{
                            await webService.PostRequestAsync(Constants.ROOT_URL + Constants.FB_URL, json, cts.Token);
						}
						catch (OperationCanceledException)
						{
							DependencyService.Get<iConsole>().DisplayText("Web Service: Cancelled!");
						}
						catch (Exception)
						{
							DependencyService.Get<iConsole>().DisplayText("Web Service: Failed!");
						}

						cts = null;
#endif
                    });
                });
            }
            else
            {
                isFBClicked = false;
                if (message.Contains("Invalid key hash")) { message = "Invalid key hash"; }
                EntryError(emailEntry, message);
            }
        }

        void OnForgotPassword_Clicked(Object sender, EventArgs e)
        {
            if (isForgetPasswordClick == false)
            {
                isForgetPasswordClick = true;
                Navigation.PushAsync(new ForgotPasswordPage());
            }
        }

        public void ReceiveJSONData(JObject jsonData)
        {

        }

        public void ReceiveJSONData(JObject jsonData, CancellationToken ct)
        {
            Device.BeginInvokeOnMainThread(async () =>
            {
                if (int.Parse(jsonData["status"].ToString()) == 200)
                {
                    switch (serviceType)
                    {
                        case 0: // sign in

                            var user = JsonConvert.DeserializeObject<User>(jsonData["user"].ToString());

                            Application.Current.Properties["user_id"] = user.id;
                            Application.Current.Properties["user_email"] = user.email;
                            Application.Current.Properties["user_image"] = user.image;
                            Application.Current.Properties["user_is_online"] = user.is_online;
                            Application.Current.Properties["user_has_stripe"] = user.has_stripe;
                            Application.Current.Properties["user_is_confirmed"] = user.is_confirmed;
                            Application.Current.Properties["user_is_performer"] = user.is_performer;
                            Application.Current.Properties["user_token"] = user.token;
                            Application.Current.Properties["user_perform_progress"] = JsonConvert.SerializeObject(user.perform);
                            Application.Current.Properties["user_post_progress"] = JsonConvert.SerializeObject(user.post);

                            await Application.Current.SavePropertiesAsync();

                            dataClass.Update(true);

                            await locationHelper.StartListening();
                            App.ShowMainPage();

                            break;
                        case 1: //send stripe

                            dataClass.email = dataClass.UserEmail;
                            await Navigation.PushModalAsync(new SignUpPage(2), false);

                            break;
                    }

                    isInThisPage = false;
                }
                else if (jsonData["status"].ToString() == "201")
                {
                    var action = await DisplayAlert("Confirm Your Account", jsonData["error"].ToString(), "Okay", "Cancel");
                    if (action)
                    {
                        dataClass.email = emailEntry.Text;
                        await Navigation.PushModalAsync(new SignUpPage(2));
                    }
                }
                else
                {
                    switch (serviceType)
                    {
                        case 0:
                            EntryError(this.emailEntry, jsonData["error"].ToString());
                            this.emailEntry.FontSize = 11.scale();
                            EntryError(this.passwordEntry, "");
                            break;
                        case 1: //send stripe
                            await Navigation.PushModalAsync(new SignUpPage(1), false);
                            await DisplayAlert("Stripe Connect Failed", jsonData["error"].ToString(), "Okay");
                            break;
                    }

                }
                isClicked = false;
                isFBClicked = false;
            });

        }

        public void ReceiveTimeoutError(string title, string error)
        {
            isClicked = false;
            isFBClicked = false;
#if DEBUG == false
            Device.BeginInvokeOnMainThread(async () =>
            {

                if (title.Equals("No Internet Connection!"))
                {
                    if (LocationHelper.IsAlreadyNotify == false)
                    {
                        LocationHelper.IsAlreadyNotify = true;
                        await DisplayAlert(title, error, "Okay");
                    }
                }
                else
                {
					await DisplayAlert(title, error, "Okay");
				}
            });
#endif
        }
    }
}
