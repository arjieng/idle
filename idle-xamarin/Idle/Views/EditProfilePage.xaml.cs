﻿using System;
using System.Collections.Generic;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Plugin.Media;
using Plugin.Media.Abstractions;
using Plugin.Permissions.Abstractions;
using Xamarin.Forms;
using ExifLib;
using System.Threading;

namespace idle.Views
{
    public partial class EditProfilePage : RootViewPage, iFileConnector, iRestConnector
    {
        bool isSave = false;
		User oldUser;
		User newUser = new User();
        CameraHelper cameraHelper = new CameraHelper();
        MediaFile file;
#if DEBUG 
        FileReader fileReader = new FileReader();
#else
        RestServices webService = new RestServices();
#endif
		public EditProfilePage(int id, User user)
        {
            InitializeComponent();
#if DEBUG
            fileReader.FileReaderDelegate = this;
			
#else
            webService.WebServiceDelegate = this;
#endif
			oldUser = user;
			newUser.Update(user);
            newUser.email = DataClass.GetInstance.UserEmail;
			BindingContext = newUser;

            this.PageTitle = "EDIT PROFILE";
            this.TitleFontFamily = Constants.LULO;
            this.TitleFontColor = Constants.DARK_STRONG;
            this.RightIcon2 = "CloseDark";
            this.RightButton2Command = new Command((obj) => { Navigation.PopModalAsync(); });

        }

        protected override void OnAppearing()
        {
            base.OnAppearing();

            DependencyService.Get<DependencyServices.iStatusStyle>().StatusStyle(1);
        }

		void OnEntryContainer_Tapped(Object sender, EventArgs e)
		{
			var parent = sender as StackLayout;
			var entry = parent.Children[1] as Entry;
			Focus_Entry(entry);
		}

        void EntryError(Entry entry, string errorMessage)
        {
            entry.Text = "";
            entry.PlaceholderColor = Color.Red;
            entry.Placeholder = errorMessage;
        }

        async void OnSaveButton_Clicked(object sender, EventArgs e)
        {
            bool isNotError = true;
            if (string.IsNullOrEmpty(nameEntry.Text))
            {
                EntryError(nameEntry, "Name is required!");
                isNotError = false;
            }

            if (isNotError && isSave == false)
            {
                isSave = true;
#if DEBUG
				oldUser.Update(newUser);
                await DisplayAlert(" Edit Profile", "Profile successfully updated!", "Ok");
                await Navigation.PopModalAsync();
#else
				string json = JsonConvert.SerializeObject(new { user = new { newUser.name , newUser.age, description = newUser.about, newUser.image, newUser.school } , token = DataClass.GetInstance.UserToken });
                 string url = Constants.ROOT_URL + Constants.USERS_URL +"/" +DataClass.GetInstance.UserID;
                //await webService.PutRequestAsync(url, json);
                loadingIndicator.IsRunning = true;
                //var str = file.GetStream();
                if (file != null)
                {
                    await webService.MultiPartDataContentAsync(url, "user", json, file.GetStream());
                }
                else
                {
                    await webService.MultiPartDataContentAsync(url, "user", json);
                }

#endif
			}
		}

        void OnAgeEntry_Completed(Object sender, EventArgs e)
        {
            Focus_Entry(this.schoolEntry);
        }

		void OnNameEntry_Completed(Object sender, EventArgs e)
		{
			Focus_Entry(this.ageEntry);
		}

		void OnSchoolEntry_Completed(Object sender, EventArgs e)
		{
			aboutEntry.Focus();
		}

		void Focus_Entry(Entry entry)
		{
			entry.Focus();
		}

		int Age_Validation()
		{
			if (String.IsNullOrEmpty(this.ageEntry.Text))
			{
				return 0;
			}
			else
			{
				return int.Parse(this.ageEntry.Text);
			}
		}

		async void OnImage_Tapped(object sender, EventArgs e)
		{
            var myAction = await DisplayActionSheet(null, "Cancel", null, "Take a photo", "Choose existing");
            if(myAction!=null)
            {
                if(myAction.ToString()!="Cancel")
                {
                    await CrossMedia.Current.Initialize();
					if (myAction.ToString() == "Take a photo")
                    {
                        file = await cameraHelper.TakePhotoAsync(new StoreCameraMediaOptions()
                        {
                            Directory = "Idle",
                            Name = $"{DateTime.UtcNow}.jpg",
                            CompressionQuality = 92,
                            MaxWidthHeight = 320.scale(),
                            DefaultCamera = CameraDevice.Front,
                            PhotoSize = PhotoSize.Custom,
                            AllowCropping = true
                        });
					}
					else if (myAction.ToString() == "Choose existing")
                    {
                        file = await cameraHelper.PickPhotoAsync(new PickMediaOptions()
                        {
                            MaxWidthHeight = 320.scale(),
                            CompressionQuality = 92,
                            PhotoSize = PhotoSize.Custom,
                            RotateImage = true
                        });
					}

					if (file != null)
					{
						var source = ImageSource.FromStream(() => {  var stream = file.GetStream();  return stream; });

						profileImage.Source = source;
						backGroundImage.Source = source;



						var picInfo = ExifReader.ReadJpeg(file.GetStream());

                        //picInfo.Orientation = ExifOrientation.BottomRight;
						ExifOrientation orientation = picInfo.Orientation;

                        //await   DisplayAlert("Stream", orientation.ToString() , "Okay");


                        /*  Get the public album path
				         *  var aPpath = file.AlbumPath;
				         *  Get private path
				         *  var path = file.Path;
				         */
					}
                }
            }
		}

        protected override void OnDisappearing()
        {
            base.OnDisappearing();
        }

        void OnAge_TextChanged(object sender, Xamarin.Forms.TextChangedEventArgs e)
        {
			if (ageEntry.Text.Length > 3)
			{
				ageEntry.Text = e.OldTextValue;
			}
			else
			{
				ageEntry.Text = e.NewTextValue;
			}
        }

		public void ReceiveJSONData(JObject jsonData, CancellationToken ct)
		{

		}

        public void ReceiveJSONData(JObject jsonData)
		{
            Device.BeginInvokeOnMainThread( async () => 
            {
                isSave = false;
				if (jsonData["status"].ToString() == "200")
				{
					var user = JsonConvert.DeserializeObject<User>(jsonData["user"].ToString());
					oldUser.Update(user);
                    Application.Current.Properties["user_image"] = oldUser.image;
                    await Application.Current.SavePropertiesAsync();
                    MasterPage.UpdateProfile.Invoke(this,oldUser);
					await Navigation.PopModalAsync();
				}
				else
				{

				}
                loadingIndicator.IsRunning = false;
            });
		}

		public void ReceiveTimeoutError(string title, string error)
		{
			isSave = false;
			Device.BeginInvokeOnMainThread(async delegate
			{
				await DisplayAlert(title, error, "Okay");
			});
            loadingIndicator.IsRunning = false;
		}
    }
}
