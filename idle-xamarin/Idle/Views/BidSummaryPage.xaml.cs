﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Xamarin.Forms;

namespace idle.Views
{
    public partial class BidSummaryPage : RootViewPage, iRestConnector
    {
        DataClass dataClass = DataClass.GetInstance;
        bool IsClicked = false;
        Bid bid = new Bid();
        int serviceType = 0, offSet, taskID;
        CancellationTokenSource cts;

#if DEBUG == false
        RestServices webService = new RestServices();
#endif

        public BidSummaryPage(Bid bid, int taskId, int offSet)
        {
            InitializeComponent();

            this.PageTitle = "BID SUMMARY";
            this.TitleFontFamily = Constants.LULO;
            this.TitleFontColor = Constants.DARK_STRONG;
            this.LeftIcon = "ArrowDark";
            this.LeftButtonCommand = new Command((obj) => { Navigation.PopAsync(); });


#if DEBUG
            this.bid = bid;
            BindingContext = this.bid;
#else
            this.offSet = offSet;
            taskID = taskId;
            this.bid = bid;
            BindingContext = this.bid;
            webService.WebServiceDelegate = this;
            Task.Run(() =>
            {
                //API
            });
#endif
		}

        protected override void OnAppearing()
        {
            base.OnAppearing();

            DependencyService.Get<DependencyServices.iStatusStyle>().StatusStyle(1);

			MessagingCenter.Subscribe<App, JObject>(this, "BidCancelled", (sender, obj) =>
			{
				if (int.Parse(obj["custom_data"]["task_id"].ToString()) == taskID)
				{
					if (dataClass.BidList.ToList().Exists(cancelBid => cancelBid.id == int.Parse(obj["custom_data"]["bid_id"].ToString())))
					{
						dataClass.BidList.Remove(dataClass.BidList.ToList().Find(cancelBid => cancelBid.id == int.Parse(obj["custom_data"]["bid_id"].ToString())));
					}
					Navigation.PopAsync();
				}
			});
        }

        protected override void OnDisappearing()
        {
            base.OnDisappearing();

            MessagingCenter.Unsubscribe<App, JObject>(this, "BidCancelled");

			if (cts != null)
			{
				cts.Cancel();
			}
        }

        void OnViewProfile_Clicked(object sender, EventArgs e)
        {
            Navigation.PushAsync(new ProfilePage(1, bid.user.id));
        }

        async void OnIgnoreButton_Clicked(object sender, EventArgs e)
        {
            if (IsClicked == false)
            {
                IsClicked = true;

                var action = await DisplayActionSheet("Do you want to ignore this bid?", "No", null, "Yes");
                if (action != null){
                    if (action.Equals("Yes"))
                    {
#if DEBUG
                        IsClicked = false;
                        dataClass.BidList.RemoveAt(dataClass.BidList.ToList().FindIndex((obj) => obj.id == bid.id));
                        await Navigation.PopAsync();
#else
						serviceType = 1;
						var url = Constants.ROOT_URL + Constants.ACCOUNTS_URL + Constants.FETCH_TASK_URL + "/" + this.taskID + Constants.BID_URL + "/" + bid.id + Constants.TOKEN_URL + dataClass.UserToken + "&off_set=" + offSet;

						await Task.Run(async () =>
						{
    						cts = new CancellationTokenSource();
    						try
    						{
    							await webService.DeleteRequestAsync(url, cts.Token);
    						}
    						catch (OperationCanceledException)
    						{
    							DependencyService.Get<iConsole>().DisplayText("Web Service: Cancelled!");
    						}
    						catch (Exception)
    						{
    							DependencyService.Get<iConsole>().DisplayText("Web Service: Failed!");
    						}

    						cts = null;
                        });
#endif
					}
                    else
                    {
                        IsClicked = false;    
                    }
                }
				else
				{
					IsClicked = false;
				}
            }
        }

        async void OnAction_Clicked(object sender, EventArgs e)
        {
            var action = await DisplayActionSheet(null, "Cancel", null, "Report User");
            if (action != null)
            {
                if (action.ToString() == "Report User")
                {
                    await Navigation.PushAsync(new ReportUserPage(0, bid.user));
                }
            }
        }

        async void OnAcceptButton_Clicked(object sender, EventArgs e)
        {
			if (IsClicked == false)
			{
				IsClicked = true;

				var action = await DisplayActionSheet("Are you sure you want to accept this bid?", "No", null, "Yes");
				if (action != null)
				{
                    if (action.Equals("Yes"))
					{
#if DEBUG
						IsClicked = false;
                        dataClass.BidUser = bid.user;
                        dataClass.bid = bid.amount;
						dataClass.BidList.Clear();
						await Navigation.PopModalAsync();
#else
                        serviceType = 2;
						var url = Constants.ROOT_URL + Constants.ACCOUNTS_URL + Constants.BID_URL + "/" + bid.id + "/accept";
						var json = JsonConvert.SerializeObject(new { token = dataClass.UserToken });

						await Task.Run(async () =>
						{
    						cts = new CancellationTokenSource();
    						try
    						{
    							await webService.PutRequestAsync(url, json, cts.Token);
    						}
    						catch (OperationCanceledException)
    						{
    							DependencyService.Get<iConsole>().DisplayText("Web Service: Cancelled!");
    						}
    						catch (Exception)
    						{
    							DependencyService.Get<iConsole>().DisplayText("Web Service: Failed!");
    						}

    						cts = null;
                        });
#endif
					}
                    else
                    {
                        IsClicked = false;    
                    }
				}
                else{
                    IsClicked = false;
                }
			}
        }

        void OnMessage_ImageTapped(object sender, ImageTappedEventArgs e)
        {
            Navigation.PushAsync(new MessagePage(bid.user.name, bid.user,bid.user.conversation_id));
        }

		public void ReceiveJSONData(JObject jsonData)
		{

		}

        public void ReceiveJSONData(JObject jsonData, CancellationToken ct)
        {
            Device.BeginInvokeOnMainThread(() => {
				if (int.Parse(jsonData["status"].ToString()) == 300)
				{
                    
				}
                else{
                    int HTTPStatus = int.Parse(jsonData["status"].ToString());
					switch (serviceType)
					{
						case 0: //fetch bid
							bid = JsonConvert.DeserializeObject<Bid>(jsonData["bid"].ToString());
							BindingContext = bid;


							//update bid on list
							break;
						case 1: //ignore bid
                            //if(HTTPStatus == 200){
                            //    bid = JsonConvert.DeserializeObject<Bid>(jsonData["bid"].ToString());
                            //    dataClass.BidList.Add(bid);
                            //}

							dataClass.BidList.RemoveAt(dataClass.BidList.ToList().FindIndex((obj) => obj.id == bid.id));
							Navigation.PopAsync();
							break;
						case 2: //accept bid
							dataClass.BidUser = bid.user;
							dataClass.bid = bid.amount;
							dataClass.BidList.Clear();
							Navigation.PopModalAsync();
                            DisplayAlert("Bid accepted", jsonData["success"].ToString(), "Okay");
							break;
					}
                }
            });
        }

        public  void ReceiveTimeoutError(string title, string error)
        {
			Device.BeginInvokeOnMainThread(async delegate
			{
				await DisplayAlert(title, error, "Okay");
			});
        }
    }
}
