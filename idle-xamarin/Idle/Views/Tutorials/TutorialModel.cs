﻿using System;
using Xamarin.Forms;
namespace idle.Views
{
    public class TutorialModel
    {
        public string ImageSource { get; set; }
        public int PagePosition{get;set;}
    }
}
