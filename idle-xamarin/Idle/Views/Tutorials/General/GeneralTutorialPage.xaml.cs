﻿﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace idle.Views
{
    public partial class GeneralTutorialPage : ContentPage
    {
        public int PagePosition = 0;
        public ObservableCollection<TutorialModel> view;
        public GeneralTutorialPage()
        {
            InitializeComponent();


            NavigationPage.SetHasNavigationBar(this, false  );

            view = new ObservableCollection<TutorialModel>();

            view.Add(new TutorialModel{ ImageSource="GeneralFIrst" , PagePosition=0});
			view.Add(new TutorialModel { ImageSource = "GeneralSecond" ,PagePosition = 1 });
			view.Add(new TutorialModel { ImageSource = "PerformerFirst" ,PagePosition = 2  });
            view.Add(new TutorialModel { ImageSource = "PerformerSecond" ,PagePosition = 3 });
            view.Add(new TutorialModel { ImageSource = "PerformerThird" ,PagePosition = 4 });
			view.Add(new TutorialModel { ImageSource = "PerformerFourth" ,PagePosition = 5 });
			view.Add(new TutorialModel { ImageSource = "PerformerFifth" ,PagePosition = 6 });
			view.Add(new TutorialModel { ImageSource = "PosterFirst" ,PagePosition = 7 });
            view.Add(new TutorialModel { ImageSource = "PosterSecond" ,PagePosition = 8 });
            view.Add(new TutorialModel { ImageSource = "PosterThird" ,PagePosition = 9});

			BindingContext = view[PagePosition];
            //cv.ItemsSource = view;
            ////cv.ItemSelected += OnItemSelected;
            //cv.PositionSelected += OnPosition_Selected;
        }

        protected override void OnAppearing()
        {
            base.OnAppearing();

            DependencyService.Get<DependencyServices.iStatusStyle>().StatusStyle(0);
        }

        private  void OnPosition_Selected(object sender, SelectedPositionChangedEventArgs e)
        {
            BindingContext = new { PagePosition };
        }

       async void OnNextButton_Clicked(object sender, EventArgs e)
        {
            if (PagePosition < 9)
            {
				PagePosition++;
                var image = view[PagePosition];
                BindingContext =  image ;
                //cv.Opacity = 0;
                //cv.Position = PagePosition;
                //await Task.Delay(350);
                //cv.Opacity = 1;
                //await   cv.FadeTo(1, 500);
            }
            else
            {
               await Navigation.PopModalAsync();
            }
        }
		async void OnCloseButton_Clicked(object sender, EventArgs e)
		{
            await Navigation.PopModalAsync();
		}

		async void OnBackButton_Clicked(object sender, EventArgs e)
		{
			if (PagePosition > 0)
			{
				PagePosition--;
				var image = view[PagePosition];
				BindingContext =  image ;
				//cv.Opacity = 0;
				//cv.Position = PagePosition;
                //await Task.Delay(350);
                //cv.Opacity = 1;
                //await cv.FadeTo(1, 500);
			}
			
		}
    }
}
