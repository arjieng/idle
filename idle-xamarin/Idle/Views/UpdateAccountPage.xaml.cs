﻿using System;
using System.Collections.Generic;
using System.Threading;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Xamarin.Forms;

namespace idle.Views
{
    public partial class UpdateAccountPage : RootViewPage, iFileConnector, iRestConnector
    {
        bool isSave = false;
        int serviceType = 0;
        User updateUser;
        DataClass dataClass = DataClass.GetInstance;
        CancellationTokenSource cts;
#if DEBUG
        FileReader fileReader = new FileReader();
#else
        RestServices webService = new RestServices();
#endif
        public UpdateAccountPage()
        {
            InitializeComponent();
#if DEBUG
            fileReader.FileReaderDelegate = this;
#else
            webService.WebServiceDelegate = this;
#endif
            this.PageTitle = "UPDATE ACCOUNT";
            this.TitleFontFamily = Constants.LULO;
            this.TitleFontColor = Constants.DARK_STRONG;
            this.LeftIcon = "ArrowDark";
            this.LeftButtonCommand = new Command(async (obj) => { await Navigation.PopAsync(); });

            FetchData();
        }

        protected override void OnAppearing()
        {
            base.OnAppearing();

            DependencyService.Get<DependencyServices.iStatusStyle>().StatusStyle(1);
        }

		protected override void OnDisappearing()
		{
			base.OnDisappearing();
			if (cts != null)
			{
				cts.Cancel();
			}
		}

        async void FetchData()
        {
			cts = new CancellationTokenSource();
#if DEBUG
            serviceType = 0;
            await fileReader.ReadFile("User.json", true, cts.Token);
#else
            serviceType = 0;
            loadingIndicator.IsRunning = true;

			try
			{
                await webService.GetRequest(Constants.ROOT_URL + Constants.USERS_URL + "/" + dataClass.UserID + Constants.TOKEN_URL + dataClass.UserToken, cts.Token);
			}
			catch (OperationCanceledException)
			{
				DependencyService.Get<iConsole>().DisplayText("Web Service: Cancelled!");
			}
			catch (Exception)
			{
				DependencyService.Get<iConsole>().DisplayText("Web Service: Failed!");
			}

			cts = null;
#endif
        }

        void EntryError(Entry entry, string errorMessage)
        {
            entry.Text = "";
            entry.PlaceholderColor = Color.Red;
            entry.Placeholder = errorMessage;
        }

        async void OnSaveAccount_Clicked(object sender, EventArgs e)
        {
            bool isNotError = true;

            if (string.IsNullOrEmpty(emailEntry.Text))
            {
                EntryError(emailEntry, "Email is required!");
                isNotError = false;
            }

            if (string.IsNullOrEmpty(this.mobileNumberEntry.Text))
            {
                EntryError(mobileNumberEntry, "Mobile number is required for confirmation!");
                isNotError = false;
            }

            if (!string.IsNullOrEmpty(passwordEntry.Text))
            {
                if (string.IsNullOrEmpty(repeatPasswordEntry.Text))
                {
                    EntryError(repeatPasswordEntry, "Repeat password is required!");
                    isNotError = false;
                }
                else if (!passwordEntry.Text.Equals(repeatPasswordEntry.Text))
                {
                    EntryError(passwordEntry, "Password does not match!");
                    EntryError(repeatPasswordEntry, "");
                    isNotError = false;
                }
            }

            if (isNotError && isSave == false)
            {
                isSave = true;
#if DEBUG
                await DisplayAlert(" Update Account", "Account successfully updated!", "Ok");
                await Navigation.PopAsync();
#else
                serviceType = 1;
                loadingIndicator.IsRunning = true;
                var email = emailEntry.Text.Equals(dataClass.UserEmail) ? "" : emailEntry.Text;
                string json = JsonConvert.SerializeObject(new { user = new { email, password = passwordEntry.Text, mobile = mobileNumberEntry.Text }, token = dataClass.UserToken });
                await webService.MultiPartDataContentAsync(Constants.ROOT_URL + Constants.USERS_URL + "/" + dataClass.UserID, "user", json);
#endif
            }
        }

        void OnEntryContainer_Tapped(Object sender, EventArgs e)
        {
            var parent = sender as StackLayout;
            var entry = parent.Children[1] as Entry;
            Focus_Entry(entry);
        }

        void OnSchoolEntry_Completed(Object sender, EventArgs e)
        {
            Focus_Entry(this.emailEntry);
        }

        void OnEmailEntry_Completed(Object sender, EventArgs e)
        {
            Focus_Entry(this.passwordEntry);
        }

        void OnPasswordEntry_Completed(Object sender, EventArgs e)
        {
            Focus_Entry(this.repeatPasswordEntry);
        }

        void OnRepeatEntry_Completed(Object sender, EventArgs e)
        {
            Focus_Entry(this.mobileNumberEntry);
        }

        void Focus_Entry(Entry entry)
        {
            entry.Focus();
        }

		public void ReceiveJSONData(JObject jsonData, CancellationToken ct)
		{
			if (int.Parse(jsonData["status"].ToString()) == 200)
			{
				// fetch user data
				updateUser = JsonConvert.DeserializeObject<User>(jsonData["user"].ToString());
				updateUser.email = dataClass.UserEmail;
				BindingContext = updateUser;
			}
			else
			{

			}
			loadingIndicator.IsRunning = false;
		}

        public void ReceiveJSONData(JObject jsonData)
        {
            if (int.Parse(jsonData["status"].ToString()) == 200)
            {
                // update account
                Application.Current.Properties["user_email"] = emailEntry.Text;
                Application.Current.SavePropertiesAsync();

                Navigation.PopAsync();
            }
            else
            {

            }
            loadingIndicator.IsRunning = false;
        }

        public async void ReceiveTimeoutError(string title, string error)
        {
            isSave = false;
            loadingIndicator.IsRunning = false;
            await DisplayAlert(title, error, "Okay");
        }
    }
}
