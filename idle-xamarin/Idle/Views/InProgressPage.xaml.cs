﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Plugin.Geolocator;
using Plugin.Geolocator.Abstractions;
using Xamarin.Forms;
using Xamarin.Forms.GoogleMaps;

namespace idle.Views
{
    public partial class InProgressPage : RootViewPage, iFileConnector, iRestConnector
    {
		ObservableCollection<Tasks> tasks { get; set; }
		int serviceType = 0;
		DataClass dataClass = DataClass.GetInstance;
		Pin workerPin = null;
        Pin userPin = null;
        Polyline polyline = null;
#if DEBUG || STAGING
		FileReader fileReader = new FileReader();
#else
        RestServices webService = new RestServices();
#endif
		public InProgressPage()
        {
            InitializeComponent();

			// testing adding binding context for in progress

			Tasks task = new Tasks
			{
				id = 1,
				title = "Buy stuff for party",
				user = new User() { name = "Childish Gambino" },
				task_type = 0
			};
            int count = 0;
			string taskCount = count == 0 ? "0" : count == 1 ? "1 task" : count.ToString() + " tasks";
			BindingContext = new { image = DataClass.GetInstance.UserImage, taskCount, task };

			//end 


			this.PageTitle = "IN PROGRESS";
			this.TitleFontFamily = Constants.LULO_BOLD;
			this.TitleFontColor = Constants.LIGHT;
            this.RightIcon2 = "CloseLight";
            this.RightButton2Command = new Command((obj) => { Navigation.PopModalAsync(); });
#if DEBUG || STAGING
			fileReader.FileReaderDelegate = this;
#else
            webService.WebServiceDelegate = this;
#endif
			progressMap.CameraPositionIdle += OnCameraPositionIdle;

			progressMap.MyLocationEnabled = true;
			if (dataClass.hasLocation == true)
			{
				progressMap.MoveToRegion(MapSpan.FromCenterAndRadius(new Xamarin.Forms.GoogleMaps.Position(dataClass.latitude, dataClass.longitude), Distance.FromMiles(1)));
				userPin = new Pin()
				{
					Type = PinType.Place,
					Label = "",
					Position = new Xamarin.Forms.GoogleMaps.Position(dataClass.latitude, dataClass.longitude),
					Icon = BitmapDescriptorFactory.FromView(new WorkerPinView(5, dataClass.UserImage)),
					Tag = "user",
					IsVisible = true
				};
				progressMap.Pins.Add(userPin);
			}
            progressMap.PinClicked += OnPinClicked;
			CrossGeolocator.Current.PositionChanged += PositionChanged;
			CrossGeolocator.Current.PositionError += PositionError;
		}

        void OnCurrent_Clicked(object sender, EventArgs e)
        {
            
        }

		void OnProgress_Clicked(object sender, EventArgs e)
		{

		}

        void PositionError(object sender, PositionErrorEventArgs e)
		{
			System.Diagnostics.Debug.WriteLine(e.Error);
		}

		void PositionChanged(object sender, PositionEventArgs e)
		{
			//progressMap.MoveCamera(CameraUpdateFactory.NewPosition(new Xamarin.Forms.GoogleMaps.Position(e.Position.Latitude, e.Position.Longitude)));
			dataClass.latitude = e.Position.Latitude;
			dataClass.longitude = e.Position.Longitude;
			dataClass.hasLocation = true;
			userPin.Position = new Xamarin.Forms.GoogleMaps.Position(e.Position.Latitude, e.Position.Longitude);
            //DisplayDirections();
		}

        void OnPinClicked(object sender, PinClickedEventArgs e)
        {
			if (e.Pin.Tag.ToString() == "user")
			{
                //Navigate to profile
			}
            else
            {
                Navigation.PushAsync(new ProfilePage(1, int.Parse(e.Pin.Tag.ToString())));
			}
        }

        async void OnCameraPositionIdle(object sender, CameraPositionIdleEventArgs e)
		{
#if DEBUG || STAGING
			await fileReader.ReadFile("WorkerTasks.json", true);
			System.Diagnostics.Debug.WriteLine(e.Bounds.NorthEast.Latitude + "     " + e.Bounds.NorthEast.Longitude + "     " + e.Bounds.SouthWest.Latitude + "     " + e.Bounds.SouthWest.Longitude);
#else
            await webService.GetRequest(Constants.ROOT_URL + Constants.FETCH_TASK_URL + "?token=" + Application.Current.Properties["user_token"] + "&off_set=0&sw_lat=" + e.Bounds.SouthWest.Latitude + "&sw_long=" + e.Bounds.SouthWest.Longitude + "&ne_lat=" + e.Bounds.NorthEast.Latitude + "&ne_long=" + e.Bounds.NorthEast.Longitude);

#endif
        }

		protected override void OnAppearing()
		{
			base.OnAppearing();
			DependencyService.Get<DependencyServices.iStatusStyle>().StatusStyle(0);
			MapType_Refresh();
			DisplayDirections();
		}

		protected override void OnDisappearing()
		{
			base.OnDisappearing();
			MapType_Refresh();
		}

		void MapType_Refresh()
		{
			if (Device.RuntimePlatform == Device.iOS)
			{
				progressMap.MapType = MapType.Hybrid;
				progressMap.MapType = MapType.Street;
			}
		}

		public void ReceiveJSONData(JObject jsonData)
		{
			if (int.Parse(jsonData["status"].ToString()) == 200)
			{
				var json = JsonConvert.DeserializeObject<ObservableCollection<Tasks>>(jsonData["tasks"].ToString());

				switch (serviceType)
				{
					case 0: //fetch and pagination
						foreach (var task in json)
						{
							if (!progressMap.Pins.ToList().Exists(pin => pin.Tag.ToString() == task.id.ToString()))
							{
								workerPin = new Pin()
								{
									Type = PinType.Place,
									Label = "",
									Position = new Xamarin.Forms.GoogleMaps.Position(task.latitude, task.longitude),
									//Icon = BitmapDescriptorFactory.FromView(new WorkerPinView(task.task_type,task.user_image)),
                                    Icon = BitmapDescriptorFactory.FromView(new WorkerPinView(task.task_type, "")),
									Tag = task.id,
									IsVisible = true
								};
								progressMap.Pins.Add(workerPin);
							}
						}
                        DisplayDirections();

						break;
					case 1: //other web service process


						break;
				}
			}
			else
			{

			}
		}

		public void ReceiveTimeoutError(string title, string error)
		{

		}

		async void DisplayDirections()
		{
			if (userPin == null || workerPin == null)
				return;
			var directions = new Directions();
			var result = await directions.RequestDirections(userPin.Position.Latitude.ToString() + "," + userPin.Position.Longitude.ToString(), workerPin.Position.Latitude.ToString() + "," + workerPin.Position.Longitude.ToString(), Constants.GOOGLE_API_KEY, mode:"driving");

			if (result == null)
				return;

			progressMap.Polylines.Remove(polyline);
			polyline = new Polyline();
            polyline.Positions.Add(userPin.Position);
			foreach (Xamarin.Forms.GoogleMaps.Position point in result)
			{
				polyline.Positions.Add(point);
			}
            polyline.Positions.Add(workerPin.Position);
			Title = String.Empty;
			polyline.StrokeColor = Constants.THEME_COLOR_GREEN;
            polyline.StrokeWidth = 5;
			progressMap.Polylines.Add(polyline);
		}
    }
}
