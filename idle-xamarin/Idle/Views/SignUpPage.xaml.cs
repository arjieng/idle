﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.Diagnostics;
using Xamarin.Forms;
using idle.Views.SignUpPages;
using System.Threading.Tasks;
using System.Windows.Input;

namespace idle.Views
{
    public partial class SignUpPage : ContentPage
    {
        public static readonly BindableProperty PagePositionProperty = BindableProperty.Create(nameof(PagePosition), typeof(int),typeof(SignUpPage),0);
        public int PagePosition
        {
            get { return (int)GetValue(PagePositionProperty); }
            set{SetValue(PagePositionProperty,value);}
        }

		public static readonly BindableProperty CloseButtonProperty = BindableProperty.Create(nameof(CloseButton), typeof(ICommand), typeof(SignUpPage), null);
		public ICommand CloseButton
		{
			get { return (ICommand)GetValue(CloseButtonProperty); }
			set { SetValue(CloseButtonProperty, value); }
  		}

        public static EventHandler<int> PositionChange;

        bool isCloseClick;
        public SignUpPage()
        {
            InitializeComponent();

            PositionChange += OnPositionChange;
			cv.PositionSelected += OnPositionSelected;

            CloseButton = new Command(async(obj) => { if (isCloseClick == false) { isCloseClick = true; await Navigation.PopModalAsync(); } });

			FormattedString label = new FormattedString();

			label.Spans.Add(new Span() { Text = "Idle uses Stripe to handle payments for transactions. Stripe is an online and mobile payment platform that makes it really easy to send and accept payments through your phone. Learn more here:", ForegroundColor = Color.Black, FontFamily = Constants.PROXIMA_REG, FontSize = Constants.BUTTON_FONTSIZE });
			label.Spans.Add(new Span() { Text = " https://stripe.com/about", ForegroundColor = Constants.LINK_COLOR, FontFamily = Constants.PROXIMA_REG, FontSize = Constants.BUTTON_FONTSIZE });

			var view = new List<View>();

            view.Add(new SignUpFirstPage());
            view.Add(new SignUpSecondPage(){ BindingContext = 1 });
            view.Add(new SignUpThirdPage(){ BindingContext = new { about = label } });

            cv.ItemsSource = view;
            cv.ItemSelected += OnItemSelected;

            DependencyService.Get<DependencyServices.iStatusStyle>().StatusStyle(1);
        }

		public SignUpPage(int pageNum)
		{
			InitializeComponent();

			PositionChange += OnPositionChange;
			cv.PositionSelected += OnPositionSelected;

			CloseButton = new Command(async (obj) => { if (isCloseClick == false) { isCloseClick = true; await Navigation.PopModalAsync(); } });

			var view = new List<View>();
            if (Device.RuntimePlatform == Device.Android)
            {
                view.Add(new SignUpFirstPage() { BindingContext = 1 });
                view.Add(new SignUpSecondPage() { BindingContext = 1 });
            }
			view.Add(new SignUpThirdPage() { BindingContext = 1 });

			cv.ItemsSource = view;
			cv.ItemSelected += OnItemSelected;

            PagePosition = pageNum;
			if (Device.RuntimePlatform == Device.Android)
			{
            cv.Position = pageNum;
            }
			
			DependencyService.Get<DependencyServices.iStatusStyle>().StatusStyle(1);
		}

        protected override void OnAppearing()
		{
			base.OnAppearing();
			DependencyService.Get<DependencyServices.iStatusStyle>().StatusStyle(1);
		}

        protected override void OnDisappearing()
        {
            base.OnDisappearing();
        }

		private void OnPositionChange(object sender, int e)
        {
            cv.Position = e;

        }

        private void OnItemSelected(object sender, SelectedItemChangedEventArgs e)
        {
            var view = (ContentView)e.SelectedItem;
        }

        private void OnPositionSelected(object sender, SelectedPositionChangedEventArgs e)
        {
            PagePosition = (int)e.SelectedPosition;
        }

		public static int position = 0;
       
    }
}
