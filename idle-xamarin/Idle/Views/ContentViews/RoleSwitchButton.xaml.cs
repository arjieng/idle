﻿using System;
using System.Collections.Generic;

using Xamarin.Forms;

namespace idle
{
    public partial class RoleSwitchButton : ContentView
    {
		public static readonly BindableProperty IsToggledProperty = BindableProperty.Create("IsToggled", typeof(int), typeof(RoleSwitchButton), 0, propertyChanged: OnProperty_Changed);
        public RoleSwitchButton()
        {
            InitializeComponent();
        }

		static void OnProperty_Changed(BindableObject bindable, object oldValue, object newValue)
		{
			CircularBoxView backgroundButton = (bindable as ContentView).Content.FindByName<CircularBoxView>("roleButtonBackground");
			CircularBoxView switchButton = (bindable as ContentView).Content.FindByName<CircularBoxView>("roleButtonSwitch");
            TextView switchLabel = (bindable as ContentView).Content.FindByName<TextView>("roleLabelSwitch"); 

			if (Int32.Parse(newValue.ToString()) == 1)
			{
				backgroundButton.BackgroundColor = Constants.SWITCH_ON_BACKGROUND_COLOR;
				switchButton.BackgroundColor = Constants.SWITCH_ON_COLOR;
				switchButton.TranslateTo(16, 0, 500, Easing.CubicOut);
                switchLabel.Text = "PERFORMER";
			}
			else
			{
				backgroundButton.BackgroundColor = Constants.SWITCH_OFF_BACKGROUND_COLOR;
				switchButton.BackgroundColor = Constants.SWITCH_OFF_COLOR;
				switchButton.TranslateTo(0, 0, 500, Easing.CubicOut);
                switchLabel.Text = "POSTER";
			}
		}

		public int IsToggled
		{
			get { return Int32.Parse(base.GetValue(IsToggledProperty).ToString()); }
			set { SetValue(IsToggledProperty, value); }
		}
    }
}
