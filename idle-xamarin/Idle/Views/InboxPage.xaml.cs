﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Xamarin.Forms;
using System.Linq;
using System.Threading;

namespace idle.Views
{
    public partial class InboxPage : RootViewPage, iFileConnector, iRestConnector
    {
#if DEBUG
		FileReader fileReader = new FileReader();
#else
        RestServices webService = new RestServices();
#endif
        CancellationTokenSource cts;
        DataClass dataClass = DataClass.GetInstance;
        //ObservableCollection<Conversation> conversations;
        JToken pagination;

        public InboxPage()
        {
            InitializeComponent();

            //conversations = new ObservableCollection<Conversation>();
            dataClass.MyConversations = new ObservableCollection<Conversation>();
#if DEBUG 
			fileReader.FileReaderDelegate = this;
#else
            webService.WebServiceDelegate = this;
#endif
            this.PageTitle = "MESSAGES";
            this.TitleFontFamily = Constants.LULO;
            this.TitleFontColor = Constants.DARK_STRONG;
            this.LeftIcon = "BurgerDark";
            this.LeftButtonCommand = new Command((obj) => { MyMasterDetailPage detail = (MyMasterDetailPage)this.Parent.Parent; detail.IsPresented = true; DependencyService.Get<DependencyServices.iStatusStyle>().StatusStyle(0); });

            GetData();

        }

        protected override void OnAppearing()
        {
            base.OnAppearing();

			MessagingCenter.Subscribe<App, JObject>(this, "MessageReceived", (sender, obj) =>
			{
				int convoID = int.Parse(obj["custom_data"]["conversation_id"].ToString());
				string messageJson = obj["custom_data"]["message"].ToString();
				Message message = JsonConvert.DeserializeObject<Message>(obj["custom_data"]["message"].ToString());
				int messageIndex = dataClass.MyConversations.ToList().FindIndex((mes) => mes.id == convoID);
				dataClass.MyConversations[messageIndex].message = message.body;
				dataClass.MyConversations.Move(messageIndex, 0);
			});

            MyMasterDetailPage.isHomePage = true;

            DependencyService.Get<DependencyServices.iStatusStyle>().StatusStyle(1);
        }

        protected override void OnDisappearing()
        {
            base.OnDisappearing();

            MessagingCenter.Unsubscribe<App, JObject>(this, "MessageReceived");

            MyMasterDetailPage.isHomePage = false;

			if (cts != null)
			{
				cts.Cancel();
			}
        }

        async void GetData()
        {
            dataClass.MyConversations.Clear();
            loadingIndicator.IsRunning = true;
            cts = new CancellationTokenSource();
#if DEBUG
			await fileReader.ReadFile("Inbox.json", true, cts.Token);
#else
            string url = Constants.ROOT_URL + Constants.ACCOUNTS_URL + Constants.CONVERSATION_URL + Constants.TOKEN_URL + DataClass.GetInstance.UserToken;
			
			try
			{
                await webService.GetRequest(url, cts.Token);
			}
			catch (OperationCanceledException)
			{
				DependencyService.Get<iConsole>().DisplayText("Web Service: Cancelled!");
			}
			catch (Exception)
			{
				DependencyService.Get<iConsole>().DisplayText("Web Service: Failed!");
			}

			cts = null;
#endif

        }

        async void OnLast_ItemAppearing(object sender, ItemVisibilityEventArgs e)
        {
#if DEBUG == false
            var currentItem = e.Item as Conversation;
            var lastItem = dataClass.MyConversations[dataClass.MyConversations.Count - 1];
            if (currentItem == lastItem && int.Parse(pagination["load_more"].ToString()) == 1)
            {
                loadingIndicator.IsRunning = true;

				cts = new CancellationTokenSource();
				try
				{
                    await webService.GetRequest(Constants.ROOT + pagination["url"], cts.Token);
				}
				catch (OperationCanceledException)
				{
					DependencyService.Get<iConsole>().DisplayText("Web Service: Cancelled!");
				}
				catch (Exception)
				{
					DependencyService.Get<iConsole>().DisplayText("Web Service: Failed!");
				}

				cts = null;
		    }
#endif
		}

        void OnMessage_ItemTapped(object sender, ItemTappedEventArgs e)
        {
            inboxList.SelectedItem = null;
            var message = e.Item as Conversation;
            Navigation.PushAsync(new Views.MessagePage(message.user.name, message.user, message.id, 0));
        }

		public void ReceiveJSONData(JObject jsonData)
		{

		}

        public void ReceiveJSONData(JObject jsonData, CancellationToken ct)
        {
            if (int.Parse(jsonData["status"].ToString()) == 200)
            {
                var inbox = JsonConvert.DeserializeObject<ObservableCollection<Conversation>>(jsonData["conversations"].ToString());
                foreach (var convo in inbox)
                {
                    dataClass.MyConversations.Add(convo);
                }
                inboxList.ItemsSource = dataClass.MyConversations;

                pagination = jsonData["pagination"];
            }
            else
            {

            }
            loadingIndicator.IsRunning = false;
        }

        public async void ReceiveTimeoutError(string title, string error)
        {
            loadingIndicator.IsRunning = false;
            await DisplayAlert(title, error, "Okay");
			
        }
    }
}
