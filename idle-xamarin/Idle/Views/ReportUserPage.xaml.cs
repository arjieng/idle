﻿using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Xamarin.Forms;

namespace idle.Views
{
    public partial class ReportUserPage : RootViewPage, iRestConnector
    {
        bool IsClicked = false;
        int pageType = 0;
        CancellationTokenSource cts;
#if DEBUG == false
        RestServices webService = new RestServices();
#endif
        public ReportUserPage(int id, User user, int pageType = 0)
        {
            InitializeComponent();
            this.pageType = pageType;

            this.PageTitle = "REPORT USER";
            this.TitleFontFamily = Constants.LULO;
            this.TitleFontColor = Constants.DARK_STRONG;
            this.LeftIcon = "ArrowDark";
            this.LeftButtonCommand = new Command((obj) => { Navigation.PopAsync(); if(pageType == 1){ Navigation.PopAsync(false); } });

            BindingContext = user;
        }

        protected override void OnAppearing()
        {
            base.OnAppearing();
            IsClicked = false;
            DependencyService.Get<DependencyServices.iStatusStyle>().StatusStyle(1);

#if DEBUG == false
            webService.WebServiceDelegate = this;
#endif
        }

        async void OnReport_Clicked(object sender, System.EventArgs e)
        {
			bool isEmpty, isNotError = true;
			if (Device.RuntimePlatform == Device.iOS)
			{
				isEmpty = reportEditor.Text.Equals(reportEditor.Placeholder);
			}
			else
			{
				isEmpty = string.IsNullOrEmpty(reportEditor.Text);
			}

			if (isEmpty)
			{
				EditorError(reportEditor, "Explanation is required to proceed!");
				isNotError = false;
			}

            if(isNotError && !IsClicked)
            {
                IsClicked = true;
#if DEBUG
                await DisplayAlert("Report Submitted", "Thank you for your message, we will get back to you shortly", "Okay");
                await Navigation.PopAsync();
#else

				var user = (User)BindingContext;
				var json = JsonConvert.SerializeObject(new { report = new { block_id = user.id, description = reportEditor.Text }, token = DataClass.GetInstance.UserToken });
				var url = Constants.ROOT_URL + Constants.USERS_URL + "/" + user.id + Constants.REPORT_URL;

				cts = new CancellationTokenSource();
				try
				{
					await webService.PostRequestAsync(url, json, cts.Token);
				}
				catch (OperationCanceledException)
				{
					DependencyService.Get<iConsole>().DisplayText("Web Service: Cancelled!");
				}
				catch (Exception)
				{
					DependencyService.Get<iConsole>().DisplayText("Web Service: Failed!");
				}

				cts = null;
#endif
            }
        }

		void EditorError(CustomEditor entry, string errorMessage)
		{
			entry.PlaceholderColor = Color.Red;
			entry.Placeholder = errorMessage;
			entry.Text = "";
		}

		public void ReceiveJSONData(JObject jsonData)
		{

		}

        public void ReceiveJSONData(JObject jsonData, CancellationToken ct)
        {
            if (int.Parse(jsonData["status"].ToString()) == 200)
            {
                DisplayAlert("Report Submitted", "Thank you for your message, we will get back to you shortly", "Okay");
                Navigation.PopAsync();
                if (pageType == 1)
                { 
                    Navigation.PopAsync(false);
                }
            }
            else
            {
                
            }

            IsClicked = false;
        }

        public async void ReceiveTimeoutError(string title, string error)
        {
            IsClicked = false;
			await DisplayAlert(title, error, "Okay");
        }
    }
}
