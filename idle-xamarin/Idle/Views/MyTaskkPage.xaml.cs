﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.Threading.Tasks;
using idle.Views;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Xamarin.Forms;
using System.Linq;
using System.Collections;

namespace idle.Views
{
    public partial class MyTaskkPage : RootViewPage, iFileConnector, iRestConnector
    {
        int buttonType = 0;
        bool isAddClick = false, isInProgressButton = true;
        Button previousButton;
        ObservableCollection<Tasks> inProgress;
        ObservableCollection<Tasks> historyBid;


        //sample
        int isOwner = 1;
        JObject inProgressPagination;
        JObject histBidPagination;

        int requestType;

#if DEBUG || STAGING
        FileReader fileReader = new FileReader();
#else
        RestService webService = new RestService();
#endif
        public MyTaskkPage()
        {
            InitializeComponent();

            this.PageTitle = "TASKS";
            this.TitleFontFamily = Constants.LULO;
            this.TitleFontColor = Constants.DARK_STRONG;

            // initializing lists
            inProgress = new ObservableCollection<Tasks>();
            historyBid = new ObservableCollection<Tasks>();
            inProgressPagination = new JObject();
            histBidPagination = new JObject();

            //set default clicked button
            SetButtonClicked(inProgressButton);

            if (DataClass.GetInstance.UserIsPerfomer == 1)
            {
                histBidButton.Text = "Bid";
            }
            Debug.WriteLine("IsOwner: {0}", DataClass.GetInstance.UserIsPerfomer);

#if DEBUG || STAGING
            fileReader.FileReaderDelegate = this;
#else
            webService.WebServiceDelegate = this;
#endif

            this.LeftIcon = "BurgerDark";
            this.LeftButtonCommand = new Command((obj) => { MyMasterDetailPage detail = (MyMasterDetailPage)this.Parent.Parent; detail.IsPresented = true; DependencyService.Get<DependencyServices.iStatusStyle>().StatusStyle(0); });
            this.RightIcon2 = "PlusDark";
            // 0 - New Task
            // 0 - ID
            this.RightButton2Command = new Command((obj) => { Navigation.PushModalAsync(new NewTaskPage(-1)); });

            //Task.Run( async () => 
            //{
#if DEBUG || STAGING
            GetData();
#else

#endif
            //});

            //this.type = type;
            taskList.ItemsSource = inProgress;
            DependencyService.Get<DependencyServices.iStatusStyle>().StatusStyle(1);
        }
        async void GetData()
        {
            await fileReader.ReadFile("InProgress.json", true);
            requestType = 1;
            await fileReader.ReadFile("History.json", true);
        }


        void OnMyTask_Clicked(object sender, EventArgs e)
        {
            var button = (Button)sender;

            //if (isOwner == 1)
            //{
            if (int.Parse(button.StyleId) == 0)
            {
                isInProgressButton = true;
                taskList.ItemsSource = inProgress;
				var first = taskList.ItemsSource.Cast<object>().FirstOrDefault();
				taskList.ScrollTo(first, ScrollToPosition.Start, false);
            }
            else
            {
                isInProgressButton = false;
                taskList.ItemsSource = historyBid;
				var first = taskList.ItemsSource.Cast<object>().FirstOrDefault();
				taskList.ScrollTo(first, ScrollToPosition.Start, false);
            }
            //}

            SetButtonClicked(button);
        }

        void SetButtonClicked(Button button)
        {
            if (previousButton != null)
            {
                previousButton.BackgroundColor = Color.White;
                previousButton.TextColor = Constants.DARK_MEDIUM;
                previousButton.FontFamily = Constants.LULO;
                previousButton.BorderColor = Constants.LIGHT_GRAY;
            }

            button.BackgroundColor = Constants.BUTTON_GREEN;
            button.BorderColor = Constants.BUTTON_GREEN;
            button.TextColor = Color.White;
            button.FontFamily = Constants.LULO_BOLD;
            previousButton = button;


        }
        async void OnTask_ItemTapped(object sender, ItemTappedEventArgs e)
        {

            taskList.SelectedItem = null;
            if (DataClass.GetInstance.UserIsPerfomer == 0 )
            {
				await Navigation.PushAsync(new TaskSummaryPage(((Tasks)e.Item).id, (Tasks)e.Item));
            }
            else
            await Navigation.PushAsync(new ViewTaskPage(((Tasks)e.Item).id, (Tasks)e.Item));
        }

        async void OnLast_ItemAppearing(object sender, Xamarin.Forms.ItemVisibilityEventArgs e)
        {
            var currentItem = e.Item as Tasks;

            var lastItem = inProgress[inProgress.Count - 1];
            if (isInProgressButton)
            {

                if (currentItem == lastItem)
                {


                    if (inProgressPagination["load_more"].ToString().Equals("1"))
                    {
                        Debug.WriteLine("Pulled");
                        requestType = 0;
                        await fileReader.ReadFile(inProgressPagination["url"].ToString(), true);
                    }
                }
            }
            else
            {
                lastItem = historyBid[historyBid.Count - 1];
                if (currentItem == lastItem)
                {
                    Debug.WriteLine("Pulled hist");
                    if (histBidPagination["load_more"].ToString().Equals("1"))
                    {
                        Debug.WriteLine("Pulled hist");
                        requestType = 1;
                        await fileReader.ReadFile(histBidPagination["url"].ToString(), true);
                    }
                }
            }
        }

        void AddToList(ObservableCollection<Tasks> tasks, IEnumerable newList)
        {
            foreach (Tasks task in newList)
                tasks.Add(task);
        }

        public void ReceiveJSONData(JObject jsonData)
        {

            if (int.Parse(jsonData["status"].ToString()) == 200)
            {
                switch (requestType)
                {
                    case 0:
                        var tasks = JsonConvert.DeserializeObject<ObservableCollection<Tasks>>(jsonData["inprogress"].ToString());
                        AddToList(inProgress, tasks);
                        taskList.ItemsSource = inProgress;
                        inProgressPagination = jsonData["pagination"].ToObject<JObject>();
                        break;
                    case 1:
                        var history = JsonConvert.DeserializeObject<ObservableCollection<Tasks>>(jsonData["history"].ToString());
                        AddToList(historyBid, history);
                        histBidPagination = jsonData["pagination"].ToObject<JObject>();
                        break;
                }


            }
        }

        public void ReceiveTimeoutError(string title, string error)
        {
            throw new NotImplementedException();
        }
    }
}