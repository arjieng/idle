﻿using System;
using System.Collections.Generic;

using Xamarin.Forms;

namespace idle.Views
{
    public partial class PricingPage : RootViewPage
    {
        public PricingPage()
        {
            InitializeComponent();

			this.PageTitle = "PRICING";
			this.TitleFontFamily = Constants.LULO;
			this.TitleFontColor = Constants.DARK_STRONG;
			this.LeftIcon = "ArrowDark";
			this.LeftButtonCommand = new Command((obj) => { Navigation.PopAsync(); });

		}
    }
}
