﻿﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Plugin.Geolocator;
using Plugin.Geolocator.Abstractions;
using Plugin.Permissions;
using Xamarin.Forms;
using Xamarin.Forms.GoogleMaps;

namespace idle.Views
{
    public partial class MapHomePage : ContentPage, iFileConnector, iRestConnector
    {
        bool listIsClicked = false, IsAddClicked = false, allIsClicked = false, progressIsClicked = false, isShown = true, isPinClick = false, isClick = false  ;
        Tasks progressTask = new Tasks();
        Tasks prevTask;
        int serviceType = 0, TotalInProgress = 0, IsSelected = 0, offSet = 0;
        string taskCount = string.Empty;
        DataClass dataClass = DataClass.GetInstance;
        LocationHelper locationHelper = new LocationHelper();
        Pin taskPin = null;
        Pin userPin = null;
        Pin workerPin = null;
        Pin onlineUserPin = null;
        Polyline polyline = null;
        public static MapBounds bounds = new MapBounds();
        public ObservableCollection<User> OnLineUser = new ObservableCollection<User>();
#if DEBUG
        FileReader fileReader = new FileReader();
#else
        RestServices webService = new RestServices();
#endif

        public MapHomePage(int selectedButton)
        {
            InitializeComponent();
#if DEBUG
            fileReader.FileReaderDelegate = this;
#else
			webService.WebServiceDelegate = this;
#endif

			//map.MyLocationEnabled = true;
			map.CameraPositionIdle+= OnCameraPositionIdle;
            map.ClusterOptions.EnableBuckets = false;
            map.ClusterOptions.EnableClickZoomAnimation = true;
            map.ClusterClicked += OnCluster_Clicked;
            map.UiSettings.ZoomControlsEnabled = false;

            if (dataClass.hasLocation == true)
            {
                AddUserPin();
            }

            map.ClusterPinClicked += OnClusterPin_Clicked;
            map.PinClicked += OnPin_Clicked;

            dataClass.TaskList = new ObservableCollection<Tasks>();
            IsSelected = selectedButton;
            InProgressList_Trigger();

            if(IsSelected == 1)
            {
                InProgress_Trigger();
            }
            else
            {
                All_Trigger();
            }

            HideInProgressList();

            App.UiTestTapPin += OnUiTestTapPin;

            isClick = true;
            Task.Delay(2000);
        }

        protected async override void OnAppearing()
		{
			base.OnAppearing();
            isPinClick = false;
            await Task.Delay(3000);
            isClick = false;

			CrossGeolocator.Current.PositionChanged += OnPosition_Changed;
			CrossGeolocator.Current.PositionError += OnPosition_Error;

#if DEBUG
            fileReader.FileReaderDelegate = this;
#else
			webService.WebServiceDelegate = this;
#endif
			if (workerPin != null)
			{
				workerPin.IsVisible = false;
			}

			MessagingCenter.Subscribe<App>(this, "BiddingAccepted", (App) =>
			{
				if (dataClass.TaskList.ToList().Exists(acceptedTask => acceptedTask.id == (dataClass.InProgressPerformList[0]).id))
				{
					dataClass.TaskList.Remove(dataClass.TaskList.ToList().Find(acceptedTask => acceptedTask.id == (dataClass.InProgressPerformList[0]).id));
				}

				InProgressList_Trigger();
				InProgress_Trigger();
			});

			MessagingCenter.Subscribe<App, JObject>(this, "BiddingDeclined", (sender, obj) =>
			{
				if (dataClass.TaskList.ToList().Exists(bidTask => bidTask.id == int.Parse(obj["custom_data"]["task_id"].ToString())))
				{
					dataClass.TaskList.ToList().Find(bidTask => bidTask.id == int.Parse(obj["custom_data"]["task_id"].ToString())).bid = "0.00";
				}
			});

			
			MessagingCenter.Subscribe<App, JObject>(this, "TaskFinished", (sender, obj) =>
			{
				if (dataClass.UserIsPerfomer == 1 && IsSelected == 1)
				{
					if (dataClass.InProgressPerformList.ToList().Exists(inProgressTask => inProgressTask.id == int.Parse(obj["custom_data"]["task_id"].ToString())))
					{
						dataClass.InProgressPerformList.Remove(dataClass.InProgressPerformList.ToList().Find(inProgressTask => inProgressTask.id == int.Parse(obj["custom_data"]["task_id"].ToString())));
					}
					InProgressList_Trigger();
					InProgress_Trigger();
				}
			});

			
			MessagingCenter.Subscribe<App>(this, "TaskCancelled", (App) =>
			{
				InProgressList_Trigger();
				InProgress_Trigger();
			});


            MessagingCenter.Subscribe<App, JObject>(this, "PerformerLocationUpdated", (sender, obj) =>
			{
                dataClass.InProgressPosterList.ToList().Find(task => task.id == int.Parse(obj["task_id"].ToString())).user.latitude = double.Parse(obj["latitude"].ToString());
                dataClass.InProgressPosterList.ToList().Find(task => task.id == int.Parse(obj["task_id"].ToString())).user.longitude = double.Parse(obj["longitude"].ToString());

                dataClass.Update(false);

                if(map.CustomPins.ToList().Find(pin => pin.Tag.ToString() == obj["user_id"].ToString()) != null)
                {
                    (map.CustomPins.ToList().Find(pin => pin.Tag.ToString() == obj["user_id"].ToString())).Position = new Xamarin.Forms.GoogleMaps.Position(double.Parse(obj["latitude"].ToString()), double.Parse(obj["longitude"].ToString()));
                }


                if(OnLineUser.ToList().Find(user => user.id.ToString() == obj["user_id"].ToString()) != null)
                {
                    (OnLineUser.ToList().Find(user => user.id.ToString() == obj["user_id"].ToString())).latitude = double.Parse(obj["latitude"].ToString());
                    (OnLineUser.ToList().Find(user => user.id.ToString() == obj["user_id"].ToString())).longitude = double.Parse(obj["longitude"].ToString());
                }


                if(IsSelected == 1 && dataClass.UserIsPerfomer == 0 && workerPin.Tag.ToString() == obj["user_id"].ToString())
                {
                    workerPin.Position = new Xamarin.Forms.GoogleMaps.Position(double.Parse(obj["latitude"].ToString()), double.Parse(obj["longitude"].ToString()));
                    DisplayDirections(userPin.Position, workerPin.Position);
                }
			});


			DependencyService.Get<DependencyServices.iStatusStyle>().StatusStyle(0);

			if (dataClass.hasLocation != true)
			{
				var answer = await DisplayAlert("Location Services Disabled", "Idle app would like to access your current location in order to see nearby tasks.", "Yes", "No");
				if (answer == true)
				{
					CrossPermissions.Current.OpenAppSettings();
				}
			}

			listIsClicked = false;
			IsAddClicked = false;

			if (dataClass.PinToRemove != null)
			{
                if (dataClass.PinTypeToRemove == 0)
                {
                    if (map.ClusteredPins.ToList().Exists(pin => pin.Tag.ToString() == dataClass.PinToRemove.id.ToString()))
                    {
                        if (dataClass.TaskList.ToList().Exists(pin => pin.id == dataClass.PinToRemove.id))
                        {
                            dataClass.TaskList.Remove(dataClass.PinToRemove);
                        }
                        var pinRemove = map.ClusteredPins.ToList().Find(pin => pin.Tag.ToString() == dataClass.PinToRemove.id.ToString());
                        map.ClusteredPins.Remove(pinRemove);
                        map.Cluster();
                    }
                }
                else
                {
                    workerPin.IsVisible = false;
                }
                dataClass.PinToRemove = null;
			}

			if (polyline != null)
			{
				map.Polylines.Remove(polyline);
				polyline = null;
			}

            InProgressList_Trigger();

			if (IsSelected == 1)
			{
				InProgress_Trigger();
			}
			else
			{
				All_Trigger();
			}
		}

		protected override void OnDisappearing()
		{
			base.OnDisappearing();

			if (cts != null)
			{
				cts.Cancel();
			}

			CrossGeolocator.Current.PositionChanged -= OnPosition_Changed;
			CrossGeolocator.Current.PositionError -= OnPosition_Error;

            MessagingCenter.Unsubscribe<App>(this, "BiddingAccepted");
            MessagingCenter.Unsubscribe<App, JObject>(this, "BiddingDeclined");
            MessagingCenter.Unsubscribe<App, JObject>(this, "TaskFinished");
            MessagingCenter.Unsubscribe<App>(this, "TaskCancelled");
            MessagingCenter.Unsubscribe<App, JObject>(this, "PerformerLocationUpdated");
		}

		void OnUiTestTapPin(object sender, int e)
		{
			Navigation.PushAsync(new ViewTaskPage(offSet, dataClass.TaskList.Where(arg => arg.id.Equals(int.Parse(map.ClusteredPins[e].Tag.ToString()))).Single(), 2));
		}

        void OnCluster_Clicked(object sender, ClusterClickedEventArgs e)
        {
            e.Handled = true;   
        }

        void OnBack_Clicked(object sender, System.EventArgs e)
        {
            if (isClick == false)
            {
				if (cts != null)
				{
					cts.Cancel();
				}
                Navigation.PopAsync(false); dataClass.TaskList = null;
            }
        }

        void OnList_Clicked(object sender, System.EventArgs e)
        {
            if(!listIsClicked)
            {
				if (cts != null)
				{
					cts.Cancel();
				}
                NavigationPage navPage = new NavigationPage(new TaskPage(5, 0)); Navigation.PushModalAsync(navPage);
                listIsClicked = true;
            }
        }

        void OnAdd_Clicked(object sender, System.EventArgs e)
        {
            if(!IsAddClicked)
            {
                IsAddClicked = true;
				if (dataClass.UserIsPerfomer == 1)
				{
					DisplayAlert("Performer Restriction", "You are currently on performing mode. Switch back to poster mode in order for you to post new tasks.", "Okay");
                    IsAddClicked = false;
				}
				else
				{
					if (cts != null)
					{
						cts.Cancel();
					}
					Navigation.PushModalAsync(new NewTaskPage(-1));
				}
			}  
        }

        void OnAll_Clicked(object sender, System.EventArgs e)
        {
            if(!allIsClicked)
            {
                All_Trigger();	
            }
        }

        void All_Trigger()
        {
			IsSelected = 0;
			BindingContext = new { IsSelected, taskCount, TotalInProgress };

			if (polyline != null)
			{
				map.Polylines.Remove(polyline);
				polyline = null;
			}

			if (userPin != null)
			{
				userPin.Position = new Xamarin.Forms.GoogleMaps.Position(dataClass.latitude, dataClass.longitude);
			}

			if (workerPin != null)
			{
				workerPin.IsVisible = false;
                workerPin.Tag = "worker";
			}

			AddTaskPin();

            OnlineUsers_Show(true);

			allIsClicked = true;
			progressIsClicked = false;   
        }

        void OnInProgress_Clicked(object sender, System.EventArgs e)
        {
            if(!progressIsClicked)
            {
                InProgress_Trigger();	
            }
        }

        void OnlineUsers_Show(bool isVisible)
        {
			foreach (var onlineUser in OnLineUser)
			{
			    (map.CustomPins.ToList().Find(pin => pin.Tag.ToString() == onlineUser.id.ToString())).IsVisible = isVisible;
                (map.CustomPins.ToList().Find(pin => pin.Tag.ToString() == onlineUser.id.ToString())).Position = new Xamarin.Forms.GoogleMaps.Position(onlineUser.latitude, onlineUser.longitude);
                if(map.CustomPins.ToList().Find(pin => pin.Tag.ToString() == "worker") != null)
                {
                    (map.CustomPins.ToList().Find(pin => pin.Tag.ToString() == "worker")).IsVisible = !isVisible;
                }
			}
        }

        void InProgress_Trigger()
        {
			IsSelected = 1;
			BindingContext = new { IsSelected, taskCount, TotalInProgress };

            OnlineUsers_Show(false);
            map.ClusteredPins.Clear();

            if(TotalInProgress > 0)
            {
				progressTask = dataClass.UserIsPerfomer == 1 ? dataClass.InProgressPerformList[0] : dataClass.InProgressPosterList[0];
				AddWorkerPin(progressTask);
				prevTask = progressTask;    
            }
            else
            {
                ListContainer.IsVisible = false;
                ListContainer.InputTransparent = true;
            }

			progressIsClicked = true;
			allIsClicked = false;   
        }

        void InProgressList_Trigger()
        {
            if(dataClass.UserIsPerfomer == 1)
            {
				if (dataClass.InProgressPerformList.Count > 0)
				{
					progressTask = dataClass.InProgressPerformList[0];
                    taskList.ItemsSource = dataClass.InProgressPerformList;
                    TotalInProgress = dataClass.InProgressPerformList.Count;
                    inProgressButton.Text = "IN PROGRESS ( " + TotalInProgress + " )";
				}
                else
                {
					taskList.ItemsSource = null;
					TotalInProgress = 0;
                    inProgressButton.Text = "IN PROGRESS";
                    HideCenterButton();      
                }
            }
            else
            {
				if (dataClass.InProgressPosterList.Count > 0)
				{
					progressTask = dataClass.InProgressPosterList[0];
                    taskList.ItemsSource = dataClass.InProgressPosterList;
                    TotalInProgress = dataClass.InProgressPosterList.Count;
                    inProgressButton.Text = "IN PROGRESS ( " + TotalInProgress + " )";
				}
				else
				{
					taskList.ItemsSource = null;
					TotalInProgress = 0;
                    inProgressButton.Text = "IN PROGRESS";
                    HideCenterButton();
				}
            }

		    taskCount = TotalInProgress == 0 ? "0" : TotalInProgress == 1 ? "1 task" : TotalInProgress.ToString() + " tasks";
			BindingContext = new { IsSelected, taskCount, TotalInProgress };
        }

        void HideInProgressList()
        {
			if (isShown && TotalInProgress > 1)
			{
                switch(TotalInProgress)
                {
					case 1: break;
					case 2: ListContainer.TranslateTo(0, Constants.INPROGRESS_ONE_HEIGHT, 300, Easing.CubicOut); break;
					default: ListContainer.TranslateTo(0, Constants.INPROGRESS_TWO_HEIGHT, 300, Easing.CubicOut); break;
				}
                dropDownLogo.Rotation = 180;
                isShown = false;
			}
        }

		void HideCenterButton()
		{
            All_Trigger();
            ListContainer.IsVisible = false;
            ListContainer.InputTransparent = true;
		}

        void AddUserPin()
        {
            map.MoveToRegion(MapSpan.FromCenterAndRadius(new Xamarin.Forms.GoogleMaps.Position(dataClass.latitude, dataClass.longitude), Distance.FromMiles(0.1)));
            if(userPin == null)
            {
				userPin = new Pin()
				{
					Icon = BitmapDescriptorFactory.FromView(new WorkerPinView(5, dataClass.UserImage)),
					Type = PinType.Place,
					Label = "",
					Position = new Xamarin.Forms.GoogleMaps.Position(dataClass.latitude, dataClass.longitude),
					Tag = "user",
					IsVisible = true
				};
                if (map != null)
                {
                    map.CustomPins.Add(userPin);
                }
            }
            if(workerPin == null)
            {
				workerPin = new Pin()
				{
					Icon = BitmapDescriptorFactory.FromView(new WorkerPinView(5, dataClass.UserImage)),
					Type = PinType.Place,
					Label = "",
					Position = new Xamarin.Forms.GoogleMaps.Position(dataClass.latitude, dataClass.longitude),
					Tag = "worker",
					IsVisible = false
				};
                if (map != null)
                {
                    map.CustomPins.Add(workerPin);
                }
            }
        }

		void AddWorkerPin(Tasks task)
        {
            try
            {
				if (dataClass.UserIsPerfomer == 1)
				{
					if (userPin != null)
					{
						userPin.Position = new Xamarin.Forms.GoogleMaps.Position(dataClass.latitude, dataClass.longitude);
					}
					if (workerPin != null)
					{
						workerPin.IsVisible = true;
						workerPin.Position = new Xamarin.Forms.GoogleMaps.Position(task.latitude, task.longitude);
						workerPin.Tag = task.user.id;
						workerPin.Icon = BitmapDescriptorFactory.FromView(new WorkerPinView(task.task_type, task.user.image));
						DisplayDirections(new Xamarin.Forms.GoogleMaps.Position(dataClass.latitude, dataClass.longitude), new Xamarin.Forms.GoogleMaps.Position(task.latitude, task.longitude));
					}
				}
				else
				{
					if (userPin != null)
					{
						userPin.Position = new Xamarin.Forms.GoogleMaps.Position(task.latitude, task.longitude);
					}
					if (workerPin != null)
					{
						workerPin.IsVisible = true;
						workerPin.Position = new Xamarin.Forms.GoogleMaps.Position(task.user.latitude, task.user.longitude);
						workerPin.Tag = task.user.id;
						workerPin.Icon = BitmapDescriptorFactory.FromView(new WorkerPinView(task.task_type, task.user.image));
						DisplayDirections(new Xamarin.Forms.GoogleMaps.Position(task.latitude, task.longitude), new Xamarin.Forms.GoogleMaps.Position(task.user.latitude, task.user.longitude));
					}
				}
            }
            catch(NullReferenceException ex)
            {
                DependencyService.Get<iConsole>().DisplayText("Add Worker Pin Error: " + ex);
            }
        }

        async void DisplayDirections(Xamarin.Forms.GoogleMaps.Position userPosition, Xamarin.Forms.GoogleMaps.Position workerPosition)
		{
			if (userPin == null || workerPin == null)
				return;
			var directions = new Directions();
			var result = await directions.RequestDirections(userPosition.Latitude.ToString() + "," + userPosition.Longitude.ToString(), workerPosition.Latitude.ToString() + "," + workerPosition.Longitude.ToString(), Constants.GOOGLE_DIRECTION_API, mode: "driving");

			if (result == null)
				return;

			map.Polylines.Remove(polyline);
			polyline = new Polyline();

			polyline.Positions.Add(userPosition);

			foreach (Xamarin.Forms.GoogleMaps.Position point in result)
			{
				polyline.Positions.Add(point);
			}

			polyline.Positions.Add(workerPosition);

			Title = String.Empty;
			polyline.StrokeColor = Constants.THEME_COLOR_GREEN;
			polyline.StrokeWidth = 5;
            if(map != null)
            {
                if(IsSelected == 1)
                {
                    map.Polylines.Add(polyline);
				}
            }
		}

        void AddTaskPin()
        {
            if (dataClass.TaskList != null)
            {
                if (dataClass.TaskList.Count > 0)
                {
                	foreach (Tasks task in dataClass.TaskList)
                	{
                		if (!map.ClusteredPins.ToList().Exists(pin => pin.Tag.ToString() == task.id.ToString()) && !dataClass.InProgressPerformList.ToList().Exists(progress => progress.id.ToString() == task.id.ToString()))
                        {
                            taskPin = new Pin()
                            {
                                Type = PinType.Place,
                                Label = "",
                                Position = new Xamarin.Forms.GoogleMaps.Position(task.latitude, task.longitude),
                                Icon = BitmapDescriptorFactory.FromBundle(Constants.PinIcon(task.task_type)),
                                Tag = task.id,
                                IsVisible = true
                            };
                            if (map != null)
                            {
                                map.ClusteredPins.Add(taskPin);
                                map.Cluster();
                            }
                        }
                    }
                }
            }
        }

        void ShowHideList()
        {
			if (isShown)
			{
				switch (TotalInProgress)
				{
					case 1: break;
					case 2: ListContainer.TranslateTo(0, Constants.INPROGRESS_ONE_HEIGHT, 300, Easing.CubicOut); break;
					default: ListContainer.TranslateTo(0, Constants.INPROGRESS_TWO_HEIGHT, 300, Easing.CubicOut); break;
				}
				dropDownLogo.Rotation = 180;
                isShown = false;
			}
			else
			{
				ListContainer.TranslateTo(0, 0, 300, Easing.CubicOut);
				dropDownLogo.Rotation = 0;
                isShown = true;
			}    
        }

		void OnDropDown_Tapped(object sender, EventArgs e)
		{
            ShowHideList();   
		}

        void OnList_SwipedUp(object sender, System.EventArgs e)
        {
            ShowHideList();   
        }

		void OnList_SwipedDown(object sender, System.EventArgs e)
		{
            ShowHideList();
		}

        void OnTask_ItemTapped(object sender, ItemTappedEventArgs e)
		{
            taskList.SelectedItem = null;
			if (cts != null)
			{
				cts.Cancel();
			}
            if (dataClass.UserIsPerfomer == 1)
            {
				// offSet, task, pageType
				Navigation.PushAsync(new ViewTaskPage(offSet, (Tasks)e.Item, 2));
            }
            else
            {
                Navigation.PushAsync(new TaskSummaryPage(((Tasks)e.Item).id, (Tasks)e.Item, 10 , 0));
            }
		}

        void OnCheck_Clicked(object sender, System.EventArgs e)
        {
			if (prevTask.id != int.Parse(((Button)sender).ClassId))
			{
                if (dataClass.UserIsPerfomer == 1)
                {
                    prevTask = dataClass.InProgressPerformList.ToList().Find(arg => arg.id == int.Parse(((Button)sender).ClassId));
                }
                else
                {
                    prevTask = dataClass.InProgressPosterList.ToList().Find(arg => arg.id == int.Parse(((Button)sender).ClassId));
                }
				
				if (polyline != null)
				{
					map.Polylines.Remove(polyline);
					polyline = null;
				}

                AddWorkerPin(prevTask);
			}
        }

        //Plugin Geolocator
        void OnPosition_Error(object sender, PositionErrorEventArgs e)
        {
            DependencyService.Get<iConsole>().DisplayText(e.Error.ToString());
        }

        //Plugin Geolocator
        void OnPosition_Changed(object sender, PositionEventArgs e)
        {
            if (userPin == null && dataClass.hasLocation == true)
            {
                AddUserPin();
                dataClass.hasLocation = true;
            }
            else
            {
                dataClass.hasLocation = true;
                if(!(dataClass.UserIsPerfomer == 0 && IsSelected == 1))
                {
                    userPin.Position = new Xamarin.Forms.GoogleMaps.Position(e.Position.Latitude, e.Position.Longitude);
                    DependencyService.Get<iConsole>().DisplayText("Map Home Page Location: " + userPin.Position.Latitude + "   " + userPin.Position.Longitude);
                }
            }

			if (dataClass.UserIsPerfomer == 1 && IsSelected == 1 && dataClass.InProgressPerformList.Count > 0)
			{
			    DisplayDirections(new Xamarin.Forms.GoogleMaps.Position(dataClass.latitude, dataClass.longitude), new Xamarin.Forms.GoogleMaps.Position(prevTask.latitude, prevTask.longitude));
			}
        }

        void OnPin_Clicked(object sender, PinClickedEventArgs e)
        {
            e.Handled = true;
            if (!isPinClick)
            {
				if (e.Pin.Tag.ToString() == "user" || e.Pin.Tag.ToString() == "worker")
				{
                    
				}
				else
				{
					if (cts != null)
					{
						cts.Cancel();
					}
                    isPinClick = true;
					Navigation.PushAsync(new ProfilePage(1, int.Parse(e.Pin.Tag.ToString())));
				}
            }
        }

		void OnClusterPin_Clicked(object sender, PinClickedEventArgs e)
		{
            e.Handled = true;
            if(!isPinClick)
            {
				// offSet, task, pageType
				if (dataClass.TaskList != null)
				{
					if (cts != null)
					{
						cts.Cancel();
					}
                    isPinClick = true;
					Navigation.PushAsync(new ViewTaskPage(offSet, dataClass.TaskList.Where(arg => arg.id.Equals(int.Parse(e.Pin.Tag.ToString()))).Single(), 2));
				}
            }
		}

		CancellationTokenSource cts;

		async void OnCameraPositionIdle(object sender, CameraPositionIdleEventArgs e)
        {
            if (dataClass.hasLocation == true && this.IsSelected == 0)
            {
                bounds.Update((MapBounds)e.Bounds);
                cts = new CancellationTokenSource();
#if DEBUG
				await fileReader.ReadFile("Map.json", true, cts.Token);
#else            
				try
				{
					serviceType = 0;
					await webService.GetRequest(Constants.ROOT_URL + Constants.FETCH_TASK_URL + "?token=" + dataClass.UserToken + "&off_set=0&sw_lat=" + e.Bounds.SouthWest.Latitude + "&sw_long=" + e.Bounds.SouthWest.Longitude + "&ne_lat=" + e.Bounds.NorthEast.Latitude + "&ne_long=" + e.Bounds.NorthEast.Longitude + "&type=0", cts.Token);
				}
				catch (OperationCanceledException)
				{
                    DependencyService.Get<iConsole>().DisplayText("Web Service: Cancelled!");
				}
				catch (Exception)
				{
					DependencyService.Get<iConsole>().DisplayText("Web Service: Failed!");
				}

				cts = null;
#endif
			}
		}

		public void ReceiveJSONData(JObject jsonData)
		{

		}

#if DEBUG
        public void ReceiveJSONData(JObject jsonData, CancellationToken ct)
#else
		public async void ReceiveJSONData(JObject jsonData, CancellationToken ct)
#endif
		{
            isClick = true;
			if (int.Parse(jsonData["status"].ToString()) == 200)
			{
				switch (serviceType)
				{
					case 0: //fetch and pagination task pins
                        DependencyService.Get<iConsole>().DisplayText(jsonData.ToString());
						var tempTasks = JsonConvert.DeserializeObject<ObservableCollection<Tasks>>(jsonData["tasks"].ToString());
						foreach (var task in tempTasks)
						{
                            if (!map.ClusteredPins.ToList().Exists(pin => pin.Tag.ToString() == task.id.ToString()) && !dataClass.InProgressPerformList.ToList().Exists(progress => progress.id.ToString() == task.id.ToString()))
                            {
								taskPin = new Pin()
								{
									Type = PinType.Place,
									Label = "",
									Position = new Xamarin.Forms.GoogleMaps.Position(task.latitude, task.longitude),
									Icon = BitmapDescriptorFactory.FromBundle(Constants.PinIcon(task.task_type)),
									Tag = task.id,
                                    IsVisible = IsSelected == 0
                                };

								if (map != null)
								{
									map.ClusteredPins.Add(taskPin);
								}
                                if (dataClass.TaskList != null)
                                {
                                    dataClass.TaskList.Add(task);
                                }
							}
						}

						var onlineUsers = JsonConvert.DeserializeObject<ObservableCollection<User>>(jsonData["users"].ToString());
						foreach (var onlineUser in onlineUsers)
						{
							if (!map.CustomPins.ToList().Exists(pin => pin.Tag.ToString() == onlineUser.id.ToString()))
							{
								onlineUserPin = new Pin()
								{
									Type = PinType.Place,
									Label = "",
									Position = new Xamarin.Forms.GoogleMaps.Position(onlineUser.latitude, onlineUser.longitude),
									Icon = BitmapDescriptorFactory.FromView(new WorkerPinView(5, onlineUser.image)),
									Tag = onlineUser.id,
									IsVisible = IsSelected == 0
								};

								if (map != null)
								{
									map.CustomPins.Add(onlineUserPin);
								}

								if (OnLineUser != null)
								{
									OnLineUser.Add(onlineUser);
								}
							}
						}
#if DEBUG == false
						var pagination = JObject.Parse(jsonData["pagination"].ToString());
                        offSet = int.Parse(pagination["off_set"].ToString());
                        if (int.Parse(pagination["load_more"].ToString()) == 1)
                        {
                            serviceType = 0;
							
							cts = new CancellationTokenSource();
							try
							{
                                await webService.GetRequest(Constants.ROOT + pagination["url"], cts.Token);
							}
							catch (OperationCanceledException)
							{
								DependencyService.Get<iConsole>().DisplayText("Web Service: Cancelled!");
							}
							catch (Exception)
							{
								DependencyService.Get<iConsole>().DisplayText("Web Service: Failed!");
							}

							cts = null;
                        }
#endif
						break;
					case 1: 
                        
						break;
				}
			}
			else
			{

			}
            isClick = false;
        }

        public async void ReceiveTimeoutError(string title, string error)
        {
            await DisplayAlert(title,error,"Okay");
		}
    }
}
