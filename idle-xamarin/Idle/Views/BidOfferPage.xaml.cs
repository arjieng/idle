﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Xamarin.Forms;

namespace idle.Views
{
    public partial class BidOfferPage : RootViewPage, iRestConnector
    {
        DataClass dataClass = DataClass.GetInstance;
        int serviceType = 0, bidID, taskType;

        JToken pagination;
        int taskID, offSet;
        CancellationTokenSource cts;

#if DEBUG == false
        RestServices webService = new RestServices();
#endif

        public BidOfferPage(int id, string taskTitle, int task_type)
        {
            InitializeComponent();
            this.PageTitle = "BID OFFERS";
            this.TitleFontFamily = Constants.LULO;
            this.TitleFontColor = Constants.DARK_STRONG;
            this.RightIcon2 = "CloseDark";
            this.RightButton2Command = new Command((obj) => { Navigation.PopModalAsync(); });
            BindingContext = new { task_type = task_type };
            taskType = task_type;
            titleLabel.Text = taskTitle;

#if DEBUG
            bidList.ItemsSource = dataClass.BidList;
#else
            taskID = id;

            GetData();
#endif
        }

        protected override void OnAppearing()
        {
            base.OnAppearing();

			MessagingCenter.Subscribe<App, JObject>(this, "BiddingOffers", (sender, obj) =>
			{
				if (int.Parse(obj["custom_data"]["task_id"].ToString()) == taskID)
				{
					dataClass.BidList.Insert(0, JsonConvert.DeserializeObject<Bid>(obj["custom_data"].ToString()));
					bidList.ItemsSource = dataClass.BidList;
				}
			});

			MessagingCenter.Subscribe<App, JObject>(this, "BidCancelled", (sender, obj) =>
			{
				if (int.Parse(obj["custom_data"]["task_id"].ToString()) == taskID)
				{
					if (dataClass.BidList.ToList().Exists(cancelBid => cancelBid.id == int.Parse(obj["custom_data"]["bid_id"].ToString())))
					{
						dataClass.BidList.Remove(dataClass.BidList.ToList().Find(cancelBid => cancelBid.id == int.Parse(obj["custom_data"]["bid_id"].ToString())));
					}
					bidList.ItemsSource = dataClass.BidList;
				}
			});

#if DEBUG
           
#else
            webService.WebServiceDelegate = this;
            //GetData();
#endif
            bidList.ItemsSource = dataClass.BidList;
			DependencyService.Get<DependencyServices.iStatusStyle>().StatusStyle(1);
        }

        protected override void OnDisappearing()
        {
            base.OnDisappearing();
            MessagingCenter.Unsubscribe<App, JObject>(this, "BiddingOffers");
            MessagingCenter.Unsubscribe<App, JObject>(this, "BidCancelled");
			if (cts != null)
			{
				cts.Cancel();
			}
        }

        async void GetData()
        {
#if DEBUG == false
			serviceType = 0;
            loadingIndicator.IsRunning = true;  
			webService.WebServiceDelegate = this;
            dataClass.BidList.Clear();

			//API
			var url = Constants.ROOT_URL + Constants.ACCOUNTS_URL + Constants.FETCH_TASK_URL + "/" + taskID + Constants.BID_URL + Constants.TOKEN_URL + dataClass.UserToken;

			cts = new CancellationTokenSource();
			try
			{
                await webService.GetRequest(url, cts.Token);
			}
			catch (OperationCanceledException)
			{
				DependencyService.Get<iConsole>().DisplayText("Web Service: Cancelled!");
			}
			catch (Exception)
			{
				DependencyService.Get<iConsole>().DisplayText("Web Service: Failed!");
			}

			cts = null;

#endif

        }

        void OnBid_ItemTapped(object sender, ItemTappedEventArgs e)
        {
            bidList.SelectedItem = null;
            var bid = e.Item as Bid;
            Navigation.PushAsync(new BidSummaryPage(bid, taskID, offSet));
        }

        void OnAcceptButton_Clicked(object sender, EventArgs e)
        {
            bidID = int.Parse(((Button)sender).ClassId);
            Navigation.PushAsync(new BidSummaryPage(dataClass.BidList.ToList().Find((obj) => obj.id == bidID),taskID, offSet  ));
        }

        async void OnIgnoreButton_Clicked(object sender, EventArgs e)
        {
            bidID = int.Parse(((Button)sender).ClassId);
#if DEBUG
            dataClass.BidList.RemoveAt(dataClass.BidList.ToList().FindIndex((obj) => obj.id == bidID));
#else
			serviceType = 1;
			//API
			var json = JsonConvert.SerializeObject(new { token = dataClass.UserToken, off_set = pagination["off_set"].ToString() });
			var url = Constants.ROOT_URL + Constants.ACCOUNTS_URL + Constants.FETCH_TASK_URL + "/" + this.taskID + Constants.BID_URL + "/" + bidID + Constants.TOKEN_URL + dataClass.UserToken + "&off_set=" + offSet;

			
			await Task.Run(async delegate
			{
                cts = new CancellationTokenSource();
    			try
    			{
    				await webService.DeleteRequestAsync(url, cts.Token);
    			}
    			catch (OperationCanceledException)
    			{
    				DependencyService.Get<iConsole>().DisplayText("Web Service: Cancelled!");
    			}
    			catch (Exception)
    			{
    				DependencyService.Get<iConsole>().DisplayText("Web Service: Failed!");
    			}

    			cts = null;
            });
#endif
        }

        async void OnLast_ItemAppearing(object sender, Xamarin.Forms.ItemVisibilityEventArgs e)
        {
            //serviceType = 0;
#if DEBUG == false
            var currentItem = e.Item as Bid;
            var lastItem = dataClass.BidList[dataClass.BidList.Count - 1];
            if (currentItem == lastItem && int.Parse(pagination["load_more"].ToString()) == 1)
            {
                serviceType = 0;
                loadingIndicator.IsRunning = true;
				
				cts = new CancellationTokenSource();
				try
				{
                    await webService.GetRequest(Constants.ROOT + pagination["url"], cts.Token);
				}
				catch (OperationCanceledException)
				{
					DependencyService.Get<iConsole>().DisplayText("Web Service: Cancelled!");
				}
				catch (Exception)
				{
					DependencyService.Get<iConsole>().DisplayText("Web Service: Failed!");
				}

				cts = null;
            }
#endif
        }

		public void ReceiveJSONData(JObject jsonData)
		{

		}

        public void ReceiveJSONData(JObject jsonData, CancellationToken ct)
        {
            Device.BeginInvokeOnMainThread(() =>
            {
                //DisplayAlert(null, taskID.ToString(), "Okay");
                if (int.Parse(jsonData["status"].ToString()) == 300){
                    
                }
                else
                {
                    int HTTPStatus = int.Parse(jsonData["status"].ToString());
                    switch (serviceType)
                    {
                        case 0: //fetch and pagination
                            var bids = JsonConvert.DeserializeObject<ObservableCollection<Bid>>(jsonData["bids"].ToString());
                            foreach (var bid in bids)
                            {
                                dataClass.BidList.Add(bid);
                            }
                            //bidList.BindingContext = new { task_type = taskType };
                            bidList.ItemsSource = dataClass.BidList;
                            pagination = jsonData["pagination"];
                            offSet = int.Parse(pagination["off_set"].ToString());

    						break;
                        case 1: //ignore
                            dataClass.BidList.RemoveAt(dataClass.BidList.ToList().FindIndex((obj) => obj.id == bidID));

                            if(HTTPStatus == 201){
                                var bid = JsonConvert.DeserializeObject<Bid>(jsonData["bid"].ToString());
                                dataClass.BidList.Add(bid);
                            }

                            break;
                    }
                }
            });
            loadingIndicator.IsRunning = false;
        }

        public void ReceiveTimeoutError(string title, string error)
        {
            loadingIndicator.IsRunning = false;
			Device.BeginInvokeOnMainThread(async delegate
			{
				await DisplayAlert(title, error, "Okay");
			});
        }
    }
}
