﻿using System;
using System.Collections.Generic;
using System.Threading;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Xamarin.Forms;

namespace idle.Views
{
    public partial class GetInTouchPage : RootViewPage, iFileConnector, iRestConnector
    {
#if DEBUG
		FileReader fileReader = new FileReader();
#else
        RestServices webServices = new RestServices();
#endif
        CancellationTokenSource cts;
        public GetInTouchPage()
        {
            InitializeComponent();
			this.PageTitle = "GET IN TOUCH";
			this.TitleFontFamily = Constants.LULO;
			this.TitleFontColor = Constants.DARK_STRONG;
			this.LeftIcon = "ArrowDark";
			this.LeftButtonCommand = new Command((obj) => { Navigation.PopAsync(); });

#if DEBUG
			fileReader.FileReaderDelegate = this;
#else
            webServices.WebServiceDelegate = this;
#endif
		}

		protected override void OnAppearing()
		{
			base.OnAppearing();
			DependencyService.Get<DependencyServices.iStatusStyle>().StatusStyle(1);
		}

		protected override void OnDisappearing()
		{
			base.OnDisappearing();
			if (cts != null)
			{
				cts.Cancel();
			}
		}

		async void OnSendButton_Clicked(object sender, EventArgs e)
		{
			bool isEmpty , isNotError = true;
			if (Device.RuntimePlatform == Device.iOS)
			{
				isEmpty = feedbackEditor.Text.Equals(feedbackEditor.Placeholder);
			}
			else
			{
				isEmpty = string.IsNullOrEmpty(feedbackEditor.Text);
			}

            if(isEmpty)
            {
                EditorError(feedbackEditor, "Feedback is required to proceed!");
                isNotError = false;
            }
            if (isNotError)
            {
#if DEBUG
                await DisplayAlert("Success", "Thank you so much for writing in! We will get back to you as soon as we can.", "Okay");
#else
                var url = Constants.ROOT_URL + Constants.ACCOUNTS_URL +"/feedback";
                var json = JsonConvert.SerializeObject(new { token = DataClass.GetInstance.UserToken, message = feedbackEditor.Text});

				cts = new CancellationTokenSource();
				try
				{
                    await webServices.PutRequestAsync(url, json, cts.Token);
				}
				catch (OperationCanceledException)
				{
					DependencyService.Get<iConsole>().DisplayText("Web Service: Cancelled!");
				}
				catch (Exception)
				{
					DependencyService.Get<iConsole>().DisplayText("Web Service: Failed!");
				}

				cts = null;
               
#endif
            }
	    }

		void EditorError(CustomEditor entry, string errorMessage)
		{
			entry.PlaceholderColor = Color.Red;
			entry.Placeholder = errorMessage;
            entry.Text = "";
		}

		public void ReceiveJSONData(JObject jsonData)
		{

		}

        public void ReceiveJSONData(JObject jsonData, CancellationToken ct)
        {
			if (int.Parse(jsonData["status"].ToString()) == 200)
			{
                DisplayAlert("Success","Thank you so much for writing in! We will get back to you as soon as we can.","Okay");
				feedbackEditor.PlaceholderColor = Constants.DARK_MEDIUM;
				feedbackEditor.Placeholder = "Write here...";
                feedbackEditor.Text = string.Empty;
                feedbackEditor.TextColor = Constants.DARK_MEDIUM;
			}
        }

        public async void ReceiveTimeoutError(string title, string error)
        {
            await DisplayAlert(title,error,"Okay");
        }
    }
}
