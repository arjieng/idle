﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Net.Http;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Xamarin.Forms;
using static Xamarin.Forms.Button;
//using FFImageLoading.Cache;
using System.Collections.ObjectModel;
using System.Linq;
using System.Threading;

namespace idle.Views
{
    public partial class HomePage : RootViewPage, iRestConnector
    {
        DataClass dataClass = DataClass.GetInstance;
        static int TotalInProgress = 0;
        Button previousButton;
        bool isClick;
        CancellationTokenSource cts;

#if DEBUG == false
        RestServices webServices = new RestServices();
#endif
        public HomePage()
        {
            InitializeComponent();

            this.PageTitle = "LET'S START!";
            this.TitleFontFamily = Constants.LULO_BOLD;
            this.TitleFontColor = Constants.LIGHT;
            this.LeftIcon = "BurgerLight";
            this.LeftButtonCommand = new Command((obj) => { MyMasterDetailPage detail = (MyMasterDetailPage)this.Parent.Parent; detail.IsPresented = true; });

            InProgressList_Trigger();
            OnSetButton_Clicked(dataClass.UserIsPerfomer == 0 ? posterButton : performerButton);
        }

        protected override void OnAppearing()
        {
            base.OnAppearing();

            MyMasterDetailPage.isToastBack = false;

			MessagingCenter.Subscribe<App>(this, "BiddingAccepted", (App) =>
			{
				InProgressList_Trigger();
			});

			MessagingCenter.Subscribe<App, JObject>(this, "TaskFinished", (sender, obj) =>
			{
				if (dataClass.UserIsPerfomer == 1)
				{
					if (dataClass.InProgressPerformList.ToList().Exists(inProgressTask => inProgressTask.id == int.Parse(obj["custom_data"]["task_id"].ToString())))
					{
						dataClass.InProgressPerformList.Remove(dataClass.InProgressPerformList.ToList().Find(inProgressTask => inProgressTask.id == int.Parse(obj["custom_data"]["task_id"].ToString())));
					}
					InProgressList_Trigger();
				}
			});

			MessagingCenter.Subscribe<App>(this, "TaskCancelled", (App) =>
			{
				InProgressList_Trigger();
			});

            MyMasterDetailPage.isHomePage = true;

            isClick = false;
#if DEBUG == false
            webServices.WebServiceDelegate = this;
#endif
            DependencyService.Get<DependencyServices.iStatusStyle>().StatusStyle(0);

            isClick = false;
            InProgressList_Trigger();
            OnSetButton_Clicked(dataClass.UserIsPerfomer == 0 ? posterButton : performerButton);
        }

        protected override void OnDisappearing()
        {
            base.OnDisappearing();

            MyMasterDetailPage.isHomePage = false;

            MessagingCenter.Unsubscribe<App>(this, "BiddingAccepted");
            MessagingCenter.Unsubscribe<App, JObject>(this, "TaskFinished");
            MessagingCenter.Unsubscribe<App>(this, "TaskCancelled");

			if (cts != null)
			{
				cts.Cancel();
			}
        }

        void InProgressList_Trigger()
        {
            Tasks progress = new Tasks();
            if (dataClass.UserIsPerfomer == 1)
            {
                if (dataClass.InProgressPerformList.Count > 0)
                {
                    progress = dataClass.InProgressPerformList[0];
                }

                TotalInProgress = dataClass.InProgressPerformList.Count;
            }
            else
            {

                //dataClass.InProgressPosterList = new ObservableCollection<Tasks>();
                if (dataClass.InProgressPosterList.Count > 0)
                {
                    progress = dataClass.InProgressPosterList[0];
                }

                TotalInProgress = dataClass.InProgressPosterList.Count;
            }

            string taskCount = TotalInProgress == 0 ? "0" : TotalInProgress == 1 ? "1 task" : TotalInProgress.ToString() + " tasks";
            BindingContext = new { image = dataClass.UserImage, taskCount, progress };
        }

        void OnCategory_Clicked(object sender, ImageTappedEventArgs e)
        {
            if (!isClick)
            {
                isClick = true;
                var parent = (StackLayout)((CircleImage)sender).Parent;
                var label = ((Label)parent.Children[1]);

                if (dataClass.UserIsPerfomer == 1)
                {
                    //Navigation.PushAsync(new TaskPage(label.Text, 1));
                    Navigation.PushAsync(new TaskPage(int.Parse(label.StyleId), 1));
                }
                else
                {
                    Navigation.PushModalAsync(new NewTaskPage(int.Parse(label.StyleId)));
                }
            }
        }

        void OnCenterButton_Clicked(object sender, ImageTappedEventArgs e)
        {
            Navigation.PushAsync(new MapHomePage(0), true);
            //MyMasterDetailPage detail = (MyMasterDetailPage)this.Parent.Parent;
            //detail.Detail = new NavigationPage(new ProfilePage(0, dataClass.UserID));
        }

        void OnProgress_Clicked(object sender, System.EventArgs e)
        {
            Navigation.PushAsync(new MapHomePage(1), true);
        }

        void OnSetButton_Clicked(Button button)
        {
            if (previousButton != null)
            {

                previousButton.BackgroundColor = Constants.BUTTON_GREEN;
                previousButton.BorderColor = Color.White;
                previousButton.TextColor = Color.White;
                previousButton.FontFamily = Constants.PROXIMA_REG;
            }

            button.BackgroundColor = Color.White;
            button.TextColor = Constants.BUTTON_GREEN;
            button.FontFamily = Constants.PROXIMA_BOLD;
            button.BorderColor = Color.White;

            previousButton = button;
        }

        async void OnTabs_Clicked(Object sender, EventArgs e)
        {
          
            var button = (Button)sender;
            if (button != previousButton)
            {
				OnSetButton_Clicked(button);
				dataClass.UserIsPerfomer = int.Parse(button.StyleId);
				InProgressList_Trigger();

#if DEBUG == false
                //API

                var json = JsonConvert.SerializeObject(new { token = dataClass.UserToken });
                var url = Constants.ROOT_URL + Constants.ACCOUNTS_URL + Constants.CHANGE_ROLE_URL;

				cts = new CancellationTokenSource();
				try
				{
                    await webServices.PutRequestAsync(url, json, cts.Token);
				}
				catch (OperationCanceledException)
				{
					DependencyService.Get<iConsole>().DisplayText("Web Service: Cancelled!");
				}
				catch (Exception)
				{
					DependencyService.Get<iConsole>().DisplayText("Web Service: Failed!");
				}

				cts = null;
#endif

            }

        }

		public void ReceiveJSONData(JObject jsonData)
		{

		}

        public void ReceiveJSONData(JObject jsonData, CancellationToken ct)
        {

            if (int.Parse(jsonData["status"].ToString()) == 200)
            {
#if DEBUG == false
    //            var button = posterButton.StyleId.Equals(previousButton.StyleId) ? performerButton : posterButton;
				//OnSetButton_Clicked(button);
				//dataClass.UserIsPerfomer = int.Parse(button.StyleId);
				//InProgressList_Trigger();
#endif
            }
        }

        public async void ReceiveTimeoutError(string title, string error)
        {
#if DEBUG == false
           await  DisplayAlert(title, error, "Okay");
#endif
		}
    }
}
