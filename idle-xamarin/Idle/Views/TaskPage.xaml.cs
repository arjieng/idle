﻿﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Diagnostics;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Xamarin.Forms;
using System.Linq;
using System.Threading.Tasks;
using Plugin.Permissions;
using System.Threading;

namespace idle.Views
{
    public partial class TaskPage : RootViewPage, iRestConnector, iFileConnector
    {
        DataClass dataClass = DataClass.GetInstance;
        int pageType = 0, serviceType = 0, taskID;
        bool IsFirstLoad = true;
        readonly String[] categories = { "QUICK TASK", "GROCERY", "LAUNDRY", "TRANSPORT", "OTHERS", "TASK OFFERS", "MY TASKS" };
        int type = 0, taskType = 0, prevRadius = 0;
        bool isAddClick = false;
        public static ObservableCollection<Tasks> tasks;
        string pageTitle;
        int loadMore = 0, offSet = 0;
        string paginationUrl = string.Empty;
        public static int categoryTaskCount = 0;
        CancellationTokenSource cts;
#if DEBUG
        FileReader fileReader = new FileReader();
#else
        RestServices webService = new RestServices();
#endif

        //public TaskPage(string title, int type)
        public TaskPage(int category, int type)
        {
            InitializeComponent();
            DependencyService.Get<DependencyServices.iStatusStyle>().StatusStyle(1);


            //pageTitle = title;
            pageTitle = categories[category];
            taskType = category;

            this.TitleFontFamily = Constants.LULO;
            this.TitleFontColor = Constants.DARK_STRONG;

            switch (type)
            {
                case 0: // task offers
                    //this.PageTitle = title;
                    this.PageTitle = pageTitle;
                    this.RightIcon1 = "RadiusIcon";
                    this.RightButton1Command = new Command((obj) => { Navigation.PushAsync(new RadiusPage()); });
                    this.RightIcon2 = "CloseDark";
                    this.RightButton2Command = new Command((obj) => { IsFirstLoad = true; Navigation.PopModalAsync(); ClearList(); });

                    dataClass.TaskOffersList = new ObservableCollection<Tasks>();
                    break;
                case 1: // task categories 
                    //this.PageTitle = title + " (0)";
                    this.PageTitle = pageTitle + " (0)";
                    this.LeftIcon = "ArrowDark";
                    this.LeftButtonCommand = new Command((obj) => { Navigation.PopAsync(); ClearList(); });
                    this.RightIcon2 = "RadiusIcon";
                    this.RightButton2Command = new Command((obj) => { Navigation.PushAsync(new RadiusPage()); });
                    dataClass.TaskList = new ObservableCollection<Tasks>();
                    break;
            }

            pageType = type;

        }

        protected async override void OnAppearing()
        {
            base.OnAppearing();

            MessagingCenter.Subscribe<App>(this, "BiddingAccepted", (App) =>
            {
                BidAccepted();
            });

            MessagingCenter.Subscribe<App, JObject>(this, "BiddingDeclined", (sender, obj) =>
            {
                if (dataClass.TaskList.ToList().Exists(bidTask => bidTask.id == int.Parse(obj["custom_data"]["task_id"].ToString())))
                {
                    dataClass.TaskList.ToList().Find(bidTask => bidTask.id == int.Parse(obj["custom_data"]["task_id"].ToString())).bid = "0.00";
                }
                if (pageType == 0)
                {
                    if (dataClass.TaskOffersList.ToList().Exists(bidTask => bidTask.id == int.Parse(obj["custom_data"]["task_id"].ToString())))
                    {
                        dataClass.TaskOffersList.ToList().Find(bidTask => bidTask.id == int.Parse(obj["custom_data"]["task_id"].ToString())).bid = "0.00";
                    }
                }
            });

#if DEBUG
            fileReader.FileReaderDelegate = this;
#else
			webService.WebServiceDelegate = this;
#endif

            if (Application.Current.Properties.ContainsKey("map_radius"))
            {
                prevRadius = dataClass.Radius;
                dataClass.Radius = int.Parse(Application.Current.Properties["map_radius"].ToString());
                if (prevRadius != dataClass.Radius && !IsFirstLoad)
                {
                    if (pageType == 0)
                    {
                        dataClass.TaskOffersList.Clear();
                    }
                    else
                    {
                        dataClass.TaskList.Clear();
                    }
                    FetchData();
                }
            }
            else
            {
                dataClass.Radius = 5;
                Application.Current.Properties["map_radius"] = dataClass.Radius;
                await Application.Current.SavePropertiesAsync();
            }

            DependencyService.Get<DependencyServices.iStatusStyle>().StatusStyle(1);

            if (IsFirstLoad == true && dataClass.hasLocation == true)
            {
                IsFirstLoad = false;
                FetchData();
            }
            else if (dataClass.hasLocation == false)
            {
                var answer = await DisplayAlert("Location Services Disabled", "Idle app would like to access your current location in order to see nearby tasks.", "Yes", "No");
                if (answer == true)
                {
                    //DependencyService.Get<iLocationServices>().OpenSettingsUrl();
                    CrossPermissions.Current.OpenAppSettings();
                }
            }

            if (pageType == 1)
            {
                this.PageTitle = pageTitle + " (" + categoryTaskCount + ")";
            }
        }

        protected override void OnDisappearing()
        {
            base.OnDisappearing();

            MessagingCenter.Unsubscribe<App>(this, "BiddingAccepted");
            MessagingCenter.Unsubscribe<App, JObject>(this, "BiddingDeclined");

			if (cts != null)
			{
				cts.Cancel();
			}
        }

        void BidAccepted()
        {
            if (pageType == 0)
            {
                if (dataClass.TaskOffersList.ToList().Exists(acceptedTask => acceptedTask.id == (dataClass.InProgressPerformList[0]).id))
                {
                    dataClass.TaskOffersList.Remove(dataClass.TaskOffersList.ToList().Find(acceptedTask => acceptedTask.id == (dataClass.InProgressPerformList[0]).id));
                }
                dataClass.PinToRemove = new Tasks();
                dataClass.PinToRemove = dataClass.InProgressPerformList[0];
                dataClass.PinTypeToRemove = 0;
            }
            else
            {
                if (dataClass.TaskList.ToList().Exists(acceptedTask => acceptedTask.id == (dataClass.InProgressPerformList[0]).id))
                {
                    dataClass.TaskList.Remove(dataClass.TaskList.ToList().Find(acceptedTask => acceptedTask.id == (dataClass.InProgressPerformList[0]).id));
                }
                if (categoryTaskCount > 0)
                {
                    this.PageTitle = pageTitle + " (" + (categoryTaskCount - 1) + ")";
                }
            }
        }

        void FetchData()
        {
            activityIndicator.IsRunning = true;
            Task.Run(async () =>
            {
                cts = new CancellationTokenSource();
#if DEBUG
                await fileReader.ReadFile("Map.json", true, cts.Token);
#else
				try
				{
					if (pageType == 0)
					{
						await webService.GetRequest(Constants.ROOT_URL + Constants.FETCH_TASK_URL + "?token=" + dataClass.UserToken + "&off_set=0&type=1&radius=" + dataClass.Radius + "&latitude=" + dataClass.latitude + "&longitude=" + dataClass.longitude, cts.Token);
					}
					else
					{
						await webService.GetRequest(Constants.ROOT_URL + Constants.FETCH_TASK_URL + "?token=" + dataClass.UserToken + "&off_set=0&type=1&radius=" + dataClass.Radius + "&task=" + taskType + "&latitude=" + dataClass.latitude + "&longitude=" + dataClass.longitude, cts.Token);
					}
				}
				catch (OperationCanceledException)
				{
					DependencyService.Get<iConsole>().DisplayText("Web Service: Cancelled!");
				}
				catch (Exception)
				{
					DependencyService.Get<iConsole>().DisplayText("Web Service: Failed!");
				}

				cts = null;
#endif
            });
        }

        void ClearList()
        {
            if (pageType == 0)
            {
                dataClass.TaskOffersList.Clear();
            }
            else
            {
                dataClass.TaskList.Clear();
            }
        }

        void OnAcceptButton_Clicked(object sender, EventArgs e)
        {
            taskID = int.Parse(((Button)sender).ClassId);
            if (pageType == 0)
            {
                // offSet, task, pageType
                Navigation.PushAsync(new ViewTaskPage(offSet, dataClass.TaskOffersList.ToList().Find((obj) => obj.id == taskID), pageType));
            }
            else
            {
                // offSet, task, pageType
                Navigation.PushAsync(new ViewTaskPage(offSet, dataClass.TaskList.ToList().Find((obj) => obj.id == taskID), pageType));
            }
        }

#if DEBUG
        void OnIgnoreButton_Clicked(object sender, EventArgs e)
#else
		async void OnIgnoreButton_Clicked(object sender, EventArgs e)
#endif
        {
            taskID = int.Parse(((Button)sender).ClassId);
#if DEBUG
            if (pageType == 0)
            {
                dataClass.TaskOffersList.RemoveAt(dataClass.TaskOffersList.ToList().FindIndex((obj) => obj.id == taskID));
                //remove pin on map
                dataClass.PinToRemove = new Tasks();
                dataClass.PinToRemove = (Tasks)BindingContext;
                dataClass.PinTypeToRemove = 0;
            }

            if (dataClass.TaskList.Count > 0)
            {
                dataClass.TaskList.RemoveAt(dataClass.TaskList.ToList().FindIndex((obj) => obj.id == taskID));
            }

#else
			serviceType = 1;
            //API ignore
            if (((Button)sender).Text.Equals("IGNORE"))
            {
                var json = pageType == 0 ?
                    JsonConvert.SerializeObject(new { token = dataClass.UserToken, off_set = offSet, radius = dataClass.Radius, type = 1 }) :
                    JsonConvert.SerializeObject(new { token = dataClass.UserToken, off_set = offSet, radius = dataClass.Radius, type = 1, task = taskType });

				await Task.Run(async delegate
				{
    				cts = new CancellationTokenSource();
    				try
    				{
    					await webService.PostRequestAsync(Constants.ROOT_URL + Constants.FETCH_TASK_URL + "/" + taskID + "/ignores", json, cts.Token);
    				}
    				catch (OperationCanceledException)
    				{
    					DependencyService.Get<iConsole>().DisplayText("Web Service: Cancelled!");
    				}
    				catch (Exception)
    				{
    					DependencyService.Get<iConsole>().DisplayText("Web Service: Failed!");
    				}

    				cts = null;
                });
            }
            else
            {
				if (pageType == 0)
				{
					// offSet, task, pageType
					await Navigation.PushAsync(new ViewTaskPage(offSet, dataClass.TaskOffersList.ToList().Find((obj) => obj.id == taskID), pageType));
				}
				else
				{
                    // offSet, task, pageType
					await Navigation.PushAsync(new ViewTaskPage(offSet, dataClass.TaskList.ToList().Find((obj) => obj.id == taskID), pageType));
				}
            }
#endif
        }

        void OnTask_ItemTapped(object sender, ItemTappedEventArgs e)
        {
            // offSet, task, pageType
            Navigation.PushAsync(new ViewTaskPage(offSet, (Tasks)e.Item, pageType));
            taskList.SelectedItem = null;
        }

#if DEBUG
        void OnLast_ItemAppearing(object sender, Xamarin.Forms.ItemVisibilityEventArgs e)
#else
        async void OnLast_ItemAppearing(object sender, Xamarin.Forms.ItemVisibilityEventArgs e)
#endif
        {
#if DEBUG == false
            var currentItem = e.Item as Tasks;
            var lastItem = pageType == 0 ? dataClass.TaskOffersList[dataClass.TaskOffersList.Count - 1] : dataClass.TaskList[dataClass.TaskList.Count - 1];
            if (currentItem == lastItem && loadMore == 1)
            {
                activityIndicator.IsRunning = true;
                serviceType = 0;

				cts = new CancellationTokenSource();
				try
				{
                    await webService.GetRequest(Constants.ROOT + paginationUrl, cts.Token);
				}
				catch (OperationCanceledException)
				{
					DependencyService.Get<iConsole>().DisplayText("Web Service: Cancelled!");
				}
				catch (Exception)
				{
					DependencyService.Get<iConsole>().DisplayText("Web Service: Failed!");
				}

				cts = null;
            }
#endif
        }

		public void ReceiveJSONData(JObject jsonData)
		{

		}

        public void ReceiveJSONData(JObject jsonData, CancellationToken ct)
        {
            Device.BeginInvokeOnMainThread(() =>
            {
            if (int.Parse(jsonData["status"].ToString()) == 300)
            {

            }
            else
            {
                int HTTPStatus = int.Parse(jsonData["status"].ToString());
                switch (serviceType)
                {
                    case 0: //fetch
                        var tempTasks = JsonConvert.DeserializeObject<ObservableCollection<Tasks>>(jsonData["tasks"].ToString());
                        foreach (var task in tempTasks)
                        {
                                if (pageType == 0)
                                {
                                    dataClass.TaskOffersList.Add(task);
                                }
                                else
                                {
#if DEBUG
                                    if(taskType == task.task_type)
                                    {
                                        dataClass.TaskList.Add(task);
                                    }
#else
                                    dataClass.TaskList.Add(task);
#endif
                                }
							}

							if (pageType == 0)
							{
                                taskList.ItemsSource = dataClass.TaskOffersList;
							}
							else
							{
#if DEBUG
                                this.PageTitle = pageTitle+ " (" + dataClass.TaskList.Count() + ")";
#endif
								taskList.ItemsSource = dataClass.TaskList;
							}
#if DEBUG == false
							var pagination = JObject.Parse(jsonData["pagination"].ToString());
                            loadMore = int.Parse(pagination["load_more"].ToString());
                            paginationUrl = pagination["url"].ToString();
                            offSet = int.Parse(pagination["off_set"].ToString());
                            if (pageType == 1)
                            {
                                categoryTaskCount = int.Parse(pagination["tasks"].ToString());
                                this.PageTitle = pageTitle + " (" + categoryTaskCount + ")";
                            }
#endif
							break;
                        case 1: //ignore task
							if(pageType == 0){
								//remove pin on map
								dataClass.PinToRemove = new Tasks();
								dataClass.PinToRemove = dataClass.TaskOffersList.ToList().Find((obj) => obj.id == taskID);
                                dataClass.PinTypeToRemove = 0;
								if (dataClass.TaskList.Count > 0)
								{
									dataClass.TaskList.RemoveAt(dataClass.TaskList.ToList().FindIndex((obj) => obj.id == taskID));
								}

                                if (dataClass.TaskOffersList.Count > 0)
                                {
                                    dataClass.TaskOffersList.RemoveAt(dataClass.TaskOffersList.ToList().FindIndex((obj) => obj.id == taskID));
                                }

								if (HTTPStatus == 201)
								{
									var task = JsonConvert.DeserializeObject<Tasks>(jsonData["task"].ToString());
									dataClass.TaskOffersList.Add(task);
								}
                            }
                            else
                            {
								if (dataClass.TaskList.Count > 0)
								{
									dataClass.TaskList.RemoveAt(dataClass.TaskList.ToList().FindIndex((obj) => obj.id == taskID));
								}

								if (HTTPStatus == 201)
								{
									var task = JsonConvert.DeserializeObject<Tasks>(jsonData["task"].ToString());
									dataClass.TaskList.Add(task);
								}
                                else
                                {
                                    if(categoryTaskCount > 0)
                                    {
                                        categoryTaskCount -= 1;
                                        this.PageTitle = pageTitle + " (" + categoryTaskCount + ")";   
                                    }
                                }
                            }

                            break;
                    }	
				}
                activityIndicator.IsRunning = false;
            });
        }

        public  void ReceiveTimeoutError(string title, string error)
        {
            Device.BeginInvokeOnMainThread( async delegate {
                await DisplayAlert(title, error, "Okay");
            });
             
        }
    }
}
