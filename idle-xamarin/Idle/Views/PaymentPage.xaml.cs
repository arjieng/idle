﻿using System;
using Xamarin.Forms;

namespace idle.Views
{
    public partial class PaymentPage : RootViewPage
    {
        DataClass dataClass = DataClass.GetInstance;
        bool isClicked = false;

        public PaymentPage()
        {
            InitializeComponent();

            this.PageTitle = "PAYMENT";
			this.TitleFontFamily = Constants.LULO;
			this.TitleFontColor = Constants.DARK_STRONG;
			this.RightIcon2 = "CloseDark";
			this.RightButton2Command = new Command((obj) => { Navigation.PopModalAsync(); DependencyService.Get<DependencyServices.iStatusStyle>().StatusStyle(0); });

			expiryDatePicker.Date = DateTime.Now.Date;
            expiryDatePicker.MinimumDate = DateTime.Now.Date;
        }

        protected override void OnAppearing()
        {
            base.OnAppearing();
            dataClass.stripeToken = null;
        }

        void OnCardNumber_TextChanged(object sender, Xamarin.Forms.TextChangedEventArgs e)
        {
            if (e.NewTextValue != null && e.OldTextValue != null)
            {
                if (e.NewTextValue.Length > e.OldTextValue.Length)
                {
                    var text = e.NewTextValue.Split(' ');
                    var entry = sender as Entry;
                    if (text[text.Length - 1].Length >= 4)
                    {
                        entry.Text = e.NewTextValue + " ";
                    }
                }
            }
        }

		void OnCvv_TextChanged(object sender, Xamarin.Forms.TextChangedEventArgs e)
		{
			if (cvcEntry.Text.Length > 3)
			{
				cvcEntry.Text = e.OldTextValue;
			}
			else
			{
				cvcEntry.Text = e.NewTextValue;
			}
		}

        void OnCardNumber_Completed(object sender , EventArgs e)
        {
            expiryDatePicker.Focus();
        }

        void OnPayButton_Clicked(object sender, EventArgs e)
        {
            bool isNotError = true;
			if (string.IsNullOrEmpty(cardNumberEntry.Text))
			{
				EntryError(cardNumberEntry, "Card number is required!");
                isNotError = false;
			}

			if (string.IsNullOrEmpty(cvcEntry.Text))
			{
				EntryError(cvcEntry, "Required!");
                isNotError = false;
			}

            if (isNotError && !isClicked)
            {
                isClicked = true;
				CreditCard CreditCardData = new CreditCard()
				{
					Numbers = cardNumberEntry.Text,
					Month = expiryDatePicker.Date.ToString("MM"),
					Year = expiryDatePicker.Date.ToString("yyyy"),
					Cvc = cvcEntry.Text
				};
                DependencyService.Get<IStripeServices>().CardToToken(CreditCardData, Application.Current.Properties["stripe_api_key"].ToString(), OnStripeComplete);
            }
        }

        void OnStripeComplete(string stripeTokenId, string stripeError)
        {
            if (string.IsNullOrEmpty(stripeTokenId))
            {
                isClicked = false;
                DisplayAlert("Stripe", stripeError, "Okay");
            }
            else
            {
                dataClass.stripeToken = stripeTokenId;
				isClicked = false;
				Navigation.PopModalAsync();
			}
        }

        void EntryError(Entry entry, string errorMessage)
		{
			entry.Text = "";
			entry.PlaceholderColor = Color.Red;
			entry.Placeholder = errorMessage;
		}
    }
}
