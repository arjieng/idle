﻿using System;
using System.Collections.Generic;
using System.Threading;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Xamarin.Forms;

namespace idle.Views
{
    public partial class ForgotPasswordPage : RootViewPage, iRestConnector
    {
#if DEBUG == false
        RestServices webService = new RestServices();
#endif
        CancellationTokenSource cts;
        public ForgotPasswordPage()
        {
            DependencyService.Get<DependencyServices.iStatusStyle>().StatusStyle(1);

            InitializeComponent();

			this.PageTitle = "FORGOT PASSWORD";
			this.TitleFontFamily = Constants.LULO;
			this.TitleFontColor = Constants.DARK_STRONG;
			this.LeftIcon = "ArrowDark";
			this.LeftButtonCommand = new Command((obj) => { Navigation.PopAsync(); DependencyService.Get<DependencyServices.iStatusStyle>().StatusStyle(0); });

#if DEBUG == false
            webService.WebServiceDelegate = this;
#endif
		}

        protected override void OnDisappearing()
        {
            base.OnDisappearing();

			if (cts != null)
			{
				cts.Cancel();
			}
        }

        async void OnSendButton_Clicked(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(emailAddressEntry.Text))
            {
                EntryError(emailAddressEntry, "Email is required");
            }

#if DEBUG == false
            else
            {
                string json = JsonConvert.SerializeObject(new { email = emailAddressEntry.Text });
				
				cts = new CancellationTokenSource();
				try
				{
                    await webService.PostRequestAsync(Constants.ROOT_URL + Constants.FORGOT_PASSWORD_URL, json, cts.Token);
				}
				catch (OperationCanceledException)
				{
					DependencyService.Get<iConsole>().DisplayText("Web Service: Cancelled!");
				}
				catch (Exception)
				{
					DependencyService.Get<iConsole>().DisplayText("Web Service: Failed!");
				}

				cts = null;
            }
#endif
		}

		void OnEntryContainer_Tapped(Object sender, EventArgs e)
		{
            emailAddressEntry.Focus();
		}

		void EntryError(Entry entry, string errorMessage)
		{
			entry.Text = "";
			entry.PlaceholderColor = Color.Red;
			entry.Placeholder = errorMessage;
		}

		public void ReceiveJSONData(JObject jsonData)
		{

		}

        public void ReceiveJSONData(JObject jsonData, CancellationToken ct)
        {
            if( int.Parse(jsonData["status"].ToString()) == 200)
            {
                DisplayAlert("Alert", jsonData["success"].ToString(),"Okay");
            }
            else
            {
                EntryError(emailAddressEntry, jsonData["error"].ToString());
                emailAddressEntry.FontSize = 11.scale();
            }
        }

        public void ReceiveTimeoutError(string title, string error)
        {
            DisplayAlert(title, error, "Okay");
        }
    }
}
