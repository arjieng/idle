﻿using System;
using System.Collections.Generic;
using Xamarin.Forms;

namespace idle.Views
{
    public partial class RadiusPage : RootViewPage
    {
        DataClass dataClass = DataClass.GetInstance;
        int selectedRadius = 0;
        RadiusItem previousItem;

        public RadiusPage()
        {
            InitializeComponent();

			this.PageTitle = "RADIUS";
			this.TitleFontFamily = Constants.LULO;
			this.TitleFontColor = Constants.DARK_STRONG;
			this.LeftIcon = "ArrowDark";
			this.LeftButtonCommand = new Command((obj) => { Navigation.PopAsync(); });

            selectedRadius = dataClass.Radius;

			var radiusItem = new List<RadiusItem>();

            for (int x = 1; x <= 10; x++)
            {
                radiusItem.Add(new RadiusItem { radius = x * 5, isSelected = selectedRadius == x * 5 });       
            }

			radiusSelection.ItemsSource = radiusItem;
            previousItem = radiusItem.Find((obj) => obj.radius == dataClass.Radius);
        }

		protected override void OnAppearing()
		{
			base.OnAppearing();
			DependencyService.Get<DependencyServices.iStatusStyle>().StatusStyle(1);
		}

		void OnRadius_ItemTapped(object sender, ItemTappedEventArgs e)  
		{
            radiusSelection.SelectedItem = null;
            if(previousItem != null) { previousItem.isSelected = false; }
            previousItem = (RadiusItem)e.Item;
            ((RadiusItem)e.Item).isSelected = true;
            selectedRadius = ((RadiusItem)e.Item).radius;
		}

		async void OnUpdate_Clicked(object sender, System.EventArgs e)
		{
            Application.Current.Properties["map_radius"] = selectedRadius;
            await Application.Current.SavePropertiesAsync();
            await Navigation.PopAsync();
		}
    }
}
