﻿﻿using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Xamarin.Forms;

namespace idle.Views
{
    public partial class CancelAccountPage : RootViewPage, iFileConnector , iRestConnector
    {
#if DEBUG
		FileReader fileReader = new FileReader();
#else
        RestServices webServices = new RestServices();
#endif
        CancellationTokenSource cts;
		public CancelAccountPage()
        {
            InitializeComponent();
			this.PageTitle = "CANCEL ACCOUNT";
			this.TitleFontFamily = Constants.LULO;
			this.TitleFontColor = Constants.DARK_STRONG;
			this.LeftIcon = "ArrowDark";
			this.LeftButtonCommand = new Command( async (obj) => { await Navigation.PopAsync(); });
#if DEBUG
			fileReader.FileReaderDelegate = this;
#else
            webServices.WebServiceDelegate = this;
#endif
		}

        protected override void OnAppearing()
		{
			base.OnAppearing();
			DependencyService.Get<DependencyServices.iStatusStyle>().StatusStyle(1);
		}

        protected override void OnDisappearing()
        {
            base.OnDisappearing();
            if (cts != null)
            {
                cts.Cancel();
            }
        }

		async void OnCancelAccount_Clicked(object sender, EventArgs e)
		{
            var action = await DisplayActionSheet("Do you really want to cancel this account!",null, "Cancel", "Yes");
            if (action == "Yes")
            {
#if DEBUG
                await Task.Run(() =>
                {
                    Device.BeginInvokeOnMainThread(async () =>
                    {
                        Application.Current.Properties.Remove("user_token");
                        Application.Current.Properties.Remove("user_id");
                        Application.Current.Properties.Remove("user_image");
                        Application.Current.Properties.Remove("user_is_online");
                        Application.Current.Properties.Remove("user_has_stripe");
                        Application.Current.Properties.Remove("user_is_confirmed");
                        Application.Current.Properties.Remove("user_is_performer");
                        Application.Current.Properties.Remove("user_perform_progress");
                        Application.Current.Properties.Remove("user_post_progress");
                        Application.Current.Properties.Remove("user_email");
                        await Application.Current.SavePropertiesAsync();
                        App.Logout();
                    });
                });
#else
				bool isEmpty;
				if (Device.RuntimePlatform == Device.iOS)
				{
					isEmpty = cancelMessageEditor.Text.Equals(cancelMessageEditor.Placeholder);
				}
				else
				{
					isEmpty = cancelMessageEditor.Text.Equals(cancelMessageEditor.Placeholder);
					isEmpty = string.IsNullOrEmpty(cancelMessageEditor.Text);
				}

                string json;

                if(isEmpty)
                {
					json = JsonConvert.SerializeObject(new { token = DataClass.GetInstance.UserToken});
                }
                else
                {
					json = JsonConvert.SerializeObject(new { token = DataClass.GetInstance.UserToken, message = cancelMessageEditor.Text });
                }

				await Task.Run(async () =>
				{
    				cts = new CancellationTokenSource();
    				try
    				{
						var url = Constants.ROOT_URL + Constants.ACCOUNTS_URL + "/cancel";
						await webServices.PutRequestAsync(url, json, cts.Token);
    				}
    				catch (OperationCanceledException)
    				{
    					DependencyService.Get<iConsole>().DisplayText("Web Service: Cancelled!");
    				}
    				catch (Exception)
    				{
    					DependencyService.Get<iConsole>().DisplayText("Web Service: Failed!");
    				}

    				cts = null;
                });
#endif
            }
		}

		public void ReceiveJSONData(JObject jsonData)
		{

		}

		public void ReceiveJSONData(JObject jsonData, CancellationToken ct)
		{
            Device.BeginInvokeOnMainThread(async () =>
            {
                if (int.Parse(jsonData["status"].ToString()) == 200)
                {
                    Application.Current.Properties.Remove("user_token");
                    Application.Current.Properties.Remove("user_id");
                    Application.Current.Properties.Remove("user_image");
                    Application.Current.Properties.Remove("user_is_online");
                    Application.Current.Properties.Remove("user_has_stripe");
                    Application.Current.Properties.Remove("user_is_confirmed");
                    Application.Current.Properties.Remove("user_is_performer");
                    Application.Current.Properties.Remove("user_perform_progress");
                    Application.Current.Properties.Remove("user_post_progress");
                    Application.Current.Properties.Remove("user_email");
                    await Application.Current.SavePropertiesAsync();
                    App.Logout();
                }
                else
                {

                }
            });
		}

		public void ReceiveTimeoutError(string title, string error)
		{
            Device.BeginInvokeOnMainThread( async delegate {
                await DisplayAlert(title, error, "Okay");
            });
			
		}
	}
}
