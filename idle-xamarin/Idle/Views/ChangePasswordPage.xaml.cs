﻿using System;
using System.Collections.Generic;
using System.Threading;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Xamarin.Forms;

namespace idle.Views
{
    public partial class ChangePasswordPage : RootViewPage, iRestConnector, iFileConnector
    {
        bool isClicked = false;
        LocationHelper locationHelper = LocationHelper.GetInstance;
        DataClass dataClass = DataClass.GetInstance;
        CancellationTokenSource cts;
#if DEBUG
		FileReader fileReader = new FileReader();
#else
        RestServices webService = new RestServices();
#endif
		public ChangePasswordPage()
        {
            InitializeComponent();
#if DEBUG
			fileReader.FileReaderDelegate = this;
#else
            webService.WebServiceDelegate = this;
#endif
			this.PageTitle = "CHANGE PASSWORD";
			this.TitleFontFamily = Constants.LULO;
			this.TitleFontColor = Constants.DARK_STRONG;
			this.RightIcon2 = "CloseDark";
			this.RightButton2Command = new Command((obj) => { App.Logout(); });
        }

        protected override void OnAppearing()
        {
            base.OnAppearing();
            DependencyService.Get<DependencyServices.iStatusStyle>().StatusStyle(1);
        }

		protected override void OnDisappearing()
		{
			base.OnDisappearing();
			if (cts != null)
			{
				cts.Cancel();
			}
		}

		async void OnChangeButton_Clicked(object sender, EventArgs e)
		{
            bool isNotError = true;
			if (isClicked == false)
			{
				if (string.IsNullOrEmpty(newPasswordEntry.Text))
				{
					EntryError(newPasswordEntry, "Password is required!");
					isNotError = false;
				}
				else if (!newPasswordEntry.Text.Equals(confirmNewPasswordEntry.Text))
				{
					EntryError(confirmNewPasswordEntry, "Password does not match");
					isNotError = false;
				}
			}
            if (isNotError && isClicked == false)
            {
                isClicked = true;
#if DEBUG
                App.ShowMainPage();
#else

				var json = JsonConvert.SerializeObject(new { email = dataClass.email , password = newPasswordEntry.Text } );

				cts = new CancellationTokenSource();
				try
				{
					await webService.PostRequestAsync(Constants.ROOT_URL + Constants.ACCOUNTS_URL + Constants.CHANGE_PASSWORD_URL, json, cts.Token);
					await Navigation.PopModalAsync();
				}
				catch (OperationCanceledException)
				{
					DependencyService.Get<iConsole>().DisplayText("Web Service: Cancelled!");
				}
				catch (Exception)
				{
					DependencyService.Get<iConsole>().DisplayText("Web Service: Failed!");
				}

				cts = null;
#endif
			}
		}

		void OnNewPasswordEntryContainer_Tapped(Object sender, EventArgs e)
		{
			
			Focus_Entry(newPasswordEntry);
		}
		void OnCofrmEntryContainer_Tapped(Object sender, EventArgs e)
		{

			Focus_Entry(confirmNewPasswordEntry);
		}

		void EntryError(Entry entry, string errorMessage)
		{
			entry.Text = "";
			entry.PlaceholderColor = Color.Red;
			entry.Placeholder = errorMessage;
		}

		void OnNewPasswordEntry_Completed(Object sender, EventArgs e)
		{
			Focus_Entry(this.confirmNewPasswordEntry);
		}

		void Focus_Entry(Entry entry)
		{
			entry.Focus();
		}

		public void ReceiveJSONData(JObject jsonData)
		{

		}

        public async void ReceiveJSONData(JObject jsonData, CancellationToken ct)
        {
#if DEBUG == false  
            if (int.Parse(jsonData["status"].ToString()) == 200)
			{
				var user = JsonConvert.DeserializeObject<User>(jsonData["user"].ToString());

				Application.Current.Properties["user_id"] = user.id;
				Application.Current.Properties["user_email"] = user.email;
				Application.Current.Properties["user_image"] = user.image;
				Application.Current.Properties["user_is_online"] = user.is_online;
				Application.Current.Properties["user_has_stripe"] = user.has_stripe;
				Application.Current.Properties["user_is_confirmed"] = user.is_confirmed;
				Application.Current.Properties["user_is_performer"] = user.is_performer;
				Application.Current.Properties["user_token"] = user.token;
				Application.Current.Properties["user_perform_progress"] = JsonConvert.SerializeObject(user.perform);
				Application.Current.Properties["user_post_progress"] = JsonConvert.SerializeObject(user.post);

				await Application.Current.SavePropertiesAsync();

				dataClass.Update(true);

                await locationHelper.StartListening();
				App.ShowMainPage();
			}
			else
			{

			}
#endif
            isClicked = false;
        }

        public void ReceiveTimeoutError(string title, string error)
        {
            isClicked = false;
            DisplayAlert(title, error, "Okay");
        }
    }
}
