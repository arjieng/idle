﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Xamarin.Forms;

namespace idle.Views
{
    public partial class NewTaskPage : RootViewPage, iFileConnector, iRestConnector
    {
        int previousTaskType;
        int taskID, pageType = 0, paymentType = 1, serviceType = 0;
        Button previousButton;
        DataClass dataClass = DataClass.GetInstance;
        bool IsFirstLoad = true, FromBidPage = false, postIsClick, closeIsClick;
        //string stripeUrl;
        Tasks oldTask;
        Tasks newTask = new Tasks();
        CancellationTokenSource cts;
#if DEBUG
        FileReader file = new FileReader();
#else
        RestServices webService = new RestServices();
#endif

        public NewTaskPage(int taskType)
        {
            InitializeComponent();

            this.TitleFontFamily = Constants.LULO;
            this.TitleFontColor = Constants.DARK_STRONG;

#if DEBUG
            file.FileReaderDelegate = this;
#else
            webService.WebServiceDelegate = this;
#endif

            previousTaskType = taskType;
            this.PageTitle = "NEW TASK";
            this.RightIcon2 = "CloseDark";
            this.RightButton2Command = new Command((obj) => { if (closeIsClick == false) { closeIsClick = true; Navigation.PopModalAsync(); } });

            var task = new Tasks();
            task.task_type = taskType;

            BindingContext = task;
            SetPayment_Selected(cashButton);

            if (taskType == 8)
            {
                FromBidPage = true;
            }

			MessagingCenter.Unsubscribe<Object, string>(this, "StripeConnectCode");
			MessagingCenter.Subscribe<Object, string>(this, "StripeConnectCode", async (sender, code) =>
			{
#if DEBUG == false
                serviceType = 5;
				webService.WebServiceDelegate = this;
				var json = JsonConvert.SerializeObject(new { token = DataClass.GetInstance.UserToken, stripe_account_id = code });
				
				cts = new CancellationTokenSource();
				try
				{
                    await webService.PostRequestAsync(Constants.ROOT_URL + Constants.STRIPE_URL, json, cts.Token);
				}
				catch (OperationCanceledException)
				{
					DependencyService.Get<iConsole>().DisplayText("Web Service: Cancelled!");
				}
				catch (Exception)
				{
					DependencyService.Get<iConsole>().DisplayText("Web Service: Failed!");
				}

				cts = null;
#endif
			});
        }

        public NewTaskPage(int id, Tasks task)
        {
            InitializeComponent();

            this.TitleFontFamily = Constants.LULO;
            this.TitleFontColor = Constants.DARK_STRONG;

            this.PageTitle = "EDIT TASK";
            this.LeftIcon = "ArrowDark";
            this.LeftButtonCommand = new Command((obj) => { Navigation.PopAsync(); });
            taskID = id;
            doneButton.Text = "UPDATE";
            pageType = 1;
            serviceType = 1;

            oldTask = task;
            newTask.Update(task);
            BindingContext = newTask;
            SetPayment_Selected(task.payment == "Cash" ? cashButton : stripeButton);

			//MessagingCenter.Unsubscribe<Object, string>(this, "StripeConnectCode");
			MessagingCenter.Subscribe<Object, string>(this, "StripeConnectCode", async (sender, code) =>
			{
#if DEBUG == false
				serviceType = 5;
				webService.WebServiceDelegate = this;
				var json = JsonConvert.SerializeObject(new { token = DataClass.GetInstance.UserToken, stripe_account_id = code });
				
				cts = new CancellationTokenSource();
				try
				{
                    await webService.PostRequestAsync(Constants.ROOT_URL + Constants.STRIPE_URL, json, cts.Token);
				}
				catch (OperationCanceledException)
				{
					DependencyService.Get<iConsole>().DisplayText("Web Service: Cancelled!");
				}
				catch (Exception)
				{
					DependencyService.Get<iConsole>().DisplayText("Web Service: Failed!");
				}

				cts = null;
#endif
			});

#if DEBUG == false  
            webService.WebServiceDelegate = this;

			Task.Run(async () => 
            {
    			serviceType = 1;
    			cts = new CancellationTokenSource();
    			try
    			{
					var url = Constants.ROOT_URL + Constants.FETCH_TASK_URL + task.id + Constants.TOKEN_URL + dataClass.UserToken;
					await webService.GetRequest(url, cts.Token);
    			}
    			catch (OperationCanceledException)
    			{
    				DependencyService.Get<iConsole>().DisplayText("Web Service: Cancelled!");
    			}
    			catch (Exception)
    			{
    				DependencyService.Get<iConsole>().DisplayText("Web Service: Failed!");
    			}

    			cts = null;
            });
#endif
        }

        protected override async void OnAppearing()
        {
            base.OnAppearing();
            DependencyService.Get<DependencyServices.iStatusStyle>().StatusStyle(1);

#if DEBUG == false
            webService.WebServiceDelegate = this;
#endif

            if (IsFirstLoad == true && dataClass.hasLocation == true)
            {
                IsFirstLoad = false;
#if DEBUG == false
                //Task.Run(() => {
                //    serviceType = 3;
                //    //API to change status
                //});
#endif
            }
            else
            {
                var answer = await DisplayAlert("Location Services Disabled", "Idle app would like to access your current location in order to add geolocation when saving your task.", "Yes", "No");
                if (answer == true)
                {
                    DependencyService.Get<iLocationServices>().OpenSettingsUrl();
                }
            }
        }

		protected override void OnDisappearing()
		{
			base.OnDisappearing();

			MessagingCenter.Unsubscribe<Object, string>(this, "StripeConnectCode");
		}

		void OnEntryContainer_Tapped(Object sender, EventArgs e)
		{
			costEntry.Focus();
		}

        void OnCategory_Clicked(object sender, ImageTappedEventArgs e)
        {
            var buttton = (CircleImage)sender;
            var parent = (StackLayout)buttton.Parent;

            previousTaskType = int.Parse(buttton.StyleId);
            var task = (Tasks)BindingContext;
            task.task_type = previousTaskType;
        }

        async void OnPayment_Clicked(object sender, EventArgs e)
        {
            var button = (Button)sender;
            if (button.Text.Equals("Stripe") && dataClass.UserHasStripe == 0)
            {
                var answer = await DisplayAlert("Stripe Connect Account", "You need to set up your stripe account to allow this payment. Connecting now will disregard your new created task.", "Use Cash", "Connect Now");
                if (answer != true)
                {
#if DEBUG == false
					await Task.Run(async () =>
					{
						serviceType = 4;
						var url = Constants.ROOT_URL + Constants.STRIPE_URL + "/link?token=" + dataClass.UserToken;
    					cts = new CancellationTokenSource();
    					try
    					{
							await webService.GetRequest(url, cts.Token);
    					}
    					catch (OperationCanceledException)
    					{
    						DependencyService.Get<iConsole>().DisplayText("Web Service: Cancelled!");
    					}
    					catch (Exception)
    					{
    						DependencyService.Get<iConsole>().DisplayText("Web Service: Failed!");
    					}

    					cts = null;
                    });
#endif
				}
            }
            else
            {
                SetPayment_Selected(button);
            }
        }

        void SetPayment_Selected(Button button)
        {
            if (previousButton != null)
            {
                previousButton.BackgroundColor = Color.White;
                previousButton.TextColor = Constants.DARK_MEDIUM;
                previousButton.FontFamily = Constants.PROXIMA_REG;
                previousButton.BorderColor = Constants.LIGHT_GRAY;
            }

            button.BackgroundColor = Constants.BUTTON_GREEN;
            button.BorderColor = Constants.BUTTON_GREEN;
            button.TextColor = Color.White;
            button.FontFamily = Constants.PROXIMA_BOLD;
            previousButton = button;

            paymentType = button.Text.Equals("Stripe") ? 0 : 1;
            newTask.payment = paymentType == 0 ? "Stripe" : "Cash";
        }

        void TaskTitle_Completed(Object sender, EventArgs e)
        {
            descriptionEntry.Focus();
        }

        async void PostTask_Clicked(Object sender, EventArgs e)
        {
            if (doneButton.BackgroundColor.Equals(Constants.BUTTON_GREEN))
            {
                if (postIsClick == false)
                {
                    postIsClick = true;
                    if (dataClass.hasLocation == false)
                    {
                        var answer = await DisplayAlert("Location Services Disabled", "Idle app would like to access your current location in order to add geolocation when saving your task.", "Yes", "No");
                        if (answer == true)
                        {
                            DependencyService.Get<iLocationServices>().OpenSettingsUrl();
                        }
                    }
                    else
                    {
#if DEBUG
                        var json = JsonConvert.SerializeObject(new { task = new { title = titleEntry.Text, description = descriptionEntry.Text, task_type = previousTaskType, amount = double.Parse(costEntry.Text), payment = paymentType == 1 ? "Stripe" : "Cash", latitude = dataClass.latitude, longitude = dataClass.longitude, bids = new List<Tasks>(), user = new User() }, token = dataClass.UserToken });

                        if (pageType == 0)
                        {
                            if (FromBidPage == true)
                            {
                                var jsonObject = JObject.Parse(json);
                                dataClass.TaskList.Insert(0, JsonConvert.DeserializeObject<Tasks>(jsonObject["task"].ToString()));
                            }

                            await DisplayAlert("Task Successfully Posted", "Your task was successfully posted on idle. Performers will start bidding on your task. Check out your bids section on Task Page.", "Okay");
                            await Navigation.PopModalAsync();
                        }
                        else
                        {
                            oldTask.Update(newTask);
                            await DisplayAlert("Task Successfully Updated", "Your task was successfully updated on idle. Changes will automatically reflect on the performers map and offers page.", "Okay");
                            await Navigation.PopAsync();
                        }
#else
                        if (pageType == 0)
                        {
                            var json = JsonConvert.SerializeObject(new { task = new { title = titleEntry.Text, description = descriptionEntry.Text, task_type = previousTaskType, price = double.Parse(costEntry.Text), is_cash = paymentType, latitude = dataClass.latitude, longitude = dataClass.longitude }, token = dataClass.UserToken });
                        
                            serviceType = 0;
    						
                            await Task.Run(async delegate 
                            {
        						cts = new CancellationTokenSource();
        						try
        						{
        							await webService.PostRequestAsync(Constants.ROOT_URL + Constants.ACCOUNTS_URL + Constants.FETCH_TASK_URL, json, cts.Token);
        						}
        						catch (OperationCanceledException)
        						{
        							DependencyService.Get<iConsole>().DisplayText("Web Service: Cancelled!");
        						}
        						catch (Exception)
        						{
        							DependencyService.Get<iConsole>().DisplayText("Web Service: Failed!");
        						}

        						cts = null;
                            });
                        }
                        else
                        {
                           var json = JsonConvert.SerializeObject(new { task = new { title = titleEntry.Text, description = descriptionEntry.Text, task_type = previousTaskType, price = double.Parse(costEntry.Text), is_cash = paymentType }, token = dataClass.UserToken });
                                var url = Constants.ROOT_URL + Constants.ACCOUNTS_URL + Constants.FETCH_TASK_URL +"/" + taskID;
                            serviceType = 2;

    						await Task.Run(async delegate
    						{
        						cts = new CancellationTokenSource();
        						try
        						{
        							await webService.PutRequestAsync(url, json, cts.Token);    
        						}
        						catch (OperationCanceledException)
        						{
        							DependencyService.Get<iConsole>().DisplayText("Web Service: Cancelled!");
        						}
        						catch (Exception)
        						{
        							DependencyService.Get<iConsole>().DisplayText("Web Service: Failed!");
        						}

        						cts = null;
                            });
    				    }
#endif
					}
                }
            }
        }

		public void ReceiveJSONData(JObject jsonData)
		{

		}

        public void ReceiveJSONData(JObject jsonData, CancellationToken ct)
        {
            Device.BeginInvokeOnMainThread( async delegate {
				if (int.Parse(jsonData["status"].ToString()) == 200)
				{
                   switch (serviceType)
					{
						case 0: //create Task
							var task = JsonConvert.DeserializeObject<Tasks>(jsonData["task"].ToString());
							if (FromBidPage == true)
							{
								dataClass.TaskList.Insert(0, task);
							}

							await DisplayAlert("Task successfully posted", "Your task can be found under the Bids section of the Tasks page. Any bids for your task will also be found under that section of the Tasks page.", "Okay");
							await Navigation.PopModalAsync();

							break;
						case 1: //Edit Task
							var updatedTask = JsonConvert.DeserializeObject<Tasks>(jsonData["task"].ToString());
							SetPayment_Selected(updatedTask.payment == "Cash" ? cashButton : stripeButton);
							//BindingContext = task;
                            newTask.Update(updatedTask);

                            oldTask.Update(updatedTask);

							break;
						case 2: //Update Task
							oldTask.Update(newTask);

							await DisplayAlert("Task Successfully Updated", "Your task was successfully updated on idle. Changes will automatically reflect on the performers map and offers page.", "Okay");
							await Navigation.PopAsync();

							break;
                        case 3: //update location api
                            
                            break;
                        case 4: //fetch stripe url
                            Device.OpenUri(new Uri(jsonData["url"].ToString()));
                            break;
                        case 5: //connecting to stripe
							Application.Current.Properties["user_has_stripe"] = 1;
                            break;
					}

				}
				else
				{
				    	
				}
            });
        }

        public  void ReceiveTimeoutError(string title, string error)
        {
			Device.BeginInvokeOnMainThread(async delegate
			{
				await DisplayAlert(title, error, "Okay");
			});
        }
    }
}
