﻿﻿﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Xamarin.Forms;

namespace idle.Views
{
    
    public partial class ProfilePage : RootViewPage, iFileConnector, iRestConnector
    {
        DataClass dataClass = DataClass.GetInstance;
        User user;
        ObservableCollection<Tasks> tasks = new ObservableCollection<Tasks>();
        int serviceType = 0, type, userID, offSet = 0;
        bool addTaskIsClick , isClick , isUserLoaded, isLoading, IsSwitchClicked = false;
        CancellationTokenSource cts;
#if DEBUG
        FileReader fileReader = new FileReader();
#else
        RestServices webService = new RestServices();
#endif

		public ProfilePage(int type, int id)
		{
			InitializeComponent();

#if DEBUG
            fileReader.FileReaderDelegate = this;
#else
            webService.WebServiceDelegate = this;
#endif

            this.PageTitle = "PROFILE";
            this.TitleFontFamily = Constants.LULO;
            this.TitleFontColor = Constants.DARK_STRONG;

            //MasterPage.PageSelected.Invoke(this, typeof(ProfilePage));

            switch (type)
            {
                case 0: // my profile
					this.LeftIcon = "BurgerDark";
					this.RightIcon2 = "PlusDark";
					this.LeftButtonCommand = new Command((obj) => { MyMasterDetailPage detail = (MyMasterDetailPage)this.Parent.Parent; detail.IsPresented = true; DependencyService.Get<DependencyServices.iStatusStyle>().StatusStyle(0); });
                    this.RightButton2Command = new Command((obj) => { if (addTaskIsClick == false) { addTaskIsClick = true; Navigation.PushModalAsync(new NewTaskPage(-1)); } });
                    IconVisible(true, false);
                    break;

                case 1: // other profile 
                    this.LeftIcon = "ArrowDark";
                    this.LeftButtonCommand = new Command((obj) => { if (!isClick) { isClick = true; Navigation.PopAsync(); } });
                    IconVisible(false, true);
					break;
			}

            this.type = type;
            userID = id;
			Task.Run(() => {
                FetchData();
            });
            var tapGestureRecognizer = new TapGestureRecognizer();
            tapGestureRecognizer.Tapped += SwitchButton_TapGestureRecognizer;
            SwitchButton.GestureRecognizers.Add(tapGestureRecognizer);
        }

        protected override void OnAppearing()
        {
            isClick = false;
            base.OnAppearing();

            if(type == 1)
            {
                MyMasterDetailPage.isHomePage = false;
            }
            else
            {
                MyMasterDetailPage.isHomePage = true;
            }

#if DEBUG
            fileReader.FileReaderDelegate = this;
#else
			webService.WebServiceDelegate = this;
#endif
			addTaskIsClick = false;
            DependencyService.Get<DependencyServices.iStatusStyle>().StatusStyle(1);
        }

        protected override void OnDisappearing()
        {
            base.OnDisappearing();
            MyMasterDetailPage.isHomePage = false;

			if (cts != null)
			{
				cts.Cancel();
			}
        }

        async void FetchData()
        {
            isLoading = true;
            isClick = true;
            loadingIndicator.IsRunning = true;
            serviceType = 0;
            cts = new CancellationTokenSource();
#if DEBUG
			await fileReader.ReadFile("User.json", true, cts.Token);
#else
			try
			{
                await webService.GetRequest(Constants.ROOT_URL + Constants.USERS_URL + "/" + userID + Constants.TOKEN_URL + dataClass.UserToken, cts.Token);
			}
			catch (OperationCanceledException)
			{
				DependencyService.Get<iConsole>().DisplayText("Web Service: Cancelled!");
			}
			catch (Exception)
			{
				DependencyService.Get<iConsole>().DisplayText("Web Service: Failed!");
			}

			cts = null;
#endif
		}
                     
        void IconVisible(bool owner, bool other)
        {
            SwitchButton.IsVisible = owner;
            MessageButton.IsVisible = other;
        }

        void OnTask_ItemTapped(object sender, ItemTappedEventArgs e)
        {
			// offSet, task, pageType
			Navigation.PushAsync(new ViewTaskPage(offSet, (Tasks)e.Item, -1));
			taskList.SelectedItem = null;
		}

        async void SwitchButton_TapGestureRecognizer(Object sender, EventArgs e)
        {
            if (!IsSwitchClicked)
            {
                IsSwitchClicked = true;
                if (SwitchButton.IsToggled == 1)
                {
                    SwitchButton.IsToggled = 0;
                }
                else
                {
                    SwitchButton.IsToggled = 1;
                }
                serviceType = 1;
#if DEBUG
                Application.Current.Properties["user_is_online"] = SwitchButton.IsToggled;
                await Application.Current.SavePropertiesAsync();
                user.is_online = SwitchButton.IsToggled;
                IsSwitchClicked = false;
#else
                var json = JsonConvert.SerializeObject(new { token = dataClass.UserToken });
                var url = Constants.ROOT_URL + Constants.ACCOUNTS_URL + Constants.STATUS_URL;
				
				cts = new CancellationTokenSource();
				try
				{
                    await webService.PutRequestAsync(url, json, cts.Token);
				}
				catch (OperationCanceledException)
				{
					DependencyService.Get<iConsole>().DisplayText("Web Service: Cancelled!");
				}
				catch (Exception)
				{
					DependencyService.Get<iConsole>().DisplayText("Web Service: Failed!");
				}

				cts = null;
#endif
            }
		}

        async void OnFilter_Clicked(object sender, System.EventArgs e)
        {
            var myAction = await DisplayActionSheet(null, "Cancel", null, "Quick Task", "Grocery", "Laundry", "Transport", "Other", "All");
            if(myAction!=null)
            if (myAction.ToString() != "Cancel")
            {
                if (myAction.ToString() == "Quick Task")
                {
                    categoryFilter.BackgroundColor = Constants.QuickieColor;
                    FilterIndicator();
                    DisplayFilteredData(0);
                }
                else if (myAction.ToString() == "Grocery")
                {
                    categoryFilter.BackgroundColor = Constants.GroceryColor;
                    FilterIndicator();
                    DisplayFilteredData(1);
                }
                else if (myAction.ToString() == "Laundry")
                {
                    categoryFilter.BackgroundColor = Constants.LaundryColor;
                    FilterIndicator();
                    DisplayFilteredData(2);
                }
                else if (myAction.ToString() == "Transport")
                {
                    categoryFilter.BackgroundColor = Constants.TransportColor;
                    FilterIndicator();
                    DisplayFilteredData(3);
                }
                else if (myAction.ToString() == "Other")
                {
                    categoryFilter.BackgroundColor = Constants.OtherColor;
                    FilterIndicator();
                    DisplayFilteredData(4);
                }
                else if (myAction.ToString() == "All")
                {
                    categoryFilter.IsVisible = false;
                    allFilter.IsVisible = true;
                    DisplayFilteredData(5);
                }
            }
        }

        void DisplayFilteredData(int taskType)
        {
            if (taskType == 5)
            {
                taskList.ItemsSource = tasks;
            }
            else
            {
                taskList.ItemsSource = tasks.Where(arg => arg.task_type == taskType);
            }
        }

        void FilterIndicator()
        {
            categoryFilter.IsVisible = true;
            allFilter.IsVisible = false;
        }

        async void OnAction_Clicked(object sender, System.EventArgs e)
        {
            switch (type)
            {
                case 0: // my profile
                    var myAction = await DisplayActionSheet(null, "Cancel", null, "Edit Profile");
                    if(myAction!=null)
                    if (myAction.ToString() == "Edit Profile")
                    {
                            if (user != null)
                            {
                                await Navigation.PushModalAsync(new EditProfilePage(dataClass.UserID, user));
                            }
                    }
                    break;
                case 1: // other profile 
                    var userAction = await DisplayActionSheet(null, "Cancel", null, "Report User");
                    if(userAction!=null)
                    if (userAction.ToString() == "Report User")
                    {
                            if (user != null)
                            {
                            await Navigation.PushAsync(new ReportUserPage(userID, user));
                            }
                    }

                    break;
            }
        }
        async void OnProfile_Tapped(object sender, EventArgs e)
        {
            if (isLoading == false)
            {
                if (userID == dataClass.UserID && isClick == false)
                {
                    isClick = true;
                    await Navigation.PushModalAsync(new EditProfilePage(userID, user), true);
                }
            }
        }

        async void OnProfile_ImageTapped(object sender, idle.ImageTappedEventArgs e)
		{
            if (isLoading == false)
            {
                if (userID == dataClass.UserID && isClick == false)
                {
                    await Navigation.PushModalAsync(new EditProfilePage(userID, user), true);
                }
            }
		}

        async void OnMessage_Clicked(object sender, idle.ImageTappedEventArgs e)
        {
            if (isLoading == false)
            {
                await Navigation.PushAsync(new MessagePage(userName.Text, user, user.conversation_id));
            }
        }

		public void ReceiveJSONData(JObject jsonData)
		{

		}

        public void ReceiveJSONData(JObject jsonData, CancellationToken ct)
        {
            Device.BeginInvokeOnMainThread(()=>{
				if (int.Parse(jsonData["status"].ToString()) == 200)
				{
                    switch (serviceType)
					{
						case 0:
                            if (isUserLoaded == false)
                            {
                                user = JsonConvert.DeserializeObject<User>(jsonData["user"].ToString());
                                isUserLoaded = true;
                                Application.Current.Properties["user_is_online"] = user.is_online;
                                Application.Current.SavePropertiesAsync();
                                this.BindingContext = user;
                                if (type == 0)
                                {
                                    MasterPage.UpdateProfile.Invoke(this, user);
                                }
								foreach (var task in user.tasks)
								{
									tasks.Add(task);
								}

                            }
                            else
                            {
                                var userTasks = JsonConvert.DeserializeObject<ObservableCollection<Tasks>>(jsonData["tasks"].ToString());

                                foreach (var task in userTasks )
								{
									tasks.Add(task);
								}
                            }

							taskList.ItemsSource = tasks;

#if DEBUG == false
                            Task.Run( async () => {
                                serviceType = 0;
                                var pagination = jsonData["pagination"];
                                offSet = int.Parse(pagination["off_set"].ToString());
                                if(int.Parse(pagination["load_more"].ToString()) == 1)
                                {
									cts = new CancellationTokenSource();
									try
									{
                                        await webService.GetRequest(pagination["url"].ToString(), cts.Token);
									}
									catch (OperationCanceledException)
									{
										DependencyService.Get<iConsole>().DisplayText("Web Service: Cancelled!");
									}
									catch (Exception)
									{
										DependencyService.Get<iConsole>().DisplayText("Web Service: Failed!");
									}

									cts = null;
                                }
                            });
#endif
							break;
						case 1: //online offline
                            IsSwitchClicked = false;
							break;
					}
				}
				else
				{
                    DisplayAlert("Alert",jsonData["error"].ToString(),"Okay");
				}

                loadingIndicator.IsRunning = false;
                isLoading = false;
                isClick = false;
            });
        }

        public void ReceiveTimeoutError(string title, string error)
        {
            Device.BeginInvokeOnMainThread( async delegate {
				loadingIndicator.IsRunning = false;
				isLoading = false;
                isClick = false;
				IsSwitchClicked = false;
               await DisplayAlert(title, error, "Okay");
            });
           
		}
    }
}
