﻿using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Xamarin.Forms;

namespace idle.Views
{
    public partial class TaskSummaryPage : RootViewPage, iRestConnector
    {
        DataClass dataClass = DataClass.GetInstance;
        bool IsFirstLoad = true;
        int serviceType = 0, offSet, pageType;
        Tasks task = new Tasks();
        CancellationTokenSource cts;
#if DEBUG == false
        RestServices webService = new RestServices();
#endif
        //page type: 0 = map inprogress, 1 = mytask
        public TaskSummaryPage(int id, Tasks task, int offSet, int pageType)
        {
            InitializeComponent();

            this.TitleFontFamily = Constants.LULO;
            this.TitleFontColor = Constants.DARK_STRONG;
            this.LeftIcon = "ArrowDark";
            this.LeftButtonCommand = new Command((obj) =>
            {
                if (dataClass.stripeToken != null && task.payment.Equals("Stripe"))
                {
                    DisplayAlert("End Task First", "Sorry you cannot exit from page unless you end task.", "Okay");
                }
                else
                {
                    Navigation.PopAsync();
                }
            });
            this.PageTitle = "TASK SUMMARY";
            this.RightIcon2 = "PlusDark";
            this.RightButton2Command = new Command((obj) => { Navigation.PushModalAsync(new NewTaskPage(-1)); });

            this.offSet = offSet;
            this.pageType = pageType;
            this.task = task;
            BindingContext = this.task;

#if DEBUG == false
            webService.WebServiceDelegate = this;
            var url = Constants.ROOT_URL + Constants.FETCH_TASK_URL + "/" + task.id + Constants.TOKEN_URL + dataClass.UserToken;
			
            Task.Run(async delegate 
            {
    			cts = new CancellationTokenSource();
    			try
    			{
                     await webService.GetRequest(url, cts.Token); 
    			}
    			catch (OperationCanceledException)
    			{
    				DependencyService.Get<iConsole>().DisplayText("Web Service: Cancelled!");
    			}
    			catch (Exception)
    			{
    				DependencyService.Get<iConsole>().DisplayText("Web Service: Failed!");
    			}

    			cts = null;
            });
#endif
        }

        protected override void OnAppearing()
        {
            base.OnAppearing();

			MessagingCenter.Subscribe<App, JObject>(this, "TaskCompleted", (sender, obj) =>
			{
				if (int.Parse(obj["custom_data"]["task_id"].ToString()) == task.id)
				{
					task.status = int.Parse(obj["custom_data"]["status"].ToString());
					BindingContext = task;
				}
			});

			MessagingCenter.Subscribe<App, JObject>(this, "BidCancelled", (sender, obj) =>
			{
				if (int.Parse(obj["custom_data"]["task_id"].ToString()) == task.id)
				{
					if (dataClass.BidList.ToList().Exists(cancelBid => cancelBid.id == int.Parse(obj["custom_data"]["bid_id"].ToString())))
					{
						dataClass.BidList.Remove(dataClass.BidList.ToList().Find(cancelBid => cancelBid.id == int.Parse(obj["custom_data"]["bid_id"].ToString())));
					}
				}
			});

			MessagingCenter.Subscribe<App, JObject>(this, "TaskCancelled", (sender, obj) =>
			{
				if (int.Parse(obj["custom_data"]["task_id"].ToString()) == task.id)
				{
					task.status = int.Parse(obj["custom_data"]["status"].ToString());
					BindingContext = task;
				}
			});

            DependencyService.Get<DependencyServices.iStatusStyle>().StatusStyle(1);
#if DEBUG == false
            webService.WebServiceDelegate = this;
#endif
            Device.BeginInvokeOnMainThread(() =>
            {
				if (IsFirstLoad == true) { IsFirstLoad = false; }
				else
				{
					if (dataClass.BidList != null)
					{
						if (dataClass.BidList.Count == 0 && dataClass.BidUser != null)
						{
							task.status = 1;
							task.user = dataClass.BidUser;
							task.bid = dataClass.bid;
							BindingContext = task;

							dataClass.TaskList.Remove((Tasks)BindingContext);

							dataClass.InProgressPosterList.Insert(0, task);
							dataClass.BidUser = null;
							dataClass.bid = String.Empty;
						}
					}
				}
            });

            if (dataClass.stripeToken != null)
            {
                task.is_paid = 1;
                BindingContext = task;
            }
        }

        protected override void OnDisappearing()
        {
            base.OnDisappearing();
            MessagingCenter.Unsubscribe<App, JObject>(this, "TaskCompleted");
            MessagingCenter.Unsubscribe<App, JObject>(this, "BidCancelled");
            MessagingCenter.Unsubscribe<App, JObject>(this, "TaskCancelled");

			if (cts != null)
			{
				cts.Cancel();
			}
        }

        async void OnMessage_Clicked(object sender, idle.ImageTappedEventArgs e)
        {
            if (task.status != 0)
            {
                await Navigation.PushAsync(new MessagePage(UserName.Text, task.user, task.user.conversation_id));
            }
        }

        async void OnAction_Clicked(object sender, System.EventArgs e)
        {
            if (task.status == 0)
            {
                var myAction = await DisplayActionSheet(null, "Cancel", "Delete Task", "Edit Task");
                if (myAction != null)
                {
                    if (myAction.ToString() == "Delete Task")
                    {
                        var alert = await DisplayAlert("Proceed Delete?", "Are you sure you want to delete this task? There might be performers who already bid on the task!", "No", "Yes");
                        if (alert == false)
                        {
#if DEBUG
                            dataClass.TaskList.Remove(task);
                            await Navigation.PopAsync();
#else
	                        serviceType = 2;
	                        //API to delete task
	                        var url = Constants.ROOT_URL + Constants.ACCOUNTS_URL + Constants.FETCH_TASK_URL + "/" + task.id + Constants.TOKEN_URL + dataClass.UserToken + "&off_set=" + offSet;
							
                            await Task.Run(async delegate 
                            {
    							cts = new CancellationTokenSource();
    							try
    							{
                                     await webService.DeleteRequestAsync(url, cts.Token); 
    							}
    							catch (OperationCanceledException)
    							{
    								DependencyService.Get<iConsole>().DisplayText("Web Service: Cancelled!");
    							}
    							catch (Exception)
    							{
    								DependencyService.Get<iConsole>().DisplayText("Web Service: Failed!");
    							}

    							cts = null;
                            });
#endif
                        }
                    }
                    else if (myAction.ToString() == "Edit Task")
                    {
                        await Navigation.PushAsync(new NewTaskPage(task.id, task));
                    }
                }
            }
            else if (task.status == 1 || task.status == 2)
            {
                var myAction = await DisplayActionSheet(null, "Cancel", null, "Edit Task");
                if (myAction != null)
                {
                    if (myAction.ToString() == "Edit Task")
                    {
                        await Navigation.PushAsync(new NewTaskPage(task.id, task));
                    }
                }
            }
        }

#if DEBUG
        void OnNotYetButton_Clicked(object sender, EventArgs e)
#else
        async void OnNotYetButton_Clicked(object sender, EventArgs e)
#endif
        {
            serviceType = 5;
            var json = JsonConvert.SerializeObject(new { token = dataClass.UserToken, status = 1 });
#if DEBUG == false
			if (cts != null)
			{
				cts.Cancel();
			}
            await Task.Run(async delegate 
            {
    			cts = new CancellationTokenSource();
    			try
    			{
                     await webService.PutRequestAsync(Constants.ROOT_URL + Constants.ACCOUNTS_URL + Constants.FETCH_TASK_URL + "/" + task.id + "/completed", json, cts.Token); 
    			}
    			catch (OperationCanceledException)
    			{
    				DependencyService.Get<iConsole>().DisplayText("Web Service: Cancelled!");
    			}
    			catch (Exception)
    			{
    				DependencyService.Get<iConsole>().DisplayText("Web Service: Failed!");
    			}

    			cts = null;
            });
#endif
        }

#if DEBUG
        void OnCompleteButton_Clicked(object sender, EventArgs e)
#else
        async void OnCompleteButton_Clicked(object sender, EventArgs e)
#endif
        {
            serviceType = 4;
            var json = JsonConvert.SerializeObject(new { token = dataClass.UserToken, status = 5 });
#if DEBUG == false
			if (cts != null)
			{
				cts.Cancel();
			}
            await Task.Run(async delegate 
            {
    			cts = new CancellationTokenSource();
    			try
    			{
                     await webService.PutRequestAsync(Constants.ROOT_URL + Constants.ACCOUNTS_URL + Constants.FETCH_TASK_URL + "/" + task.id + "/completed", json, cts.Token); 
    			}
    			catch (OperationCanceledException)
    			{
    				DependencyService.Get<iConsole>().DisplayText("Web Service: Cancelled!");
    			}
    			catch (Exception)
    			{
    				DependencyService.Get<iConsole>().DisplayText("Web Service: Failed!");
    			}

    			cts = null;
            });
#endif
        }

        async void OnEndTaskButton_Clicked(object sender, EventArgs e)
        {
            if (((Button)sender).Text == "BIDS")
            {
                NavigationPage navPage = new NavigationPage(new BidOfferPage(task.id, taskTitle.Text, task.task_type));
                await Navigation.PushModalAsync(navPage);
            }
            else if (((Button)sender).Text == "CANCEL")
            {
                var AlertString = "Are you sure you want to cancel this task? " + task.user.name + " will be notified.";
                var action = await DisplayAlert("Cancel Task?", AlertString, "No", "Yes");
                if (action == false)
                {
#if DEBUG
                    dataClass.InProgressPosterList.Remove(task);
                    await Navigation.PopAsync();
#else
                    serviceType = 1;
					//API to cancel task
					var json = JsonConvert.SerializeObject(new { token = dataClass.UserToken, type = 1, status = 3 });
					if (cts != null)
					{
						cts.Cancel();
					}
					await Task.Run(async delegate
					{
    					cts = new CancellationTokenSource();
    					try
    					{
    						await webService.PutRequestAsync(Constants.ROOT_URL + Constants.ACCOUNTS_URL + Constants.FETCH_TASK_URL + "/" + task.id + "/cancel", json, cts.Token);
    					}
    					catch (OperationCanceledException)
    					{
    						DependencyService.Get<iConsole>().DisplayText("Web Service: Cancelled!");
    					}
    					catch (Exception)
    					{
    						DependencyService.Get<iConsole>().DisplayText("Web Service: Failed!");
    					}

    					cts = null;
                    });
#endif
                }
            }
            else if (((Button)sender).Text == "END TASK")
            {

#if DEBUG
                dataClass.InProgressPosterList.Remove(task);
                await Navigation.PopAsync();
#else
                serviceType = 3;
                var json = "";
                if (task.payment.Equals("Cash"))
                {
                    json = JsonConvert.SerializeObject(new { token = dataClass.UserToken, charge = new { user_id = dataClass.UserID, amount = task.amount }, ratings = new { user_id = task.user.id, rate = RateMe.Rating } });
                }
                else
                {
                    json = JsonConvert.SerializeObject(new { token = dataClass.UserToken, stripe_token = dataClass.stripeToken, charge = new { user_id = dataClass.UserID, amount = task.amount, task_id = task.id }, ratings = new { user_id = task.user.id, rate = RateMe.Rating } });
                }

				if (cts != null)
				{
					cts.Cancel();
				}

				await Task.Run(async delegate
				{
    				cts = new CancellationTokenSource();
    				try
    				{
    					await webService.PutRequestAsync(Constants.ROOT_URL + Constants.ACCOUNTS_URL + Constants.FETCH_TASK_URL + "/" + task.id + "/finished", json, cts.Token);
    				}
    				catch (OperationCanceledException)
    				{
    					DependencyService.Get<iConsole>().DisplayText("Web Service: Cancelled!");
    				}
    				catch (Exception)
    				{
    					DependencyService.Get<iConsole>().DisplayText("Web Service: Failed!");
    				}

    				cts = null;
                });
#endif
            }
            else if (((Button)sender).Text == "PAY")
            {
                await Navigation.PushModalAsync(new PaymentPage());
            }
            else if (((Button)sender).Text == "CONFIRM CANCELLATION")
            {
                var action = await DisplayAlert("Confirm Cancellation?", "Are you sure you want to cancel this task performed by " + task.user.name + "?", "No", "Yes");
                var json = "";
                if (action == false)
                {
                    serviceType = 6;
                    json = JsonConvert.SerializeObject(new { token = dataClass.UserToken, type = 0, status = 0 });
                }
                else
                {
                    serviceType = 7;
                    json = JsonConvert.SerializeObject(new { token = dataClass.UserToken, type = 0, status = 1 });
                }
#if DEBUG == false
				if (cts != null)
				{
					cts.Cancel();
				}
				await Task.Run(async delegate
				{
    				cts = new CancellationTokenSource();
    				try
    				{
    					await webService.PutRequestAsync(Constants.ROOT_URL + Constants.ACCOUNTS_URL + Constants.FETCH_TASK_URL + "/" + task.id + "/cancel", json, cts.Token);
    				}
    				catch (OperationCanceledException)
    				{
    					DependencyService.Get<iConsole>().DisplayText("Web Service: Cancelled!");
    				}
    				catch (Exception)
    				{
    					DependencyService.Get<iConsole>().DisplayText("Web Service: Failed!");
    				}

    				cts = null;
                });
#endif
            }
		}

		void OnProfile_Tapped(object sender, EventArgs e)
		{
            if(task.status != 0)
            {
                Navigation.PushAsync(new ProfilePage(1, task.user.id));
            }
		}

		public void ReceiveJSONData(JObject jsonData)
		{

		}

		public void ReceiveJSONData(JObject jsonData, CancellationToken ct)
		{
            Device.BeginInvokeOnMainThread(async () =>
            {
                if (int.Parse(jsonData["status"].ToString()) == 300)
                {
					dataClass.stripeToken = null;      
                }
                else
                {
                    int HTTPStatus = int.Parse((jsonData["status"].ToString()));
                    //int ListIndex = 0;
                    switch (serviceType)
                    {
                        case 0: //fetch no alert
                            task.Update(JsonConvert.DeserializeObject<Tasks>(jsonData["task"].ToString()));
                            BindingContext = task;
                            break;

                        case 1: //cancel task with alert
                            await DisplayAlert("Task Cancelled", jsonData["success"].ToString(), "Okay");
                            task.status = 3;
                            BindingContext = task;
                            break;

                        case 2: //delete task with alert
                            if (HTTPStatus == 201)
                            {
                                var tempTask = JsonConvert.DeserializeObject<Tasks>(jsonData["task"].ToString());
                                if (pageType == 1)
                                {
                                    dataClass.TaskList.Add(tempTask);
                                }
                                else
                                {
                                    dataClass.InProgressPosterList.Add(tempTask);
                                } 
                            }

                            if (pageType == 1)
                            {
                                dataClass.TaskList.Remove(task);
                            }
                            else
                            {
                                dataClass.InProgressPosterList.Remove(task);
                            }

                            if(HTTPStatus == 200)
                            {
                                await DisplayAlert("Task Deleted", jsonData["success"].ToString(), "Okay");
                            }

                            await Navigation.PopAsync();

                            break;

                        case 3: // end task no alert
                            dataClass.InProgressPosterList.Remove(task);
                            if (task.payment.Equals("Stripe"))
                            {
                                dataClass.stripeToken = null;
                            }
                            await Navigation.PopAsync();
                            break;

                        case 4: //confirm complete task with alert
                            await DisplayAlert("Task Marked as Completed", jsonData["success"].ToString(), "Okay");
                            task.status = 5;
                            BindingContext = task;
                            break;

                        case 5: //task not yet done with alert
                            await DisplayAlert("Task marked as not complete", jsonData["success"].ToString(), "Okay");
                            task.status = 1;
                            BindingContext = task;
                            break;

                        case 6: //allow cancellation with alert
                            if (dataClass.InProgressPosterList.ToList().Exists(inProgressTask => inProgressTask.id == task.id))
                            {
                                dataClass.InProgressPosterList.Remove(dataClass.InProgressPosterList.ToList().Find(inProgressTask => inProgressTask.id == task.id));
                            }

                            task.status = 0;
                            BindingContext = task;
                            var action = await DisplayAlert("Cancellation Allowed", jsonData["success"].ToString(), "No", "Yes");
                            if (action == false)
                            {
                                await Navigation.PushAsync(new ReportUserPage(task.user.id, task.user, 1));
                            }
                            else
                            {
                                await Navigation.PopAsync();
                            }
                            break;

                        case 7: //deny cancellation with alert
                            await DisplayAlert("Cancellation Denied", jsonData["success"].ToString(), "Okay");
                            task.status = 1;
                            BindingContext = task;
                            break;
                    }

                }
            });
		}

		public void ReceiveTimeoutError(string title, string error)
        {
            Device.BeginInvokeOnMainThread( async delegate {
                await DisplayAlert(title, error, "Okay");
            });
           
		}
	}
}
