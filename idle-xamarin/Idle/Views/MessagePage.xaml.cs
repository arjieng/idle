﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Xamarin.Forms;
using System.Linq;
using System.Threading.Tasks;
using Plugin.Media;
using Plugin.Media.Abstractions;
using System.Threading;

namespace idle.Views
{
    public partial class MessagePage : RootViewPage, iFileConnector, iRestConnector
    {
        public ImageSource image = "IdleLogo";
        public static readonly BindableProperty TextProperty = BindableProperty.Create(nameof(Text), typeof(string), typeof(MessagePage), "Test");
        public string Text
        {
            get { return (string)GetValue(TextProperty); }
            set { SetValue(TextProperty, value); }
        }

        public static readonly BindableProperty UserImageProperty = BindableProperty.Create(nameof(UserImage), typeof(ImageSource), typeof(MessagePage), null);
        public ImageSource UserImage
        {
            get { return (ImageSource)GetValue(UserImageProperty); }
            set { SetValue(UserImageProperty, value); }
        }

        public static readonly BindableProperty OtherUserImageProperty = BindableProperty.Create(nameof(OtherUserImage), typeof(ImageSource), typeof(MessagePage), null);
        public ImageSource OtherUserImage
        {
            get { return (ImageSource)GetValue(OtherUserImageProperty); }
            set { SetValue(OtherUserImageProperty, value); }
        }


        ObservableCollection<Message> messages;
        bool isContacted, isSending, isLoaded;
        int conversationId, serviceType, pageType;
        User user;
        JToken pagination;
        DataClass dataClass = DataClass.GetInstance;
        CameraHelper cameraHelper = new CameraHelper();
        MediaFile file;
        CancellationTokenSource cts;
#if DEBUG
        FileReader fileReader = new FileReader();
#else   
        RestServices webService = new RestServices();
#endif
        //pageType: 0 - inbox page 
        public MessagePage(string name, User user, int conversationID, int pageType = 1)
        {
            InitializeComponent();
            //initialize connections
#if DEBUG
            fileReader.FileReaderDelegate = this;
#else
            webService.WebServiceDelegate = this;
#endif
            this.user = user;
            messages = new ObservableCollection<Message>();
            var names = name.Split(' ');
            this.PageTitle = names[0];
            this.TitleFontFamily = Constants.LULO;
            this.TitleFontColor = Constants.DARK_STRONG;
            this.LeftIcon = "ArrowDark";
            this.LeftButtonCommand = new Command(async (obj) => { await Navigation.PopAsync(); });

            UserImage = DataClass.GetInstance.UserImage;
#if DEBUG
            OtherUserImage = "https://dab1nmslvvntp.cloudfront.net/wp-content/uploads/2016/03/1458289957powerful-images3.jpg";
#else
            OtherUserImage = user.image;
#endif
            this.pageType = pageType;
            conversationId = conversationID;
            isContacted = conversationID != 0;

            messageList.ItemsSource = messages;

            GetData();
            //messageList.ItemsSource = lists;


        }

        protected override void OnAppearing()
        {
            base.OnAppearing();

            MessagingCenter.Subscribe<App, JObject>(this, "MessageReceived", (sender, obj) =>
            {
                Device.BeginInvokeOnMainThread(async () =>
                {

                    int convoID = int.Parse(obj["custom_data"]["conversation_id"].ToString());
                    Message message = JsonConvert.DeserializeObject<Message>(obj["custom_data"]["message"].ToString());
                    if (convoID == conversationId)
                    {
                        messages.Add(message);
                    }
                    await Task.Delay(250);
                    messageList.ScrollTo(message, ScrollToPosition.End, false);

                });


            });

            DependencyService.Get<DependencyServices.iStatusStyle>().StatusStyle(1);
        }

        protected override void OnDisappearing()
        {
            base.OnDisappearing();

            MessagingCenter.Unsubscribe<App, JObject>(this, "MessageReceived");

			if (cts != null)
			{
				cts.Cancel();
			}
        }

        async void GetData()
        {
            serviceType = 0;
            cts = new CancellationTokenSource();
#if DEBUG
            await fileReader.ReadFile("Conversation.json", true, cts.Token);
#else
            if (isContacted)
            {
                loadingIndicator.IsRunning = true;
                string url = Constants.ROOT_URL + Constants.ACCOUNTS_URL + Constants.CONVERSATION_URL + "/" + conversationId + Constants.MESSAGE_URL + Constants.TOKEN_URL + DataClass.GetInstance.UserToken;

				try
				{
                    await webService.GetRequest(url, cts.Token);
				}
				catch (OperationCanceledException)
				{
					DependencyService.Get<iConsole>().DisplayText("Web Service: Cancelled!");
				}
				catch (Exception)
				{
					DependencyService.Get<iConsole>().DisplayText("Web Service: Failed!");
				}

				cts = null;
            }
#endif
        }

        async void OnSend_Clicked(object sender, EventArgs e)
        {
            bool isEmpty;
            if (Device.RuntimePlatform == Device.iOS)
            {
                isEmpty = messageEditor.Text.Equals(messageEditor.Placeholder);
            }
            else
            {
                isEmpty = string.IsNullOrEmpty(messageEditor.Text);
            }

            if (isEmpty == false)
            {
                serviceType = 1;
#if DEBUG
                var newMessage = new Message()
                {
                    id = messages[messages.Count - 1].id,
                    user = new User() { id = user.id, image = DataClass.GetInstance.UserImage, is_owner = 1 },
                    body = messageEditor.Text,
                    timestamp = DateTime.Now.ToLocalTime().Hour + ":" + DateTime.Now.ToLocalTime().Minute + "PM"
                };
                messages.Add(newMessage);
                if (Device.RuntimePlatform == Device.iOS)
                {
                    messageEditor.Text = messageEditor.Placeholder;
                    messageEditor.TextColor = messageEditor.PlaceholderColor;
                }
                else
                {
                    messageEditor.Text = "";
                }


                messageEditor.Unfocus();
#else
                string json;
                if (isContacted)
                {
                    json = JsonConvert.SerializeObject(new { token = DataClass.GetInstance.UserToken, message = new { text = messageEditor.Text.Trim() }, conversation_id = conversationId });
                }
                else
                {
                    json = JsonConvert.SerializeObject(new { token = DataClass.GetInstance.UserToken, message = new { text = messageEditor.Text.Trim() } });
                }
                string url = Constants.ROOT_URL + Constants.USERS_URL + "/" + user.id + Constants.MESSAGE_URL;
                if (isSending == false)
                {
                    isSending = true;

					cts = new CancellationTokenSource();
					try
					{
                        await webService.PostRequestAsync(url, json, cts.Token);
					}
					catch (OperationCanceledException)
					{
						DependencyService.Get<iConsole>().DisplayText("Web Service: Cancelled!");
					}
					catch (Exception)
					{
						DependencyService.Get<iConsole>().DisplayText("Web Service: Failed!");
					}

					cts = null;

                }
#endif
                //await Task.Delay();
                //messageList.ScrollTo(newMessage, ScrollToPosition.End, true);
            }
        }

        async void OnImageSend_Clicked(object sender, EventArgs e)
        {
            var myAction = await DisplayActionSheet(null, "Cancel", null, "Take a photo", "Choose existing");
            if (myAction != null)
            {
                if (myAction.ToString() != "Cancel")
                {
                    await CrossMedia.Current.Initialize();
                    if (myAction.ToString() == "Take a photo")
                    {
                        file = await cameraHelper.TakePhotoAsync(new StoreCameraMediaOptions()
                        {
                            Directory = "Idle",
                            Name = $"{DateTime.UtcNow}.jpg",
                            CompressionQuality = 92,
                            MaxWidthHeight = 320.scale(),
                            DefaultCamera = CameraDevice.Front,
                            PhotoSize = PhotoSize.Custom,
                            AllowCropping = true
                        });
                    }
                    else if (myAction.ToString() == "Choose existing")
                    {
                        file = await cameraHelper.PickPhotoAsync(new PickMediaOptions()
                        {
                            MaxWidthHeight = 320.scale(),
                            CompressionQuality = 92,
                            PhotoSize = PhotoSize.Custom,
                            RotateImage = true
                        });
                    }

                    if (file != null)
                    {
                        var source = ImageSource.FromStream(() => { var stream = file.GetStream(); return stream; });
                        //send image
                        //TODO

                        /*  Get the public album path
                         *  var aPpath = file.AlbumPath;
                         *  Get private path
                         *  var path = file.Path;
                         */
#if DEBUG
                        StreamImageSource stem;
                        var newMessage = new Message()
                        {
                            id = messages[messages.Count - 1].id,
                            user = new User() { id = user.id, image = DataClass.GetInstance.UserImage, is_owner = 1 },
                            body = "",
                            image = file.Path,
                            timestamp = DateTime.Now.ToLocalTime().Hour + ":" + DateTime.Now.ToLocalTime().Minute + "PM"
                        };
                        messages.Add(newMessage);

                        messageEditor.Unfocus();
#endif
                    }
                }
            }
        }

        void OnImage_Tapped(object sender, System.EventArgs e)
        {
            var imageSource = sender as Image;
            BindingContext = new { image = imageSource.Source, visibility = true };
        }

        void OnImageVisibility_Tapped(object sender, System.EventArgs e)
        {
            BindingContext = new { image = "", visibility = false };
        }

        void Message_ItemTapped(object sender, Xamarin.Forms.ItemTappedEventArgs e)
        {
            messageList.SelectedItem = null;
            messageEditor.Unfocus();
        }


        async void OnMessage_Refreshing(object sender, System.EventArgs e)
        {
            serviceType = 0;
            if (messageList.IsRefreshing)
            {
#if DEBUG
                GetData();
                messageList.IsRefreshing = false;
#else
                if (pagination != null)
                {
                    if (int.Parse(pagination["load_more"].ToString()) == 1)
                    {
						cts = new CancellationTokenSource();
						try
						{
                            await webService.GetRequest(Constants.ROOT + pagination["url"],cts.Token);
						}
						catch (OperationCanceledException)
						{
							DependencyService.Get<iConsole>().DisplayText("Web Service: Cancelled!");
						}
						catch (Exception)
						{
							DependencyService.Get<iConsole>().DisplayText("Web Service: Failed!");
						}

						cts = null;
                    }
                    else
                    {
                        messageList.IsRefreshing = false;
                    }
                }
                else
                {
                    messageList.IsRefreshing = false;
                }
#endif
            }
        }

		public void ReceiveJSONData(JObject jsonData)
		{

		}

        public async void ReceiveJSONData(JObject jsonData, CancellationToken ct)
        {
            if (int.Parse(jsonData["status"].ToString()) == 200)
            {
                switch (serviceType)
                {
                    case 0: //fetch messages
                        var oldMessages = JsonConvert.DeserializeObject<ObservableCollection<Message>>(jsonData["messages"].ToString());

                        if (messages != null)
                            foreach (var message in messages)
                            {
                                oldMessages.Add(message);
                            }
                        messages = oldMessages;
#if DEBUG
                        foreach(var message in messages)
                        {
                            if(message.user.is_owner == 1)
                            {
                                message.user.image = dataClass.UserImage;
                            }
                            else
                            {
                                message.user.image = user.image;
                            }
                        }
#endif
                        messageList.ItemsSource = messages;
                        if (isLoaded == false)
                        {
                            isLoaded = true;
                            messageList.ScrollTo(messages[messages.Count - 1], ScrollToPosition.End, false);
                        }

                        pagination = jsonData["pagination"];
                        break;
                    case 1://send or reply message

                        var newMessage = new Message()
                        {
                            id = isContacted == true ? int.Parse(jsonData["id"].ToString()) : int.Parse(jsonData["conversation_id"].ToString()),
                            user = new User() { id = user.id, image = DataClass.GetInstance.UserImage, is_owner = 1 },
                            body = messageEditor.Text.Trim(),
                            timestamp = DateTime.Now.ToLocalTime().Hour + ":" + DateTime.Now.ToLocalTime().Minute + "PM"
                        };
                        messages.Add(newMessage);

                        if (pageType == 0)
                        {
                            int convoIndex = dataClass.MyConversations.ToList().FindIndex((obj) => obj.id == conversationId);
                            dataClass.MyConversations[convoIndex].message = newMessage.body;
                            dataClass.MyConversations.Move(convoIndex, 0);

                            await Task.Delay(250);
                            messageList.ScrollTo(newMessage, ScrollToPosition.End, false);
                        }

                        if (Device.RuntimePlatform == Device.iOS)
                        {
                            messageEditor.Text = messageEditor.Placeholder;
                            messageEditor.TextColor = messageEditor.PlaceholderColor;
                        }
                        else
                        {
                            messageEditor.Text = "";
                        }

                        if (isContacted == false)
                        {

                            isContacted = true;
                            conversationId = int.Parse(jsonData["conversation_id"].ToString());
                            this.user.conversation_id = conversationId;
                            if (pageType != 0)
                            {
                                TaskList_Update(dataClass.InProgressPosterList);
                                TaskList_Update(dataClass.InProgressPerformList);
                                TaskList_Update(dataClass.TaskList);
                                TaskList_Update(dataClass.TaskOffersList);
                            }
                        }
                        messageEditor.Unfocus();
                        break;
                }

            }

            loadingIndicator.IsRunning = false;
            messageList.IsRefreshing = false;
            isSending = false;
        }

        void TaskList_Update(ObservableCollection<Tasks> tasks)
        {
            if (tasks != null)
            {
                foreach (var task in tasks)
                {
                    if (task.user.id == this.user.id)
                    {
                        task.user.conversation_id = conversationId;
                    }
                }
            }
        }

        public async void ReceiveTimeoutError(string title, string error)
        {
            loadingIndicator.IsRunning = false;
            messageList.IsRefreshing = false;
            isSending = false;
            await DisplayAlert(title, error, "Okay");

        }
    }
}
