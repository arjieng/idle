﻿using System;
using System.Collections.Generic;
using Plugin.Messaging;
using Plugin.Share;
using Plugin.Share.Abstractions;
using Xamarin.Forms;

namespace idle.Views
{
    public partial class ShareIdlePage : RootViewPage
    {
        public ShareIdlePage()
        {
            InitializeComponent();

			this.PageTitle = "SHARE";
			this.TitleFontFamily = Constants.LULO;
			this.TitleFontColor = Constants.DARK_STRONG;
			this.LeftIcon = "BurgerDark";
			this.LeftButtonCommand = new Command((obj) => { MyMasterDetailPage detail = (MyMasterDetailPage)this.Parent.Parent; detail.IsPresented = true; DependencyService.Get<DependencyServices.iStatusStyle>().StatusStyle(0); });

			var settingsItem = new List<ShareIdleItem>();
			// add more item here
			settingsItem.Add(new ShareIdleItem { title = "Message", type = 0, icon = "ShareMessage" });
			settingsItem.Add(new ShareIdleItem { title = "Email", type = 1, icon = "ShareEmail" });
			settingsItem.Add(new ShareIdleItem { title = "Social Media", type = 2, icon = "ShareIdle" });

			shareIdle.ItemsSource = settingsItem;
        }

        protected override void OnAppearing()
        {
            base.OnAppearing();
			MyMasterDetailPage.isHomePage = true;
			DependencyService.Get<DependencyServices.iStatusStyle>().StatusStyle(1);
        }

		protected override void OnDisappearing()
		{
			base.OnDisappearing();
			MyMasterDetailPage.isHomePage = false;
		}

		void OnShare_ItemTapped(object sender, ItemTappedEventArgs e)
		{
			shareIdle.SelectedItem = null;
			var item = (ShareIdleItem)e.Item;
			switch(item.type)
            {
                case 0:
					var smsMessenger = CrossMessaging.Current.SmsMessenger;
					if (smsMessenger.CanSendSms)
					{
						smsMessenger.SendSms("", Constants.SHARE_IDLE_LINKS);
					}
                    break;
                case 1:
					var emailTask = CrossMessaging.Current.EmailMessenger;
					if (emailTask.CanSendEmail)
					{
						emailTask.SendEmail("", Constants.SHARE_IDLE_TITLE, Constants.SHARE_IDLE_LINKS);
					}
                    break;
                case 2:
					if (!CrossShare.IsSupported)
						return;
					CrossShare.Current.Share(new ShareMessage() { Title = Constants.SHARE_IDLE_TITLE, Text = Constants.SHARE_IDLE_BODY, Url = Constants.URL });
                    break;
            }
		}
    }
}
