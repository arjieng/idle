﻿﻿using System;
using System.Threading.Tasks;
using Plugin.Connectivity;
using Xamarin.Forms;

namespace idle
{
    public class NetworkHelper : iNetwork
    {
        public static NetworkHelper network;
        public static NetworkHelper GetInstance
        {
            get
            {
                if (network == null)
                {
                    network = new NetworkHelper();
                }
                return network;
            }
        }

        public bool HasInternet()
        {
            if (!CrossConnectivity.IsSupported)
            {
                return true;
            }
            return CrossConnectivity.Current.IsConnected;
        }

        public async Task<bool> IsHostReachable()
        {
            if (!CrossConnectivity.Current.IsConnected)
            {
                return false;
            }
#if DEBUG
            return true;
#else
            //var reachable = await CrossConnectivity.Current.IsRemoteReachable(Constants.URL, int.Parse(Constants.PORT), 5000);
            //return reachable;
            return true;
#endif
		}
	}
}
