﻿using System;
using System.Threading.Tasks;

namespace idle
{
    public interface iNetwork
    {
        bool HasInternet();
        Task<bool> IsHostReachable();
    }
}
