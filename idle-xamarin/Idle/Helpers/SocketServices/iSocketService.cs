﻿using System;
using System.Threading.Tasks;

namespace idle
{
    public interface iSocketService
    {
        Task ConnectServer();
        Task DisconnectServer();
        Task ListenSocket();
    }
}
