﻿using System;
using System.Threading;
using System.Threading.Tasks;

namespace idle
{
    public interface iFileReader
    {
        Task WriteFile(string fileName, string json, bool isEmbed, CancellationToken ct);
        Task ReadFile(string fileName, bool isEmbed, CancellationToken ct);
    }
}
