﻿using System;
using System.Diagnostics;
using System.IO;
using System.Reflection;
using System.Threading;
using System.Threading.Tasks;
using Newtonsoft.Json.Linq;
using PCLStorage;

namespace idle
{
    public class FileReader : iFileReader
    {
		WeakReference<iFileConnector> _fileReaderDelegate;
		public iFileConnector FileReaderDelegate
		{
			get
			{
				iFileConnector fileReaderDelegate;
				return _fileReaderDelegate.TryGetTarget(out fileReaderDelegate) ? fileReaderDelegate : null;
			}

			set
			{
				_fileReaderDelegate = new WeakReference<iFileConnector>(value);
			}
		}

		public async Task WriteFile(string fileName, string json, bool isEmbed, CancellationToken ct)
		{
			IFolder rootFolder = FileSystem.Current.LocalStorage;
			IFolder folder = await rootFolder.CreateFolderAsync("IdleFolder", CreationCollisionOption.OpenIfExists);
			IFile file = await folder.CreateFileAsync(fileName, CreationCollisionOption.ReplaceExisting);

			await file.WriteAllTextAsync(json);

            FileReaderDelegate?.ReceiveJSONData(JObject.Parse(json), ct);
		}

		public async Task ReadFile(string fileName, bool isEmbed, CancellationToken ct)
		{
			if (isEmbed)
			{
				var assembly = typeof(FileReader).GetTypeInfo().Assembly;

				Stream stream = assembly.GetManifestResourceStream("idle.Files." + fileName);

				using (var reader = new System.IO.StreamReader(stream))
				{
					var json = reader.ReadToEnd();

					FileReaderDelegate?.ReceiveJSONData(JObject.Parse(json), ct);
				}
			}
			else
			{
				IFolder rootFolder = FileSystem.Current.LocalStorage;
				IFolder folder = await rootFolder.CreateFolderAsync("IdleFolder", CreationCollisionOption.OpenIfExists);
				IFile file = await folder.GetFileAsync(fileName);

                FileReaderDelegate?.ReceiveJSONData(JObject.Parse(file.ReadAllTextAsync().Result), ct);
			}
		}
    }
}
