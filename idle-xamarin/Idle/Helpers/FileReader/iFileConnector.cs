﻿using System;
using System.Threading;
using Newtonsoft.Json.Linq;

namespace idle
{
    public interface iFileConnector
    {
		void ReceiveJSONData(JObject jsonData, CancellationToken ct);
	}
}
