﻿using System;
using System.Threading;
using Newtonsoft.Json.Linq;

namespace idle
{
    public interface iRestConnector
    {
		void ReceiveJSONData(JObject jsonData);
        void ReceiveJSONData(JObject jsonData, CancellationToken ct);
		void ReceiveTimeoutError(string title, string error);
    }
}
