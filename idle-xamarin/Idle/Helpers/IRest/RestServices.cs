﻿using System;
using System.Net.Http;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Newtonsoft.Json.Linq;
using Xamarin.Forms;

namespace idle
{
    public class RestServices : iRestService
    {
		NetworkHelper networkHelper = NetworkHelper.GetInstance;
        HttpClient client;

        static RestServices _restService;
		WeakReference<iRestConnector> _webServiceDelegate;
		public iRestConnector WebServiceDelegate
		{
			get
			{
				iRestConnector webServiceDelegate;
				return _webServiceDelegate.TryGetTarget(out webServiceDelegate) ? webServiceDelegate : null;
			}

			set
			{
				_webServiceDelegate = new WeakReference<iRestConnector>(value);
			}
		}

		public static RestServices GetInstance
		{
			get { if (_restService == null) _restService = new RestServices(); return _restService; }
		}

        public RestServices()
        {
            client = new HttpClient();
            client.DefaultRequestHeaders.Accept.Clear();
            client.MaxResponseContentBufferSize = 256000;
        }

        public async Task GetRequest(string url, CancellationToken ct)
        {
			if (networkHelper.HasInternet())
            {
                if (await networkHelper.IsHostReachable() == true)
                {
					try
					{
                        var uri = new Uri(url);
						HttpResponseMessage response = await client.GetAsync(uri, ct);
						await RequestAsync(response, ct);
					}
					catch (HttpRequestException ex)
					{
						DependencyService.Get<iConsole>().DisplayText("Get Request Error" + ex);
					}
                }
                else
                {
                    WebServiceDelegate?.ReceiveTimeoutError("Host Unreachable!", "The URL host for Idle cannot be reached and seems to be unavailable. Please try again later!");
                }
            }
            else
            {
				WebServiceDelegate?.ReceiveTimeoutError("No Internet Connection!", "Please check your internet connection, and try again!");
            }
        }

        public async Task PostRequestAsync(string url, string dictionary, CancellationToken ct)
        {
			if (networkHelper.HasInternet())
			{
				if (await networkHelper.IsHostReachable() == true)
				{
					try
					{
						var uri = new Uri(url);
						var content = new StringContent(dictionary, Encoding.UTF8, "application/json");
						HttpResponseMessage response = await client.PostAsync(uri, content, ct);
						await RequestAsync(response, ct);
					}
					catch (HttpRequestException ex)
					{
						DependencyService.Get<iConsole>().DisplayText("Post Request Error" + ex);
					}
				}
				else
				{
					WebServiceDelegate?.ReceiveTimeoutError("Host Unreachable!", "The URL host for Idle cannot be reached and seems to be unavailable. Please try again later!");
				}
			}
			else
			{
				WebServiceDelegate?.ReceiveTimeoutError("No Internet Connection!", "Please check your internet connection, and try again!");
			}

        }

        async Task RequestAsync(HttpResponseMessage response, CancellationToken ct)
        {
            ct.ThrowIfCancellationRequested();
			if (response.IsSuccessStatusCode)
			{
                ct.ThrowIfCancellationRequested();
                var result = await response.Content.ReadAsStringAsync().ConfigureAwait(false);
                ct.ThrowIfCancellationRequested();
				WebServiceDelegate?.ReceiveJSONData(JObject.Parse(result), ct);
			}
			else
			{
				//300, 404, 500
				WebServiceDelegate?.ReceiveTimeoutError("Error!", response.StatusCode.ToString());
			}
        }

        public async Task PutRequestAsync(string url, string dictionary, CancellationToken ct)
        {
			if (networkHelper.HasInternet())
			{
				if (await networkHelper.IsHostReachable() == true)
				{
					try
					{
						var uri = new Uri(url);
						var content = new StringContent(dictionary, Encoding.UTF8, "application/json");
						HttpResponseMessage response = await client.PutAsync(uri, content, ct);
						await RequestAsync(response, ct);
					}
					catch (HttpRequestException ex)
					{
						DependencyService.Get<iConsole>().DisplayText("Put Request Error" + ex);
					}
				}
				else
				{
					WebServiceDelegate?.ReceiveTimeoutError("Host Unreachable!", "The URL host for Idle cannot be reached and seems to be unavailable. Please try again later!");
				}
			}
			else
			{
				WebServiceDelegate?.ReceiveTimeoutError("No Internet Connection!", "Please check your internet connection, and try again!");
			}
			
        }

        public async Task DeleteRequestAsync(string url, CancellationToken ct)
        {
			if (networkHelper.HasInternet())
			{
				if (await networkHelper.IsHostReachable() == true)
				{
					try
					{
						var uri = new Uri(url);
						//var content = new StringContent(dictionary, Encoding.UTF8, "application/json");
						HttpResponseMessage response = await client.DeleteAsync(uri, ct);
						await RequestAsync(response, ct);
					}
					catch (HttpRequestException ex)
					{
						DependencyService.Get<iConsole>().DisplayText("Delete Request Error" + ex);
					}
				}
				else
				{
					WebServiceDelegate?.ReceiveTimeoutError("Host Unreachable!", "The URL host for Idle cannot be reached and seems to be unavailable. Please try again later!");
				}
            }
			else
			{
                WebServiceDelegate?.ReceiveTimeoutError("No Internet Connection!", "Please check your internet connection, and try again!");
			}
			
        }

        public async Task MultiPartDataContentAsync(string url, string key, string dictionary, System.IO.Stream image = null)
        {
			if (networkHelper.HasInternet())
			{
				if (await networkHelper.IsHostReachable() == true)
				{
					var uri = new Uri(url);
					var json = JObject.Parse(dictionary)[key];
					var tokenJson = JObject.Parse(dictionary)["token"];
					var multipartFormData = new MultipartFormDataContent();

					foreach (var obj in json)
					{
						string[] keys = obj.Path.Split('.');

						if (keys[1].ToString() == "image" && image != null)
						{
							string name = "\"" + keys[0] + "[" + keys[1] + "]" + "\"";
							//StreamContent content = new StreamContent(file.GetStream());
							StreamContent content = new StreamContent(image);
							content.Headers.ContentDisposition = new System.Net.Http.Headers.ContentDispositionHeaderValue("form-data") { FileName = "\"upload-image.jpeg\"", Name = name };
							multipartFormData.Add(content);
						}
						else
						{
							if (!string.IsNullOrEmpty(obj.First.ToString()))
							{
								string keyName = "\"" + keys[0] + "[" + keys[1] + "]" + "\"";
								StringContent content = new StringContent(obj.First.ToString(), System.Text.Encoding.UTF8);
								content.Headers.ContentDisposition = new System.Net.Http.Headers.ContentDispositionHeaderValue("form-data") { Name = keyName };
								multipartFormData.Add(content);
							}
						}
					}

					string tokenName = "\"token\"";
					StringContent contentTokens = new StringContent(tokenJson.ToString(), System.Text.Encoding.UTF8);
					contentTokens.Headers.ContentDisposition = new System.Net.Http.Headers.ContentDispositionHeaderValue("form-data") { Name = tokenName };
					multipartFormData.Add(contentTokens);

					try
					{
						HttpResponseMessage response = await client.PutAsync(uri, multipartFormData);
						//await RequestAsync(response);
						if (response.IsSuccessStatusCode)
						{
							var result = await response.Content.ReadAsStringAsync().ConfigureAwait(false);
							WebServiceDelegate?.ReceiveJSONData(JObject.Parse(result));
						}
						else
						{
							//300, 404, 500
							WebServiceDelegate?.ReceiveTimeoutError("Error!", response.StatusCode.ToString());
						}
					}
					catch (HttpRequestException ex)
					{
						DependencyService.Get<iConsole>().DisplayText("Multi Part Data Error" + ex);
					}
				}
				else
				{
					WebServiceDelegate?.ReceiveTimeoutError("Host Unreachable!", "The URL host for Idle cannot be reached and seems to be unavailable. Please try again later!");
				}
			}
			else
			{
				WebServiceDelegate?.ReceiveTimeoutError("No Internet Connection!", "Please check your internet connection, and try again!");
			}

        }
    }
}
