﻿using System;
using System.Net.Http;
using System.Threading;
using System.Threading.Tasks;

namespace idle
{
    public interface iRestService
    {
        Task PostRequestAsync(string url, string dictionary, CancellationToken ct);
        Task GetRequest(string url, CancellationToken ct);
        Task PutRequestAsync(string url, string dictionary, CancellationToken ct);
        Task DeleteRequestAsync(string url, CancellationToken ct);
        Task MultiPartDataContentAsync(string url, string key, string dictionary, System.IO.Stream image);
    }
}
