﻿using System;
using System.Threading;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Xamarin.Forms;

namespace idle
{
    public class ChangeStatusHelper : iRestConnector
    {
        DataClass dataClass = DataClass.GetInstance;

		public ChangeStatusHelper()
        {
        }

		public async void ChangeUserStatus()
		{
#if DEBUG == false
			RestServices webService = new RestServices();
            CancellationTokenSource cts;
			var json = JsonConvert.SerializeObject(new { token = dataClass.UserToken });

			cts = new CancellationTokenSource();
			try
			{
				await webService.PutRequestAsync(Constants.ROOT_URL + Constants.ACCOUNTS_URL + Constants.STATUS_URL, json, cts.Token);
			}
			catch (OperationCanceledException)
			{
				DependencyService.Get<iConsole>().DisplayText("Web Service: Cancelled!");
			}
			catch (Exception)
			{
				DependencyService.Get<iConsole>().DisplayText("Web Service: Failed!");
			}

			cts = null;
#endif
		}

        public void ReceiveJSONData(JObject jsonData)
        {
            
        }

        public void ReceiveJSONData(JObject jsonData, CancellationToken ct)
        {
            if (int.Parse(jsonData["status"].ToString()) == 200)
            {
                Application.Current.Properties["user_is_online"] = 0;
                Application.Current.SavePropertiesAsync();
            }
			else
			{
				Application.Current.MainPage.DisplayAlert("Something went wrong", "", "Okay");
			}
        }

        public void ReceiveTimeoutError(string title, string error)
        {
            Application.Current.MainPage.DisplayAlert(title, error, "Okay");
        }
    }
}
