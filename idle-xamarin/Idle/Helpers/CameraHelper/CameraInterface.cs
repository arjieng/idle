﻿using System;
using System.Threading.Tasks;
using Plugin.Media.Abstractions;

namespace idle
{
    public interface CameraInterface
    {
        Task<MediaFile> PickPhotoAsync(PickMediaOptions options = null);
        Task<MediaFile> TakePhotoAsync(StoreCameraMediaOptions options);
        Task<MediaFile> PickVideoAsync();
        Task<MediaFile> TakeVideoAsync(StoreVideoOptions options);
    }
}
