﻿using System;
using System.Threading;
using Newtonsoft.Json.Linq;
using Xamarin.Forms;

namespace idle
{
    public class StripeHelper : iRestConnector
    {
#if DEBUG == false
		RestServices webServices = new RestServices();
        CancellationTokenSource cts;
#endif
		public StripeHelper()
        {
            
        }

        public async void GetStripeAPIKey()
        {
#if DEBUG == false
			webServices.WebServiceDelegate = this;
		
			cts = new CancellationTokenSource();
			try
			{
                await webServices.PostRequestAsync(Constants.ROOT_URL + Constants.STRIPE_URL + "/api", "", cts.Token);
			}
			catch (OperationCanceledException)
			{
				DependencyService.Get<iConsole>().DisplayText("Web Service: Cancelled!");
			}
			catch (Exception)
			{
				DependencyService.Get<iConsole>().DisplayText("Web Service: Failed!");
			}

			cts = null;
#endif
		}

		public void ReceiveJSONData(JObject jsonData)
		{

		}

		public void ReceiveJSONData(JObject jsonData, CancellationToken ct)
		{
			if (int.Parse(jsonData["status"].ToString()) == 200)
			{ 
				Application.Current.Properties["stripe_api_key"] = jsonData["stripe_api_key"].ToString();
				Application.Current.SavePropertiesAsync();
			}
			else
			{
			    Application.Current.MainPage.DisplayAlert("Something went wrong", "", "Okay");
			}
		}

		public void ReceiveTimeoutError(string title, string error)
		{
			Application.Current.MainPage.DisplayAlert(title, error, "Okay");
		}
    }
}
