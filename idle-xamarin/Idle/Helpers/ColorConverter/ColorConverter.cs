﻿﻿using System;
using System.Globalization;
using Xamarin.Forms;

namespace idle
{
    public class ColorConverter : IValueConverter
    {
        public ColorConverter()
        {
        }

        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            var task_type = parameter.ToString();
            //var bind = value as string ?? parameter as string;
            //var task_type = (string)bind.;
                
            switch(task_type)
            {
                case "0" : return Constants.QuickieColor; 
				case "1": return Constants.GroceryColor; 
				case "2": return Constants.LaundryColor; 
				case "3": return Constants.TransportColor; 
				case "4": return Constants.OtherColor;
                default : return Color.Green;
            }

            //return Color.Default;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
