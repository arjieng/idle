﻿using System;
using Xamarin.Forms;

namespace idle
{
    public static class ScaleCS
    {
		public static Thickness scale(this Thickness thickness)
		{
			return new Thickness(thickness.Left * App.ScreenScale, thickness.Top * App.ScreenScale, thickness.Right * App.ScreenScale, thickness.Bottom * App.ScreenScale);
		}

		public static Rectangle scale(this Rectangle rectangle)
		{
			return new Rectangle(rectangle.X * App.ScreenScale, rectangle.Y * App.ScreenScale, rectangle.Width * App.ScreenScale, rectangle.Height * App.ScreenScale);
		}

		public static double scale(this double number)
		{
			return number * App.ScreenScale;
		}

		public static float scale(this float number)
		{
			return (float)(number * App.ScreenScale);
		}

		public static int scale(this int number)
		{
			return (int)(number * App.ScreenScale);
		}
    }
}
