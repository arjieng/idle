﻿using System;
using System.Diagnostics;
using System.Globalization;
using Xamarin.Forms;

namespace idle
{
    public class NoSelected : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if(value is int)
            {
                var taskType = (int)value;
                if(taskType >= 0 && taskType <= 4)
                {
                    return true;
                }
            }
            return false;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
