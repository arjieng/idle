﻿using System;
using System.Globalization;
using Xamarin.Forms;

namespace idle
{
    public class NotNullConverter : IValueConverter
    {
        public NotNullConverter()
        {
        }

        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
			if ((int)value > 0) // length > 0 ?
				return true;            // some data has been entered
			else
				return false;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
