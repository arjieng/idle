﻿using System;
//using System.Diagnostics;
using System.Globalization;
using Xamarin.Forms;

namespace idle
{
    public class TaskConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            var stacks = (StackLayout)value;
            //Debug.WriteLine("Task Converter: {0}",stacks.Children!=null);
            var stackQuickie = (CircleImage)((StackLayout)stacks.Children[0]).Children[0];
			var stackLaundry = (CircleImage)((StackLayout)stacks.Children[1]).Children[0];
			var stackGrocery = (CircleImage)((StackLayout)stacks.Children[2]).Children[0];
			var stackTransport = (CircleImage)((StackLayout)stacks.Children[3]).Children[0];
			var stackOthers =(CircleImage) ((StackLayout)stacks.Children[4]).Children[0];

            //Debug.WriteLine("Task ImageName: {0}", ((FileImageSource)stackQuickie.Source).File);
            if (((FileImageSource)stackQuickie.Source).File.Equals("QuickieSelected"))
                 return true;
			else if (((FileImageSource)stackLaundry.Source).File.Equals("LaundrySelected"))
				return true;
			else if (((FileImageSource)stackGrocery.Source).File.Equals("GrocerySelected"))
				return true;
			else if (((FileImageSource)stackTransport.Source).File.Equals("TransportSelected"))
				return true;
			else if (((FileImageSource)stackOthers.Source).File.Equals("OthersSelected"))
				return true;

            return false;
            //throw new NotImplementedException();
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
