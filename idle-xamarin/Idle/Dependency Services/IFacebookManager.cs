﻿using System;
namespace idle
{
	public interface IFacebookManager
	{
		void Login(Action<FacebookUser, string> onLoginComplete);
		void Logout();
	}
}
