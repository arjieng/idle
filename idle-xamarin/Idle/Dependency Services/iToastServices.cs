﻿using System;
using Xamarin.Forms;

namespace idle
{
	public interface iToastServices
	{
		void DisplayToast(string text);
	}
}
