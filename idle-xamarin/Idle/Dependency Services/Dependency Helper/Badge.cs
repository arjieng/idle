﻿using System;
using System.Threading;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Xamarin.Forms;

namespace idle
{
    public class Badge : iRestConnector
    {
#if DEBUG == false
        static RestServices webServices = new RestServices();
        static iRestConnector connect = new Badge();
        static int _count;
#endif
        public static async void SetBadge(int count)
        {
#if DEBUG == false
            webServices.WebServiceDelegate = connect;
            _count = count;

            var url = Constants.ROOT_URL + Constants.ACCOUNTS_URL + "/read";
            var json = JsonConvert.SerializeObject(new { token = DataClass.GetInstance.UserToken });

            CancellationTokenSource cts;
			cts = new CancellationTokenSource();
			try
			{
                await webServices.PutRequestAsync(url, json, cts.Token);
			}
			catch (OperationCanceledException)
			{
				DependencyService.Get<iConsole>().DisplayText("Web Service: Cancelled!");
			}
			catch (Exception)
			{
				DependencyService.Get<iConsole>().DisplayText("Web Service: Failed!");
			}

			cts = null;

#endif
		}

        public void ReceiveJSONData(JObject jsonData, CancellationToken ct)
        {
            if(int.Parse(jsonData["status"].ToString()) == 200)
            {
                if (Device.RuntimePlatform == Device.iOS)
                {
#if DEBUG == false
                    DependencyService.Get<iBadge>().SetBadge(_count);
#endif
				}
            }
        }

        public void ReceiveJSONData(JObject jsonData)
        {
            
        }

        public void ReceiveTimeoutError(string title, string error)
        {
            iRestConnector rest = new Views.SignInPage();
            rest.ReceiveTimeoutError(title,error);
        }
    }
}
