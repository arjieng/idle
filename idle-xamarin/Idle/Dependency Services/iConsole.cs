﻿using System;
using Newtonsoft.Json.Linq;

namespace idle
{
	public interface iConsole
	{
		void DisplayText(string text);
		void DisplayText(JObject text);
	}
}