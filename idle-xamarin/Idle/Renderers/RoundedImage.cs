﻿using System;
using Xamarin.Forms;

namespace idle
{
    public class RoundedImage : Image
    {
        public float BorderRadius { get; set; }
        public float BorderThickness { get; set; }
    }
}
