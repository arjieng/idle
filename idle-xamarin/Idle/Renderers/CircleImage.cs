﻿using System;
using Xamarin.Forms;

namespace idle
{

    public enum ImageTappedEventArgs 
    {
        Down,
        Up
    }
    public class CircleImage : Image
    {
        public CircleImage()
        {
            this.WidthRequest = 35;
            this.HeightRequest = 35;
            this.BackgroundColor = Color.Transparent;
           
            var tapGestureRecognizer = new TapGestureRecognizer();
            tapGestureRecognizer.Tapped += OnTapped;
            var panGestureRecognizer = new PanGestureRecognizer();
            panGestureRecognizer.PanUpdated += OnPan;
            //this.GestureRecognizers.Add(panGestureRecognizer);
            this.GestureRecognizers.Add(tapGestureRecognizer);
        }

        private  void OnPan(object sender, PanUpdatedEventArgs e)
        {
            var parent = ((View)((CircleImage)sender).Parent);
            if (((CircleImage)sender).IsAnimated)
            {
                parent.Opacity = .4;
                if (e.StatusType == GestureStatus.Running )
                {
                    parent.Opacity = .4;
                }
                if(e.StatusType == GestureStatus.Started)
                {
					parent.Opacity = .4;
                }
            }
			if (e.StatusType == GestureStatus.Completed)
			{
				parent.FadeTo(1, 500, Easing.CubicInOut);
				//if (ImageTapped != null)
				//{
				//	this.ImageTapped.Invoke(this, e);
				//}
			}
        }

        private void OnTapped(object sender, EventArgs e)
        {
			if (((CircleImage)sender).IsAnimated)
			{
				var parent = ((View)((CircleImage)sender).Parent);
				parent.Opacity = .4;
				parent.FadeTo(1, 250, Easing.CubicInOut);
			}
            if (ImageTapped != null)
            {
                this.ImageTapped.Invoke(this, ImageTappedEventArgs.Down);
            }

           
        }

        //public static readonly BindableProperty ImageTappedProperty =
            //BindableProperty.Create("ImageTapped", typeof(EventHandler<EventArgs>), typeof(CircleImage), null);

        public event EventHandler<ImageTappedEventArgs> ImageTapped;
		//{
  //          add
		//	get { return (EventHandler<EventArgs>)GetValue(ImageTappedProperty); }
		//	set { SetValue(ImageTappedProperty, value); }
		//}

		public static readonly BindableProperty IsAnimatedProperty =
            BindableProperty.Create("IsAnimated", typeof(bool), typeof(CircleImage), false);

		public bool IsAnimated
		{
			get { return (bool)GetValue(IsAnimatedProperty); }
			set { SetValue(IsAnimatedProperty, value); }
		}

		public static readonly BindableProperty BorderColorProperty =
			BindableProperty.Create("BorderColor", typeof(Color), typeof(CircleImage), Color.Default);

		public Color BorderColor
		{
			get { return (Color)GetValue(BorderColorProperty); }
			set { SetValue(BorderColorProperty, value); }
		}
		public static readonly BindableProperty BorderThicknessProperty =
			BindableProperty.Create("BorderThickness", typeof(double), typeof(CircleImage), 0d);

		public double BorderThickness
		{
			get { return (double)GetValue(BorderThicknessProperty); }
			set { SetValue(BorderThicknessProperty, value); }
		}

		public static readonly BindableProperty FillColorProperty =
			BindableProperty.Create("FillColor", typeof(Color), typeof(CircleImage), Color.Transparent);

		public Color FillColor
		{
			get { return (Color)GetValue(FillColorProperty); }
			set { SetValue(FillColorProperty, value); }
		}
    }
}
