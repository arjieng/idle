﻿using System;
using Xamarin.Forms;

namespace idle
{
    public class CarouselIndicator : Button
    {
        public CarouselIndicator()
        {
            this.InputTransparent = true;
        }
    }
}
