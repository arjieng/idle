﻿using System;
using idle;
using idle.Droid.Renderers;
using Xamarin.Forms;
using Xamarin.Forms.Platform.Android;

[assembly: ExportRenderer(typeof(LogoutButton), typeof(LogoutButtonRenderer))]
namespace idle.Droid.Renderers
{
    public class LogoutButtonRenderer : ButtonRenderer
    {
		protected override void OnElementChanged(ElementChangedEventArgs<Button> e)
		{
			base.OnElementChanged(e);
			Control.SetBackgroundColor(((Button)e.NewElement).BackgroundColor.ToAndroid());
			//Control.SetPaddingRelative(-100, -100, -100, -100);
			Control.SetShadowLayer(0, 0, 0, Color.Transparent.ToAndroid());
			Control.SetPadding(0, 0, 0, 0);
			Control.SetAllCaps(false);

		}

		protected override void OnDraw(Android.Graphics.Canvas canvas)
		{
			base.OnDraw(canvas);
		}
    }
}
