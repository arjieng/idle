﻿using System;
using System.ComponentModel;
using idle.Droid.Renderers;
using Xamarin.Forms;
using Xamarin.Forms.Platform.Android;

[assembly: ExportRenderer(typeof(DatePicker), typeof(CustomDatePickerRenderer))]
namespace idle.Droid.Renderers
{
    public class CustomDatePickerRenderer : DatePickerRenderer
    {

	    protected override void OnElementChanged(ElementChangedEventArgs<DatePicker> e)
        {
            base.OnElementChanged(e);
            Control.TextAlignment = Android.Views.TextAlignment.TextStart;
            this.Control.SetBackgroundColor(Android.Graphics.Color.Transparent);

			//Control.Typeface = Android.Graphics.Typeface.CreateFromAsset(Forms.Context.Assets, Constants.PROXIMA_REG);

			Control.TextSize = (float)Constants.ENTRY_FONTSIZE;
        }

    }
}
