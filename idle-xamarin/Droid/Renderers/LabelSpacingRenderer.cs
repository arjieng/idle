﻿using System;
using idle;
using idle.Droid;
using Xamarin.Forms;
using Xamarin.Forms.Platform.Android;

[assembly: ExportRenderer(typeof(LabelSpacing), typeof(LabelSpacingRenderer))]
namespace idle.Droid
{
    public class LabelSpacingRenderer : LabelRenderer
    {
		protected LabelSpacing LineSpacingLabel { get; private set; }

		protected override void OnElementChanged(ElementChangedEventArgs<Label> e)
		{
			base.OnElementChanged(e);

			if (e.OldElement == null)
			{
				this.LineSpacingLabel = (LabelSpacing)this.Element;
			}

			this.Control.SetLineSpacing(0, (float)this.LineSpacingLabel.LineSpacing);

			this.UpdateLayout();
		}
    }
}
