﻿using System;
using Android.Views;
using idle.Droid;
using Xamarin.Forms;
using Xamarin.Forms.Platform.Android.AppCompat;

[assembly: ExportRenderer(typeof(MasterDetailPage), typeof(IdleMasterDetailPageRenderer))]
namespace idle.Droid
{
    public class IdleMasterDetailPageRenderer : MasterDetailPageRenderer
    {
		bool firstDone;
        public override void AddView(Android.Views.View child)
        {
			if (firstDone)
			{
				LayoutParams p = (LayoutParams)child.LayoutParameters;
				p.Width = Resources.DisplayMetrics.WidthPixels;
				base.AddView(child, p);
			}
			else
			{
				firstDone = true;
				base.AddView(child);
			}
        }

        public override bool OnTouchEvent(Android.Views.MotionEvent e)
        {
			if (IsDrawerOpen(Android.Support.V4.View.GravityCompat.Start))
				return base.OnTouchEvent(e);
			else
			{
				if (e.Action == MotionEventActions.Up || e.Action == MotionEventActions.Down)
					return base.OnTouchEvent(e);
				else
				{
					CloseDrawers();
					return true;
				}
			}
        }
	}
}
