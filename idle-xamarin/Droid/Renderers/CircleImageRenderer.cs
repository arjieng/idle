﻿using System;
using System.IO;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Android.Graphics;
using Android.Views;
using idle;
using idle.Droid.Renderers;
using Java.Nio;
using Xamarin.Forms;
using Xamarin.Forms.Platform.Android;
using Xamarin.Forms.PlatformConfiguration;

[assembly: ExportRenderer(typeof(CircleImage), typeof(CircleImageRenderer))]
namespace idle.Droid.Renderers
{
    public class CircleImageRenderer : ImageRenderer
    {
		protected override void OnElementChanged(ElementChangedEventArgs<Image> e)
		{
			base.OnElementChanged(e);

			if (e.OldElement == null)
			{
				//Only enable hardware accelleration on lollipop
				if ((int)Android.OS.Build.VERSION.SdkInt < 21)
				{
					SetLayerType(LayerType.Software, null);
				}
            }
		}

        protected override async void OnElementPropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
        {
            base.OnElementPropertyChanged(sender, e);
			if (e.PropertyName == CircleImage.BorderColorProperty.PropertyName ||
			  e.PropertyName == CircleImage.BorderThicknessProperty.PropertyName ||
			  e.PropertyName == CircleImage.FillColorProperty.PropertyName)
			{
				this.Invalidate();
			}
            //if(e.PropertyName == CircleImage.SourceProperty.PropertyName)
            //{
            //    if ((Element.Source is UriImageSource))
            //    {
            //        //var uriImage = ((UriImageSource)Element.Source).Uri.AbsoluteUri;
            //        //var imageBitmap = await GetBitmap(Element);
            //        //var bitmap = ImageRotationExtension.ToRotatedBitmap(imageBitmap, uriImage.GetExifRotationDegrees());
            //        //var imageSource = ImageSource.FromStream(() => new MemoryStream((ToByte(bitmap))));
            //    }
            //    //Element.Source = imageSource ;
            //}
        }

		private static async Task<Stream> GetStreamFromImageSourceAsync(StreamImageSource imageSource, CancellationToken cancellationToken = default(CancellationToken))
		{
			if (imageSource.Stream != null)
			{
				return await imageSource.Stream(cancellationToken);
			}
			return null;
		}

		Task<Bitmap> GetBitmap(Xamarin.Forms.Image image)
		{
			var handler = new ImageLoaderSourceHandler();
			return handler.LoadImageAsync(image.Source, Forms.Context);

		}

        byte[] ToByte(Bitmap bitmap)
        {
			ByteBuffer byteBuffer = ByteBuffer.Allocate(bitmap.ByteCount);
			bitmap.CopyPixelsToBuffer(byteBuffer);
			byte[] bytes = byteBuffer.ToArray<byte>();
			return bytes;
        }
        protected override bool DrawChild(Canvas canvas, global::Android.Views.View child, long drawingTime)
		{
			try
			{

				var radius = Math.Min(Width, Height) / 2;

				var borderThickness = (float)((CircleImage)Element).BorderThickness;

				int strokeWidth = 0;

				if (borderThickness > 0)
				{
					var logicalDensity = Xamarin.Forms.Forms.Context.Resources.DisplayMetrics.Density;
					strokeWidth = (int)Math.Ceiling(borderThickness * logicalDensity + .5f);
				}

				radius -= strokeWidth / 2;




				var path = new Android.Graphics.Path();
				path.AddCircle(Width / 2.0f, Height / 2.0f, radius, Android.Graphics.Path.Direction.Ccw);


				canvas.Save();
				canvas.ClipPath(path);



				var paint = new Paint();
				paint.AntiAlias = true;
				paint.SetStyle(Paint.Style.Fill);
				paint.Color = ((CircleImage)Element).FillColor.ToAndroid();
				canvas.DrawPath(path, paint);
				paint.Dispose();


				var result = base.DrawChild(canvas, child, drawingTime);

				path.Dispose();
				canvas.Restore();

				path = new Android.Graphics.Path();
				path.AddCircle(Width / 2, Height / 2, radius, Android.Graphics.Path.Direction.Ccw);


				if (strokeWidth > 0.0f)
				{
					paint = new Paint();
					paint.AntiAlias = true;
					paint.StrokeWidth = strokeWidth;
					paint.SetStyle(Paint.Style.Stroke);
					paint.Color = ((CircleImage)Element).BorderColor.ToAndroid();
					canvas.DrawPath(path, paint);
					paint.Dispose();
				}

				path.Dispose();
				return result;
			}
			catch (Exception ex)
			{
				System.Diagnostics.Debug.WriteLine("Unable to create circle image: " + ex);
			}

			return base.DrawChild(canvas, child, drawingTime);
		}
    }
}
