﻿
using System;
using idle;
using idle.Droid.Renderers;
using Xamarin.Forms;
using Xamarin.Forms.Platform.Android;

[assembly: ExportRenderer(typeof(Button), typeof(CustomButtonRenderer))]
namespace idle.Droid.Renderers
{
    public class CustomButtonRenderer : ButtonRenderer
    {
		 protected override void OnElementChanged(ElementChangedEventArgs<Button> e)
        {
            base.OnElementChanged(e);
            Control.SetPadding(0,-10,0,-5);
            //Control.SetBackgroundColor(((Button)e.NewElement).BackgroundColor.ToAndroid());
            //Control.SetBackgroundColor(Color.Transparent.ToAndroid());
			//Control.SetPaddingRelative(-100, -100, -100, -100);
			Control.SetShadowLayer(0, 0, 0, Color.Transparent.ToAndroid());
			Control.SetPadding(0, 0, 0, 0);
            Control.SetAllCaps(false);

        }

    }
}
