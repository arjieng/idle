﻿using System;
using idle;
using idle.Droid;
using Xamarin.Forms;
using Xamarin.Forms.Platform.Android;

[assembly: ExportRenderer(typeof(CustomStackLayout), typeof(CustomStackLayoutRenderer))]
namespace idle.Droid
{
	public class CustomStackLayoutRenderer : ViewRenderer
	{
		protected override void OnElementChanged(ElementChangedEventArgs<View> e)
		{
			base.OnElementChanged(e);
			this.SetBackgroundResource(Resource.Drawable.RoundCorner);
		}
	}
}
