﻿
using System;
using idle;
using idle.Droid.Renderers;
using Xamarin.Forms;
using Xamarin.Forms.Platform.Android;

[assembly: ExportRenderer(typeof(CarouselIndicator), typeof(CarouselIndicatorRenderer))]
namespace idle.Droid.Renderers
{
    public class CarouselIndicatorRenderer : ButtonRenderer
    {
		protected override void OnDraw(Android.Graphics.Canvas canvas)
		{
			base.OnDraw(canvas);
            Element.InputTransparent = true;
		}
       
    }
}
