﻿using System;
using idle.Droid.Renderers;
using Xamarin.Forms;
using Xamarin.Forms.Platform.Android;

[assembly: ExportRenderer(typeof(ListView), typeof(CustomListViewNoScrollRenderer))]
namespace idle.Droid.Renderers
{
    public class CustomListViewNoScrollRenderer : ListViewRenderer
    {

		protected override void OnElementChanged(ElementChangedEventArgs<ListView> e)
		{
			base.OnElementChanged(e);
			this.Control.HorizontalScrollBarEnabled = false;
			this.Control.VerticalScrollBarEnabled = false;
		}

    }
}
