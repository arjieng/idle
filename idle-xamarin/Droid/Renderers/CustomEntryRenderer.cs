﻿using Android.Text;
using Android.Views.InputMethods;
using Android.Widget;
using idle;
using idle.Droid;
using Xamarin.Forms;
using Xamarin.Forms.Platform.Android;

[assembly: ExportRenderer(typeof(CustomEntry), typeof(CustomEntryRenderer))]
namespace idle.Droid
{
    public class CustomEntryRenderer : EntryRenderer
    {
		protected override void OnElementChanged(ElementChangedEventArgs<Entry> e)
		{
			base.OnElementChanged(e);
            if (Control != null)
            {
                this.Control.SetBackgroundColor(Android.Graphics.Color.Transparent);
				Control.SetRawInputType(InputTypes.TextFlagNoSuggestions);
                //Control.InputType(InputTypes.)
                Element.Focused += Element_Focused;
            }

			if (e.OldElement != null)
			{
				Element.Focused -= Element_Focused;
			}
		}
       

        private void Element_Focused(object sender, FocusEventArgs e)
        {
			
			var imm = (InputMethodManager)Context.GetSystemService(Android.Content.Context.InputMethodService);
			imm.ShowSoftInput(Control, Android.Views.InputMethods.ShowFlags.Implicit);

			
        }
    }
}
