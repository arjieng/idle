﻿using System;
using System.IO;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Android.Graphics;
using Android.Views;
using idle;
using idle.Droid.Renderers;
using Java.Nio;
using Xamarin.Forms;
using Xamarin.Forms.Platform.Android;
using Xamarin.Forms.PlatformConfiguration;

[assembly: ExportRenderer(typeof(RoundedImage), typeof(RoundedImageRenderer))]
namespace idle.Droid.Renderers
{
    public class RoundedImageRenderer : ImageRenderer
    {
		protected override void OnElementChanged(ElementChangedEventArgs<Image> e)
		{
			base.OnElementChanged(e);

			if (e.OldElement == null)
			{
				//Only enable hardware accelleration on lollipop
				if ((int)Android.OS.Build.VERSION.SdkInt < 21)
				{
					SetLayerType(LayerType.Software, null);
				}
            }
		}

        protected override async void OnElementPropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
        {
            base.OnElementPropertyChanged(sender, e);
			
				this.Invalidate();
        }

		protected override bool DrawChild(Canvas canvas, global::Android.Views.View child, long drawingTime)
		{
			try
			{

				

				var borderThickness = (float)((RoundedImage)Element).BorderThickness;
                var borderRadius = (float)((RoundedImage)Element).BorderRadius;
                var radius = Math.Min(Width, Height) - borderRadius;


				int strokeWidth = 0;

				if (borderThickness > 0)
				{
					var logicalDensity = Xamarin.Forms.Forms.Context.Resources.DisplayMetrics.Density;
					strokeWidth = (int)Math.Ceiling(borderThickness * logicalDensity + .5f);
				}

				radius -= strokeWidth / 2;




				var path = new Android.Graphics.Path();
				//path.AddCircle(Width / 2.0f, Height / 2.0f, radius, Android.Graphics.Path.Direction.Ccw);
				RectF rect = new RectF(15, 15, Width, Height);
                path.AddRoundRect(rect, borderRadius, borderRadius,Android.Graphics.Path.Direction.Cw);

				//path.add
				//path.AddOval(borderRadius,borderRadius,borderRadius,borderRadius,Android.Graphics.Path.Direction.Ccw);

				canvas.Save();
				canvas.ClipPath(path);



				var paint = new Paint();
				paint.AntiAlias = true;
				paint.SetStyle(Paint.Style.Fill);
				paint.Color = Xamarin.Forms.Color.Transparent.ToAndroid();
				canvas.DrawPath(path, paint);
				paint.Dispose();


				var result = base.DrawChild(canvas, child, drawingTime);

				path.Dispose();
				canvas.Restore();

				path = new Android.Graphics.Path();
                //path.AddCircle(Width / 2, Height / 2, radius, Android.Graphics.Path.Direction.Ccw);
                //path.AddOval(borderRadius, borderRadius, borderRadius, borderRadius, Android.Graphics.Path.Direction.Ccw);


				if (strokeWidth > 0.0f)
				{
					paint = new Paint();
					paint.AntiAlias = true;
					paint.StrokeWidth = strokeWidth;
					paint.SetStyle(Paint.Style.Stroke);
					paint.Color =  Xamarin.Forms.Color.Transparent.ToAndroid();
					canvas.DrawPath(path, paint);
					paint.Dispose();
				}

				path.Dispose();
				return result;
			}
			catch (Exception ex)
			{
				System.Diagnostics.Debug.WriteLine("Unable to create circle image: " + ex);
			}

			return base.DrawChild(canvas, child, drawingTime);
		}
    }
}
