﻿using System;

using Xamarin.Forms;
using Xamarin.Forms.Platform.Android;

using idle;
using idle.Droid;
using Android.Views;

[assembly: ExportRenderer (typeof(ListViewNoScroll), typeof(ListViewNoScrollRenderer))]

namespace idle.Droid
{
	public class ListViewNoScrollRenderer : ListViewRenderer
	{
        protected ListViewNoScroll listViewNoScroll { get; private set; }
		protected override void OnElementChanged (ElementChangedEventArgs<ListView> e)
		{
			base.OnElementChanged (e);
			this.Control.HorizontalScrollBarEnabled = false;
			this.Control.VerticalScrollBarEnabled = false;
            if (Control != null || e.NewElement != null)
            {
                this.listViewNoScroll = (ListViewNoScroll)this.Element;
            }
		}

        public override bool DispatchTouchEvent(MotionEvent e)
        {
            if(!listViewNoScroll.Scroll)
            {
				if (e.Action == MotionEventActions.Move)
					return true;   
            }
			return base.DispatchTouchEvent(e);
        }
	}
}
