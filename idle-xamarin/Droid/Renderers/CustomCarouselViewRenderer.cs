﻿using System;
using idle.Droid.Renderers;
using Xamarin.Forms;
using Xamarin.Forms.Platform;
using Android.Support.V7.Widget;
using Android.Views;

[assembly: ExportRenderer(typeof(CarouselView), typeof(CustomCarouselViewRenderer))]
namespace idle.Droid.Renderers
{
    public class CustomCarouselViewRenderer : CarouselViewRenderer
    {

		public override bool DispatchTouchEvent(MotionEvent e)
		{
			if (e.Action == MotionEventActions.Move)
				return true;

			return base.DispatchTouchEvent(e);
		}

    }
}
