﻿using System;
using idle.Droid;
using idle;
using Xamarin.Forms;
using Xamarin.Forms.Platform.Android;
using Android.Views;

[assembly: ExportRenderer(typeof(ControlTemplateButton), typeof(ControlTemplateButtonRenderer))]
namespace idle.Droid
{
    public class ControlTemplateButtonRenderer : ButtonRenderer
    {
		protected override void OnDraw(Android.Graphics.Canvas canvas)
		{
			base.OnDraw(canvas);
		}

		protected override void OnElementChanged(ElementChangedEventArgs<Button> e)
		{
			base.OnElementChanged(e);
			Control.SetPadding(0, 0, 0, 0);
            Control.Gravity = GravityFlags.Left;
			Control.SetAllCaps(false);
		}
    }
}
