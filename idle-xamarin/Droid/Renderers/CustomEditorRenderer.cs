﻿﻿﻿using System;
using System.ComponentModel;
using Android.Graphics.Drawables;
using Android.Text;
using Android.Views.InputMethods;
using idle;
using idle.Droid.Renderers;
using Xamarin.Forms;
using Xamarin.Forms.Platform.Android;

[assembly: ExportRenderer(typeof(CustomEditor), typeof(CustomEditorRenderer))]
namespace idle.Droid.Renderers
{
    public class CustomEditorRenderer : EditorRenderer
    {
        protected CustomEditor customEditor { get; private set; }
        protected override void OnElementChanged(ElementChangedEventArgs<Editor> e)
        {
            base.OnElementChanged(e);
            if (Control != null || e.NewElement != null)
            {
                this.Control.SetBackgroundColor(Android.Graphics.Color.Transparent);
                Control.SetPadding(Control.PaddingLeft+3, Control.PaddingTop , 0, 9);
                Control.SetRawInputType(InputTypes.TextFlagNoSuggestions);
                //Control.TextAlignment = Android.Views.TextAlignment.Center;
                this.customEditor = (CustomEditor)this.Element;


                //if(customEditor.BorderRadius != 0)
                Control.SetBackgroundResource(Resource.Drawable.RoundCorners);

                Control.SetBackgroundColor(Android.Graphics.Color.Transparent);
                Control.Gravity = Android.Views.GravityFlags.CenterHorizontal;

                Element.TextChanged += OnEditor_TextChanged;
                // add placeholder
                //if (string.IsNullOrEmpty(Element.Text))
                    //Element.Text = ((CustomEditor)Element).Placeholder;

                Element.Focused += Element_Focused;
                Element.Unfocused += Element_Unfocused;

                if (e.OldElement != null)
                {
                    Element.Focused -= Element_Focused;
                    Element.Unfocused -= Element_Unfocused;
                }

                if (e.OldElement == null)
                {

                }

                Control.Hint = ((CustomEditor)Element).Placeholder;
                Control.SetHintTextColor( ((CustomEditor)Element).PlaceholderColor.ToAndroid());

                switch (customEditor.VerticalTextAlignment)
                {
                    case EditorTextAlignment.Start:
                        Control.TextAlignment = Android.Views.TextAlignment.ViewStart;
                        break;
                    case EditorTextAlignment.Center:
                        Control.TextAlignment = Android.Views.TextAlignment.Center;
                        break;
                    case EditorTextAlignment.End:
                        Control.TextAlignment = Android.Views.TextAlignment.ViewEnd;
                        break;
                }

                switch (customEditor.HorizontalTextAlignment)
                {
                    case EditorTextAlignment.Start:
                        Control.Gravity = Android.Views.GravityFlags.Top;
                        break;
                    case EditorTextAlignment.Center:
                        Control.Gravity = Android.Views.GravityFlags.CenterHorizontal;
                        break;
                    case EditorTextAlignment.End:


                        Control.Gravity = Android.Views.GravityFlags.Bottom;
                        break;
                }

                // this.Control.SetLineSpacing(24, 1);
                this.Control.SetLineSpacing((float)customEditor.LabelSpacing, 1);

                this.UpdateLayout();

                if(customEditor.BorderRadius != 0)
                Control.SetBackgroundResource(Resource.Drawable.RoundCorners);
            }
        }

		protected override void OnElementPropertyChanged(object sender,PropertyChangedEventArgs e)
		{
			base.OnElementPropertyChanged(sender, e);

            if (e.PropertyName == CustomEditor.PlaceholderProperty.PropertyName)
			{
				var element = this.Element as CustomEditor;
				this.Control.Hint = element.Placeholder;
                this.Control.SetHintTextColor( element.PlaceholderColor.ToAndroid());
			}
		}

        private void OnEditor_TextChanged(object sender, Xamarin.Forms.TextChangedEventArgs e)
        {
            if(customEditor !=null)
            if (customEditor.CharacterLimit != -1)
            {
                if (e.NewTextValue != null)
                {
					if (e.NewTextValue.Length <= customEditor.CharacterLimit)
					{
						((CustomEditor)sender).Text = e.NewTextValue;
					}
					else
					{
						((CustomEditor)sender).Text = e.OldTextValue;
					}   
                }
            }

            customEditor.InvalidateLayout();
        }

        private void Element_Unfocused(object sender, FocusEventArgs e)
        {
           //AddPlaceholder();
        }

        private void Element_Focused(object sender, FocusEventArgs e)
		{
            //ClearPlaceHolder();
			var imm = (InputMethodManager)Context.GetSystemService(Android.Content.Context.InputMethodService);
			imm.ShowSoftInput(Control, Android.Views.InputMethods.ShowFlags.Implicit);
		}

		void AddPlaceholder()
		{
			if (string.IsNullOrEmpty(Element.Text))
			{
				Element.Text = ((CustomEditor)Element).Placeholder;
			}
		}
		void ClearPlaceHolder()
        {
            if(!string.IsNullOrEmpty(Element.Text))
			if (Element.Text.Equals(((CustomEditor)Element).Placeholder))
			{
				Element.Text = "";
			}
		}
	}


}
