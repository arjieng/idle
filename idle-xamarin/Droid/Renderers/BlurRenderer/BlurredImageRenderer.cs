﻿using System;
using System.ComponentModel;
using System.Reflection;
using System.Threading.Tasks;
using Android.Graphics;
using Android.Renderscripts;
using idle;
using idle.Droid;
using Xamarin.Forms;
using Xamarin.Forms.Platform.Android;

[assembly: ExportRenderer(typeof(BlurredImage), typeof(BlurredImageRenderer))]
namespace idle.Droid
{
	public class BlurredImageRenderer : ImageRenderer
	{
		IImageSourceHandler handler;
		protected override void OnElementChanged(ElementChangedEventArgs<Image> e)
		{
			base.OnElementChanged(e);
			if (Element.Source != null)
			{
				SetTintAndBlur();
			}
		}

		protected override void OnElementPropertyChanged(object sender, PropertyChangedEventArgs e)
		{
			base.OnElementPropertyChanged(sender, e);
			if (e.PropertyName == Image.SourceProperty.PropertyName)
			{
                if (Element.Source != null)
                {
                    SetTintAndBlur();
                }
			}
		}

		async void SetTintAndBlur()
		{
			Bitmap blurredImage = await BlurImage();
			System.Diagnostics.Debug.WriteLine(blurredImage);
            if (blurredImage != null)
            {
                Control.SetImageBitmap(blurredImage);

                var colorFilter = new PorterDuffColorFilter(Android.Graphics.Color.Argb(187, 187, 187, 187), PorterDuff.Mode.SrcAtop);
                Control.SetColorFilter(colorFilter);
            }
		}

		async Task<Bitmap> BlurImage()
		{
			var imageSource = Element.Source;

			if (imageSource is FileImageSource)
			{
				handler = new FileImageSourceHandler();
			}
			else if (imageSource is StreamImageSource)
			{
				handler = new StreamImagesourceHandler(); // sic
			}
			else if (imageSource is UriImageSource)
			{
				handler = new ImageLoaderSourceHandler(); // sic
			}
			else
			{
				throw new NotImplementedException();
			}

			var originalBitmap = await handler.LoadImageAsync(imageSource, Context);
            if (originalBitmap != null)
            {
                var blurredBitmap = await Task.Run(() => CreateBlurredImage(originalBitmap, 25));
                return blurredBitmap;
            }
            return null;
		}

		Bitmap CreateBlurredImage(Bitmap originalBitmap, int radius)
		{
			// Create another bitmap that will hold the results of the filter.
			Bitmap blurredBitmap;
			blurredBitmap = Bitmap.CreateBitmap(originalBitmap);

			// Create the Renderscript instance that will do the work.
			RenderScript rs = RenderScript.Create(Context);

			// Allocate memory for Renderscript to work with
			Allocation input = Allocation.CreateFromBitmap(rs, originalBitmap, Allocation.MipmapControl.MipmapFull, AllocationUsage.Script);
			Allocation output = Allocation.CreateTyped(rs, input.Type);

			// Load up an instance of the specific script that we want to use.
			ScriptIntrinsicBlur script = ScriptIntrinsicBlur.Create(rs, Android.Renderscripts.Element.U8_4(rs));
			script.SetInput(input);

			// Set the blur radius
			script.SetRadius(radius);

			// Start Renderscript working.
			script.ForEach(output);

			// Copy the output to the blurred bitmap
			output.CopyTo(blurredBitmap);

			return blurredBitmap;
		}

		private static FieldInfo _isLoadingPropertyKeyFieldInfo;

		private static FieldInfo IsLoadingPropertyKeyFieldInfo
		{
			get
			{
				if (_isLoadingPropertyKeyFieldInfo == null)
				{
					_isLoadingPropertyKeyFieldInfo = typeof(Image).GetField("IsLoadingPropertyKey", BindingFlags.Static | BindingFlags.NonPublic);
				}
				return _isLoadingPropertyKeyFieldInfo;
			}
		}

		private void SetIsLoading(bool value)
		{
			var fieldInfo = IsLoadingPropertyKeyFieldInfo;
			((IElementController)base.Element).SetValueFromRenderer((BindablePropertyKey)fieldInfo.GetValue(null), value);
		}

	}
}
