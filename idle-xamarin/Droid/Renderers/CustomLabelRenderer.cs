﻿
using System;
using idle.Droid.Renderers;
using Xamarin.Forms;
using Xamarin.Forms.Platform.Android;

[assembly: ExportRenderer(typeof(Label), typeof(CustomLabelRenderer))]
namespace idle.Droid.Renderers
{
    public class CustomLabelRenderer : LabelRenderer
    {
       protected override void OnElementChanged(ElementChangedEventArgs<Label> e)
        {
            base.OnElementChanged(e);
            Control.SetPadding(0,0,0,0);
            //Element.Margin = new Thickness(Element.Margin.Left + 0,Element.Margin.Top-4, Element.Margin.Right + 0,Element.Margin.Bottom + 0);     
        }
    }
}
