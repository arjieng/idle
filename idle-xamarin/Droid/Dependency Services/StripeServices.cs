﻿using System;
using idle.Droid;
using Stripe;

[assembly: Xamarin.Forms.Dependency(typeof(StripeServices))]
namespace idle.Droid
{
    public class StripeServices : IStripeServices
    {
        public Action<string, string> _onStripeComplete;

        string message = string.Empty;

        public void CardToToken(CreditCard creditCard, string stripeAPIKey, Action<string, string> onStripeComplete)
        {
            _onStripeComplete = onStripeComplete;

			StripeConfiguration.SetApiKey(stripeAPIKey);

			var stripeTokenCreateOptions = new StripeTokenCreateOptions
			{
				Card = new StripeCreditCardOptions
				{
					Number = creditCard.Numbers,
					ExpirationMonth = int.Parse(creditCard.Month),
					ExpirationYear = int.Parse(creditCard.Year),
					Cvc = creditCard.Cvc
				}
			};

			var tokenService = new StripeTokenService();
			try
			{
				var stripeToken = tokenService.Create(stripeTokenCreateOptions);
				_onStripeComplete?.Invoke(stripeToken.Id, string.Empty);
			}
			catch (StripeException e)
			{
				switch (e.StripeError.ErrorType)
				{
					case "card_error": message = e.StripeError.Message; break;
					case "api_connection_error": message = "Failure to connect to Stripe API."; break;
					case "api_error": message = "Stripe API Error"; break;
					case "authentication_error": message = "Failure to properly authenticate yourself in the request."; break;
					case "invalid_request_error": message = "Invalid request parameters."; break;
					case "rate_limit_error": message = "Too many requests."; break;
					case "validation_error": message = "Validation Error"; break;
					default: message = "Stripe Error. Please try again."; break;
				}
				_onStripeComplete?.Invoke(string.Empty, message);
			}
			catch (OperationCanceledException e)
			{
				_onStripeComplete?.Invoke(string.Empty, e.Message);
			}
			catch (Exception e)
			{
				_onStripeComplete?.Invoke(string.Empty, e.Message);
			}
        }
    }
}
