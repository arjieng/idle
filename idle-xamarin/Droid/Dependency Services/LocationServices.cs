﻿using System;
using Android.Content;
using Android.Locations;
using Android.Provider;
using idle.Droid;
using Xamarin.Forms;

[assembly: Dependency(typeof(LocationServices))]
namespace idle.Droid
{
	public class LocationServices : iLocationServices
	{
		public bool IsLocationServiceOn()
		{
			LocationManager locationManager = (LocationManager)Forms.Context.GetSystemService(Context.LocationService);

			if (locationManager.IsProviderEnabled(LocationManager.GpsProvider) == false)
				return false;
			return true;
		}

		public void OpenSettingsUrl()
		{
			LocationManager locationManager = (LocationManager)Forms.Context.GetSystemService(Context.LocationService);

			if (locationManager.IsProviderEnabled(LocationManager.GpsProvider) == false)
			{
				Intent gpsSettingIntent = new Intent(Settings.ActionLocationSourceSettings);
				Forms.Context.StartActivity(gpsSettingIntent);
			}
		}
	}
}
