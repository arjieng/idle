﻿using System;
using Android.Widget;
using idle.Droid;
using Xamarin.Forms;

[assembly: Dependency(typeof(ToastServices))]
namespace idle.Droid
{
	public class ToastServices : iToastServices
	{
		public void DisplayToast(string text)
		{
			Toast.MakeText(Forms.Context, text, ToastLength.Short).Show();
		}
	}
}
