﻿using System;

using Xamarin.Forms;
using idle.Droid;
using Newtonsoft.Json.Linq;

[assembly: Dependency(typeof(ConsoleServices))]
namespace idle.Droid
{
	public class ConsoleServices : iConsole
	{
		public void DisplayText(string text)
		{
			Console.WriteLine(text);
		}

		public void DisplayText(JObject text)
		{
			Console.WriteLine(text);
		}
	}
}
