﻿using System;
using Android.Views;
using idle.DependencyServices;
using idle.Droid.DependencyServices;
using Xamarin.Forms.Platform.Android;
using Android.App;
using Xamarin.Forms;

[assembly: Xamarin.Forms.Dependency(typeof(StatusStyleServices))]
namespace idle.Droid.DependencyServices
{
    public class StatusStyleServices : iStatusStyle
    {
        public void StatusStyle(int status)
        {
			var window = ((Activity)Forms.Context).Window;
			window.ClearFlags(WindowManagerFlags.TranslucentStatus);
			window.AddFlags(WindowManagerFlags.DrawsSystemBarBackgrounds);
            switch (status)
            {
                case 0:
                    window.SetStatusBarColor((Constants.THEME_COLOR_GREEN).ToAndroid());
                    break;
                case 1:
                    window.SetStatusBarColor((Constants.THEME_COLOR_WHITE).ToAndroid());
                    break;
            }

        }
    }
}
