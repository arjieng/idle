﻿using System;
using Android.App;
using idle.Droid;
using Xamarin.Forms;

[assembly: Dependency(typeof(CloseApplication))]
namespace idle.Droid
{
    public class CloseApplication : ICloseApplication
    {
        public void ClosingApplication()
        {
			((Activity)Forms.Context).FinishAffinity();
        }
    }
}
