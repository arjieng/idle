﻿//﻿using System;
//using System.Collections.Generic;
//using System.Collections.ObjectModel;
//using System.Diagnostics;
//using System.Linq;
//using System.Threading.Tasks;
//using Newtonsoft.Json;
//using Newtonsoft.Json.Linq;
//using Xamarin.Forms;

//namespace idle.Views
//{
//    public partial class BidOfferPage : RootViewPage, iRestConnector
//    {
//        DataClass dataClass = DataClass.GetInstance;
//        int serviceType = 0, bidID, taskType;

//        JToken pagination;
//        int taskID, offSet;

//#if DEBUG == false

//        RestServices webService = RestServices.GetInstance;
//#endif

//        public BidOfferPage(int id, string taskTitle, int task_type)
//        {
//            InitializeComponent();
//            this.PageTitle = "BID OFFERS";
//            this.TitleFontFamily = Constants.LULO;
//            this.TitleFontColor = Constants.DARK_STRONG;
//            this.RightIcon2 = "CloseDark";
//            this.RightButton2Command = new Command((obj) => { Navigation.PopModalAsync(); });
//            BindingContext = new { task_type = task_type };
//            taskType = task_type;
//            Debug.WriteLine("Task Type: {0}",dataClass.BidList.Count());
//            titleLabel.Text = taskTitle;

//            //testViewCell.BindingContext = new { task_type = task_type };
//#if DEBUG
//            bidList.ItemsSource = dataClass.BidList;
//#else
//            taskID = id;
//            //GetData(); 
//#endif
//        }

//        protected override void OnAppearing()
//        {
//            base.OnAppearing();
//#if DEBUG
           
//#else
//            webService.WebServiceDelegate = this;
//            GetData();
//#endif
//			DependencyService.Get<DependencyServices.iStatusStyle>().StatusStyle(1);
//        }

//        void GetData()
//        {
//#if DEBUG == false
//			serviceType = 0;
			
//			webService.WebServiceDelegate = this;
//            dataClass.BidList.Clear();
//			//API
//			var url = Constants.ROOT_URL + Constants.ACCOUNTS_URL + Constants.FETCH_TASK_URL + "/" + taskID + Constants.BID_URL + Constants.TOKEN_URL + dataClass.UserToken;
//			Task.Run( async () =>  
//            {

//				await webService.GetRequest(url);
//            });
          

//#endif

//        }

//        void OnBid_ItemTapped(object sender, ItemTappedEventArgs e)
//        {
//            bidList.SelectedItem = null;
//            var bid = e.Item as Bid;
//            Navigation.PushAsync(new BidSummaryPage(bid, taskID, offSet));
//        }

//        void OnAcceptButton_Clicked(object sender, EventArgs e)
//        {
//            bidID = int.Parse(((Button)sender).ClassId);
//            Navigation.PushAsync(new BidSummaryPage(dataClass.BidList.ToList().Find((obj) => obj.id == bidID),taskID, offSet  ));
//        }

//        async void OnIgnoreButton_Clicked(object sender, EventArgs e)
//        {
//            bidID = int.Parse(((Button)sender).ClassId);
//#if DEBUG
//            dataClass.BidList.RemoveAt(dataClass.BidList.ToList().FindIndex((obj) => obj.id == bidID));
//#else

//            await Task.Run( async() =>
//            {
//                serviceType = 1;
//              //dataClass.BidList.RemoveAt(dataClass.BidList.ToList().FindIndex((obj) => obj.id == bidID));
//                //API
//                var json = JsonConvert.SerializeObject(new { token = dataClass.UserToken, off_set = pagination["off_set"].ToString() });
//                var url = Constants.ROOT_URL + Constants.ACCOUNTS_URL + Constants.FETCH_TASK_URL + "/" + this.taskID + Constants.BID_URL + "/" + bidID + Constants.TOKEN_URL + dataClass.UserToken + "&off_set=" + offSet ;

//                await  webService.DeleteRequestAsync(url);
                
//            });
//#endif
//        }

//        async void OnLast_ItemAppearing(object sender, Xamarin.Forms.ItemVisibilityEventArgs e)
//        {
//            //serviceType = 0;
//#if DEBUG == false
//            var currentItem = e.Item as Bid;
//            var lastItem = dataClass.BidList[dataClass.BidList.Count - 1];
//            if (currentItem == lastItem && int.Parse(pagination["load_more"].ToString()) == 1)
//            {
//                serviceType = 0;
//                await webService.GetRequest(Constants.ROOT + pagination["url"].ToString());
//            }
//#endif
//        }

//        public void ReceiveJSONData(JObject jsonData)
//        {
//            Device.BeginInvokeOnMainThread(() =>
//            {
//                //DisplayAlert(null, taskID.ToString(), "Okay");
//                if (int.Parse(jsonData["status"].ToString()) == 300){
                    
//                }
//                else
//                {
//                    int HTTPStatus = int.Parse(jsonData["status"].ToString());
//                    switch (serviceType)
//                    {
//                        case 0: //fetch and pagination
//                            var bids = JsonConvert.DeserializeObject<ObservableCollection<Bid>>(jsonData["bids"].ToString());
//                            foreach (var bid in bids)
//                            {
//                                dataClass.BidList.Add(bid);
//                            }
//                            //bidList.BindingContext = new { task_type = taskType };
//                            bidList.ItemsSource = dataClass.BidList;
//                            pagination = jsonData["pagination"];
//                            offSet = int.Parse(pagination["off_set"].ToString());

//    						break;
//                        case 1: //ignore
//                            dataClass.BidList.RemoveAt(dataClass.BidList.ToList().FindIndex((obj) => obj.id == bidID));

//                            if(HTTPStatus == 201){
//                                var bid = JsonConvert.DeserializeObject<Bid>(jsonData["bid"].ToString());
//                                dataClass.BidList.Add(bid);
//                            }

//                            break;
//                    }
//                }
//            });
//        }

//        public void ReceiveTimeoutError(string title, string error)
//        {
//            DisplayAlert(title,error,"Okay");
//        }
//    }
//}
