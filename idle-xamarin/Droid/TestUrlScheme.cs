﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;

namespace idle.Droid
{
    [Activity(Label = "TestUrlScheme")]
	//[Activity(Label = "Idle", Icon = "@drawable/AppIcon", Theme = "@style/MyTheme", ConfigurationChanges = ConfigChanges.ScreenSize | ConfigChanges.Orientation, ScreenOrientation = ScreenOrientation.Portrait)]
	[IntentFilter(new[] { Intent.ActionView }, Categories = new[] { Intent.CategoryBrowsable, Intent.CategoryDefault }, DataScheme = "theidleapp")]
    public class TestUrlScheme : Activity
    {
        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);

			// Create your application here

            Intent resumeIntent = PackageManager.GetLaunchIntentForPackage(PackageName);
			resumeIntent.SetAction(Intent.ActionView);
			resumeIntent.AddCategory(Intent.CategoryLauncher);
			StartActivity(resumeIntent);
        }
    }
}
