﻿﻿using System;
using Android.App;
using Android.Content;
using Android.Content.PM;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Android.OS;
using Android.Graphics;
using Plugin.Permissions;
using Plugin.FirebasePushNotification;
using Firebase;
using Xamarin.Forms;
using System.Threading.Tasks;
using Android.Provider;
using Xamarin.Facebook;
using HockeyApp.Android;

namespace idle.Droid
{
    [Activity(Label = "Idle", Icon = "@drawable/AppIcon", Theme = "@style/MyTheme", ConfigurationChanges = ConfigChanges.ScreenSize | ConfigChanges.Orientation, ScreenOrientation = ScreenOrientation.Portrait)]
    [IntentFilter(new[] { Intent.ActionView }, Categories = new[] { Intent.CategoryBrowsable, Intent.CategoryDefault }, DataScheme = "theidleapp")]
    public class MainActivity : global::Xamarin.Forms.Platform.Android.FormsAppCompatActivity
    {
        protected override async void OnCreate(Bundle savedInstanceState)
        {
            TabLayoutResource = Resource.Layout.Tabbar;
            ToolbarResource = Resource.Layout.Toolbar;

            Xamarin.FormsGoogleMaps.Init(this, savedInstanceState);

            //FirebaseApp.InitializeApp(this);

            base.OnCreate(savedInstanceState);

            var width = Resources.DisplayMetrics.WidthPixels;
            var height = Resources.DisplayMetrics.HeightPixels;
            var density = Resources.DisplayMetrics.Density;

            App.ScreenWidth = width / density;
            App.ScreenHeight = (height / density);
            App.DeviceScale = density;

            global::Xamarin.Forms.Forms.Init(this, savedInstanceState);

			FirebasePushNotificationManager.IconResource = Resource.Drawable.AppIcon;

            LoadApplication(new App());


            FirebasePushNotificationManager.ProcessIntent(Intent);
			Intent outsideIntent = Intent;
			try
			{
			    var url = Intent.Data.ToString();

			    Object[] arr = url.ToString().Split(new char[] { '=' });

    			if (url.ToString().Contains("reset"))
    			{
    				var chunk = arr[1].ToString();
    				Object[] getCode = arr[1].ToString().Split(new char[] { '&' });
    				var reset = getCode[0].ToString();
    				var email = arr[2].ToString();

    				if (reset == "1")
    				{
    					MessagingCenter.Send<Object, string>(this, "ResetPassword", email);
    				}
    			}
    			else if (url.ToString().Contains("stripe"))
    			{
    				var stripe = arr[3].ToString();
    				Object[] getCode = arr[2].ToString().Split(new char[] { '&' });
    				var code = getCode[0].ToString();

    				if (stripe == "1")
    				{
    					MessagingCenter.Send<Object, string>(this, "StripeConnectCode_Droid", code);
    				}
    			}
			}
			catch (Exception ex)
			{
                DependencyService.Get<iConsole>().DisplayText("MainActivity: Stacktrace: " + ex.StackTrace + " Message: " + ex.Message);
			}
		}

        protected override void OnStart()
        {
            base.OnStart();
        }

        protected override void OnResume()
        {
            base.OnResume();
			CrashManager.Register(this, "bdf7461967bd4b8a925eaf423e307417");
		}

        public override void OnRequestPermissionsResult(int requestCode, string[] permissions, Permission[] grantResults)
        {
            base.OnRequestPermissionsResult(requestCode, permissions, grantResults);
            PermissionsImplementation.Current.OnRequestPermissionsResult(requestCode, permissions, grantResults);
        }

		protected override void OnActivityResult(int requestCode, Result resultCode, Intent data)
		{
			base.OnActivityResult(requestCode, resultCode, data);
			var manager = DependencyService.Get<IFacebookManager>();
			if (manager != null)
			{
				(manager as FacebookManager)._callbackManager.OnActivityResult(requestCode, (int)resultCode, data);
			}
		}

		[Java.Interop.Export("myBackdoorMethod")]
		public void MyBackdoorMethod(int value)
		{
			App.UiTestTapPin(this, value);
		}

		[Java.Interop.Export("backDoorCameraCapture")]
		public void BackDoorCameraCapture()
		{
			Intent intent = new Intent(MediaStore.ActionImageCapture);
            StartActivityForResult(intent, 0);
		}
    }
}
