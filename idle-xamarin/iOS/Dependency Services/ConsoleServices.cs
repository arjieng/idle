﻿using System;

using Xamarin.Forms;
using idle.iOS;
using Newtonsoft.Json.Linq;

[assembly: Dependency(typeof(ConsoleServices))]
namespace idle.iOS
{
	public class ConsoleServices : iConsole
	{
		public void DisplayText(string text)
		{
			Console.WriteLine(text);
		}

		public void DisplayText(JObject text)
		{
			Console.WriteLine(text);
		}
	}
}
