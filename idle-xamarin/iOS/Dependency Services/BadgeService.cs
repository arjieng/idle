﻿using System;
using idle;
using idle.iOS;
using UIKit;
using Xamarin.Forms;

[assembly: Dependency(typeof(BadgeService))]
namespace idle.iOS
{
    public class BadgeService : iBadge
    {
        public BadgeService()
        {
        }
		public void SetBadge(int count)
		{
			UIApplication.SharedApplication.ApplicationIconBadgeNumber = Convert.ToInt32(count);
        }
    }
}