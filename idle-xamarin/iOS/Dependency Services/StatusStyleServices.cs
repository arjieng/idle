﻿using System;

using idle.DependencyServices;
using idle.iOS.DependencyServices;
using UIKit;

[assembly: Xamarin.Forms.Dependency(typeof(StatusStyleServices))] 
namespace idle.iOS.DependencyServices
{
    public class StatusStyleServices : iStatusStyle
    {
        public void StatusStyle(int status)
        {
            switch(status){
                case 0:
                    (UIApplication.SharedApplication).SetStatusBarStyle(UIStatusBarStyle.LightContent, false);
                    break;
                case 1:
                    (UIApplication.SharedApplication).SetStatusBarStyle(UIStatusBarStyle.Default, false);
                    break;
            }
        }
    }
}
