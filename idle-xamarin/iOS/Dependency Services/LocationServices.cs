﻿using System;
using Xamarin.Forms;
using idle.iOS;
using UIKit;
using Foundation;
using CoreLocation;

[assembly: Dependency(typeof(LocationServices))]
namespace idle.iOS
{
	public class LocationServices : iLocationServices
	{
		public void OpenSettingsUrl()
		{
			if (CLLocationManager.Status == CLAuthorizationStatus.Denied)
			{
				if (UIDevice.CurrentDevice.CheckSystemVersion(8, 0))
				{
					UIApplication.SharedApplication.OpenUrl(new NSUrl(UIApplication.OpenSettingsUrlString));
				}
			}
		}

		public bool IsLocationServiceOn()
		{
			if (CLLocationManager.Status == CLAuthorizationStatus.Denied)
				return false;
			return true;
		}
	}
}
