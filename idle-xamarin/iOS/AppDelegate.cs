﻿using System;
using System.Collections.Generic;
using System.Linq;
using Facebook.CoreKit;
using Foundation;
using HockeyApp.iOS;
using Plugin.FirebasePushNotification;
using UIKit;
using Xamarin.Forms;

namespace idle.iOS
{
    [Register("AppDelegate")]
    public partial class AppDelegate : global::Xamarin.Forms.Platform.iOS.FormsApplicationDelegate
    {
        public override bool FinishedLaunching(UIApplication uiApplication, NSDictionary launchOptions)
        {

//#if ENABLE_TEST_CLOUD
//			Xamarin.Calabash.Start();
//#endif

			global::Xamarin.Forms.Forms.Init();
            Xamarin.FormsGoogleMaps.Init("AIzaSyCxwTAKwcoAy4dbOTXqbxBlcshKq7k4pGc");

			Settings.AppID = "143905502874268";
			Settings.DisplayName = "Idle";

			App.ScreenWidth = UIScreen.MainScreen.Bounds.Width;
			App.ScreenHeight = UIScreen.MainScreen.Bounds.Height;
            App.DeviceScale = (float)UIScreen.MainScreen.Scale;

            Plugin.Share.ShareImplementation.ExcludedUIActivityTypes.Clear();

            FirebasePushNotificationManager.Initialize(launchOptions, true);

            LoadApplication(new App());

			var manager = BITHockeyManager.SharedHockeyManager;
			manager.Configure("bdf7461967bd4b8a925eaf423e307417");
			manager.StartManager();

            UIApplication.SharedApplication.StatusBarStyle = UIStatusBarStyle.LightContent;

            return base.FinishedLaunching(uiApplication, launchOptions);
        }

        public override bool OpenUrl(UIApplication app, NSUrl url, NSDictionary options)
        {
            //facebook
            if (url.ToString().Contains("fb143905502874268"))
            {
                return ApplicationDelegate.SharedInstance.OpenUrl(app, url, options.ValueForKey(new NSString("UIApplicationOpenURLOptionsSourceApplicationKey"))?.ToString(), options);
            }

            Object[] arr = url.ToString().Split(new char[] { '=' });

            if (url.ToString().Contains("reset"))
            {
                var chunk = arr[1].ToString();
                Object[] getCode = arr[1].ToString().Split(new char[] { '&' });
                var reset = getCode[0].ToString();
                var email = arr[2].ToString();

                if (reset == "1")
                {
                    MessagingCenter.Send<Object, string>(this, "ResetPassword", email);
                }
            }
            else if (url.ToString().Contains("stripe"))
            {
                var stripe = arr[3].ToString();
                Object[] getCode = arr[2].ToString().Split(new char[] { '&' });
                var code = getCode[0].ToString();

                if (stripe == "1")
                {
                    MessagingCenter.Send<Object, string>(this, "StripeConnectCode", code);
                }
            }

            return false;
        }

        public override void RegisteredForRemoteNotifications(UIApplication application, NSData deviceToken)
        {
            Console.WriteLine(deviceToken);
#if PRODUCTION || TESTFLIGHT
            FirebasePushNotificationManager.DidRegisterRemoteNotifications(deviceToken, FirebaseTokenType.Production);
#elif STAGING
            FirebasePushNotificationManager.DidRegisterRemoteNotifications(deviceToken, FirebaseTokenType.Sandbox);
#endif
		}

		public override void FailedToRegisterForRemoteNotifications(UIApplication application, NSError error)
		{
			FirebasePushNotificationManager.RemoteNotificationRegistrationFailed(error);
		}

		// To receive notifications in foregroung on iOS 9 and below.
		// To receive notifications in background in any iOS version
		public override void DidReceiveRemoteNotification(UIApplication application, NSDictionary userInfo, Action<UIBackgroundFetchResult> completionHandler)
		{
			FirebasePushNotificationManager.DidReceiveMessage(userInfo);
		}

		public override void OnActivated(UIApplication uiApplication)
		{
			base.OnActivated(uiApplication);
            //firebase
            FirebasePushNotificationManager.Connect();
            //facebook
            AppEvents.ActivateApp();
		}

		public override void DidEnterBackground(UIApplication application)
		{
            // Use this method to release shared resources, save user data, invalidate timers and store the application state.
			// If your application supports background exection this method is called instead of WillTerminate when the user quits.
			FirebasePushNotificationManager.Disconnect();
        }

		//[Export("myBackdoorMethod:")]
		//public void MyBackdoorMethod(int value)
		//{
  //         App.UiTestTapPin(this, value);
		//}
    }
}
