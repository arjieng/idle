﻿using System;

using Xamarin.Forms;
using Xamarin.Forms.Platform.iOS;

using idle;
using idle.iOS;

using UIKit;

[assembly: ExportRenderer (typeof(ScrollView), typeof(CustomListViewRenderer))]

namespace idle.iOS
{
    public class CustomListViewRenderer : ScrollViewRenderer
	{
        protected override void OnElementChanged(VisualElementChangedEventArgs e)
        {
            base.OnElementChanged(e);
			//e.NewElement
			this.ShowsHorizontalScrollIndicator = false;
			this.ShowsVerticalScrollIndicator = false;
        }
		
	}
}