﻿using System;
using System.ComponentModel;
using System.Diagnostics;
using CoreAnimation;
using CoreGraphics;
using idle;
using idle.iOS.Renderers;
using UIKit;
using Xamarin.Forms;
using Xamarin.Forms.Platform.iOS;
using Accelerate;

[assembly: ExportRenderer(typeof(CircleImage), typeof(CircleImageRenderer))]
namespace idle.iOS.Renderers
{
    public class CircleImageRenderer : ImageRenderer
    {
        static UIImage image;
        private void CreateCircle()
        {
			//try
			//{
			//	var min = Math.Min(Element.Width, Element.Height);
			//	Control.Layer.CornerRadius = (nfloat)(min / 2.0);
			//	Control.Layer.MasksToBounds = false;
			//	Control.BackgroundColor = ((CircleImage)Element).FillColor.ToUIColor();
			//	Control.ClipsToBounds = true;

			//	var borderThickness = ((CircleImage)Element).BorderThickness;
			//	var externalBorder = new CALayer();
			//	externalBorder.CornerRadius = Control.Layer.CornerRadius;
			//	externalBorder.Frame = new CGRect(-.5, -.5, min + 1, min + 1);
			//	externalBorder.BorderColor = ((CircleImage)Element).BorderColor.ToCGColor();
			//	externalBorder.BorderWidth = (nfloat)((CircleImage)Element).BorderThickness;

			//	Control.Layer.AddSublayer(externalBorder);
			//}
			//catch (Exception ex)
			//{
			//	Debug.WriteLine("Unable to create circle image: " + ex);
			//}
            try
            {
                if (Control != null)
                {
                    double min = Math.Min(Element.Width, Element.Height);
                    Control.Layer.CornerRadius = (float)(min / 2.0);
                    Control.Layer.MasksToBounds = false;
                    Control.ClipsToBounds = true;

                    CircleImage thisImage = (CircleImage)Element;

                    if (CircleImage.BorderColorProperty.PropertyName != null)
                    {
                        Control.Layer.BorderColor = thisImage.BorderColor.ToCGColor();
                    }
                    if (CircleImage.BorderThicknessProperty.PropertyName != null)
                    {
                        Control.Layer.BorderWidth = (nfloat)thisImage.BorderThickness;
                    }
                    if (CircleImage.FillColorProperty.PropertyName != null)
                    {
                        Control.Layer.BackgroundColor = thisImage.FillColor.ToCGColor();
                    }
                }
            }
            catch (Exception ex)
            {
                //Debug.WriteLine("Unable to create circle image: " + ex);
            }
        }

        protected override void OnElementPropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            base.OnElementPropertyChanged(sender, e);

            if (e.PropertyName == VisualElement.HeightProperty.PropertyName ||
                e.PropertyName == VisualElement.WidthProperty.PropertyName ||
                e.PropertyName == CircleImage.BorderColorProperty.PropertyName ||
                e.PropertyName == CircleImage.BorderThicknessProperty.PropertyName ||
                e.PropertyName == CircleImage.FillColorProperty.PropertyName)
            {
                CreateCircle();
            }
			if (Control.Image != null)
			{
                        //Control.Image.;
                        Control.Image = Control.Image.ApplyRotation();
                    
            }
        }

        protected override void OnElementChanged(ElementChangedEventArgs<Image> e)
        {
            base.OnElementChanged(e);
           
			if (Element == null)
				return;
			
            if(e.NewElement !=null)
            {
                CreateCircle();
                if (Control.Image != null)
                {
                    Control.Image = Control.Image.ApplyRotation();
                }

            }

        }
    }
}
