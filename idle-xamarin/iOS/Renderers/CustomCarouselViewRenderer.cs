﻿using System;
using System.ComponentModel;
using idle.iOS.Renderers;
using UIKit;
using Xamarin.Forms;
using Xamarin.Forms.Platform;
using Xamarin.Forms.Platform.iOS;

[assembly: ExportRenderer(typeof(CarouselView), typeof(CustomCarouselViewRenderer))]
namespace idle.iOS.Renderers
{
    public class CustomCarouselViewRenderer : CarouselViewRenderer
    {
        protected override void OnElementChanged(ElementChangedEventArgs<CarouselView> e)
        {
            base.OnElementChanged(e);
            Control.ScrollEnabled = false;
			//Control.SeparatorStyle = UITableViewCellSeparatorStyle.None;
        }
    }
}
