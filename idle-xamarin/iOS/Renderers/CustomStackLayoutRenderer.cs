﻿using System;
using idle;
using idle.iOS;
using Xamarin.Forms;
using Xamarin.Forms.Platform.iOS;

[assembly: ExportRenderer(typeof(CustomStackLayout), typeof(CustomStackLayoutRenderer))]
namespace idle.iOS
{
	public class CustomStackLayoutRenderer : ViewRenderer
	{
		protected override void OnElementChanged(ElementChangedEventArgs<View> e)
		{
			base.OnElementChanged(e);
			Layer.BorderColor = Constants.REPORT_TASK_BORDER.ToCGColor();
			Layer.BorderWidth = 2;
			Layer.CornerRadius = 2;
		}
	}
}
