﻿using System;
using idle.iOS.Renderers;
using UIKit;
using Xamarin.Forms;
using Xamarin.Forms.Platform.iOS;

[assembly: ExportRenderer(typeof(DatePicker), typeof(CustomDatePickerCellRenderer))]
namespace idle.iOS.Renderers
{
    public class CustomDatePickerCellRenderer : DatePickerRenderer
    {
		protected override void OnElementChanged(ElementChangedEventArgs<DatePicker> e)
        {
            base.OnElementChanged(e);
            Control.TextAlignment = UITextAlignment.Left;
            Control.BorderStyle = UITextBorderStyle.None;
			var someFontWithName = UIFont.FromName(Constants.PROXIMA_REG, (float)Constants.ENTRY_FONTSIZE);
            UIFont font = Control.Font.WithSize((float)Constants.ENTRY_FONTSIZE);
			Control.Font = font;
        }
    }
}
