﻿using System;
using Foundation;
using idle;
using idle.iOS;
using UIKit;
using Xamarin.Forms;
using Xamarin.Forms.Platform.iOS;

[assembly: ExportRenderer(typeof(LabelSpacing), typeof(LabelSpacingRenderer))]
namespace idle.iOS
{
    public class LabelSpacingRenderer: LabelRenderer
    {
        protected override void OnElementPropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
        {
            base.OnElementPropertyChanged(sender, e);
            if(Control != null){
				var lineSpacingLabel = (LabelSpacing)this.Element;
                if(lineSpacingLabel.Text != null)
                {
                    var paragraphStyle = new NSMutableParagraphStyle()
                    {
                        Alignment = (UITextAlignment)Element.HorizontalTextAlignment,
						LineSpacing = (nfloat)lineSpacingLabel.LineSpacing
					};
					var attrString = new NSMutableAttributedString(lineSpacingLabel.Text);
                    var style = UIStringAttributeKey.ParagraphStyle;
					var range = new NSRange(0, attrString.Length);

					attrString.AddAttribute(style, paragraphStyle, range);

                    this.Control.AttributedText = attrString;
                }
                else if (lineSpacingLabel.FormattedText != null)
                {
					var paragraphStyle = new NSMutableParagraphStyle()
					{
						Alignment = (UITextAlignment)Element.HorizontalTextAlignment,
						LineSpacing = (nfloat)lineSpacingLabel.LineSpacing
					};
                    //var attrString = new NSMutableAttributedString(lineSpacingLabel.Text);
                    var attrString = new NSMutableAttributedString(lineSpacingLabel.FormattedText.ToAttributed(lineSpacingLabel.Font,lineSpacingLabel.TextColor));
					var style = UIStringAttributeKey.ParagraphStyle;
					var range = new NSRange(0, attrString.Length);

					attrString.AddAttribute(style, paragraphStyle, range);

					this.Control.AttributedText = attrString;
                }

            }			
        }
    }
}
