﻿using System;

using Xamarin.Forms;
using Xamarin.Forms.Platform.iOS;

using idle;
using idle.iOS;

using UIKit;

[assembly: ExportRenderer (typeof(ListViewNoScroll), typeof(ListViewNoScrollRenderer))]

namespace idle.iOS
{
	public class ListViewNoScrollRenderer : ListViewRenderer
	{
        ListViewNoScroll listViewNoScroll { get; set; }
		protected override void OnElementChanged (ElementChangedEventArgs<ListView> e)
		{
			base.OnElementChanged (e);
			Control.ShowsHorizontalScrollIndicator = false;
			Control.ShowsVerticalScrollIndicator = false;
			Control.SeparatorStyle = UITableViewCellSeparatorStyle.None;
            if (e.NewElement != null)
            {
                this.listViewNoScroll = (ListViewNoScroll)this.Element;
            }
            Control.ScrollEnabled = listViewNoScroll.Scroll;
		}

        protected override void OnElementPropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
        {
            base.OnElementPropertyChanged(sender, e);
			if (Element == null)
				return;
            if(e.PropertyName == ListViewNoScroll.ScrollProperty.PropertyName)
            {
                Control.ScrollEnabled = listViewNoScroll.Scroll;
            }
        }
	}
}
