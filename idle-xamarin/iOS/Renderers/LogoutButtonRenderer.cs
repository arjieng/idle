﻿using System;
using idle;
using idle.iOS.Renderers;
using UIKit;
using Xamarin.Forms;
using Xamarin.Forms.Platform.iOS;


[assembly: ExportRenderer(typeof(LogoutButton), typeof(LogoutButtonRenderer))]
namespace idle.iOS.Renderers
{
    public class LogoutButtonRenderer : ButtonRenderer
    {
		protected override void OnElementChanged(ElementChangedEventArgs<Button> e)
		{
			base.OnElementChanged(e);
            Control.ImageEdgeInsets = new UIKit.UIEdgeInsets(Control.ImageEdgeInsets.Top, Control.ImageEdgeInsets.Left, 1*( Control.ImageEdgeInsets.Bottom + 30), Control.ImageEdgeInsets.Right); // top, left, bottom, right
			//UIBarButtonItem.Appearance.SetBackButtonTitlePositionAdjustment(new UIOffset(-100, -60), UIBarMetrics.Default);
			//public static Color LIGHT_GREEN = Color.FromHex("#b2e0db"); Constants
			//IsGestureEnabled = Device.RuntimePlatform == Device.iOS ? false : true; MasterDetailPage.xaml.cs
		}
    }
}
