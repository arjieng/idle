﻿using System;
using System.Diagnostics;
using CoreGraphics;
using Foundation;
using idle;
using idle.iOS.Renderers;
using UIKit;
using Xamarin.Forms;
using Xamarin.Forms.Platform.iOS;

[assembly: ExportRenderer(typeof(CustomEditor), typeof(CustomEditorRenderer))]
namespace idle.iOS.Renderers
{
    public class CustomEditorRenderer : EditorRenderer
    {
		UILabel labelPlaceHolder;
		UITextView replacingControl;
        float animatedDistance;

        string prevPlaceholder;

        CustomEditor customEditor { get; set; }
        Color oldColor { get; set; }
        protected override void OnElementChanged(ElementChangedEventArgs<Editor> e)
        {

            base.OnElementChanged(e);
            if (this.Control == null) return;
            //Control.ScrollEnabled = false;

            if (e.NewElement != null)
            {
                
                this.customEditor = (CustomEditor)this.Element;
                Control.Layer.CornerRadius = customEditor.BorderRadius;
                oldColor = ((CustomEditor)Element).TextColor;

                if (string.IsNullOrEmpty(Element.Text))
                {
                    Element.Text = ((CustomEditor)Element).Placeholder;
                    Control.TextColor = ((CustomEditor)Element).PlaceholderColor.ToUIColor();
                    SetPlaceholder();
                    prevPlaceholder = ((CustomEditor)Element).Placeholder;
                }

                CustomEditor customEntry = (CustomEditor)Element;
                //Control.BorderStyle = UITextBorderStyle.None;
                Control.SpellCheckingType = UITextSpellCheckingType.No;
                Control.AutocorrectionType = UITextAutocorrectionType.No;
                Control.Started += OnEntryDidEdit;
                Control.Ended += OnEntryDidEnd;
                // add text length limit
                Element.TextChanged += OnEditor_TextChanged;

                //Control.TextContainerInset = new UIEdgeInsets(Control.TextContainerInset.Top - 10 , Control.TextContainerInset.Left, Control.TextContainerInset.Bottom, Control.TextContainerInset.Right);

                switch (customEditor.VerticalTextAlignment)
                {
                    case EditorTextAlignment.Start:
                        Control.TextContainerInset = new UIEdgeInsets(Control.TextContainerInset.Top - 10, Control.TextContainerInset.Left, Control.TextContainerInset.Bottom, Control.TextContainerInset.Right);
                        break;
                    case EditorTextAlignment.Center:
                        Control.TextContainerInset = new UIEdgeInsets(Control.TextContainerInset.Top , Control.TextContainerInset.Left , Control.TextContainerInset.Bottom, Control.TextContainerInset.Right);
                        break;
                    case EditorTextAlignment.End:
                        Control.ContentMode = UIViewContentMode.Bottom;
                        break;
                }


                if (customEntry.ReturnKey == "Next")
                {
                    Control.ReturnKeyType = UIReturnKeyType.Default;
                }
                else if (customEntry.ReturnKey == "Done")
                {
                    Control.ReturnKeyType = UIReturnKeyType.Default;
                }

                if (customEntry.AutoCapitalization == "Words")
                {
                    Control.AutocapitalizationType = UITextAutocapitalizationType.Words;
                }
                else if (customEntry.AutoCapitalization == "Sentences")
                {
                    Control.AutocapitalizationType = UITextAutocapitalizationType.Sentences;
                }
                else
                {
                    Control.AutocapitalizationType = UITextAutocapitalizationType.None;
                }
                // line spacing

                if (customEntry.AutoCapitalization == "Words")
                {
                    Control.AutocapitalizationType = UITextAutocapitalizationType.Words;
                }
                else if (customEntry.AutoCapitalization == "Sentences")
                {
                    Control.AutocapitalizationType = UITextAutocapitalizationType.Sentences;
                }
                else
                {
                    Control.AutocapitalizationType = UITextAutocapitalizationType.None;
                }
                // line spacing

                SetEditorSettings();

                //remove done button upper part of keyboard
                Control.InputAccessoryView = null;

                //disable scrollable
                Control.ScrollEnabled = customEditor.Scroll;

                if(customEntry.ReturnKey != "Return")
                {
					var toolbar = new UIToolbar(new System.Drawing.RectangleF(0.0f, 0.0f, (float)Control.Frame.Size.Width, 44.0f));

					toolbar.Items = new[]
					{
    					new UIBarButtonItem(UIBarButtonSystemItem.FlexibleSpace),
    					new UIBarButtonItem(customEntry.ReturnKey, UIBarButtonItemStyle.Bordered, delegate
    					{
    						if(customEntry.ReturnKey == "Next")
    						{
    							if(customEntry.NextEntry != null)
    							{
    								customEntry.NextEntry.Focus();
    							}
    						}
    						else if(customEntry.ReturnKey == "Done")
    						{
    							Control.ResignFirstResponder();
    						}
    					})
				    };

					Control.InputAccessoryView = toolbar;
                }
            }
        }

        protected override void OnElementPropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
        {
            base.OnElementPropertyChanged(sender, e);
            var element = Element as CustomEditor;
			{
                if (string.IsNullOrEmpty(element.Text) && element.IsFocused == false)
                {
                    if (e.PropertyName == CustomEditor.PlaceholderProperty.PropertyName)
                    {
                        if (!element.Placeholder.Equals(prevPlaceholder))
                        {
                            SetPlaceholder();
                        }   
                    }
                    if (e.PropertyName == CustomEditor.TextProperty.PropertyName)
                    {
                        AddPlaceholder();
                    }  
                }
			}
		}
		
		void SetEditorHeight()
        {
            var textHeight = Control.Font.PointSize + customEditor.LabelSpacing;
            // Debug.WriteLine("This height: {0} , {1}", Control.TextStorage.Size.Width, textHeight);
        }

        void SetEditorSettings()
        {
            Control.Font = UIFont.SystemFontOfSize((float)Element.FontSize);
            //Debug.WriteLine("FontSIze: {0}", Control.Font.PointSize);

            var paragraphStyle = new NSMutableParagraphStyle()
            {
                LineSpacing = (float)customEditor.LabelSpacing
            };
            if (Element.Text != null)
            {
                var labelString = new NSMutableAttributedString(((CustomEditor)this.Element).Text);
                labelString.AddAttribute(UIStringAttributeKey.ParagraphStyle, paragraphStyle, new NSRange(0, labelString.Length));
                this.Control.AttributedText = labelString;
            }

            Control.TextColor = Element.TextColor.ToUIColor();

            Control.Font = UIFont.SystemFontOfSize((float)Element.FontSize);
            UITextField text = new UITextField();
            text.TextRect(Bounds);

            switch (customEditor.HorizontalTextAlignment)
            {
                case EditorTextAlignment.Start:
                    Control.TextAlignment = UITextAlignment.Natural;
                    break;
                case EditorTextAlignment.Center:
                    Control.TextAlignment = UITextAlignment.Center;
                    break;
                case EditorTextAlignment.End:
                    Control.TextAlignment = UITextAlignment.Right;
                    break;
            }
            //// for spacing
            SetEditorHeight();
        }

        private void OnEditor_TextChanged(object sender, TextChangedEventArgs e)
        {
            if (e.NewTextValue != null)
                if (customEditor.CharacterLimit != -1)
                {
					if (e.NewTextValue.Length <= customEditor.CharacterLimit)
					{
					    ((CustomEditor)sender).Text = e.NewTextValue;	
					}
                    else
                    {
                        ((CustomEditor)sender).Text = e.OldTextValue;
                    }
                }

            // line spacing

            SetEditorSettings();
            if (customEditor.Text.Equals(((CustomEditor)Element).Placeholder))
            {
                Control.TextColor = ((CustomEditor)Element).PlaceholderColor.ToUIColor();
                SetPlaceholder();
            }
            //adding swiftup control
            //OnEntryDidEdit(this, e);
            //isTyping = true;
            //Control.TextContainer.LayoutOrientation = NSTextLayoutOrientation.Vertical;
            customEditor.InvalidateLayout();
        }

        private void OnEntryDidEnd(object sender, EventArgs e)
        {
            //Debug.WriteLine("3: {0}", Control.TextColor);
            AddPlaceholder();

            if (customEditor.MoveUp)
            {
                UIView parentView = getParentView();
                var viewFrame = parentView.Bounds;

                if (customEditor.ViewToSwiftUp == null)
                    viewFrame.Y = 0.0f;
                else
                    viewFrame.Height += animatedDistance;

                UIView.BeginAnimations(null, (IntPtr)null);
                UIView.SetAnimationBeginsFromCurrentState(true);
                UIView.SetAnimationDuration(0.3);

                parentView.Frame = viewFrame;

                UIView.CommitAnimations();
            }
            //isTyping = false;

            //debug
			//Debug.WriteLine("parentheight: {0}", parentView.Frame.Y);
        }

        void AddPlaceholder()
        {
            if (Element.Text.Length == 0)
            {
                SetPlaceholder();
            }
        }

        void SetPlaceholder()
        {
            SetEditorSettings();
            Element.Text = ((CustomEditor)Element).Placeholder;
            Element.TextColor = customEditor.PlaceholderColor;
        }
        void ClearPlaceHolder()
        {
            if (Element.Text.Equals(((CustomEditor)Element).Placeholder))
            {
                Element.Text = "";
                SetEditorSettings();
                Element.TextColor = oldColor;
            }

        }

        private void OnEntryDidEdit(object sender, EventArgs e)
        {

            ClearPlaceHolder();

            if (customEditor.MoveUp)
            {
                UIView parentWindow = getParentView();
                var textfieldRect = parentWindow.ConvertRectFromView(Control.Bounds, Control);
                var viewRect = parentWindow.ConvertRectFromView(parentWindow.Bounds, parentWindow);

                float midline = (float)(textfieldRect.Y + 0.5 * textfieldRect.Height);
                float numerator = (float)(midline - viewRect.Y - 0.2 * viewRect.Height);
                float denominator = (float)((1.0f - 0.2f) * viewRect.Height);
                float heightFraction = numerator / denominator;

                if (heightFraction < 0.0)
                {
                    heightFraction = 0.0f;
                }
                else if (heightFraction > 1.0)
                {
                    heightFraction = 1.0f;
                }

                UIInterfaceOrientation orientation = UIApplication.SharedApplication.StatusBarOrientation;
                if (orientation == UIInterfaceOrientation.Portrait || orientation == UIInterfaceOrientation.PortraitUpsideDown)
                {
                    if (customEditor.ReturnKey != "Return")
                    {
                        if (customEditor.ViewToSwiftUp == null)
                            animatedDistance = (float)Math.Floor((227.0f + 44.0f) * heightFraction);
                        else
                            animatedDistance = (float)Math.Floor(215.0f + 44.0f);
                    }
                    else
                    {
                        if (customEditor.ViewToSwiftUp == null)
                            animatedDistance = (float)Math.Floor(227.0f * heightFraction);
                        else
                            animatedDistance = (float)Math.Floor(215.0f);
                    }
                }
                else
                {
                    if (customEditor.ReturnKey != "Return")
                    {
                        if (customEditor.ViewToSwiftUp == null)
                            animatedDistance = (float)Math.Floor((162.0f + 44.0f) * heightFraction);
                        else
                            animatedDistance = (float)Math.Floor(162.0f + 44.0f);
                    }
                    else
                    {
                        if (customEditor.ViewToSwiftUp == null)
                            animatedDistance = (float)Math.Floor(162.0f * heightFraction);
                        else
                            animatedDistance = (float)Math.Floor(162.0f);
                    }
                }

                var viewFrame = parentWindow.Frame;
                if (customEditor.ViewToSwiftUp == null)
                    viewFrame.Y -= animatedDistance;
                else
                    viewFrame.Height -= animatedDistance;

                UIView.BeginAnimations(null, (IntPtr)null);
                UIView.SetAnimationBeginsFromCurrentState(true);
                UIView.SetAnimationDuration(0.3);
                //if (isTyping == false)
                parentWindow.Frame = viewFrame;
                UIView.CommitAnimations();
            }
        }

        UIView getParentView()
        {
                UIView view = Control.Superview;

                while (view != null && !(view is UIWindow))
                {
                    view = view.Superview;
                }
                return view;
        }
    }

}
