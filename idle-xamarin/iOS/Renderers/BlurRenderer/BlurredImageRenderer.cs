﻿﻿using idle;
using idle.iOS;
using Xamarin.Forms;
using Xamarin.Forms.Platform.iOS;

[assembly: ExportRenderer(typeof(BlurredImage), typeof(BlurredImageRenderer))]
namespace idle.iOS
{
    public class BlurredImageRenderer: ImageRenderer
    {
        protected override void OnElementChanged(ElementChangedEventArgs<Image> e)
        {
            base.OnElementChanged(e);
		}

        protected override void OnElementPropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
        {
            base.OnElementPropertyChanged(sender, e);
            if(Control.Image!=null && e.PropertyName == Image.IsLoadingProperty.PropertyName)
            //if (Control.Image != null)
            {
                
                    CreateBlur();
                if(!Element.IsLoading)
                {
                    CreateBlur();
                }

			}
        }

        void CreateBlur()
        {
		    //Control.Image = Control.Image.ApplyLightEffect();
		    //Control.Image = Control.Image.ApplyExtraLightEffect();
			//Control.Image = Control.Image.ApplyDarkEffect();
			//Control.Image = Control.Image.ApplyTintEffect(Color.FromRgba(187, 187, 187, 187).ToUIColor());
            Control.Image = Control.Image.ApplyTintEffect(Color.Transparent.ToUIColor());
            //Control.Image = Control.Image.ScaleAndRotateImage();
		}
    }
}
