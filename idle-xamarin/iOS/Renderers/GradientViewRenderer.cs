﻿using System;
using idle;
using idle.iOS;
using CoreAnimation;
using CoreGraphics;
using Foundation;
using UIKit;
using Xamarin.Forms.Platform.iOS;
using Xamarin.Forms;
using idle.iOS.Renderers;

[assembly: ExportRenderer(typeof(GradientView), typeof(GradientViewRenderer))]
namespace idle.iOS.Renderers
{
	public class GradientViewRenderer : BoxRenderer
	{
		//protected override void OnElementChanged(ElementChangedEventArgs<BoxView> e)
		//{
		//	base.OnElementChanged(e);
		//}

   //     public override void LayoutSubviews()
   //     {
   //         base.LayoutSubviews();

			//UIColor blackColor = UIColor.Black;
			//UIColor rgbColor = UIColor.FromRGB(1.0f, 1.0f, 1.0f).ColorWithAlpha(0.0f);

			//var gradientLayer = new CAGradientLayer()
			//{
			//	Frame = Control.Bounds,
   //             StartPoint = new CGPoint(0, 0),
			//	EndPoint = new CGPoint(0, 1.2f),
			//	Colors = new[] { blackColor.CGColor, rgbColor.CGColor },
			//	Locations = new NSNumber[] { NSNumber.FromNFloat(0.0f), NSNumber.FromFloat(0.0f) }
			//};

			//Console.WriteLine(gradientLayer);

			//Control.Layer.InsertSublayer(gradientLayer, 0);
        //}

        public override void Draw(CGRect rect)
        {
            base.Draw(rect);

            GradientView boxView = (GradientView)this.Element;

            UIColor blackColor = UIColor.Black;
            UIColor rgbColor = UIColor.FromRGB(1.0f, 1.0f, 1.0f).ColorWithAlpha(0.0f);

            var gradientLayer = new CAGradientLayer();
            gradientLayer.Frame = rect;
            gradientLayer.StartPoint = new CGPoint(0, 0);
            gradientLayer.EndPoint = new CGPoint(0, 1.2f);
            gradientLayer.Colors = new[] { blackColor.CGColor, rgbColor.CGColor };
            gradientLayer.Locations = new NSNumber[] { NSNumber.FromNFloat(0.0f), NSNumber.FromFloat(0.0f) };

            NativeView.Layer.AddSublayer(gradientLayer);
            this.Layer.InsertSublayer(gradientLayer, 0);
        }
	}
}
