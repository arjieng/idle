﻿using System;

using Xamarin.Forms;
using Xamarin.Forms.Platform.iOS;

using idle;
using idle.iOS;

using UIKit;

[assembly: ExportRenderer (typeof(ListView), typeof(CustomListViewNoScrollRenderer))]

namespace idle.iOS
{
	public class CustomListViewNoScrollRenderer : ListViewRenderer
	{
		protected override void OnElementChanged (ElementChangedEventArgs<ListView> e)
		{
			base.OnElementChanged (e);
			Control.ShowsHorizontalScrollIndicator = false;
			Control.ShowsVerticalScrollIndicator = false;
			Control.SeparatorStyle = UITableViewCellSeparatorStyle.None;
		}
	}
}