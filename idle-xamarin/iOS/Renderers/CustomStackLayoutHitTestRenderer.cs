﻿using System;
using idle;
using idle.iOS;
using UIKit;
using Xamarin.Forms;
using Xamarin.Forms.Platform.iOS;

[assembly: ExportRenderer(typeof(MasksLayout), typeof(CustomStackLayoutHtiTestRenderer))]
namespace idle.iOS
{
	public class CustomStackLayoutHtiTestRenderer : ViewRenderer
	{
        public override UIKit.UIView HitTest(CoreGraphics.CGPoint point, UIKit.UIEvent uievent)
		{
			UIView hitView = base.HitTest(point, uievent);
           	if (hitView == this)
			{
				return null;
			}
            return hitView;
            //return base.HitTest(point, uievent);
		}
	}
}
