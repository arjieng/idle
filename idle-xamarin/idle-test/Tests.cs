﻿﻿﻿﻿using System;
using System.IO;
using System.Linq;
using System.Threading;
using NUnit.Framework;
using Xamarin.UITest;
using Xamarin.UITest.Queries;

namespace idletest
{
	//[TestFixture(Platform.Android)]
    [TestFixture(Platform.iOS)]
	public class Tests
	{
		IApp app;
		Platform platform;
		bool IsAndroid;
		public Tests(Platform platform)
		{
			this.platform = platform;
			IsAndroid = platform == Platform.Android;
		}

		[SetUp]
		public void BeforeEachTest()
		{
			app = AppInitializer.StartApp(platform);
        }
        //chray221 @gmail.com hello@123
		string email = "christian.tautuan@appstone.tech";
		string password = "hello@123";

        string performerEmail = "dark.light@idle.com";
        string performerPassword = "hello@123";

        [Test]
        public void TestAndroid()
		{
            
            if (app.Query((arg) => arg.Marked("HomePagePosterButton")).Count() <= 0)
            {
                if (app.Query((arg) => arg.Marked("OK")).Count() >= 1)
                {
                    app.Tap((arg) => arg.Marked("OK"));
                }
                if (app.Query((arg) => arg.Marked("GeneralTutorialNextButton")).Count() > 0)
                {
                    TestAATutorialNext();
                    Thread.Sleep(3000);
                }
                TestASignUp();

                TestBForgotPassword();

                TestCSignIn();
            }

            TestDSetting(); 
			TestEStripe();
			TestFMessage();
			TestGTaskPage();
			TestHViewUserProfile();

			TestIHome();
		}

		public void TapListViewItem(string listViewName, int AndroidIndex, int iOSIndex)
		{
			if (platform == Platform.Android)
			{
				app.Tap(x => x.Marked(listViewName).Child(AndroidIndex));
			}
			else
			{
				app.Tap(x => x.Marked(listViewName).Child(0).Child(iOSIndex));
			}
		}

		public void TapListViewItemDescendants(string listViewName, int AndroidIndex, int iOSIndex, string descendant)
		{
			if (platform == Platform.Android)
			{
				app.Tap(x => x.Marked(listViewName).Child(AndroidIndex).Descendant().Marked(descendant).Button());
			}
			else
			{
                app.Tap(x => x.Marked(listViewName).Child(0).Child(iOSIndex).Descendant().Marked(descendant).Button());
			}
		}

		public void TestIHome()
		{
			app.Tap("LeftButton");
			// select home
            TapListViewItem("SideMenuList",0,5);
            //HomePageQuickieButton
            //HomePageGroceryButton
            //HomePageTransportButton
            //HomePageLaundryButton
            //HomePageOtherButton
            //HomePagePosterButton
            //HomePagePerformerButton

            //create new task
			TestCreateNewTask("HomePageOtherButton", "Test Idle", "Testing Theidleapp...", "10.00");
			//sign out poster - crow elmo
			LogoutApp();
			//sign in performer - stephen
			SignInApp(performerEmail, performerPassword, "HomePagePerformerButton");
			//ignore task
			TestIgnoreTaskFromCategory("HomePageOtherButton");
            //log out performer - stephen
            LogoutApp();
			//sign in poster - crow elmo
            SignInApp(email, password, "HomePagePosterButton");
			//deleted task
			DeleteTask();
            //create new task
            TestCreateNewTask("HomePageOtherButton", "Test Idle", "Testing Theidleapp...", "10.00");
			//sign out poster - crow elmo
			LogoutApp();
			//sign in performer - stephen
			SignInApp(performerEmail, performerPassword, "HomePagePerformerButton");
            //bid task 
            TestBidTaskFromCategory("HomePageOtherButton", "10.00");
            //cancel bid
            CancelBid();//--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
			//bid again the task
            TestBidTaskFromCategory("HomePageOtherButton", "10.00");
			//sign out performer - stephen
            LogoutApp();
			//sign in poster - crow elmo
            SignInApp(email, password, "HomePagePosterButton");
            //ignore bid by performer
            IgnoreBid();
			//sign out poster - crow elmo
			LogoutApp();
			//sign in performer - stephen
			SignInApp(performerEmail, performerPassword, "HomePagePerformerButton");
			//bid task 
			TestBidTaskFromCategory("HomePageOtherButton", "10.00");
			//sign out performer - stephen
			LogoutApp();
			//sign in poster - crow elmo
			SignInApp(email, password, "HomePagePosterButton");
            //accept bid by performer
            AcceptBid();
			//sign out poster - crow elmo
            LogoutApp();
			//sign in performer - stephen
            SignInApp(performerEmail, performerPassword, "HomePagePerformerButton");
            //cancel task
            PerformerCancelTask();
			//sign out performer - stephen
            LogoutApp();
			//sign in poster - crow elmo
            SignInApp(email, password, "HomePagePosterButton");
            //deny task cancellation
            PosterDenyTaskCancellation();
			//sign out poster - crow elmo
            LogoutApp();
			//sign in performer - stephen
            SignInApp(performerEmail, performerPassword, "HomePagePerformerButton");
			//cancel task
            PerformerCancelTask();
			//sign out performer - stephen
            LogoutApp();
			//sign in poster - crow elmo
            SignInApp(email, password, "HomePagePosterButton");
            //allow task cancellation
            PosterAllowTaskCancellation();
			//sign out poster - crow elmo
            LogoutApp();
			//sign in performer - stephen
            SignInApp(performerEmail, performerPassword, "HomePagePerformerButton");
			//bid on task
            TestBidTaskFromCategory("HomePageOtherButton", "10.00");
			//sign out performer - stephen
            LogoutApp();
			//sign in poster - crow elmo
            SignInApp(email, password, "HomePagePosterButton");
            //accept bid
            AcceptBid();
            //cancel task
            PosterCancelTask();
			//sign out poster - crow elmo
            LogoutApp();
			//sign in performer - stephen
            SignInApp(performerEmail, performerPassword, "HomePagePerformerButton");
            //deny task cancellation
            PerformerDenyTaskCancellation();
			//sign out performer - stephen
            LogoutApp();
			//sign in poster - crow elmo
            SignInApp(email, password, "HomePagePosterButton");
			//cancel task
            PosterCancelTask();
			//sign out poster - crow elmo
            LogoutApp();
			//sign in performer - stephen
            SignInApp(performerEmail, performerPassword, "HomePagePerformerButton");
            //allow task cancellation
            PerformerAllowTaskCancellation();
			// bid task
            TestBidTaskFromCategory("HomePageOtherButton", "10.00");
			//sign out performer - stephen
            LogoutApp();
			//sign in poster - crow elmo
            SignInApp(email, password, "HomePagePosterButton");
            //accept bid
            AcceptBid();
			//sign out poster - crow elmo
            LogoutApp();
			//sign in performer - stephen
            SignInApp(performerEmail, performerPassword, "HomePagePerformerButton");
            //complete task
            PerformerCompleteTask();
			//sign out performer - stephen
            LogoutApp();
			//sign in poster - crow elmo
            SignInApp(email, password, "HomePagePosterButton");
            //not yet done task
            PosterDenyTaskCompletion();
			//sign out poster - crow elmo
            LogoutApp();
			//sign in performer - stephen
            SignInApp(performerEmail, performerPassword, "HomePagePerformerButton");
			//complete task
            PerformerCompleteTask();
			//sign out performer - stephen
            LogoutApp();
			//sign in poster - crow elmo
            SignInApp(email, password, "HomePagePosterButton");
            //complete task
            PosterAgreeTaskCompletion();
			//sign out poster - crow elmo
            LogoutApp();
			//sign in performer - stephen
            SignInApp(performerEmail, performerPassword, "HomePagePerformerButton");
            //submit rating
            PerformerSubmitRating();
			//sign out performer - stephen
            LogoutApp();
			//sign in poster - crow elmo
            SignInApp(email, password, "HomePagePosterButton");
            //end task
            PosterEndingTask();
			//create task
            TestCreateNewTask("HomePageOtherButton", "Test Idle", "Testing Theidleapp...", "10.00");
			//sign out poster - crow elmo
			LogoutApp();
			//sign in performer - stephen
			SignInApp(performerEmail, performerPassword, "HomePagePerformerButton");
			//test map
            TestMapHomePage();
			//report task
            TestReportTask();
			//report user
            TestReportUser();
			//sign out performer - stephen
			LogoutApp();
			//sign in poster - crow elmo
			SignInApp(email, password, "HomePagePosterButton");
			//deleted task
			DeleteTask();
			//sign out poster - crow elmo
			LogoutApp();
		}


        public void SignInApp(string e, string p, string status)
        {
			app.ClearText("EmailEntrySignIn");
			app.DismissKeyboard();
			app.EnterText("EmailEntrySignIn", e);
			app.PressEnter();

			app.ClearText("PasswordEntrySignIn");
			app.DismissKeyboard();
			app.EnterText("PasswordEntrySignIn", p);
			app.PressEnter();

			app.Tap("ButtonSignIn");

            Thread.Sleep(10000);
			app.WaitForElement("HomePagePerformerButton");
			app.Tap(status);
        }


        public void LogoutApp()
        {
            app.Tap("LeftButton");
            app.WaitForElement("LogOutButton");
            app.Tap("LogOutButton");
            Thread.Sleep(2000);
        }

        public void PerformerSubmitRating()
		{
			app.Tap("LeftButton");
			// select task
			TapListViewItem("SideMenuList", 2, 3);
			app.WaitForElement("MyTaskPageInProgressButton");
			app.Tap("MyTaskPageInProgressButton");
			Thread.Sleep(5000);
			TapListViewItem("MyTaskPageListView", 0, 0);
			app.WaitForElement("ViewTaskLowerButton");
            app.Tap(x => x.Marked("ViewTaskStarRating").Descendant().Marked("FourStar"));
			app.Tap("ViewTaskLowerButton");
			Thread.Sleep(3000);
			app.Tap("LeftButton");
		}
		public void PosterEndingTask()
		{
			app.Tap("LeftButton");
			// select task
			TapListViewItem("SideMenuList", 2, 3);
			app.WaitForElement("MyTaskPageInProgressButton");
			app.Tap("MyTaskPageInProgressButton");
			Thread.Sleep(5000);
			TapListViewItem("MyTaskPageListView", 0, 0);
			app.WaitForElement("TaskSummaryLowerButton");
            app.Tap(x => x.Marked("TaskSummaryStarRating").Descendant().Marked("FourStar"));
			app.Tap("TaskSummaryLowerButton");
			Thread.Sleep(3000);
		}


		public void PerformerCompleteTask()
		{
			app.Tap("LeftButton");
			// select task
			TapListViewItem("SideMenuList", 2, 3);
			app.WaitForElement("MyTaskPageInProgressButton");
			app.Tap("MyTaskPageInProgressButton");
			Thread.Sleep(5000);
			TapListViewItem("MyTaskPageListView", 0, 0);
			app.WaitForElement("ViewTaskLowerRightButton");
			app.Tap("ViewTaskLowerRightButton");
			Thread.Sleep(3000);
			app.Tap(x => x.Marked("Yes"));
			Thread.Sleep(3000);
			app.Tap(x => x.Marked("Okay"));
			Thread.Sleep(3000);
            app.Tap("LeftButton");
		}
		public void PosterDenyTaskCompletion()
		{
			app.Tap("LeftButton");
			// select task
			TapListViewItem("SideMenuList", 2, 3);
			app.WaitForElement("MyTaskPageInProgressButton");
			app.Tap("MyTaskPageInProgressButton");
			Thread.Sleep(5000);
			TapListViewItem("MyTaskPageListView", 0, 0);
			app.WaitForElement("TaskSummaryLowerLeftButton");
			app.Tap("TaskSummaryLowerLeftButton");
			Thread.Sleep(3000);
			app.Tap(x => x.Marked("Okay"));
			Thread.Sleep(3000);
			app.Tap("LeftButton");
		}
		public void PosterAgreeTaskCompletion()
		{
			app.Tap("LeftButton");
			// select task
			TapListViewItem("SideMenuList", 2, 3);
			app.WaitForElement("MyTaskPageInProgressButton");
			app.Tap("MyTaskPageInProgressButton");
			Thread.Sleep(5000);
			TapListViewItem("MyTaskPageListView", 0, 0);
			app.WaitForElement("TaskSummaryLowerRightButton");
			app.Tap("TaskSummaryLowerRightButton");
			Thread.Sleep(3000);
			app.Tap(x => x.Marked("Okay"));
			Thread.Sleep(3000);
			app.Tap("LeftButton");
		}

		//app.Tap("TaskSummaryLowerLeftButton");
        //app.Tap("TaskSummaryLowerRightButton");
        //app.Tap("TaskSummaryLowerButton");

		public void PosterCancelTask()
		{
			app.Tap("LeftButton");
			// select task
			TapListViewItem("SideMenuList", 2, 3);
			app.WaitForElement("MyTaskPageInProgressButton");
			app.Tap("MyTaskPageInProgressButton");
			Thread.Sleep(5000);
			TapListViewItem("MyTaskPageListView", 0, 0);
			app.WaitForElement("TaskSummaryLowerButton");
			app.Tap("TaskSummaryLowerButton");
			Thread.Sleep(3000);
			app.Tap(x => x.Marked("Yes"));
			Thread.Sleep(3000);
			app.Tap(x => x.Marked("Okay"));
			Thread.Sleep(3000);
			app.Tap("LeftButton");
		}
        public void PerformerAllowTaskCancellation()
		{
			app.Tap("LeftButton");
			// select task
			TapListViewItem("SideMenuList", 2, 3);
			app.WaitForElement("MyTaskPageInProgressButton");
			app.Tap("MyTaskPageInProgressButton");
			Thread.Sleep(5000);
			TapListViewItem("MyTaskPageListView", 0, 0);
			app.WaitForElement("ViewTaskLowerButton");
			app.Tap("ViewTaskLowerButton");
			Thread.Sleep(3000);
			app.Tap(x => x.Marked("Yes"));
			Thread.Sleep(3000);
			app.Tap(x => x.Marked("No"));
			Thread.Sleep(3000);
		}
        public void PerformerDenyTaskCancellation()
		{
			app.Tap("LeftButton");
			// select task
			TapListViewItem("SideMenuList", 2, 3);
			app.WaitForElement("MyTaskPageInProgressButton");
			app.Tap("MyTaskPageInProgressButton");
			Thread.Sleep(5000);
			TapListViewItem("MyTaskPageListView", 0, 0);
			app.WaitForElement("ViewTaskLowerButton");
			app.Tap("ViewTaskLowerButton");
			Thread.Sleep(3000);
			app.Tap(x => x.Marked("No"));
			Thread.Sleep(3000);
			app.Tap(x => x.Marked("Okay"));
			Thread.Sleep(3000);
            app.Tap("LeftButton");
		}


		//app.Tap("ViewTaskLowerLeftButton");
        //app.Tap("ViewTaskLowerRightButton");
        //app.Tap("ViewTaskLowerButton");
		public void PerformerCancelTask()
		{
			app.Tap("LeftButton");
			// select task
			TapListViewItem("SideMenuList", 2, 3);
			app.WaitForElement("MyTaskPageInProgressButton");
			app.Tap("MyTaskPageInProgressButton");
            Thread.Sleep(5000);
			TapListViewItem("MyTaskPageListView", 0, 0);
			app.WaitForElement("ViewTaskLowerLeftButton");
			app.Tap("ViewTaskLowerLeftButton");
            Thread.Sleep(3000);
            app.Tap(x => x.Marked("Yes"));
			Thread.Sleep(3000);
			app.Tap(x => x.Marked("Okay"));
            Thread.Sleep(3000);
            app.Tap("LeftButton");
		}
        public void PosterAllowTaskCancellation()
		{
			app.Tap("LeftButton");
			// select task
			TapListViewItem("SideMenuList", 2, 3);
			app.WaitForElement("MyTaskPageInProgressButton");
			app.Tap("MyTaskPageInProgressButton");
			Thread.Sleep(5000);
			TapListViewItem("MyTaskPageListView", 0, 0);
			app.WaitForElement("TaskSummaryLowerButton");
			app.Tap("TaskSummaryLowerButton");
			Thread.Sleep(3000);
			app.Tap(x => x.Marked("Yes"));
			Thread.Sleep(3000);
			app.Tap(x => x.Marked("No"));
			Thread.Sleep(3000);
		}
		public void PosterDenyTaskCancellation()
		{
			app.Tap("LeftButton");
			// select task
			TapListViewItem("SideMenuList", 2, 3);
			app.WaitForElement("MyTaskPageInProgressButton");
			app.Tap("MyTaskPageInProgressButton");
			Thread.Sleep(5000);
			TapListViewItem("MyTaskPageListView", 0, 0);
			app.WaitForElement("TaskSummaryLowerButton");
			app.Tap("TaskSummaryLowerButton");
			Thread.Sleep(3000);
			app.Tap(x => x.Marked("No"));
			Thread.Sleep(3000);
			app.Tap(x => x.Marked("Okay"));
			Thread.Sleep(3000);
			app.Tap("LeftButton");
		}


        public void AcceptBid()
        {
			app.Tap("LeftButton");
			// select task
			TapListViewItem("SideMenuList", 2, 3);
			app.WaitForElement("MyTaskPageBidButton");
			app.Tap("MyTaskPageBidButton");
            Thread.Sleep(5000);
			TapListViewItem("MyTaskPageListView", 0, 0);
            app.WaitForElement("TaskSummaryLowerButton");
			app.Tap("TaskSummaryLowerButton");
            app.WaitForElement("BidOffersListView");
            Thread.Sleep(5000);
            TapListViewItem("BidOffersListView", 0, 0);
            app.WaitForElement("BidSummaryLowerRightButton");
			app.Tap("BidSummaryLowerRightButton");
            Thread.Sleep(3000);
			app.Tap(c => c.Marked("Yes"));
            Thread.Sleep(3000);
			app.Tap(c => c.Marked("Okay"));
            Thread.Sleep(3000);
			app.Tap("LeftButton");
		}

		public void IgnoreBid()
		{
			app.Tap("LeftButton");
			// select task
			TapListViewItem("SideMenuList", 2, 3);
			app.WaitForElement("MyTaskPageBidButton");
			app.Tap("MyTaskPageBidButton");
            Thread.Sleep(5000);
			TapListViewItem("MyTaskPageListView", 0, 0);
			app.WaitForElement("TaskSummaryLowerButton");
			app.Tap("TaskSummaryLowerButton");
			app.WaitForElement("BidOffersListView");
            Thread.Sleep(5000);
            TapListViewItemDescendants("BidOffersListView", 0, 0, "IGNORE");
            Thread.Sleep(5000);
            app.Tap("Right2Button");
            Thread.Sleep(3000);
            app.Tap("LeftButton");
		}


        public void CancelBid()
        {
            Thread.Sleep(5000);
            app.Tap("LeftButton");
			// select task
			TapListViewItem("SideMenuList", 2, 3);
            app.WaitForElement("MyTaskPageBidButton");
            app.Tap("MyTaskPageBidButton");
            Thread.Sleep(5000);
            TapListViewItem("MyTaskPageListView", 0, 0);
            app.WaitForElement("ViewTaskLowerButton");
			app.Tap("ViewTaskLowerButton");
			app.Tap(x => x.Marked("Proceed"));
		}


        public void TestIgnoreTaskFromCategory(string category)
        {
			app.WaitForElement("HomePagePerformerButton");
			app.Tap("HomePagePerformerButton");

			app.WaitForElement(category);
			app.Tap(category);

			app.WaitForElement("TaskPageListview");
            TapListViewItemDescendants("TaskPageListview", 0, 0, "IGNORE");
            Thread.Sleep(5000);
            app.Flash("LeftButton");
            app.Tap("LeftButton");
        }


		public void TestBidTaskFromCategory(string category, string taskCost)
		{
			app.Tap("LeftButton");
			// select home
			TapListViewItem("SideMenuList", 0, 5);

			app.WaitForElement("HomePagePerformerButton");
			app.Tap("HomePagePerformerButton");

			app.WaitForElement(category);
			app.Tap(category);

			app.WaitForElement("TaskPageListview");
            TapListViewItemDescendants("TaskPageListview", 0, 0, "ACCEPT");
            Thread.Sleep(3000);
			app.WaitForElement("ViewTaskBidEntry");
			app.EnterText("ViewTaskBidEntry", taskCost);
			app.DismissKeyboard();

			app.Tap("ViewTaskLowerRightButton");

			app.Tap(c => c.Marked("Yes")); // actionsheet

			app.Tap(c => c.Marked("Okay")); //display alert

			app.Tap("LeftButton");
			app.Tap("LeftButton");
		}

		public void DeleteTask()
		{
			app.Tap("LeftButton");
			// select task
			TapListViewItem("SideMenuList", 2, 3);
			app.WaitForElement("MyTaskPageBidButton");
			app.Tap("MyTaskPageBidButton");
            Thread.Sleep(5000);
			TapListViewItem("MyTaskPageListView", 0, 0);
			app.WaitForElement("TaskSummaryActionButton");
			app.Tap("TaskSummaryActionButton");
            Thread.Sleep(5000);
			app.Tap(x => x.Marked("Delete Task"));
            Thread.Sleep(5000);
            app.Tap(x => x.Marked("Yes"));
            Thread.Sleep(5000);
            app.Tap(x => x.Marked("Okay"));
		}


		public void TestCreateNewTask(string category, string taskTitle, string taskDescription, string taskCost)
		{
			app.Tap("LeftButton");
			// select home
			TapListViewItem("SideMenuList", 0, 5);

			app.WaitForElement("HomePagePosterButton");
			app.Tap("HomePagePosterButton");

			app.WaitForElement(category);
			app.Tap(category);

			app.WaitForElement("NewTaskTitleEntry");
			app.ClearText("NewTaskTitleEntry");
			app.DismissKeyboard();
			app.EnterText("NewTaskTitleEntry", taskTitle);
			app.PressEnter();
			app.ClearText("NewTaskDescriptionEntry");
			app.DismissKeyboard();
			app.EnterText("NewTaskDescriptionEntry", taskDescription);
			app.PressEnter();
			app.DismissKeyboard();
			app.EnterText("NewTaskCostEntry", taskCost);
			app.DismissKeyboard();
			app.Tap("NewTaskCashButton");
			app.Tap("NewTaskPostButton");
			Thread.Sleep(5000);
			app.Tap(c => c.Marked("Okay"));
		}


		public void TestMapHomePage()
		{
			app.WaitForElement("HomePageMapButton");
			app.Tap("HomePageMapButton");

			app.WaitForElement("MapHomePageAllButton");
			app.Tap("MapHomePageAllButton");
			Thread.Sleep(10000);

			app.Invoke(platform == Platform.Android ? "myBackdoorMethod" : "myBackdoorMethod:", 0);
			Thread.Sleep(5000);
			app.Tap("LeftButton");
			Thread.Sleep(10000);
			app.Tap("LeftButton");
		}


		public void TestReportTask()
		{
			app.WaitForElement("HomePagePerformerButton");
			app.Tap("HomePagePerformerButton");
			app.Tap("HomePageQuickieButton");
            Thread.Sleep(5000);
            TapListViewItem("TaskPageListview", 0, 0);

			app.Tap("ViewTaskActionButton");
			app.Tap(x => x.Marked("Report Task"));

			app.EnterText("ReportTaskMessage", "Report this Task for testing...");

			app.DismissKeyboard();

			app.Tap("ReportTaskButton");

			app.Tap(x => x.Marked("Okay"));
			app.Tap("LeftButton");
			app.Tap("LeftButton");
		}


		public void TestReportUser()
		{
			app.WaitForElement("HomePagePerformerButton");
			app.Tap("HomePagePerformerButton");
			app.Tap("HomePageQuickieButton");
            Thread.Sleep(5000);
            TapListViewItem("TaskPageListview", 0, 0);
			app.Tap("ViewTaskOtherProfileCircleImage");
			app.Tap("ProfileActionButton");
			app.Tap(x => x.Marked("Report User"));
			app.EnterText("ReportUserMessage", "Report this user for testing...");
			app.DismissKeyboard();
			app.Tap("ReportUserButton");
			app.Tap(x => x.Marked("Okay"));
			app.Tap("LeftButton");
			app.Tap("LeftButton");
			app.Tap("LeftButton");
		}


		public void TestHViewUserProfile()
		{
			app.Tap("LeftButton");
            app.WaitForElement("SideMenuProileCircleImage");
			app.Tap("SideMenuProileCircleImage");
			app.WaitForElement("ProfileUserStatusSwitch");
			app.Tap("ProfileUserStatusSwitch");
			Thread.Sleep(2000);
			app.Tap("ProfileUserStatusSwitch");
			Thread.Sleep(2000);
			app.Tap("ProfileFilterButton");
			app.Tap(x => x.Marked("Quick Task"));
			Thread.Sleep(2000);
			app.Tap("ProfileFilterButton");
			app.Tap(x => x.Marked("Grocery"));
			Thread.Sleep(2000);
			app.Tap("ProfileFilterButton");
			app.Tap(x => x.Marked("Laundry"));
			Thread.Sleep(2000);
			app.Tap("ProfileFilterButton");
			app.Tap(x => x.Marked("Transport"));
			Thread.Sleep(2000);
			app.Tap("ProfileFilterButton");
			app.Tap(x => x.Marked("Other"));
			Thread.Sleep(2000);
			app.Tap("ProfileFilterButton");
			app.Tap(x => x.Marked("All"));
			Thread.Sleep(2000);
			app.Tap("ProfileActionButton");
			app.Tap(x => x.Marked("Cancel"));

			//EditProfileLayoutButton
			//ProfileChatOtherCircleImage
			//ProfileFilterButton

			//TestEditProfile();        //Call TestEditProfile if camera problem fixed      
		}

		//Problem in Android - Tap camera to capture image
		public void TestEditProfile()
		{
			app.Tap(x => x.Marked("EditProfileLayoutButton"));

			//backDoorCameraCapture
			app.Tap(x => x.Marked("ProfilePicCircleImage"));
			app.Tap(x => x.Marked("Take a photo"));
			app.Invoke("backDoorCameraCapture");
			//app.Repl();

			//var picture = "Take a photo";
			var picture = "Choose existing";

			if (IsAndroid == false)
			{
				app.Tap(x => x.Marked("ProfilePicCircleImage"));

				//app.Tap(x => x.Marked("Take a photo"));

				app.Tap(x => x.Marked(picture));


				//iOS camera device choose from existing
				if (picture.Equals("Choose existing"))
				{
					app.Tap(x => x.Marked("Camera Roll")); // pick camera roll
					app.Tap(x => x.Marked("PhotosGridView").Child(0));// picking photos on camera roll
				}
				else
				{


					//capture image using camera
					app.Tap(x => x.Marked("PhotoCapture"));

					// retake
					app.Tap(x => x.Marked("Retake"));

					app.Tap(x => x.Marked("PhotoCapture"));

					// retake
					app.Tap(x => x.Marked("Retake"));

					//cancel capture
					app.Tap(x => x.Marked("Cancel"));

					app.Tap("ProfilePicCircleImage");

					app.Tap(x => x.Marked("Take a photo"));

					//capture image using camera
					app.Tap(x => x.Marked("PhotoCapture"));

					//capture image using camera
					app.Tap(x => x.Marked("Use Photo"));
				}
			}

			app.ClearText("NameEntry");
			app.DismissKeyboard();
			app.EnterText("NameEntry", "Christian Jay");
			app.PressEnter();
			//TextEntry("AgeEntry","27");

			app.ClearText("AgeEntry");
			app.DismissKeyboard();
			app.EnterText("AgeEntry", "27");
			app.PressEnter();

			//TextEntry("SchoolEntry", "Appstone Academy");
			app.ClearText("SchoolEntry");
			app.DismissKeyboard();
			app.EnterText("SchoolEntry", "Appstone Academy");
			app.PressEnter();

			//TextEntry("AboutEntry", "The most handsome creature ever created, and the youngest of all. Can do all things, and can multi-task.");
			app.ClearText("AboutEntry");
			app.DismissKeyboard();
			app.EnterText("AboutEntry", "The most handsome creature ever created, and the youngest of all. Can do all things, and can multi-task.");
			app.DismissKeyboard();

			app.Tap("SaveButton");
		}


		public void TestGTaskPage()
		{
			app.Tap("LeftButton");

			// select task
            TapListViewItem("SideMenuList", 2, 3);

			app.WaitForElement("MyTaskPageRoleSwitchButton");
			Thread.Sleep(3000);
			
			app.Tap("MyTaskPageBidButton");
			Thread.Sleep(3000);
			
			app.Tap("MyTaskPageHistoryButton");
			Thread.Sleep(3000);

			app.Tap("MyTaskPageRoleSwitchButton");
			Thread.Sleep(3000);

			app.Tap("MyTaskPageBidButton");
			Thread.Sleep(3000);
			
			app.Tap("MyTaskPageInProgressButton");
			Thread.Sleep(3000);
		}


		public void TestFMessage()
		{
			app.Tap("LeftButton");

			// select messages
			TapListViewItem("SideMenuList", 3, 2);
			Thread.Sleep(5000);

            if (app.Flash(x => x.Marked("ConversationList").Child(0).Child()).Count() > 0)
            {
                int count = app.Flash(x => x.Marked("ConversationList").Child(0).Child()).Count();

                TapListViewItem("ConversationList", 0, count - 1);

                Thread.Sleep(5000);
                app.ClearText("MessageEditor");
                app.DismissKeyboard();
                app.EnterText("MessageEditor", "The Idle App Testing...");
                app.Tap("MessageSendButton");
                Thread.Sleep(3000);
                app.ClearText("MessageEditor");
                app.DismissKeyboard();
                app.EnterText("MessageEditor", "Sending Test.\n Testing..... ");
                app.Tap("MessageSendButton");
                Thread.Sleep(3000);

                app.Tap("LeftButton");
            }
			

		}


		public void TestEStripe()
		{
			app.Tap("LeftButton");

			// select stripe
            TapListViewItem("SideMenuList", 4, 1);
			Thread.Sleep(3000);
		}


		public void TestDSetting()
		{
			app.Tap("LeftButton");

            TapListViewItem("SideMenuList", 5, 0);

            // select get in touch 
            TapListViewItem("SettingsListview", 0, 4);

			app.WaitForElement("GetInTouchEditor");
			app.ClearText("GetInTouchEditor");
			app.DismissKeyboard();
			app.EnterText("GetInTouchEditor", "Idle Xamarin Test! This is a sample message to get in touch.");
			app.DismissKeyboard();
			app.Tap("GetInTouchSendButton");

			app.Tap(c => c.Marked("Okay"));

			app.Tap("LeftButton");

            TapListViewItem("SettingsListview", 1, 3);

			Thread.Sleep(5000);
			app.Tap("LeftButton");

			// select tutorial - skipping tutorial
            TapListViewItem("SettingsListview", 2, 2);
            TestAATutorialSkip();
			// select tutorial - following tutorial
            TapListViewItem("SettingsListview", 2, 2);
            TestAATutorialNext();

			// select update account
            TapListViewItem("SettingsListview", 3, 1);

			Thread.Sleep(2000);
			app.ClearText("UpdateAccountEmailEntry");
			app.DismissKeyboard();
			app.EnterText("UpdateAccountEmailEntry", email);
			app.DismissKeyboard();

			app.ClearText("UpdateAccountPasswordEntry");
			app.DismissKeyboard();
			app.EnterText("UpdateAccountPasswordEntry", password);
			app.DismissKeyboard();

			app.ClearText("UpdateAccountRepeatPasswordEntry");
			app.DismissKeyboard();
			app.EnterText("UpdateAccountRepeatPasswordEntry", password);
			app.DismissKeyboard();

			app.ClearText("UpdateAccountMobileNumberEntry");
			app.DismissKeyboard();
			app.EnterText("UpdateAccountMobileNumberEntry", "87000");
			app.DismissKeyboard();

            //app.Flash("UpdateAccountSaveButton");
			app.Tap("UpdateAccountSaveButton");

            if(platform == Platform.iOS)
            {
                if (app.Query((arg) => arg.Marked("LeftButton")).Count() > 0)
                {
                    app.Tap("LeftButton");
                }
            }
            // select cancel account
            //app.Repl();
            TapListViewItem("SettingsListview", 4, 0);

			app.WaitForElement("CancelAccountEditor");
			app.ClearText("CancelAccountEditor");
			app.DismissKeyboard();
			app.EnterText("CancelAccountEditor", "Idle Xamarin Test! This is a sample message to cancel account. I will tap submit button but for now i will select cancel since it is only a testing.");
			app.DismissKeyboard();
			app.Tap("CancelAccountSubmitButton");
			app.Tap(c => c.Marked("Cancel"));
			Thread.Sleep(2000);
			app.Tap("LeftButton");
		}


		public void TestCSignIn()
		{
			app.ClearText("EmailEntrySignIn");
			app.DismissKeyboard();
			//check email and password empty
			app.Tap("ButtonSignIn");
			Thread.Sleep(4000);
			//check wrong email and password
			app.ClearText("EmailEntrySignIn");
			app.DismissKeyboard();
			app.EnterText("EmailEntrySignIn", "stephen");
			app.PressEnter();

			app.ClearText("PasswordEntrySignIn");
			app.DismissKeyboard();
			app.EnterText("PasswordEntrySignIn", "hello");
			app.PressEnter();

			app.Tap("ButtonSignIn");

			Thread.Sleep(5000);

			//check right email and wrong password
			app.ClearText("EmailEntrySignIn");
			app.DismissKeyboard();
			app.EnterText("EmailEntrySignIn", email);
			app.PressEnter();

			app.ClearText("PasswordEntrySignIn");
			app.DismissKeyboard();
			app.EnterText("PasswordEntrySignIn", "hello");
			app.PressEnter();

			app.Tap("ButtonSignIn");

			Thread.Sleep(5000);

			//check right email and password
			app.ClearText("EmailEntrySignIn");
			app.DismissKeyboard();
			app.EnterText("EmailEntrySignIn", email);
			app.PressEnter();

			app.ClearText("PasswordEntrySignIn");
			app.DismissKeyboard();
			app.EnterText("PasswordEntrySignIn", password);
			app.PressEnter();

			app.Tap("ButtonSignIn");

			app.WaitForElement("HomePagePerformerButton");
			app.Tap("HomePagePerformerButton");
		}


		public void TestBForgotPassword()
		{
			app.Tap("SignInForgotPassword");
			app.WaitForElement("ForgotPasswordSendButton");
			app.EnterText("ForgotPasswordEmailEntry", "stephen.mante@appstone.tech");
			app.DismissKeyboard();
			app.Tap("ForgotPasswordSendButton");
			Thread.Sleep(5000);
			app.Tap(x => x.Marked("Okay"));
            Thread.Sleep(3000);
			app.Tap("LeftButton");
		}


		public void TestASignUp()
		{
			app.Tap("SignInSignUpButton");
			Thread.Sleep(3000);
			string username = "Some Test";
			//dont change
			string userSchool = username.Split(' ')[0] + "'s School";
			string useremail = (username.Replace(' ', '.') + "@idle").ToLower();
			string password = "hello@123";
			string mobileNumber = "0999999999";

			//empty entries
			app.Tap("SignUpNextButton");
			Thread.Sleep(3000);
			//not empty username only
			app.ClearText("SignUpNameEntry");
			app.DismissKeyboard();
			app.EnterText("SignUpNameEntry", username);
			app.DismissKeyboard();

			app.Tap("SignUpNextButton");
			Thread.Sleep(3000);
			// not empty username and email
			app.ClearText("SignUpNameEntry");
			app.DismissKeyboard();
			app.EnterText("SignUpNameEntry", username);
			app.DismissKeyboard();

			app.ClearText("SignUpEmailEntry");
			app.DismissKeyboard();
			app.EnterText("SignUpEmailEntry", useremail);
			app.DismissKeyboard();

			app.Tap("SignUpNextButton");
			Thread.Sleep(3000);
			// not empty username and email and mobilenumber
			app.ClearText("SignUpMobileNumberEntry");
			app.DismissKeyboard();
			app.EnterText("SignUpMobileNumberEntry", mobileNumber);
			app.DismissKeyboard();

			app.Tap("SignUpNextButton");
			Thread.Sleep(3000);
			// valid email and password but password not match , username  and mobilenumber
			app.ClearText("SignUpPasswordEntry");
			app.DismissKeyboard();
			app.EnterText("SignUpPasswordEntry", password);
			app.DismissKeyboard();

			app.ClearText("SignUpRepeatPasswordEntry");
			app.DismissKeyboard();
			app.EnterText("SignUpRepeatPasswordEntry", "asf123");
			app.DismissKeyboard();

			app.Tap("SignUpNextButton");
			Thread.Sleep(3000);
			// valid password and  password match but empty email is empty, username  and mobilenumber
			app.ClearText("SignUpEmailEntry");
			app.DismissKeyboard();

			app.ClearText("SignUpPasswordEntry");
			app.DismissKeyboard();
			app.EnterText("SignUpPasswordEntry", password);
			app.DismissKeyboard();

			app.ClearText("SignUpRepeatPasswordEntry");
			app.DismissKeyboard();
			app.EnterText("SignUpRepeatPasswordEntry", password);
			app.DismissKeyboard();

			app.Tap("SignUpNextButton");
			Thread.Sleep(3000);
			// valid password and  password match but  email already taken, username  and mobilenumber
			app.ClearText("SignUpEmailEntry");
			app.DismissKeyboard();
			app.EnterText("SignUpEmailEntry", "christian.tautuan@appstone.tech");
			app.DismissKeyboard();

			app.ClearText("SignUpPasswordEntry");
			app.DismissKeyboard();
			app.EnterText("SignUpPasswordEntry", password);
			app.DismissKeyboard();

			app.ClearText("SignUpRepeatPasswordEntry");
			app.DismissKeyboard();
			app.EnterText("SignUpRepeatPasswordEntry", password);
			app.DismissKeyboard();

			app.Tap("SignUpNextButton");
			Thread.Sleep(3000);
			//  valid  email but invalid password. username  and mobilenumber
			app.ClearText("SignUpEmailEntry");
			app.DismissKeyboard();
			app.EnterText("SignUpEmailEntry", username);
			app.DismissKeyboard();

			app.ClearText("SignUpPasswordEntry");
			app.DismissKeyboard();
			app.EnterText("SignUpPasswordEntry", password.Substring(0, 3));
			app.DismissKeyboard();

			app.ClearText("SignUpRepeatPasswordEntry");
			app.DismissKeyboard();
			app.EnterText("SignUpRepeatPasswordEntry", password.Substring(0, 3));
			app.DismissKeyboard();

			app.Tap("SignUpNextButton");
			Thread.Sleep(3000);
			//valid signup
			app.ClearText("SignUpAgeEntry");
			app.DismissKeyboard();
			app.EnterText("SignUpAgeEntry", "18");
			app.DismissKeyboard();

			app.ClearText("SignUpSchoolEntry");
			app.DismissKeyboard();
			app.EnterText("SignUpSchoolEntry", userSchool);
			app.DismissKeyboard();

			app.ClearText("SignUpEmailEntry");
			app.DismissKeyboard();
			app.EnterText("SignUpEmailEntry", useremail);
			app.DismissKeyboard();

			app.ClearText("SignUpPasswordEntry");
			app.DismissKeyboard();
			app.EnterText("SignUpPasswordEntry", password);
			app.DismissKeyboard();

			app.ClearText("SignUpRepeatPasswordEntry");
			app.DismissKeyboard();
			app.EnterText("SignUpRepeatPasswordEntry", password);
			app.DismissKeyboard();

			app.Tap("SignUpNextButton");
			Thread.Sleep(3000);
            app.WaitForElement("SignUpSkipButton");
			//skip stripe
			app.Tap("SignUpSkipButton");
			Thread.Sleep(5000);
            app.WaitForElement("SignUpDoneButton");
			//send empty code
			app.Tap("SignUpDoneButton");
			Thread.Sleep(3000);
			//send not match code
			app.EnterText("SignUpConfirmationCode", mobileNumber);
			app.DismissKeyboard();
			app.Tap("SignUpDoneButton");
			Thread.Sleep(5000);
			//resend code
			app.Tap("SignUpResendButton");
			Thread.Sleep(5000);
			app.Tap(x => x.Marked("Okay"));
			Thread.Sleep(5000);
			app.Tap("SignUpCloseButton");
		}

		public void TestAATutorialNext()
		{
			app.Tap("GeneralTutorialNextButton");
            app.Tap("GeneralTutorialNextButton");
			app.Tap("GeneralTutorialNextButton");
            app.Tap("GeneralTutorialNextButton");
			app.Tap("GeneralTutorialBackButton");
			app.Tap("GeneralTutorialNextButton");
			app.Tap("GeneralTutorialNextButton");
			app.Tap("GeneralTutorialNextButton");
			app.Tap("GeneralTutorialNextButton");
			app.Tap("GeneralTutorialNextButton");
			app.Tap("GeneralTutorialNextButton");
			app.Tap("GeneralTutorialNextButton");
		}

		public void TestAATutorialSkip()
		{
			app.Tap("GeneralTutorialCloseButton");
		}
	}
}
